import helper_old as helper
import utils
import numpy as np
import monotonize_times
from scipy.interpolate import UnivariateSpline, interp1d
from ofaidata_utils.data_handling.matchfile import MatchFile



def get_pedals(matchfile, performance, mcu_ratio, pedal_type = 'sustain', pedal_min = 0):

    pedal = np.array([(x.Time, x.Value > pedal_min) for x in matchfile.sustain_lines])
    if pedal_type == 'soft':
        pedal = np.array([(x.Time, x.Value > pedal_min) for x in matchfile.soft_lines])

    if len(pedal) == 0:
        return np.zeros((len(performance))), np.zeros((len(performance)))

    pedal = pedal[np.argsort(pedal[:,0]), :]

    L = np.max([int(performance[-1][1]/mcu_ratio), pedal[-1][0]])
    p_curve = np.zeros((L))

    pedal_on = False
    pedal_ontime = 0
    pedal_offtime = 0

    for p in pedal:
        if p[1] == 1:                  # Value > 63
            if not pedal_on:
                pedal_on = True
                pedal_ontime = p[0]    # Time
        else:
            if pedal_on:
                pedal_offtime = p[0]
                p_curve[pedal_ontime:pedal_offtime] = 1
                pedal_on = False

    note_pedal = np.zeros((len(performance)))

    for i in range(len(performance)):
        onidx = int(performance[i][0]/mcu_ratio)
        offidx = int(performance[i][1]/mcu_ratio)
        if np.sum(p_curve[onidx:min(offidx,len(p_curve)-1)]) >= 1:  # from onset to offset, if there is one pedal on
            note_pedal[i] = 1

    return note_pedal, p_curve

# commented out save_pyc, let new_part = part
def get_score_and_performance_notes(match_fn, part, expand_grace=True, expandPedal = False):
    """Return two arrays of score and performance notes, respectively.  The arrays
    are of equal length, where `score[i]` and `performance[i]` are associated
    through the score/performance alignment. None-aligned notes in both the
    score and the performance are omitted.

    The array of score notes lists onset, offset, pitch, pitch_diff_prev, pitch_diff_next in the columns.
    The array of performance notes lists onset, offset, and velocity in the columns.
    The array of indices returns the indices of the returned score notes in the ScorePart `part`.

    :param match_fn: path of Match file
    :param part: ScorePart object of the score
    :returns: (score, performance, indices)
    :rtype: tuple of (ndarray, ndarray, ndarray)

    """

    match = MatchFile(match_fn)
    match_lines = np.array(match.note_pairs)
    xml_notes = np.array(part.notes)
    match_xml_idx = helper.map_match_and_xml(match_lines, xml_notes)

    # the match lines for which xml notes are available
    match_lines = match_lines[match_xml_idx[:, 0]]
    xml_notes = xml_notes[match_xml_idx[:, 1]]



    # score = np.array([(note.start.t, note.end.t, note.midi_pitch, note.voice, note.midi_pitch, note.midi_pitch)
    #               for note in xml_notes],
    #              dtype=[('onset', 'f4'), ('offset', 'f4'), ('pitch', 'i4'), ('voice','i4'),('pitch_diff_prev', 'i4'), ('pitch_diff_next', 'i4')])
    score = np.array([(note.start.t, note.end.t, note.midi_pitch)
                  for note in xml_notes],
                 dtype=[('onset', 'f4'), ('offset', 'f4'), ('pitch', 'i4')])


    match_onset_offset = np.array(
        [(sn.OnsetInBeats, sn.OffsetInBeats) for sn, n in match_lines])

    performance = np.array([(note.Onset, note.Offset, note.Velocity)
                            for _, note in match_lines],
                           dtype=[('onset', 'f4'), ('offset', 'f4'), ('velocity', 'i4')])

    mcu = float(match.info('midiClockUnits'))
    mcr = match.info('midiClockRate')

    mcu_ratio = mcr / (mcu * 10 ** 6)
    performance['onset'] *= mcu_ratio
    performance['offset'] *= mcu_ratio

    # use score onset/offset times from match, rather than from xml, because we
    # need score variant time, not score time
    score['onset'] = match_onset_offset[:, 0]
    score['offset'] = match_onset_offset[:, 1]


    # filter out pairs with zero duration score notes (i.e. grace
    # notes that were not, or unsuccessfully, expanded)
    valid_notes = score['onset'] < score['offset']
    # invalid_notes = np.where(~valid_notes)[0]
    score = score[valid_notes]
    performance = performance[valid_notes]
    match_xml_idx = match_xml_idx[valid_notes]

    # extract sustain and soft pedal
    note_sustain, sustain_curve = get_pedals(match, performance, mcu_ratio, 'sustain')
    note_soft, soft_curve = get_pedals(match, performance, mcu_ratio, 'soft')

    return score, performance, match_xml_idx[:, 1], np.column_stack((note_sustain, note_soft))


def encode_articulation(score_durations, performed_durations,
                        unique_onset_idxs, beat_period):

    articulation = np.zeros_like(score_durations)
    articulation_raw = np.zeros_like(score_durations)
    for idx, bp in zip(unique_onset_idxs, beat_period):
        sd = score_durations[idx]
        pd = performed_durations[idx]

        # indices of notes with duration 0 (grace notes)
        grace_mask = sd == 0

        # Grace notes have an articulation ratio of 1
        sd[grace_mask] = 1
        pd[grace_mask] = bp
        # Compute log articulation ratio
        articulation[idx] = np.log2(pd / (bp * sd))
    return articulation #, articulation_raw


def decompose_velocity(velocity, u_onset_idx):
    vel_parts = np.empty((len(velocity), 2), dtype=np.float64)
    for i, jj in enumerate(u_onset_idx):
        jj_vels = velocity[jj]
        vel_parts[jj, 0] = np.median(jj_vels)  #np.max(jj-vels)
        vel_parts[jj, 1] = vel_parts[jj, 0] - jj_vels  #jj_vels - vel_parts[jj, 0]
    return vel_parts/127.


def encode(score, performance, smoothlevel = 1.0, return_index = False):
    velocity = performance['velocity']

    # score_onsets = score['onset']
    # performed_onsets = performance['onset']
    # score_durations = (score['offset'] - score['onset'])
    # performed_durations = (performance['offset'] - performance['onset'])

    score = np.column_stack((score['onset'], score['offset']))
    performance = np.column_stack((performance['onset'], performance['offset']))

    idx, br = helper.segment_score_perf_times(np.column_stack((score[:,0], performance[:,0])))
    score_segs = np.split(score[idx], br)
    perf_segs = np.split(performance[idx], br)
    idx_segs = np.split(idx, br)
    res = []
    segment_lengths = []
    k = 0

    # beat period * 2, timing * 2 and articulation * 2
    parameters = np.empty((len(score), 6))

    for ii, s, p in zip(idx_segs, score_segs, perf_segs):
        segment_results = list(_encode(s, p, smoothlevel))
        parameters[ii] = segment_results[0]
        res.append(segment_results)
        res[-1][2] = [np.sort(idx[x + k]) for x in res[-1][2]]
        l = len(res[-1][0])
        segment_lengths.append(l)
        k += l

    # average beap period
    mbps = np.vstack([(l, x[1]) for l, x in zip(segment_lengths, res)])
    mbps[:, 0] /= np.sum(mbps[:, 0])
    mbp = np.sum(np.prod(mbps, axis=1))

    # onset index
    u_onset_idx = sum((x[2] for x in res), [])

    # velocity trend and velocity deviation
    vel_parts = decompose_velocity(velocity, u_onset_idx)

    # normalize beat period
    # parameters[:,0] = np.log2(parameters[:,0]/mbp)
    if return_index:
        return np.column_stack((vel_parts, parameters)), mbp, ['Velocity trend', 'Velocity dev','bprgrid', 'bprdev','Articulationgrid','bpr-original','time_dev-original'], u_onset_idx
    return np.column_stack((vel_parts, parameters)), mbp, ['Velocity trend', 'Velocity dev','bprgrid', 'bprdev','Articulationgrid','bpr-original','time_dev-original']


"""
The encoding of score-aligned performance
"""
def _encode(score, performance, smoothlevel):
    # get unique onset index
    unique_onset_idxs = helper.get_unique_onset_idxs((1e4 * score[:,0]).astype(np.int))

    # Compute beat period
    beat_period_smooth, s_onsets = helper.tempo_by_smooth(score, performance, unique_onset_idxs, smoothlevel)

    # Compute equivalent onsets
    #perf_onsets_smooth = (np.cumsum(beat_period_smooth* np.diff(s_onsets)) +
    #             performance[:,0][unique_onset_idxs[0]].mean())
    perf_onsets_smooth = (np.cumsum(np.r_[0, beat_period_smooth[:-1] * np.diff(s_onsets)]) +
                 performance[:,0][unique_onset_idxs[0]].mean())

    # Calculate beat deviation of each onset (average time) from the grid --- For smooth
    timedev = np.zeros((len(score)))
    for i in range(1,len(unique_onset_idxs)):
        onsets = unique_onset_idxs[i]
        prev_onsets = unique_onset_idxs[i-1]
        ioi_perf = performance[:,0][onsets] - performance[:,0][prev_onsets].mean()  # ioi_perf
        ioi_grid = perf_onsets_smooth[i] - perf_onsets_smooth[i-1]                          # ioi_grid
        timedev[onsets] = ioi_grid - ioi_perf

    # Compute articulation parameter
    articulation_smooth = encode_articulation(score_durations=score[:,1] - score[:,0],
                                            performed_durations=performance[:,1] - performance[:,0],
                                            unique_onset_idxs=unique_onset_idxs,
                                            beat_period=perf_onsets_smooth)

    # Beat method II
    beat_period_average, s_onsets_a = helper.tempo_by_average(score, performance, unique_onset_idxs,method = "byaverage")

    perf_onsets_average = (np.cumsum(np.r_[0, beat_period_average[:-1] * np.diff(s_onsets_a)]) +
                 performance[:,0][unique_onset_idxs[0]].mean())

    articulation_average = encode_articulation(score_durations=score[:,1] - score[:,0],
                                            performed_durations=performance[:,1] - performance[:,0],
                                            unique_onset_idxs=unique_onset_idxs,
                                            beat_period=perf_onsets_average)

    # Initialize array of parameters
    parameters = np.zeros((len(score), 6))
    parameters[:, 2] = articulation_smooth
    parameters[:, 5] = articulation_average

    for i, jj in enumerate(unique_onset_idxs):
        parameters[jj, 0] = beat_period_smooth[i]
        parameters[jj, 1] = timedev[jj]
        parameters[jj, 3] = beat_period_average[i]
        parameters[jj, 4] = perf_onsets_average[i] - performance[:,0][jj]

    return parameters, beat_period_average.mean(), unique_onset_idxs

