BINS = {}
#BINS['vel_onset'] = [42, 49, 54, 58, 62, 65, 68, 70, 73, 76, 79, 83, 88, 94]
BINS['vel_onsetmax'] = [ 23.  ,  26.95,  30.9 ,  34.85,  38.8 ,  42.75,  46.7 ,  50.65,
        54.6 ,  58.55,  62.5 ,  66.45,  70.4 ,  74.35,  78.3 ,  82.25,
        86.2 ,  90.15,  94.1 ,  98.05, 102.  ]
# BINS['vel_onsetincrease'] = [-55.  , -50.76, -46.52, -42.28, -38.04, -33.8 , -29.56, -25.32,
#                             -21.08, -16.84, -12.6 ,  -8.36,  -4.12,   0.12,   4.36,   8.6 ,
#                              12.84,  17.08,  21.32,  25.56,  29.8 ,  34.04,  38.28,  42.52,
#                              46.76,  51.  ]
BINS['vel_maxdiff'] = [ 0.,    4.58,   9.1, 13.75, 18.3,  22.9, 27.5,
                        32.01, 36.67, 41.25,45.83, 50.42, 55. ]
BINS['bpm_raw'] = [40,45,50,55,60,63,66,69,72,75,78,81,84,87,90,93,96,99,102,
105,106,109,112,115,118,121,124,127,130,133,136,139,142,145,148,151,154,
157,160,165,170,175,180]
#BINS['vel_diff_onset'] = [-27, -22, -18, -15, -12,  -9,  -6,  -3, -0.05]
#BINS['bpm_onset_norm'] = [0.53, 0.65,  0.73, 0.80,  0.86, 0.91, 0.95,  0.997, 1.04, 1.09, 1.15, 1.22, 1.32,  1.54]
#BINS['bpm_onset_ratio'] = [0.9, 0.95, 0.98, 0.999, 1.01, 1.05, 1.1]
BINS['measure_bpm_norm'] = [0.64 ,0.75 ,0.8  ,0.87 ,0.92 ,0.95,0.99 ,1.01  ,1.04 ,1.07 ,1.11,1.16,1.22 ,1.34]

BINS['beat_period_onbeat'] = [0.14833577, 0.23627212, 0.32420847, 0.41214482, 0.50008117,
        0.58801752, 0.67595387, 0.76389022, 0.85182657, 0.93976292,
        1.02769927]

BINS['beat_period_ratio'] = [0.        , 0.16055122, 0.32110244, 0.48165366, 0.64220488,
        0.8027561 , 0.96330732, 1.12385854, 1.28440977, 1.44496099,
        1.60551221, 1.76606343, 1.92661465, 2.08716587, 2.24771709,
        2.40826831]

BINS['beat_period_norm'] = [0.54708051, 0.73870937, 0.80123286, 0.85943676, 0.90359698,
       0.9655981 , 1.08464119, 1.24456295, 1.57779385]

BINS['log_articulation'] = [-2.86, -2.38, -2.02, -1.72, -1.44, -1.18, -0.94, -0.71, -0.48, -0.26, -0.048,  0.17, 0.44, 0.89]



#BINS['measure_tempo'] = [1.03, 1.25, 1.44 , 1.65, 1.95, 2.23, 2.67, 3.14, 4.2]#[1, 1.35, 1.6, 1.9, 2.2, 2.5, 2.85, 3.4, 4.6]  # 8-bin
#BINS['measure_tempo_ratio'] = [0.88, 1.05, 1.16]

 # [0.07, 0.15, 0.25, 0.35, 0.45, 0.56, 0.7, 0.9, 1.1, 1.5, 4.5]

xml_names = ['xml/beethoven_op002_no2_mv1.xml',
            'xml/beethoven_op002_no2_mv2.xml',
            'xml/beethoven_op002_no2_mv3.xml',
            'xml/beethoven_op002_no2_mv4.xml',
            'xml/beethoven_op013_mv1.xml',
            'xml/beethoven_op013_mv2.xml',
            'xml/beethoven_op013_mv3.xml',
            'xml/beethoven_op014_no1_mv1.xml',
            'xml/beethoven_op014_no1_mv2.xml',
            'xml/beethoven_op014_no1_mv3.xml',
            'xml/beethoven_op026_mv1.xml',
            'xml/beethoven_op026_mv2.xml',
            'xml/beethoven_op026_mv3.xml',
            'xml/beethoven_op026_mv4.xml',
            'xml/beethoven_op027_no2_mv1.xml',
            'xml/beethoven_op027_no2_mv2.xml',
            'xml/beethoven_op027_no2_mv3.xml',
            'xml/beethoven_op031_no2_mv1.xml',
            'xml/beethoven_op031_no2_mv2.xml',
            'xml/beethoven_op031_no2_mv3.xml',
            'xml/beethoven_op053_mv1.xml',
            'xml/beethoven_op053_mv2.xml',
            'xml/beethoven_op053_mv3.xml']

match_names = ['match/beethoven_op002_no2_mv1.match',
                'match/beethoven_op002_no2_mv2.match',
                'match/beethoven_op002_no2_mv3.match',
                'match/beethoven_op002_no2_mv4.match',
                'match/beethoven_op013_mv1.match',
                'match/beethoven_op013_mv2.match',
                'match/beethoven_op013_mv3.match',
                'match/beethoven_op014_no1_mv1.match',
                'match/beethoven_op014_no1_mv2.match',
                 'match/beethoven_op014_no1_mv3.match',
                 'match/beethoven_op026_mv1.match',
                 'match/beethoven_op026_mv2.match',
                 'match/beethoven_op026_mv3.match',
                 'match/beethoven_op026_mv4.match',
                 'match/beethoven_op027_no2_mv1.match',
                 'match/beethoven_op027_no2_mv2.match',
                 'match/beethoven_op027_no2_mv3.match',
                 'match/beethoven_op031_no2_mv1.match',
                 'match/beethoven_op031_no2_mv2.match',
                 'match/beethoven_op031_no2_mv3.match',
                 'match/beethoven_op053_mv1.match',
                 'match/beethoven_op053_mv2.match',
                 'match/beethoven_op053_mv3.match']
