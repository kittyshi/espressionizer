import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

import os
import random
random.seed(33)

#pkl_dir = '../data0128/20200128/'
pkl_dir = 'all_data/pkl/0128-mini/'
all_files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]
train_file = []
valid_file = []
for fn in random.sample(all_files, int(len(all_files) * 0.8)):
    if fn.endswith('.p'):
        train_file.append(fn)
for fn in all_files:
    if fn not in train_file:
        valid_file.append(fn)

# print(all_files)

import pickle
SAVE_AS_LIST = True # save as a list instead of stacking together
def getdata(files, batch_size = 16, SAVE_AS_LIST=True):
    data = {}

    if SAVE_AS_LIST:
        _X = []
        _Y = []
        _Xm = []
        _Ym = []

    for i, f in enumerate(files):
        pk = pickle.load(open(f,'rb'))
        # d['X'], d['Y_vel'],d['Y_bpm'] ,d['Xm'], d['Ym'], d['Ym_bpm']
        X  = pk['X'] * 127.
        Y  = pk['Y']  #.reshape((-1,1))
        Xm = pk['Xm'] * 127.        #.reshape((-1,1))
        Ym = pk['Ym']         #.reshape((-1,1))

#         X      =      X[:X.shape[0] // batch_size * batch_size]
#         Y      =      Y[:Y.shape[0] // batch_size * batch_size]
#         Xm     =     Xm[:Xm.shape[0] // batch_size * batch_size]
#         Ym     =     Ym[:Ym.shape[0] // batch_size * batch_size]

        if SAVE_AS_LIST:
            _X.append(X)
            _Y.append(Y)
            _Xm.append(Xm)
            _Ym.append(Ym)
        else:
            if i == 0:
                _X = X
                _Y = Y
                _Xm = Xm
                _Ym = Ym
            else:
                _X = np.vstack((_X, X))
                _Y = np.vstack((_Y, Y))
                _Xm = np.vstack((_Xm, Xm))
                _Ym = np.vstack((_Ym, Ym))

        # print('     X\t', X.shape)
        # print('     Y\t', Y.shape)
        # print('    Xm\t', Xm.shape)
        # print('    Ym\t', Ym.shape)
        # print('X:', X.shape, 'Xm:', Xm.shape)

    if SAVE_AS_LIST:
        print('X: ', len(_X), 'Xm:', len(_Xm))
    else:
        print('X:', _X.shape, 'Xm:', _Xm.shape)
    data['X'] = _X
    data['Y'] = _Y
    data['Xm'] = _Xm
    data['Ym'] = _Ym
    #print('X: ', min([min(x.flatten()) for x in data['X']]), max([max(x.flatten()) for x in data['X']]))
    #print('Xm: ', min([min(x.flatten()) for x in data['Xm']]), max([max(x.flatten()) for x in data['Xm']]))
    return data

data = {}
data['train'] = getdata(train_file, batch_size=16, SAVE_AS_LIST=True)
data['val'] = getdata(valid_file, batch_size=16, SAVE_AS_LIST=True)

import numpy as np

stride = 2
seq_len = 64

def create_examples(the_data,seq_len,stride):
    in_features = []
    out_labels = []

    L = len(the_data['X'])
    for piece_no in range(L):
        for i in range(0,len(the_data['X'][piece_no])-seq_len, stride):
            in_feature = the_data['X'][piece_no][i:i+seq_len]
            out_label = the_data['Y'][piece_no][i:i+seq_len]
            in_features.append(in_feature)
            out_labels.append(out_label)
        for i in range(0,len(the_data['Xm'][piece_no])-seq_len, stride):
            in_feature_m = the_data['Xm'][piece_no][i:i+seq_len]
            out_label_m = the_data['Ym'][piece_no][i:i+seq_len]
            in_features.append(in_feature_m)
            out_labels.append(out_label_m)
        #data['train']['X'][i], data['train']['Y'][i], data['train']['Xm'][i], data['train']['Ym'][i]

    inputs = np.zeros((len(in_features), seq_len, 386), dtype=np.int64)
#     labels = np.zeros((len(in_features), seq_len, 128), dtype=np.int64) #128
    labels = np.full((len(in_features), seq_len) , -1, dtype=np.int64)
    for i, (in_p, out_v) in enumerate(zip(in_features, out_labels)):
        inputs[i] = in_p
        labels[i] = np.argmax(out_v, axis=1)
#         print(in_p.shape, out_v.shape, labels[i].shape)
        if max(labels[i]) > 128:
            print('haha')
            break
        #         labels[i] = out_v   # 128

    # but this line is slow?
    return tuple(torch.tensor(t) for t in (inputs, labels))

# this cell should take ~60 seconds
import datetime
start_time = datetime.datetime.now()

train_dataset = create_examples(data['train'], 64,2)
valid_dataset = create_examples(data['val'], 64,2)

elapsed_time = datetime.datetime.now() - start_time
print("Took %4.1f seconds" % elapsed_time.seconds)

TRAIN_BATCH_SIZE = 16
TRAIN_SEQUENCE_LENGTH = 64
TRAIN_NUM_EPOCHS = 20
TRAIN_SUMMARY_EVERY = 10
TRAIN_EVAL_EVERY = 20
EVAL_BATCH_SIZE = 16
EVAL_SEQUENCE_LENGTH = 64
EVAL_STRIDE = 16
MODEL_FLOAT_FEATURES = True
MODEL_REGRESSION = False

from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler, TensorDataset)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# device = torch.device('cpu')

train_data = TensorDataset(*train_dataset)
train_sampler = RandomSampler(train_data)
train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=TRAIN_BATCH_SIZE, drop_last=True)
eval_data = TensorDataset(*valid_dataset)
eval_sampler = SequentialSampler(eval_data)
eval_dataloader = DataLoader(eval_data, sampler=eval_sampler, batch_size=EVAL_BATCH_SIZE, drop_last=True)


def compute_loss(logits, labels):
    #batch_size, sequence_length = labels.shape
    batch_size = 16
    sequence_length = 64
    # This should always start around ln(128) ~= 4.852
    # chris's: torch.Size([32, 64, 128]) torch.Size([32, 64])
    # torch.Size([16, 64, 128]) torch.Size([16, 64])

    loss = F.cross_entropy(
        logits.view(batch_size * sequence_length, -1),
        labels.view(batch_size * sequence_length),
        ignore_index=-1
    )
    return loss

from argparse import Namespace

flags = Namespace(
    seq_size=64,
    batch_size=16,
    lstm_size=64,
    output_size=128,

    gradients_norm=5,
    checkpoint_path='checkpoint',
)

class RNNModule(nn.Module):
    def __init__(self, input_size, seq_size, lstm_size, output_size):
        super(RNNModule, self).__init__()
        self.seq_size = seq_size
        self.lstm_size = lstm_size
        self.lstm = nn.LSTM(input_size,
                            lstm_size,
                            batch_first=True)
        self.dense = nn.Linear(lstm_size, output_size)
        #self.out   = nn.Softmax()

    def forward(self, x, prev_state):
        output, state = self.lstm(x, prev_state)
        logits = self.dense(output)

        return logits, state

    def init_hidden(self, batch_size):
        return (torch.zeros(1, batch_size, self.lstm_size),
                torch.zeros(1, batch_size, self.lstm_size))

input_vector_dim = 386
model = RNNModule(input_vector_dim, flags.seq_size, flags.lstm_size, flags.output_size)
model = model.to(device) #.float()

optimizer = torch.optim.Adam(model.parameters())
print(model)

# compute_loss = nn.CrossEntropyLoss()
step = 0
for epoch in range(TRAIN_NUM_EPOCHS):
    for batch in train_dataloader:
        if step % TRAIN_EVAL_EVERY == 0:
            model.eval()

            eval_num_batches = 0
            eval_loss = 0.
            for i, eval_batch in enumerate(eval_dataloader):
                with torch.no_grad():
                    inputs, labels = tuple(t.to(device) for t in eval_batch)
                    inputs = inputs.type(torch.float32)
                    hidden = model.init_hidden(batch_size = 16)
                    hidden = tuple([h.to(device) for h in hidden])
                    logits, _ = model(inputs, hidden) # RNN

                    #print(logits.shape, labels.shape)
                    #print(max(labels.flatten()))
                    loss = compute_loss(logits, labels)
                    eval_num_batches += 1
                    eval_loss += loss.item()
            print('(Step {}) eval loss: {}'.format(step, eval_loss / eval_num_batches))
            model.train()

        inputs, labels = tuple(t.to(device) for t in batch)
        inputs = inputs.type(torch.float32)
        optimizer.zero_grad()
        hidden = model.init_hidden(batch_size = 16)
        hidden = tuple([h.to(device) for h in hidden])

        # print(sum(torch.isnan(inputs.flatten())))

        # forward pass
        logits, _  = model(inputs, hidden)
        # compute loss
        loss = compute_loss(logits, labels)
        # bardward pass
        loss.backward()

        optimizer.step()

        if step % TRAIN_SUMMARY_EVERY == 0:
            print('(Step {}) train loss: {}'.format(step, loss.item()))

        step += 1

torch.save(model.state_dict,'test.pth')