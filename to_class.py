import numpy as np
from config import BINS

# convert 8bins to 3 bits, 4bins to 2 bits, etc.
def bin_to_bit(bins):
    num_bin = len(bins) + 1
    return int(np.log2(num_bin) + 0.5)

def convert_dur_new(val):
    if val == 0:
        return 0
    if 0 < val <= 0.25:
        return 1
    if 0.25 < val <= 0.5:
        return 2
    if 0.5 < val <= 1.0:
        return 3
    if 1.0 < val <= 1.25:
        return 4
    if 1.25 < val <= 1.5:
        return 5
    if 1.5 < val <= 2.0:
        return 6
    if 2.0 < val <= 3.0:
        return 7
    return 8

def convert_dur_old(val):
    if val == 0:
        return 0
    if 0 < val < 0.5:
        return 1
    if 0.5 <= val < 1.0:
        return 2
    if 1.0 <= val < 1.5:
        return 3
    if 1.5 <= val < 2.0:
        return 4
    # return 5
    if 2.0 <= val < 4.0:
        return 5
    return 6

def convert_dur_twohot(val):
    if val == 0:
        return [0,0,0,0,0,0]
    if val == 0.25 or (val > 0.25 and val < 0.5):
        return [0,1,0,0,0,0]
    if val == 0.5 or (val > 0.5 and val < 0.75):
        return [0,0,1,0,0,0]
    if val == 0.75 or (val > 0.75 and val < 1.0):
        return [0,1,1,0,0,0]
    if val == 1.0 or (val > 1.0 and val < 1.25):
        return [0,0,0,1,0,0]
    if val == 1.25 or (val > 1.25 and val < 1.5):
        return [0,1,0,1,0,0]
    if val == 1.5 or (val > 1.5 and val < 1.75):
        return [0,0,1,1,0,0]
    if val == 1.75 or (val > 1.75 and val < 2.0):
        return [0,1,1,1,0,0]
    if val == 2.0:
        return [0,0,0,0,1,0]
    return [0,0,0,0,0,1]


# pitch difference to the highest pitch, group in octave
def convert_pitch_diff_onehot(val):
    # group according to octave
    onehot = np.zeros((8))
    octave_diff = int(np.floor(val/12.))
    onehot[octave_diff] = 1
    return onehot


# pitch difference into categories (-13 for a lot, [-12, 12], and 13 for a lot)
def convert_pitch_diff(val):
    onehot = np.zeros((27))
    if val < -12:
        onehot[-13] = 1
        return  -13, onehot
    elif val > 12:
        onehot[13] = 1
        return 13, onehot
    else:
        onehot[int(val)] = 1
        return int(val), onehot

# 0 if it's one note, 1 if min pitch, 2 if it's max pitch, 3 otherwise
def convert_minmax_pitch(curr_p, pitches):
    if curr_p == pitches.min():
        return 1
    elif curr_p == pitches.max():
        return 2
    else:
        return 3

# number of simultaneous notes into categories (0 for single, 1 for duet, 2 for chords)
def convert_num_sim_notes(val):
    onehot = np.zeros((3))
    if val >= 2:
        onehot[2] = 1
        return 2, onehot
    else:
        onehot[int(val)] = 1
        return val, onehot

# IOI into categories
# [0,      1                   2          3         4  ]
# [<=0.25, 0.25 to 0.5, 0.5-1.0, 1.0, 1.0 to 2.0, > 2.0]
def convert_ioi(val):
    onehot = np.zeros((4))
    if val <= 0.25:
        return 0, onehot
    if 0.25 < val <= 0.5:
        onehot[1] = 1
        return 1, onehot
    if 0.5 < val <= 1.0:
        onehot[2] = 1
        return 2, onehot
    onehot[3] = 1
    return 3, onehot

# encoding as huffman encoding without compression, use for beat phase, slur value, etc.
def convert_huffman_onehot(val, bins = [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875]):
    pivot_idx = 0
    res = ""

    while(pivot_idx < len(bins)):
        pivot = bins[pivot_idx]
        if val < pivot:
            res += "0"
            pivot_idx += pivot_idx + 1
        else:
            res += "1"
            pivot_idx += pivot_idx + 2
    res_onehot = np.zeros((len(res)))
    for i in range(len(res)):
        res_onehot[i] = ord(res[i]) - ord("0")

    return res_onehot

# convert duration difference: 0 if grace note, 1 if same, 1 if <= half, 2 if half to same, 3 if longer
def convert_dur_diff(val):
    onehot = np.zeros((4))
    if val == 0:
        return 0, onehot
    if val == 1:
        onehot[1] = 1
        return 1, onehot
    if val < 1:
        onehot[2] = 1
        return 2, onehot
    onehot[3] = 1
    return 3, onehot

# find where the current value at (not used by reorganize)
def find_phase_num(index_num, mylist):
    #phase_num = np.where(mylist == val)[0]
    #if len(phase_num) > 0:
    return phase_num[0] * 1. / len(mylist)
    #return 0

# convert slur values into 0-1
# def convert_slur_val(note_start, slur_notes_infos, group, slur_bins):
#     slur_start = slur_notes_infos[group][1]
#     slur_end = slur_notes_infos[group][2]
#     slur_max_pos =  slur_end - slur_start
#     slur_pos = (note_start - slur_start) * 1. / slur_max_pos
#     return slur_pos, convert_huffman_onehot(slur_pos, slur_bins)


def fill_slur_val(i, curr_slur, onset, bins):
    # position, the n-th index in the array (0-1)
    pos = i * 1./len(curr_slur["note_idx"])  # find_phase_num(i, curr_slur["note_idx"])
    pos_huffman_onehot = convert_huffman_onehot(pos, bins)
    # phase, the n-th beat in the array (0-1)
    phase = (onset - curr_slur["start_beat"]) / (curr_slur["end_beat"] - curr_slur["start_beat"])
    phase_huffman_onehot = convert_huffman_onehot(phase, bins)
    return pos, pos_huffman_onehot, phase, phase_huffman_onehot



############ Output Encoding ############

def convert_onehot(cats, num_digits):
    if isinstance(cats, (list,np.ndarray)):
        one_hot = np.zeros((len(cats), num_digits))
        for i in range(len(cats)):
            one_hot[i][int(cats[i])] = 1
        return one_hot

    one_hot = np.zeros((num_digits))
    one_hot[int(cats)] = 1
    return one_hot

def convert_category(val, bins):
    digits = np.digitize(val, bins, right = True)
    return digits


def sample_distributes(preds):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    probs = np.exp(preds) / np.sum(np.exp(preds))
    exp_preds = np.exp(np.log(preds))
    preds = exp_preds / np.sum(exp_preds)        # softmax
    probas = np.random.multinomial(1, preds, 1)  # draw one example
    return np.argmax(probas)


def inverse_from_onehot(onehot, bins, leftbound, rightbound, sample = False):
    # take arg max
    c = int(np.argmax(onehot))
    # or sample from distribution
    if sample == True:
        c = sample_distributes(onehot)
    if c == 0:
        return 0, leftbound
    if c == len(bins):
        return len(bins), rightbound
    onehot_bi = np.zeros((len(onehot)))
    onehot_bi[c] = 1
    return onehot_bi, (bins[c-1] + bins[c])/ 2.


def inverse_from_onehots(onehots, bins, leftbound, rightbound, sample = False):
    if len(onehots.shape) > 1 and len(onehots) > 1:
        # convert to categorical
        cats = np.zeros((len(onehots) ))
        cats_onehot = np.zeros((len(onehots), len(bins) + 1))
        for i in range(len(onehots)):
            cats_onehot[i], cats[i] = inverse_from_onehot(onehots[i], bins,
                                        leftbound, rightbound, sample)
        return cats
    return inverse_from_onehot(onehots, bins, leftbound, rightbound, sample)


def decode_onehots(onehots, name, sample = False):
    if name == "vtrend":
        return inverse_from_onehots(onehots, BINS['vel_onsetmax'], 20, 103, sample)
    # if name == "vtrend_increase":
    #     return inverse_from_onehots(onehots, BINS['vel_onsetincrease'], -30, 30, sample)
    if name == "vdev":
        return inverse_from_onehots(onehots, BINS['vel_maxdiff'], 0, 55, sample)
    # 2019-12-01 update, using beat
    # if name == "ibi":
    #     return inverse_from_onehots(onehots, BINS['bpm_onset_norm'],0.49, 2, sample)
    # if name == "time":
    #     return inverse_from_onehots(onehots, BINS['bpm_onset_ratio'],0.85, 1.15, sample)
    if name == "ibi":
        return inverse_from_onehots(onehots, BINS['beat_period_onbeat'],0, 1.1, sample)
    if name == "time":
        return inverse_from_onehots(onehots, BINS['beat_period_ratio'],0, 2.5, sample)
    if name == "art":
        return inverse_from_onehots(onehots, BINS['log_articulation'], -3, 1, sample)
    if name == "pedal" or name == "softpedal" or name == "sustainpedal":
        return onehots

def inverse_articulation(c):
    if c == 0:
        return 0.08
    if c == 1:
        return 0.15
    if c == 2:
        return 0.25
    if c == 3:
        return 0.4
    if c == 4:
        return 0.625
    if c == 5:
        return 0.875
    if c == 6:
        return 1.15
    if c == 7:
        return 1.45
    if c == 8:
        return 1.8
    return 2.5

def convert_articulation(val):
    one_hot = np.zeros((10))
    # convert to np.log2, and then one-hot
    if val <= 0.1:
        one_hot[0] = 1
        return one_hot
    if val <= 0.2:
        one_hot[1] = 1
        return one_hot
    if val <= 0.3:
        one_hot[2] = 1
        return one_hot
    if val <= 0.5:
        one_hot[3] = 1
        return one_hot
    if val <= 0.75:
        one_hot[4] = 1
        return one_hot
    if val <= 1.0:
        one_hot[5] = 1
        return one_hot
    if val <= 1.3:
        one_hot[6] = 1
        return one_hot
    if val <= 1.6:
        one_hot[7] = 1
        return one_hot
    if val <= 2.0:
        one_hot[8] = 1
        return one_hot
    one_hot[9] = 1
    return one_hot



