"""
This extracts feature given a score
"""

import os
import numpy as np
import pickle
import helper
import utils
import collections
from scipy.interpolate import interp1d
from ofaidata_utils.data_handling.scoreontology import (
                                                        Note, TimeSignature, Measure, Slur, Repeat,
                                                        KeySignature,
                                                        TempoDirection,
                                                        ConstantTempoDirection,
                                                        DynamicTempoDirection,
                                                        ResetTempoDirection,
                                                        LoudnessDirection,
                                                        DynamicLoudnessDirection,
                                                        ConstantLoudnessDirection,
                                                        ImpulsiveLoudnessDirection)

from music_utils.key_id.key_identification import (
    key_identification,
    key_to_scaledegree,
    fifths_to_key,
    SCALE_DEGREES,
    KEYS)

eps = 10**-6
#eps = np.finfo(np.float64).eps
list_dynamic_marking, cat_dynamic_marking = pickle.load(open('all_data/cat_dynamic_marking.p', "rb"))
list_tempo_marking, cat_tempo_marking = pickle.load(open('all_data/cat_tempo_marking.p', "rb"))
#list_dynamic_marking, cat_dynamic_marking = pickle.load(open('data/cat_dynamic_marking_bc.p', "rb"))
#list_tempo_marking, cat_tempo_marking = pickle.load(open('data/cat_tempo_marking_bc.p', "rb"))


def unique_onset_idx(ons):
    sort_idx = np.argsort(ons)
    split_idx = np.where(np.diff(ons[sort_idx]) > epsilon)[0] + 1
    return np.split(sort_idx, split_idx)


def extract_pitchstep(xml_notes):

    pitch_diff_prev = np.zeros((len(xml_notes)))
    pitch_diff_next = np.zeros((len(xml_notes)))

    for i in range(len(xml_notes)):
        prev_notes = xml_notes[i].previous_notes_in_voice
        prev_pitches = [n.midi_pitch for n in prev_notes]
        curr_pitches = []
        v = xml_notes[i].voice
        if len(prev_notes) == 0:
            pitch_diff_prev[i] = xml_notes[i].midi_pitch
            continue
        prev_i = None
        if len(prev_notes) == 1:
            prev_i = 0   # the 0th (and the only note) to compare
        else:
            sim_notes = xml_notes[i].simultaneous_notes_in_voice
            if len(sim_notes) == 0:
                # no simultaneous notes, count the interval between current and the lowest/highest pitch on the previous
                # depending on the register of the outer voice
                if xml_notes[i].staff == 1:
                    # get the maximum pitch
                    prev_i = np.argmax(prev_pitches)
                else:
                    prev_i = np.argmin(prev_pitches)
            else:
                curr_pitches = [n.midi_pitch for n in sim_notes]
                # count the rank
                # get the index
                r = 0
                if xml_notes[i].midi_pitch >= curr_pitches[0]:
                    r = 0
                elif xml_notes[i].midi_pitch <= curr_pitches[-1]:
                    r = len(curr_pitches)
                else:
                    for r in range(len(curr_pitches)-1):
                        if xml_notes[i].midi_pitch < curr_pitches[r] and xml_notes[i].midi_pitch > curr_pitches[r+1]:
                            break
                    r += 1

                # if same number of notes, then one-on-one, figure out the ranking
                if len(prev_pitches) == len(curr_pitches) + 1:
                    prev_i = r
                # if previous notes are more than current
                elif len(prev_pitches) > len(curr_pitches) + 1:
                    #  FECBA (prev)
                    #   \ ||
                    #    321 (curr)
                    if r == len(curr_pitches):
                        prev_i = len(prev_pitches) - 1
                    else:
                        prev_i = r

                # if previous notes are less than current
                else:
                    #    321 (prev)     1 : A
                    #  //|||            2 : B
                    #  FECBA (curr)     3 : C, E, F
                    prev_i = min(r, len(prev_pitches) - 1)

        pitch_diff_prev[i] = xml_notes[i].midi_pitch - prev_notes[prev_i].midi_pitch

    pitch_diff_next[:-1] = pitch_diff_prev[1:]
    return np.column_stack((pitch_diff_prev, pitch_diff_next)), ['pitch_diff_prev','pitch_diff_next']


def extract_pitch(part):
    pitches = np.array([n.midi_pitch for n in part.notes]) / 127.
    W_pitch = np.zeros((len(part.notes),4))
    W_pitch[:,0] = pitches
    W_pitch[:,1] = pitches**2
    W_pitch[:,2] = pitches**3
    W_pitch[:,3] = np.array([n.alter if n.alter is not None else 0 for n in part.notes])
    return W_pitch, ['pitch','pitch^2','pitch^3','alter']


def extract_onsetwise_chroma(part):
    p_o = np.array([(n.midi_pitch, n.start.t) for n in part.notes])
    unique_onsets = np.unique(p_o[:, 1])
    unique_onset_idxs = [np.where(p_o[:, 1] == u)[0] for u in unique_onsets]
    pitches = [p_o[ix, 0] for ix in unique_onset_idxs]
    W_chroma = np.zeros((len(p_o), 12))
    for u, p in zip(unique_onset_idxs, pitches):
        for c in p:
            W_chroma[u,int(c)%12] = 1

    return W_chroma, ['o_chroma12']


def extract_onsetwise_extremepitch(part):
    p_o = np.array([(n.midi_pitch, n.start.t) for n in part.notes])
    unique_onsets = np.unique(p_o[:, 1])
    unique_onset_idxs = [np.where(p_o[:, 1] == u)[0] for u in unique_onsets]
    pitches = [p_o[ix, 0] for ix in unique_onset_idxs]
    W = np.zeros((len(p_o), 2))
    for u, p in zip(unique_onset_idxs, pitches):
        W[u, 0] = p.max() / 127.
        W[u, 1] = p.min() / 127.
    return W, ['o_pitch_max', 'o_pitch_min']


# 2019-08-27, found this function is never called?
def extract_dur(part):
    nd = np.array([(n.start.t, n.end.t) for n in part.notes])
    bm = part.beat_map
    note_times_beat = bm(nd[:, 1]) - bm(nd[:, 0])
    # normalize
    #W.shape = (-1, 1)
    #W_dur = utils.soft_normalize(note_times_beat, preserve_unity = True)
    W_dur = note_times_beat
    return W_dur, ['duration']

# Extract downbeat, beatphase and piecephase
def extract_metrical(part):

    nt = np.array([n.start.t for n in part.notes])
    bm = part.beat_map         # sth that can return beat function
    note_times_beat = bm(nt)   # which beat in the whole piece

    # where change the time signature
    ts = part.timeline.get_all_of_type(TimeSignature)
    ts = np.array([(t.start.t, t.beats, t.beat_type) for t in ts])
    tsi = np.searchsorted(ts[:, 0], nt, side='right') - 1

    ms = part.timeline.get_all_of_type(Measure)
    ms = bm(np.array([m.start.t for m in ms]))    # Which beat starts a measure = bm(measure_start_time) -- downbeat
    msi = np.searchsorted(ms, note_times_beat, side='right') - 1
    # assert np.all(tsi >= 0)
    # assert np.all(msi >= 0)

    N = len(part.notes)

    # Downbeat and beat phase
    W_downbeat = np.zeros((N,1))
    W_beatphase = np.zeros((N,1))

    for i in range(N):
        if note_times_beat[i] < eps:
            m_position = note_times_beat[i]
        else:
            m_position = note_times_beat[i] - ms[msi[i]]
        ts_num = ts[tsi[i], 1]

        beatphase = np.mod(m_position, ts_num) / float(ts_num)
        if beatphase == 0:
            W_downbeat[i] = 1

        W_beatphase[i] = beatphase

    W_piecephase = (note_times_beat - note_times_beat[0])/(note_times_beat[-1] - note_times_beat[0])

    W_metrical = np.column_stack((W_downbeat,W_beatphase))
    W_metrical = np.column_stack((W_metrical,W_piecephase))
    W_metrical = np.column_stack((W_metrical,note_times_beat))

    return W_metrical, ['Downbeat','Beatphase','Piecephase','Beat']


def extract_ioi_beat(part):
    # 0821 fix from 0 to 1
    bm = part.beat_map

    t_dict = {}
    for note in part.notes:
        pred1 = note.start.get_prev_of_type(Note)
        if len(pred1) > 0:
            d1 = bm(note.start.t) - bm(pred1[0].start.t)
            pred2 = pred1[0].start.get_prev_of_type(Note)
            if len(pred2) > 0:
                d2 = bm(pred1[0].start.t) - bm(pred2[0].start.t)
                pred3 = pred2[0].start.get_prev_of_type(Note)
                if len(pred3) > 0:
                    d3 = bm(pred2[0].start.t) - bm(pred3[0].start.t)
                else:
                    d3 = 0
            else:
                d2 = 0
                d3 = 0
        else:
            d1 = 0
            d2 = 0
            d3 = 0

        succ1 = note.start.get_next_of_type(Note)
        if len(succ1) > 0:
            d4 = bm(succ1[0].start.t) - bm(note.start.t)
            succ2 = succ1[0].start.get_next_of_type(Note)
            if len(succ2) > 0:
                d5 = bm(succ2[0].start.t) - bm(succ1[0].start.t)
                succ3 = succ2[0].start.get_next_of_type(Note)
                if len(succ3) > 0:
                    d6 = bm(succ3[0].start.t) - bm(succ2[0].start.t)
                else:
                    d6 = 0
            else:
                d5 = 0
                d6 = 0
        else:
            d4 = 0
            d5 = 0
            d6 = 0

        t_dict[note.start] = (d1, d2, d3, d4, d5, d6)
    W_ioi = np.array([t_dict[n.start] for n in part.notes])
    return W_ioi, ['ioi_prev1', 'ioi_prev2', 'ioi_prev3',
                   'ioi_next1', 'ioi_next2', 'ioi_next3']


def extract_rest(part):
    names = ['preceds_rest']
    W = [1 if len(n.end.get_starting_objects_of_type(Note)) == 0 else 0 for n in part.notes]

    return np.asarray(W), names


#### Note Markings ####
def extract_notemark(part):
    W_notemark = np.zeros((len(part.notes),4))
    for i in range(len(part.notes)):
        n = part.notes[i]
        W_notemark[i] = n.staccato, n.fermata, n.accent, n.staff
    # normalize staff
    W_notemark[:,3] = W_notemark[:,3]/max(W_notemark[:,3])
    return W_notemark, ['staccato','fermata','accent','staff']


######### Extracting dynamics  #############
#### About Dynamics ####
def fill_out_loudness_direction_times(directions, max_time):
    return fill_out_direction_times(directions, max_time,
                                    ConstantLoudnessDirection,
                                    DynamicLoudnessDirection,
                                    ImpulsiveLoudnessDirection)

# from featurebases line 491
def fill_out_direction_times(directions, max_time, constant_type, dynamic_type, impulsive_type=None):
    # for all dynamic: if unset end_time, end_time=timeOfNextConstant or lastTime
    # for all impulsive: end_time = begin_time+epsilon
    # for all constant: end_time = max(begin_time+epsilon,timeOfNextConstant)
    # for last constant: end_time = lastTime
    for i, d in enumerate(directions):
        if isinstance(d, dynamic_type):
            # print(d.text, d.start.t, d.end.t)
            d.intermediate.append(d.end)
            d.end = None
    for i, d in enumerate(directions):

        if impulsive_type and isinstance(d, impulsive_type):
            directions[i].end = directions[i].start
        else:
            if d.end == None:
                n = d.start.get_next_of_type(constant_type)
                if n:
                    directions[i].end = max(d.start, n[0].start)
                else:
                    directions[i].end = max_time

    return directions


def ensure_constant_at_start_end(dd, start, end, default_direction=u'mf'):
    """
    Make sure that there is a ConstantLoudnessDirection at the start and end
    time. An "mf" Direction is insterted as a default when a
    ConstantLoudnessDirection is missing.

    Parameters
    ----------

    Returns
    -------
    """

    constant = [d for d in dd
                if isinstance(d, ConstantLoudnessDirection)]

    if not constant:
        cld = ConstantLoudnessDirection(default_direction)
        start.add_starting_object(cld)
        dd.insert(0, cld)
        end.add_ending_object(cld)
    else:
        if constant[0].start.t > start:
            cld = ConstantLoudnessDirection(default_direction)
            start.add_starting_object(cld)
            dd.insert(0, cld)
            constant[0].start.add_ending_object(cld)
        if constant[-1].end.t < end:
            cld = ConstantLoudnessDirection(default_direction)
            constant[-1].end.add_starting_object(cld)
            dd.append(cld)
            end.add_ending_object(cld)

    return dd

def make_local_loudness_direction_feature(onsets, direction) :
    """
    from featurebases.py line 259, simplified version
    """
    epsilon = 1e-6

    if isinstance(direction, DynamicLoudnessDirection):
        d = np.array([(direction.start.t, 0),
                      (direction.end.t, 1),
                      (direction.end.t + epsilon, 0.0)])
    elif isinstance(direction, ConstantLoudnessDirection):
        d = np.array([(direction.start.t - epsilon, 0),
                      (direction.start.t, 1),
                      (direction.end.t - epsilon, 1),
                      (direction.end.t, 0)])
    else:
        d = np.array([(direction.start.t - epsilon, 0),
                      (direction.start.t, 1),
                      (direction.start.t + epsilon, 0)])
    return utils.interpolate_feature(onsets, d)


def make_local_tempo_direction_feature(onsets, direction):
    epsilon = 1e-6

    last_time = onsets[-1]

    if isinstance(direction, DynamicTempoDirection):
        if direction.end is None:
            end = last_time + 1
            nextd = direction.start.get_next_of_type(ConstantTempoDirection)
            if len(nextd) > 0:
                end = nextd[0].start.t
        else:
            end = direction.end.t

        d = np.array([(direction.start.t, 0),
                      (end - epsilon, 1),
                      (end, 0.0)])

    elif isinstance(direction, ConstantTempoDirection):
        end = last_time + 1
        nextd = direction.start.get_next_of_type(ConstantTempoDirection)
        if len(nextd) > 0 and nextd[0].text not in (u'a_tempo', u'in_tempo'):
            end = nextd[0].start.t

        d = np.array([(direction.start.t - epsilon, 0),
                      (direction.start.t, 1),
                      (end - epsilon, 1),
                      (end, 0)])
        return utils.interpolate_feature(onsets, d)
    else:
        d = np.array([(direction.start.t - epsilon, 0),
                      (direction.start.t, 1),
                      (direction.start.t + epsilon, 0)])
    return utils.interpolate_feature(onsets, d)


def extract_dynamic_markings(part):
    """
    Refer to LoudnessAnnotationBasis
    """
    onsets = np.array([n.start.t for n in part.notes])

    epsilon = 1e-4
    directions = part.get_loudness_directions()
    directions = fill_out_direction_times(       # 2019-08-26 found typo? directionss?
                   directions, part.timeline.points[-1], ConstantLoudnessDirection,
                                                         DynamicLoudnessDirection,
                                                         ImpulsiveLoudnessDirection)
    directions = ensure_constant_at_start_end(
                       directions,
                       part.timeline.points[0],
                       part.timeline.points[-1])

    W_loudness = np.zeros((len(onsets), len(cat_dynamic_marking)))   # cat_dynamic is the category of dynamic markings
    for i, d in enumerate(directions):
        if d.text not in cat_dynamic_marking:
            print(d.text + ' is not in cat_dynamic_marking')
        else:
            #print d.text + ' is in category: %d ' % cat_dynamic_marking[d.text]
            W_loudness[:, cat_dynamic_marking[d.text]] += make_local_loudness_direction_feature(onsets, d)

    return W_loudness, list_dynamic_marking

def extract_tempo_markings(part):
    """
    Refer to LoudnessAnnotationBasis
    """
    epsilon = 1e-4
    onsets = np.array([n.start.t for n in part.notes])

    directions = part.get_tempo_directions()
    first_tempo = next((d for d in directions if
                            isinstance(d, ConstantTempoDirection)), None)

    for d in directions:
        if d.text == 'tempo_1':
            if first_tempo and first_tempo.text != 'tempo_1':
                d.text = first_tempo.text
    # what about a tempo

    W_tempo = np.zeros((len(onsets), len(cat_tempo_marking)))   # cat_dynamic is the category of dynamic markings
    for i, d in enumerate(directions):
        if d.text not in cat_tempo_marking:
            print(d.text + ' is not in cat_tempo_marking')
        else:
            W_tempo[:, cat_tempo_marking[d.text]] += make_local_tempo_direction_feature(onsets, d)

    return W_tempo, list_tempo_marking


def extract_slur(part):
    """
    Only use slur increase
    """
    slurs = part.timeline.get_all_of_type(Slur)
    W_slur = np.zeros((len(part.notes), 1), dtype=np.float32)
    ss = np.array([(s.voice, s.start.t, s.end.t)
                               for s in slurs
                               if (s.start is not None and
                                   s.end is not None)])
    onsets = np.array([n.start.t for n in part.notes])
    eps = 10**-4

    for v, start, end in ss:
        tmap = np.array([[min(np.min(onsets), start - eps),  0],
                         [start - eps,              0],
                         [start,                    eps],
                         [end,                      1],
                         [end + eps,                0],
                         [max(np.max(onsets), end + eps),     0]])
        incr = interp1d(tmap[:, 0], tmap[:, 1])
        W_slur[:, 0] += incr(onsets)
    return W_slur, ['Slur_incr']


def extract_feature(part, feature_name):
    if feature_name == "Pitch":
        w1,n1 = extract_pitch(part)
        w2,n2 = extract_onsetwise_extremepitch(part)
        return np.column_stack((w1,w2)), n1+n2
    elif feature_name == "Harmonic":
        return extract_onsetwise_chroma(part)
    elif feature_name == "Metrical":
        return extract_metrical(part)
    elif feature_name == "IOI":
        return extract_ioi_beat(part)       # 6 IOI in beat
    elif feature_name == "Notemark":        # Feature 5: note marking (staccato, fermata, accent, staff)
        return extract_notemark(part)
    elif feature_name == "Rest":
        return extract_rest(part)
    elif feature_name == "Dynamics":
        return extract_dynamic_markings(part)
    elif feature_name == "Tempo":
        return extract_tempo_markings(part)
    elif feature_name == "Slur":
        return extract_slur(part)
    else:
        return None

#import pdb
def get_score_feature(part):
    """
    Given a score part, output features in matrix
    """
    cate_names = ['Pitch','Harmonic','Metrical','IOI','Rest','Notemark','Dynamics', 'Tempo', 'Slur']
    feature_names = []
    #pdb.set_trace()
    W = np.zeros((len(part.notes),0))
    for i in range(len(cate_names)):
        w,name = extract_feature(part, cate_names[i])
        W = np.column_stack((W, w))
        feature_names.append(name)

    return W, feature_names
