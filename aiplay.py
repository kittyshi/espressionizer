import prepare_data
#import reconstruct_performance
import numpy as np
import pickle
import score_extractor
from keras.models import load_model
from keras import backend as K
import os
import to_class
import datetime
from performance_generator import Performance

ALLOW_CACHE = 0         # read from pre-computed xmlscore and X_newpiece


def get_bpm_from_hierarchy(a, bpm0, bpm_model, num_len = 64):
    bpm_ms = np.zeros((a.num_measures,1))
    bpm_ns = np.zeros((a.N, 1))
    #bpm0 = 2.0
    input_datas = np.zeros((1, a.num_measures, 60))
    #input_datas[:,:,:57] = a.score_features
    input_datas[:,0,57] = -1   # previous bpm for the 0th is -1
    input_datas[:,:,58] = bpm0  # upper bpm for all is 2.0
    for m in range(a.num_measures):
        # construct feature
        feature_m = np.zeros((57))
        if m in a.dict_measure2xmlidx:
            xmlidxs = a.dict_measure2xmlidx[m]
            feature_m = np.mean(a.score_features[xmlidxs], axis = 0)
            input_datas[:, m, :57] = feature_m
    if input_datas.shape[1] < num_len:
        measure_datas = np.zeros((1,num_len,60))
        measure_datas[:,:input_datas.shape[1],:] = input_datas
    else:
        measure_datas = input_datas

    # predict measures
    bpm_model0 = keras.models.clone_model(bpm_model)
    for i in range(num_len-1):
        tmp_model = keras.models.clone_model(bpm_model0)

        y0      = tmp_model.predict(measure_datas[:, :num_len, :])[0][i][0]
        y0_cont = bpm_model.predict(measure_datas[:, :num_len, :])[0][i][0]

        #import pdb; pdb.set_trace()

        measure_datas[:,i+1,57] = y0
    #bpm_model = tmp_model.copy()

    if len(bpm_ms) > num_len:
        bpm_ms[:num_len] = bpm_model.predict(measure_datas[:,:num_len,:])
    else:
        bpm_ms = bpm_model.predict(measure_datas[:,:num_len,:])[:len(bpm_ms)]
        bpm_ms = bpm_ms.reshape((bpm_ms.shape[1],1))

    if a.num_measures >= num_len:
        for i in range(num_len,a.num_measures):
            measure_datas[:,i,57] = bpm_ms[i-1] # previous bpm
            bpm_ms[i] = bpm_model.predict(measure_datas[:,i-(num_len-1):i+1,:])[0][num_len-1][0]
    else:
        bpm_ms = bpm_ms[:a.num_measures]
    bpm_ms = recenter(bpm_ms, 1.5,0.3)

    note_bpms = np.zeros((1,len(a.score_features),60))
    note_bpms[:,:,:57] = a.score_features
    note_bpms[:,0,57] = -1
    note_bpms[:,:,59] = 1

    if note_bpms.shape[1] >= num_len:
        for i in range(num_len-1):
            measure_no = a.dict_xmlidx2measure[i]
            bpm_upper = bpm_ms[measure_no]
            note_bpms[:,i,58] = bpm_upper
            bpm_prev = bpm_model.predict(note_bpms[:,:num_len])[0][i][0]
            note_bpms[:,i+1,57] = bpm_prev
        bpm_ns[:num_len] = bpm_model.predict(note_bpms[:,:num_len,:])
        for i in range(num_len, len(bpm_ns)):
            measure_no = a.dict_xmlidx2measure[i]
            bpm_upper = bpm_ms[measure_no]
            note_bpms[:,i,58] = bpm_upper   # upper bpm
            note_bpms[:,i,57] = bpm_ns[i-1] # previous bpm
            bpm_ns[i] = bpm_model.predict(note_bpms[:,i-(num_len-1):i+1,:])[0][num_len-1][0]
    else:
        bpm_ns_datas = np.zeros((num_len, 1))
        bpm_ns_datas[:a.N] = bpm_ns
        note_bpms_datas = np.zeros((1,num_len,60))
        note_bpms_datas[:,:note_bpms.shape[1],:] = note_bpms
        for i in range(num_len-1):
            if i in a.dict_xmlidx2measure:
                measure_no = a.dict_xmlidx2measure[i]
                bpm_upper = bpm_ms[measure_no]
                note_bpms[:,i,58] = bpm_upper
                bpm_prev = bpm_model.predict(note_bpms_datas[:,:num_len])[0][i][0]
                note_bpms_datas[:,i+1,57] = bpm_prev
        bpm_ns_datas = bpm_model.predict(note_bpms_datas[:,:num_len,:])
        bpm_ns = bpm_ns_datas[0,:,0][:a.N]
    return bpm_ms, bpm_ns


def play(xml_fn, modeldict, BPM_AVE, out_name, add_pedal = False, option = 0, sample = False):
    """
    Input: xml_fn    - music xml file name
           modeldict - a dictionary of model_dir returned by get_model_dict
           BPM_AVE   - average bpm, i.e. 120
           out_name  - name of the output file
           sample    - Whether want to sample from distribution, or using argmax

    Output:
           score     - interpreted and encoded score custom_objects
           outY      - predicted raw one-hot value
           outYval   - predicted output value in real value
    """
    score = read_xml(xml_fn)
    piece_name = xml_fn.split('/')[-1].split('.')[0]
    input_data = prepare_data.slice_test_data(score.score_features, 64)
    Y_pred = predict(input_data, modeldict, len(score.score_features))
    p = Performance(piece_name, score, Y_pred, BPM_AVE = BPM_AVE,
                    out_name = out_name, option = option, add_pedal = add_pedal, sample = sample)

    #outYval = {}
    #for name in outY:
    #    outYval[name] = to_class.decode_onehots(outY[name], name)
    # [vel_measure, vel_dev, bpm_measure,
    # bpm_ratio, articulation, sustain, soft] = outY
    #[vel_measure, vel_dev, bpm_measure, bpm_ratio, articulation] = outY
    return score, Y_pred
    #return score, outY, outYval

def predict(input_data, modeldict, piece_length):

    #[model_vtrend, model_vdev, model_bpr, model_time, model_art, model_sustain, model_soft] = models
    # divide according to sequence length

    model_names = ['vtrend', 'vtrend_increase', 'vdev', 'ibi',
                'time','art','sustainpedal','softpedal']
    Y_pred = {}
    for model_name in model_names:
        if model_name in modeldict:
            yhat_i = modeldict[model_name].predict(input_data)
            Y_pred[model_name] = yhat_i.reshape(-1, yhat_i.shape[-1])[:piece_length,:]
    return Y_pred

def read_xml(xml_fn):
    cache_xnp = xml_fn + '.xnp.cache.p'
    cache_score = xml_fn + '.score.cache.p'

    score = None
    if ALLOW_CACHE and os.path.isfile(cache_xnp) and os.path.isfile(cache_score):
        score = pickle.load(open(cache_score,'rb'))
        print('> Load from cache: ', cache_xnp)
        print('> Load from cache: ', cache_score)
    else:
        #from ofaidata_utils.data_handling.musicxml import parse_music_xml
        part = parse_music_xml(xml_fn).score_parts[0]
        score = score_extractor.ScoreFeature(part)
        # save cache
        #pickle.dump(score, open(cache_score, 'wb'))
        #print '> Save to cache: ', cache_score

    return score


def get_model_dict(model_dir, DEBUG_MODE = 0):
    """
        DEBUG_MODE = 1   # 1 only loads vtrend for faster speed
        Note: only load pedal models for CHOPIN !!!!!!
    """

    def coeff_determination(y_true, y_pred):
        SS_res =  K.sum(K.square( y_true-y_pred ))
        SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
        return ( 1 - SS_res/(SS_tot + K.epsilon()) )

    models = dict()
    for f in os.listdir(model_dir):
        if f.endswith('.h5') or f.endswith('.hdf5'):
            print(f)
            curr_model = load_model(model_dir + f, custom_objects={'coeff_determination': coeff_determination})
            if f.endswith('vtrend.h5'):
                models['vtrend'] = curr_model
            elif f.endswith('ib_org.h5'):
                models['vtrend_increase'] = curr_model
            elif f.endswith('vdev.h5'):
                models['vdev'] = curr_model
            elif f.endswith('ibi.h5'):
                models['ibi'] = curr_model
            elif f.endswith('time.h5'):
                models['time'] = curr_model
            elif f.endswith('art.h5'):
                models['art'] = curr_model
            elif f.endswith('sus_pedal.h5'):
                models['sustainpedal'] = curr_model
            elif f.endswith('soft_pedal.h5'):
                models['softpedal'] = curr_model
    #    print '> Model loaded in %d seconds (%s) ' % ((datetime.datetime.now() - start).seconds, model['name'])
    # if DEBUG_MODE == 1:
    #     models['art']['model'] = models['vtrend']['model']
    #     models['time']['model'] = models['vtrend']['model']
    #     models['ibi']['model'] = models['vtrend']['model']
    #     models['vdev']['model'] = models['vtrend']['model']
    #     if use_pedal:
    #         models['softpedal']['model'] = models['vtrend']['model']
    #         models['sustainpedal']['model'] = models['vtrend']['model']
    return models

