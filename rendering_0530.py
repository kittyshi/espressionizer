#!/usr/bin/env python
# coding: utf-8

# In[1]:

# time python rendering_0502-c1.py --GPU_ID=0 --MODE=0 --MODEL_CODENAME=adam

import os
import sys
import gflags

FLAGS = gflags.FLAGS

gflags.DEFINE_integer(
    "GPU_ID",
    0,
    "GPU ID")
gflags.DEFINE_integer(
    "MODE",
    88,
    "0: run all, 1: vtrend, 2: vdev/ibi, 3: time/art")
gflags.DEFINE_integer(
    "NUM_EPOCH",
    200,
    "number of epoch")
gflags.DEFINE_integer(
    "BATCH_SIZE",
    32,
    "batch size")

gflags.DEFINE_string(
    "DATA_SET",
    "beethoven",
    "chopin or beethoven")
gflags.DEFINE_string(
    "MODEL_CODENAME",
    "",
    "aa")

gflags.DEFINE_integer(
    "SEQ_LEN",
    50,
    "sequence length")
gflags.DEFINE_integer(
    "UNITS",
    16,
    "unit length")
gflags.DEFINE_integer(
    "NUM_BILSTM",
    2,
    "number of bilstm layers")
gflags.DEFINE_string(
    "LR",
    "0.001",
    "learning rate")
gflags.DEFINE_string(
    "DROPOUT",
    "0.5",
    "dropout rate")
gflags.DEFINE_bool(
    "USE_BN",
    False,
    "use batch normlization or not")

argv = FLAGS(sys.argv)

os.environ["CUDA_VISIBLE_DEVICES"] = str(FLAGS.GPU_ID)
PARALLEL_MODEL = FLAGS.MODE # 0: run all
                            # 1: vtrend
                            # 2: vdev, ibi
                            # 3: time, art
DATA_SET = FLAGS.DATA_SET     # chopin or beethoven
MODEL_CODENAME = FLAGS.MODEL_CODENAME

num_epoch = FLAGS.NUM_EPOCH
batch_size = FLAGS.BATCH_SIZE
use_bn = FLAGS.USE_BN
units = FLAGS.UNITS
dropout = float(FLAGS.DROPOUT)
lr = float(FLAGS.LR)
seq_len = FLAGS.SEQ_LEN
num_bilstm = FLAGS.NUM_BILSTM

print "GPU ID: ", FLAGS.GPU_ID
print "PARALLEL MODE: ", PARALLEL_MODEL

print 'Seq length: ', seq_len
print 'Units: ', units
print 'Dropout: ', dropout
print 'LR: %.4f' % lr

import datetime
import pytz
# get PST date
datestr = datetime.datetime.now(tz=pytz.utc).astimezone(pytz.timezone('US/Pacific')).strftime("%Y%m%d")
MODEL_NAME = '%s_%s_%s_%dsqlen_%depch_%dbs_%dunit_%.1fdr_%.3flr%s%s' % (
    datestr,
    MODEL_CODENAME,
    DATA_SET,
    seq_len,
    num_epoch,
    batch_size,
    units,
    dropout,
    lr,
    '_%dlstm' % num_bilstm if num_bilstm > 2 else '',
    '_bn' if use_bn else '')

print '*'* 80
print '****', MODEL_NAME
print '*'* 80




# In[3]:


import prepare_data
import numpy as np
from sklearn.preprocessing import normalize
from sklearn.preprocessing import MinMaxScaler

# Load and stack all X and Y

# Load and stack all X and Y
if DATA_SET == "beethoven":
    DATA_SET_FILE = '../data0529/0529beethoven01.npy'
else:
    DATA_SET_FILE = '../data0529/0529chopin01.npy'

print 'Dataset file: ', DATA_SET_FILE
raw_data = np.load(DATA_SET_FILE)

X_raw = np.vstack([raw_data[i]['X'] for i in range(len(raw_data))])

for i in range(len(raw_data)):
    raw_data[i]['Y'][:,2] = np.log2(raw_data[i]['Y'][:,2]/raw_data[i]['BPR_ave'])
    raw_data[i]['Y'][:,5] = np.log2(raw_data[i]['Y'][:,5]/raw_data[i]['BPR_ave'])
    raw_data[i]['Y'][:,4] -=  np.mean(raw_data[i]['Y'][:,4])  # articulation
    raw_data[i]['Y'][:,4] /= np.std(raw_data[i]['Y'][:,4])
    raw_data[i]['Y'][:,7] -=  np.mean(raw_data[i]['Y'][:,7])  # articulation
    raw_data[i]['Y'][:,7] /= np.std(raw_data[i]['Y'][:,7])


Y_raw = np.vstack([raw_data[i]['Y'] for i in range(len(raw_data))])
Y_raw[:,1] *= -1

# move these 3 models to the end
temp1 = Y_raw[:,8].copy()
temp2 = Y_raw[:,9].copy()
temp3 = Y_raw[:,5].copy()
temp4 = Y_raw[:,6].copy()
temp5 = Y_raw[:,7].copy()

Y_raw[:,5] = temp1
Y_raw[:,6] = temp2
Y_raw[:,7] = temp3
Y_raw[:,8] = temp4
Y_raw[:,9] = temp5
del raw_data, temp1, temp2, temp3, temp4, temp5

# In[4]:

Y_raw.shape


# In[5]:


# slice data into sequences
X_input, Y_input = prepare_data.slice_data(X_raw, Y_raw, seq_len = seq_len, slidingwindow = True)

from sklearn.model_selection import train_test_split
x_train, x_valid, y_train, y_valid = train_test_split(X_input, Y_input, test_size=0.2)


# In[6]:


from keras.models import Sequential, Input, Model
from keras.layers import Dense, Activation, LeakyReLU, Dropout
from keras.layers import LSTM, Bidirectional, TimeDistributed, BatchNormalization
from keras.optimizers import RMSprop, Adam

output_size = 1
# units = 16
# dropout = 0.5

# vtrend
model0 = Sequential()
for i in range(num_bilstm):
    model0.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model0.add(BatchNormalization())
    model0.add(Dropout(dropout))
model0.add(TimeDistributed(Dense(output_size)))
model0.add(Activation('sigmoid'))

# vdev
model1 = Sequential()
for i in range(num_bilstm):
    model1.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model1.add(BatchNormalization())
    model1.add(Dropout(dropout))
model1.add(TimeDistributed(Dense(output_size)))
model1.add(Activation('sigmoid'))

# ibi
model2 = Sequential()
for i in range(num_bilstm):
    model2.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model2.add(BatchNormalization())
    model2.add(Dropout(dropout))
model2.add(TimeDistributed(Dense(output_size)))
model2.add(Activation('linear'))

# time
model3 = Sequential()
for i in range(num_bilstm):
    model3.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model3.add(BatchNormalization())
    model3.add(Dropout(dropout))
model3.add(TimeDistributed(Dense(output_size)))
model3.add(Activation('linear'))

# art
model4 = Sequential()
for i in range(num_bilstm):
    model4.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model4.add(BatchNormalization())
    model4.add(Dropout(dropout))
model4.add(TimeDistributed(Dense(output_size)))
model4.add(Activation('linear'))

# model5 model6
model5 = Sequential()
for i in range(num_bilstm):
    model5.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model5.add(BatchNormalization())
    model5.add(Dropout(dropout))
model5.add(TimeDistributed(Dense(output_size)))
model5.add(Activation('sigmoid'))

model6 = Sequential()
for i in range(num_bilstm):
    model6.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model6.add(BatchNormalization())
    model6.add(Dropout(dropout))
model6.add(TimeDistributed(Dense(output_size)))
model6.add(Activation('sigmoid'))

# model7: b-original (iborg)        <-> model2
model7 = Sequential()
for i in range(num_bilstm):
    model7.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model7.add(BatchNormalization())
    model7.add(Dropout(dropout))
model7.add(TimeDistributed(Dense(output_size)))
model7.add(Activation('linear'))

# model8: dev-original (tiorg)      <-> model3
model8 = Sequential()
for i in range(num_bilstm):
    model8.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model8.add(BatchNormalization())
    model8.add(Dropout(dropout))
model8.add(TimeDistributed(Dense(output_size)))
model8.add(Activation('linear'))

# model9: art-real (areal)          <-> model4
model9 = Sequential()
for i in range(num_bilstm):
    model9.add(Bidirectional(LSTM(units, return_sequences=True), input_shape=(X_input[0].shape)))
    if use_bn:
        model9.add(BatchNormalization())
    model9.add(Dropout(dropout))
model9.add(TimeDistributed(Dense(output_size)))
model9.add(Activation('linear'))

from keras import backend as K
def coeff_determination(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )



# In[8]:



# In[9]:


# In[10]:


model0.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])

model1.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])

# ibi, time, art
model2.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])
model3.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])
model4.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])

# In[13]:

model5.compile(loss='binary_crossentropy', optimizer=Adam(lr = lr), metrics=[coeff_determination])

model6.compile(loss='binary_crossentropy', optimizer=Adam(lr = lr), metrics=[coeff_determination])

# In[14]:
model7.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])
model8.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])
model9.compile(loss='mse', optimizer=Adam(lr = lr), metrics=[coeff_determination])


from numpy import newaxis

def bookkeep(model, filename, hc):
    with open(filename + '.txt', 'w') as fh:
        # TODO: write the configs?
        model.summary(print_fn=lambda x: fh.write(x + '\n'))
        for i in range(len(hc['loss'])):
            fh.write('loss: %.4f - coeff_determination: %.4f - val_loss: %.4f - val_coeff_determination: %.4f\n' % (hc['loss'][i], hc['coeff_determination'][i], hc['val_loss'][i], hc['val_coeff_determination'][i]))
    model.save(filename + '.h5')

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 0:
    model0.summary()

    hc = model0.fit(x_train, y_train[:,:,0,newaxis],
              validation_data = (x_valid, y_valid[:,:,0,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model0, MODEL_NAME + '_onset_vtrend', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 1:
    model1.summary()

    hc = model1.fit(x_train, y_train[:,:,1,newaxis],
              validation_data = (x_valid, y_valid[:,:,1,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model1, MODEL_NAME + '_note_vdev', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 2:
    model2.summary()

    hc = model2.fit(x_train, y_train[:,:,2,newaxis],
              validation_data = (x_valid, y_valid[:,:,2,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model2, MODEL_NAME + '_onset_ibi', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 3:
    model3.summary()

    hc = model3.fit(x_train, y_train[:,:,3,newaxis],
              validation_data = (x_valid, y_valid[:,:,3,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model3, MODEL_NAME + '_note_time', hc.history)


if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 4:
    model4.summary()

    hc = model4.fit(x_train, y_train[:,:,4,newaxis],
              validation_data = (x_valid, y_valid[:,:,4,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model4, MODEL_NAME + '_note_art', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 5:
    model5.summary()
    hc = model5.fit(x_train, y_train[:,:,5,newaxis],
              validation_data = (x_valid, y_valid[:,:,5,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model5, MODEL_NAME + '_sus_pedal', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 6:
    model6.summary()
    hc = model6.fit(x_train, y_train[:,:,6,newaxis],
              validation_data = (x_valid, y_valid[:,:,6,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model6, MODEL_NAME + '_soft_pedal', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 7:
    model7.summary()
    hc = model7.fit(x_train, y_train[:,:,7,newaxis],
              validation_data = (x_valid, y_valid[:,:,7,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model7, MODEL_NAME + '_ib_org', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 8:
    model8.summary()
    hc = model8.fit(x_train, y_train[:,:,8,newaxis],
              validation_data = (x_valid, y_valid[:,:,8,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model8, MODEL_NAME + '_ti_org', hc.history)

if PARALLEL_MODEL == 88 or PARALLEL_MODEL == 9:
    model9.summary()
    hc = model9.fit(x_train, y_train[:,:,9,newaxis],
              validation_data = (x_valid, y_valid[:,:,9,newaxis]),
              batch_size = batch_size,
              epochs = num_epoch,
              verbose = 1)
    bookkeep(model9, MODEL_NAME + '_areal', hc.history)

# In[ ]:


print MODEL_CODENAME, MODEL_NAME
