import numpy as np
import os
import pickle
import random
#import feature_extract

import partitura
import numpy as np
from score_extractor_new import ScoreFeatures
from performance_extractor_new import PerformanceFeature
import os
import pickle


def piece_to_phrase(xml_filename, match_fn, phrase_maxlen = 128):

    part = partitura.load_musicxml(xml_filename)
    a = ScoreFeatures(part,True)
    onset_phrase = []
    for i,n in enumerate(a.X['detached-legato']):
        if n == 1:
            onset_i = a.dict_xmlidx2onset[i]
            onset_phrase.append(onset_i)
    onsets = []
    for onset_i in range(len(onset_phrase)-1): 
        phrase_start = onset_phrase[onset_i]
        phrase_end = onset_phrase[onset_i+1]-1
        onsets.append(range(phrase_start, phrase_end))
    onsets.append(range(phrase_end, a.num_onsets))

    # extract performance file    
    midi_fn = match_fn[:-10] + '.mid' 
    p = PerformanceFeature(a, match_fn,midi_fn)

    # phrase information
    phrase_len = []
    phrases = np.zeros((len(onsets), phrase_maxlen, 62))
    vels_max = np.zeros((len(onsets), phrase_maxlen , 1))
    vels_diff = np.zeros((len(onsets), phrase_maxlen , 1))
    bpms = np.zeros((len(onsets), phrase_maxlen , 1))
    arts = np.zeros((len(onsets), phrase_maxlen , 1))
    sustain = np.zeros((len(onsets), phrase_maxlen , 1))
    soft = np.zeros((len(onsets), phrase_maxlen , 1))

    # for each phrase
    for i in range(len(onsets)):
        # get performance index
        mp_idxs = []
        for onset in onsets[i]:
            if onset in p.mp.onset2mp:
                mp_idxs += p.mp.onset2mp[onset]
        # get xml index
        xml_idxs = p.mp.matched_perf['xml_idx'][mp_idxs]
        # truncate if more
        if len(mp_idxs) > phrase_maxlen:
            mp_idxs = mp_idxs[:phrase_maxlen]
            xml_idxs = xml_idxs[:phrase_maxlen]
        L = len(mp_idxs)
        phrase_len.append(L)
        phrases[i][:L] = a.score_features[xml_idxs]
        
    return phrases, phrase_len, onsets



def feature_by_phrase():
    length = []
    phrase_maxlen = 64

    for data_dir in os.listdir('all_data/Matched_Data/'):
        #data_dir = 'chopin_op10_No1/'
        folder_name = 'all_data/Matched_Data/' + data_dir
        xml_filename = os.path.join(folder_name, 'xml.xml')
        if os.path.exists(xml_filename):
            part = partitura.load_musicxml(xml_filename)
            a = ScoreFeatures(part,True)
            onset_phrase = []
            for i,n in enumerate(a.X['detached-legato']):
                if n == 1:
                    onset_i = a.dict_xmlidx2onset[i]
                    onset_phrase.append(onset_i)
            onsets = []
            for onset_i in range(len(onset_phrase)-1): 
                phrase_start = onset_phrase[onset_i]
                phrase_end = onset_phrase[onset_i+1]-1
                onsets.append(range(phrase_start, phrase_end))
            onsets.append(range(phrase_end, a.num_onsets))

            # extract performance file
            for files in os.listdir(folder_name):
                if files.endswith('_match.txt'):
                    match_fn = os.path.join(folder_name, files)
                    name = files[:-10] #.split('_')[0]   
                    midi_fn = os.path.join(folder_name, name + '.mid')
                    p = PerformanceFeature(a, match_fn,midi_fn)

                    # phrase information
                    phrases = np.zeros((len(onsets), phrase_maxlen, 61))
                    vels_max = np.zeros((len(onsets), phrase_maxlen , 1))
                    vels_diff = np.zeros((len(onsets), phrase_maxlen , 1))
                    bpms = np.zeros((len(onsets), phrase_maxlen , 1))
                    arts = np.zeros((len(onsets), phrase_maxlen , 1))
                    sustain = np.zeros((len(onsets), phrase_maxlen , 1))
                    soft = np.zeros((len(onsets), phrase_maxlen , 1))

                    # for each phrase
                    for i in range(len(onsets)):
                        # get performance index
                        mp_idxs = []
                        for onset in onsets[i]:
                            if onset in p.mp.onset2mp:
                                mp_idxs += p.mp.onset2mp[onset]
                        # get xml index
                        xml_idxs = p.mp.matched_perf['xml_idx'][mp_idxs]
                        length.append(len(xml_idxs))
                        # truncate if more
                        if len(mp_idxs) > phrase_maxlen:
                            mp_idxs = mp_idxs[:phrase_maxlen]
                            xml_idxs = xml_idxs[:phrase_maxlen]
                        L = len(mp_idxs)
                        phrases[i][:L] = a.score_features[xml_idxs]
                        vels_max[i][:L] = p.Y['vel_onsetmax'][mp_idxs].reshape((L,1))
                        vels_diff[i][:L] = p.Y['vel_maxdiff'][mp_idxs].reshape((L,1))
                        bpms[i][:L] = p.Y['bpm_raw'][mp_idxs].reshape((L,1))
                        arts[i][:L] = p.Y['log_articulation'][mp_idxs].reshape((L,1))
                        sustain[i][:L] = p.Y['sus_pedal'][mp_idxs].reshape((L,1))
                        soft[i][:L] = p.Y['soft_pedal'][mp_idxs].reshape((L,1))
                    data = {}
                    data['X'] = phrases
                    data['Y'] = {}
                    data['Y']['vel_onsetmax'] = vels_max
                    data['Y']['vel_maxdiff'] = vels_diff
                    data['Y']['bpm_raw'] = bpms
                    data['Y']['log_articulation'] = arts
                    data['Y']['sus_pedal'] = sustain
                    data['Y']['soft_pedal'] = soft
                    pickle.dump(data,open('all_data/pkl/0302-64/'+name+'.p','wb'))


def create_X_vel_0311(a, b):
    features_raw = a.score_features[b.matched_perf['xml_idx']] 
    features_m = np.zeros((a.num_measures, features_raw.shape[1]))
    
    prev_vels = np.zeros((b.M,1))
    upper_vels = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    #vels = []
    vels = b.Y['vel']/127.

    upper_mean = np.average(b.Ymeasure['vel_mean'])/127.
    vels_m = np.zeros((a.num_measures, 1))
    prev_vels_m = np.zeros((a.num_measures, 1))
    upper_vels_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    vels_m = []

    for i in range(b.M):
        # Feature 0-127: current feature (pitch)
        # pitches[i,int(pitch_raw[i])] = 1
        # Feature: previous vels
        if i == 0:
            prev_vels[i] = 0
        else:
            prev_vels[i] = b.Y['vel'][i-1] / 127.
        # Feature: upper vels
        m = a.dict_xmlidx2measure[i]
        upper_vels[i] = b.Ymeasure['vel_mean'][m]/127.
        # Feature 385: whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        #vels.append(b.Y['vel'][i])

    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            feature_mean = np.mean(a.score_features[xml_idxs],axis=0)
            features_m[m] = feature_mean
            # Feature 128-256: bpm of previous measure
            if m == 0:
                prev_vels_m[m] = 0
            else:
                prev_vels_m[m] = b.Ymeasure['vel_mean'][m-1]/127.
            # Feature 257--(257+128): upper bpm
            upper_vels_m[m] = upper_mean
            # Label: average bpm
        vels_m.append(b.Ymeasure['vel_mean'][m]/127.)

    # concate
    X_feature = np.hstack((features_raw, prev_vels))
    X_feature = np.hstack((X_feature, upper_vels))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((features_m, prev_vels_m))
    Xm_feature = np.hstack((Xm_feature, upper_vels_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))

    data = {}
    data['X'] = X_feature
    data['Yvel'] = vels
    data['Xm'] = Xm_feature
    data['Ymvel'] = vels_m
    return data

def create_X_bpm_0312(a, b):
    features_raw = a.score_features[b.matched_perf['xml_idx']] 
    features_m = np.zeros((a.num_measures, features_raw.shape[1]))
    
    b.Y['bpm_raw'][b.Y['bpm_raw']<=0] = np.average(b.Y['bpm_raw'][b.Y['bpm_raw']>0])
    bpms = b.Y['bpm_raw']
    
    b.Ymeasure['bpm_raw'][b.Ymeasure['bpm_raw']<=0] = np.average(b.Ymeasure['bpm_raw'][b.Ymeasure['bpm_raw']>0])
    
    prev_bpms = np.zeros((b.M,1))
    upper_bpms = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    bpms = []
    
    upper_mean = np.average(b.Ymeasure['bpm_raw'])
    bpms_m = np.zeros((a.num_measures, 1))
    prev_bpms_m = np.zeros((a.num_measures, 1))
    upper_bpms_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    bpms_m = []

    for i in range(b.M):
        # upper bpm
        m = a.dict_xmlidx2measure[i]
        upper_bpms[i] = b.Ymeasure['bpm_raw'][m]
        # previous bpm in relative to upper measure bpm
        if i == 0:
            prev_bpms[i] = -1
        else:
            prev_bpms[i] = b.Y['bpm_raw'][i-1] / upper_bpms[i-1]  
        
        # whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        bpms.append(b.Y['bpm_raw'][i] / upper_bpms[i])

    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            feature_mean = np.mean(a.score_features[xml_idxs],axis=0)
            features_m[m] = feature_mean
            # Feature 128-256: bpm of previous measure
            if m == 0:
                prev_bpms_m[m] = -1
            else:
                prev_bpms_m[m] = b.Ymeasure['bpm_raw'][m-1] / upper_mean
            # Feature 257--(257+128): upper bpm
            # upper_vels_m[m] = upper_mean
            # Label: average bpm
        bpms_m.append(b.Ymeasure['bpm_raw'][m] / upper_mean)   # this should fix the problem
    
#     med_bpm = bpms_m[bpms_m <= 10]
#     med_bpm = med_bpm[med_bpm > 0]
#     med = np.median(med_bpm)
#     bpms_m[bpms_m > 10] = med
#     bpms_m[bpms_m < 0] = med
    
    # concate
    X_feature = np.hstack((features_raw, prev_bpms))
    X_feature = np.hstack((X_feature, upper_bpms))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((features_m, prev_bpms_m))
    Xm_feature = np.hstack((Xm_feature, upper_bpms_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))

    data = {}
    data['X'] = X_feature
    data['Ybpm'] = bpms
    data['Xm'] = Xm_feature
    data['Ymbpm'] = bpms_m
    return data



def create_X_bpm_0310(a, b, normalize = False):
    features_raw = a.score_features[b.matched_perf['xml_idx']] 
    features_m = np.zeros((a.num_measures, features_raw.shape[1]))
    
    if normalize:
        bpm_mean = np.mean(b.Y['bpm_raw'])
        bpm_std = np.std(b.Y['bpm_raw'])
        b.Y['bpm_raw'] = (b.Y['bpm_raw'] - bpm_mean) / bpm_std
        
        bpm_mean_m = np.mean(b.Ymeasure['bpm_raw'])
        bpm_std_m = np.std(b.Ymeasure['bpm_raw'])
        b.Ymeasure['bpm_raw'] = (b.Ymeasure['bpm_raw'] - bpm_mean_m) / bpm_std_m
        
    
    bpms = b.Y['bpm_raw']
    bpms[bpms <= 0] = np.mean(bpms)
    
    prev_bpms = np.zeros((b.M,1))
    upper_bpms = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    bpms = []
    
#     med_bpm = bpms[bpms < 10]
#     med_bpm = med_bpm[med_bpm > 0]
#     med = np.median(med_bpm)
#     bpms[bpms > 10] = med
#     bpms[bpms < 0] = med

    upper_mean = np.average(b.Ymeasure['bpm_raw'])
    bpms_m = np.zeros((a.num_measures, 1))
    prev_bpms_m = np.zeros((a.num_measures, 1))
    upper_bpms_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    bpms_m = []

    for i in range(b.M):
        # Feature 0-127: current feature (pitch)
        # pitches[i,int(pitch_raw[i])] = 1
        # Feature 128-256: previous bpms
        if i == 0:
            prev_bpms[i] = -1
        else:
            prev_bpms[i] = b.Y['bpm_raw'][i-1] / upper_mean  # previous bpm equals previous bpm in relative to upper bpm
        # Feature 257--384: upper bpm
        m = a.dict_xmlidx2measure[i]
        upper_bpms[i] = b.Ymeasure['bpm_raw'][m]
        # Feature 385: whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        bpms.append(b.Y['bpm_raw'][i])

    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            feature_mean = np.mean(a.score_features[xml_idxs],axis=0)
            features_m[m] = feature_mean
            # Feature 128-256: bpm of previous measure
            if m == 0:
                prev_bpms_m[m] = -1
            else:
                prev_bpms_m[m] = b.Ymeasure['bpm_raw'][m-1]
            # Feature 257--(257+128): upper bpm
            # upper_vels_m[m] = upper_mean
            # Label: average bpm
        bpms_m.append(b.Ymeasure['bpm_raw'][m])   # this should fix the problem
    
#     med_bpm = bpms_m[bpms_m <= 10]
#     med_bpm = med_bpm[med_bpm > 0]
#     med = np.median(med_bpm)
#     bpms_m[bpms_m > 10] = med
#     bpms_m[bpms_m < 0] = med
    
    # concate
    X_feature = np.hstack((features_raw, prev_bpms))
    X_feature = np.hstack((X_feature, upper_bpms))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((features_m, prev_bpms_m))
    Xm_feature = np.hstack((Xm_feature, upper_bpms_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))

    data = {}
    data['X'] = X_feature
    data['Ybpm'] = bpms
    data['Xm'] = Xm_feature
    data['Ymbpm'] = bpms_m
    return data


def create_X_bpm_0311(a, b, normalize = False):
    features_raw = a.score_features[b.matched_perf['xml_idx']] 
    features_m = np.zeros((a.num_measures, features_raw.shape[1]))
    
    if normalize:
        bpm_mean = np.mean(b.Y['bpm_raw'])
        bpm_std = np.std(b.Y['bpm_raw'])
        b.Y['bpm_raw'] = (b.Y['bpm_raw'] - bpm_mean) / bpm_std
        
        bpm_mean_m = np.mean(b.Ymeasure['bpm_raw'])
        bpm_std_m = np.std(b.Ymeasure['bpm_raw'])
        b.Ymeasure['bpm_raw'] = (b.Ymeasure['bpm_raw'] - bpm_mean_m) / bpm_std_m
        
    
    bpms = b.Y['bpm_raw']
    
    prev_bpms = np.zeros((b.M,1))
    upper_bpms = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    bpms = []
    
#     med_bpm = bpms[bpms < 10]
#     med_bpm = med_bpm[med_bpm > 0]
#     med = np.median(med_bpm)
#     bpms[bpms > 10] = med
#     bpms[bpms < 0] = med

    upper_mean = np.average(b.Ymeasure['bpm_raw'])
    bpms_m = np.zeros((a.num_measures, 1))
    prev_bpms_m = np.zeros((a.num_measures, 1))
    upper_bpms_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    bpms_m = []

    for i in range(b.M):
        # Feature 0-127: current feature (pitch)
        # pitches[i,int(pitch_raw[i])] = 1
        # Feature 128-256: previous bpms
        if i == 0:
            prev_bpms[i] = -1
        else:
            prev_bpms[i] = b.Y['bpm_raw'][i-1]
        # Feature 257--384: upper bpm
        m = a.dict_xmlidx2measure[i]
        upper_bpms[i] = b.Ymeasure['bpm_raw'][m]
        # Feature 385: whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        bpms.append(b.Y['bpm_raw'][i])
      
    first_measure = False
    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            feature_mean = np.mean(a.score_features[xml_idxs],axis=0)
            features_m[m] = feature_mean
            # Feature 128-256: bpm of previous measure
            if first_measure == False:
                prev_bpms_m[m] = -1
                first_measure = True
            else:
                prev_bpms_m[m] = b.Ymeasure['bpm_raw'][m-1]
            # Feature 257--(257+128): upper bpm
            # upper_vels_m[m] = upper_mean
            # Label: average bpm
        bpms_m.append(b.Ymeasure['bpm_raw'][m])   # this should fix the problem
    
#     med_bpm = bpms_m[bpms_m <= 10]
#     med_bpm = med_bpm[med_bpm > 0]
#     med = np.median(med_bpm)
#     bpms_m[bpms_m > 10] = med
#     bpms_m[bpms_m < 0] = med
    
    # concate
    X_feature = np.hstack((features_raw, prev_bpms))
    X_feature = np.hstack((X_feature, upper_bpms))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((features_m, prev_bpms_m))
    Xm_feature = np.hstack((Xm_feature, upper_bpms_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))

    data = {}
    data['X'] = X_feature
    data['Ybpm'] = bpms
    data['Xm'] = Xm_feature
    data['Ymbpm'] = bpms_m
    return data


def create_X_bpm_simple(a, b, batch_size=16):
    pitch_raw = a.X['pitch'][b.matched_perf['xml_idx']] 
    pitches = pitch_raw # * 127.
    pitches_m = np.zeros((a.num_measures, 1))
    bpms = b.Y['bpm_raw']
    
    prev_bpms = np.zeros((b.M,1))
    upper_bpms = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    bpms = []

    upper_mean = int(np.average(b.Ymeasure['bpm_raw']))
    bpms_m = np.zeros((a.num_measures, 1))
    prev_bpms_m = np.zeros((a.num_measures, 1))
    upper_bpms_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    bpms_m = []

    for i in range(b.M):
        # Feature 0-127: current feature (pitch)
        # pitches[i,int(pitch_raw[i])] = 1
        # Feature 128-256: previous bpms
        if i == 0:
            prev_bpms[i] = -1
        else:
            prev_bpms[i] = b.Y['bpm_raw'][i-1]
        # Feature 257--384: upper bpm
        m = a.dict_xmlidx2measure[i]
        upper_bpms[i] = b.Ymeasure['bpm_raw'][m]
        # Feature 385: whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        bpms.append(b.Y['bpm_raw'][i])

    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            pitch_mean = round(a.X['pitch'][xml_idxs].mean())
            pitches_m[m] = pitch_mean
            # Feature 128-256: bpm of previous measure
            if m == 0:
                prev_bpms_m[m] = -1
            else:
                prev_bpms_m[m] = b.Ymeasure['bpm_raw'][m-1]
            # Feature 257--(257+128): upper bpm
            # upper_vels_m[m] = upper_mean
            # Label: average bpm
            bpms_m.append(b.Ymeasure['bpm_raw'][m])
    
    # concate
    X_feature = np.hstack((pitches, prev_bpms))
    X_feature = np.hstack((X_feature, upper_bpms))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((pitches_m, prev_bpms_m))
    Xm_feature = np.hstack((Xm_feature, upper_bpms_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))

    data = {}
    data['X'] = X_feature
    data['Ybpm'] = bpms
    data['Xm'] = Xm_feature
    data['Ymbpm'] = bpms_m
    return data



def create_X_vel_simple(a, b, batch_size=16):
    """
    2020-02-06, concate pitch, prev velocity, upper velocity, note indicator
    """
    pitch_raw = a.X['pitch'][b.matched_perf['xml_idx']] 
    pitches = pitch_raw 
    prev_vels = np.zeros((b.M,1))
    upper_vels = np.zeros((b.M,1))
    is_notes = np.zeros((b.M,1))
    vels = []
    
    upper_mean = int(np.average(b.Ymeasure['vel_mean']))
    pitches_m = np.zeros((a.num_measures, 1))
    prev_vels_m = np.zeros((a.num_measures, 1))
    upper_vels_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))
    vels_m = []

    for i in range(b.M):
        # Feature 0-127: current feature (pitch)
        # pitches[i] = int(pitch_raw[i])
        # Feature 128-256: previous velocity
        if i == 0:
            prev_vels[i] = 0
        else:
            prev_vels[i] = 1 + b.Y['vel'][i-1]
        # Feature 257--384: upper velocity
        upper_vels[i] = int(round(b.Y['vel_measuremean'][i]))
        # Feature 385: whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        vels.append(b.Y['vel'][i])
    
    for m in range(a.num_measures):
        if m in a.dict_measure2xmlidx and m in b.measure2mp:
            # Feature 0-127: current feature (average pitch)
            xml_idxs = a.dict_measure2xmlidx[m]
            pitch_mean = round(a.X['pitch'][xml_idxs].mean())
            pitches_m[m] = pitch_mean
            # Feature 128-256: velocity of previous measure
            if m == 0:
                prev_vels_m[m] = 0
            else:
                prev_vels_m[m] = 1 + int(round(b.Ymeasure['vel_mean'][m-1]))
            # upper velocity
            xml_idxs = a.dict_measure2xmlidx[m]
            mps = b.measure2mp[m]
            curr_vel = int(b.Y['vel_measuremean'][mps[0]])
            #vels_m[m] = curr_vel
            vels_m.append(curr_vel)
          
    # concate
    X_feature = np.hstack((pitches, prev_vels))
    X_feature = np.hstack((X_feature, upper_vels))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((pitches_m, prev_vels_m))
    Xm_feature = np.hstack((Xm_feature, upper_vels_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))
    
    data = {}
    data['X'] = X_feature
    data['Yvel'] = vels
    data['Xm'] = Xm_feature
    data['Ymvel'] = vels_m
    return data






# given a directory of extracted pickle files, extract into a giant file
def prepare_data1218(pkl_dir = 'all_data/pkl/1218/',outp='all_data/pkl/1218.p'):
    #process_individual_file()
    all_files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]
    train_file = []
    valid_file = []
    for fn in random.sample(all_files, int(len(all_files) * 0.8)):
        if fn.endswith('.p'):
            train_file.append(fn)
    for fn in all_files:
        if fn not in train_file:
            valid_file.append(fn)

    def getdata_1218(files):
        pk = pickle.load(open(files[0],'rb'))
        data = {}
        X_raw = pk['X']
        X_onehot = pk['X_onehot']
        Y_vel = pk['Y']['vel'].reshape((-1,1))
        Y_bpm = pk['Y']['bpm'].reshape((-1,1))
        Y_art = pk['Y']['log_articulation'].reshape((-1,1))
        Y_sus = pk['Y']['sus_pedal']
        Y_soft = pk['Y']['soft_pedal']
        for i in range(1,len(files)):
            pkl = pickle.load(open(files[i],'rb'))
            print('reading: ', files[i])
            X_raw = np.vstack((X_raw,pkl['X']))
            X_onehot = np.vstack((X_onehot,pkl['X_onehot']))
            Y_vel = np.vstack((Y_vel, pkl['Y']['vel'].reshape((-1,1))))
            Y_bpm = np.vstack((Y_bpm, pkl['Y']['bpm'].reshape((-1,1))))
            Y_art = np.vstack((Y_art, pkl['Y']['log_articulation'].reshape((-1,1))))
            Y_sus = np.vstack((Y_sus,pkl['Y']['sus_pedal']))
            Y_soft = np.vstack((Y_soft, pkl['Y']['soft_pedal']))
            print(X_raw.shape, Y_bpm.shape, Y_sus.shape)

        data['X'] = X_raw
        #data['X_onehot'] = X_onehot
        data['Y'] = {}
        #data['Y']['vel'] = Y_vel
        #data['Y']['bpm'] = Y_bpm
        data['Y']['log_articulation'] = Y_art
        #data['Y']['sus_pedal'] = Y_sus
        #data['Y']['soft_pedal'] = Y_soft
        return data

    data = {}
    data['train'] = getdata_1218(train_file)
    data['val'] = getdata_1218(valid_file)

    pickle.dump(data,open(outp,'wb'))
    return data




    def process_individual_file(xml_dir = 'all_data/xml_chopin/',
                                p_dir = 'all_data/pkl/1218/'):
        # read score and performance, save as into .p file each
        # not called by the upper function because it was done in notebook

        for f in os.listdir(xml_dir):
            if f.endswith('.xml'):
                xml_fn = xml_dir + f
                piece_name = xml_fn.split('/')[-1].split('.')[0]
                match_fn = 'all_data/match_chopin/' + piece_name + '.match'
                if os.path.exists(match_fn):
                    part = parse_music_xml(xml_fn).score_parts[0]
                    score = score_extractor.ScoreFeature(part)
                    performance = performance_extractor.PerformanceFeature(score, match_fn)
                    piece = {}
                    piece['X'] = score.score_features_raw[performance.valid_xmlidx]
                    piece['X_onehot'] = score.score_features[performance.valid_xmlidx]
                    piece['Y_raw'] = {}
                    piece['Y'] = {}
                    piece['Yname'] = ['vel','bpm_raw','log_articulation','sus_pedal','soft_pedal']
                    piece['Y_raw']['vel'] = performance.Y['vel'][performance.valid_mpidx]
                    piece['Y']['vel'] = performance.Y['vel'][performance.valid_mpidx] / 127.
                    piece['Y_raw']['bpm'] = performance.Y['bpm_raw'][performance.valid_mpidx]
                    piece['bpm_ave'] = np.mean(performance.Y['bpm_raw'][performance.valid_mpidx])
                    piece['bpm_std'] = np.std(performance.Y['bpm_raw'][performance.valid_mpidx])
                    piece['Y']['bpm'] = (performance.Y['bpm_raw'][performance.valid_mpidx] - piece['bpm_ave'])/piece['bpm_std']
                    piece['Y_raw']['log_articulation'] = performance.Y['log_articulation'][performance.valid_mpidx]
                    piece['art_ave'] = np.mean(performance.Y['log_articulation'][performance.valid_mpidx])
                    piece['art_std'] = np.std(performance.Y['log_articulation'][performance.valid_mpidx])
                    piece['Y']['log_articulation'] = (performance.Y['log_articulation'][performance.valid_mpidx]-piece['art_ave'])/piece['art_std']
                    piece['Y']['sus_pedal'] = performance.Y['sus_pedal'][performance.valid_mpidx]
                    piece['Y']['soft_pedal'] = performance.Y['soft_pedal'][performance.valid_mpidx]
                    pickle.dump(piece,open(p_dir+piece_name+'.p','wb'))

                    print('save as: %s') % p_dir+piece_name+'.p'



# given a directory, extract all matched performance, and store in pkl_dir
# use retrieve_data to retrieve it out into class
def prepare_data1205(pkl_dir = 'all_data/pkl/1205/', xml_dir = 'all_data/xml_chopin/',
                     match_dir = 'all_data/match_chopin/'):
    from ofaidata_utils.data_handling.musicxml import parse_music_xml
    import score_extractor
    import performance_extractor

    for xml_fn in os.listdir(xml_dir):
        if xml_fn.endswith('.xml'):
            piece_name = xml_fn.split('.')[0]
            match_fn = match_dir + piece_name + '.match'
            if os.path.exists(match_fn):
                out_name = pkl_dir + piece_name + '.p'
                if not os.path.exists(out_name):
                    part = parse_music_xml(xml_dir + xml_fn).score_parts[0]
                    score = score_extractor.ScoreFeature(part)
                    performance = performance_extractor.PerformanceFeature(score, match_fn)
                    curr_data = extract_data_simple(score, performance)
                    if curr_data['Xdim'][0] > 1:
                        curr_data['piece_name'] = piece_name
                        pickle.dump(curr_data, open(out_name, 'wb'))
                        print(curr_data['Xdim']) #, curr_data['Ydim']
                        print("save to: " + out_name)


def extract_data_simple(score, performance):
    data = {}
    data['X'] = score.score_features[performance.valid_xmlidx]
    data['Xname'] = score.train_features
    data['Xdim'] = data['X'].shape
    data['Y'] = {}
    data['Yname'] = performance.Ynotename

    for fname in data['Yname']:
        if fname not in ['scale_beat_period_mean']:
            data['Y'][fname] = performance.Y[fname][performance.valid_mpidx]
        else:
            data['Y'][fname] = performance.Y[fname]

    return data


def retrieve_data_simple(files, pname):  #'all_data/pkl/1010/'
    """
    Given a list of pickle file, stack and extract information
    # files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]
    2019-12-07 simple as regression
    """
    pk = pickle.load(open(files[0], 'rb'))
    x = pk['X']
    Y = {}
    y = {}

    for fname in pk['Yname']:
            y[fname] = pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))

    for fn in files[1:]:
        pk = pickle.load(open(fn, 'rb'))
        x = np.vstack((x, pk['X']))
        for fname in pk['Yname']:
            y[fname] = np.vstack((y[fname], pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))))

    y['vel_onsetmax'] /= 127.
    y['vel_maxdiff'] /= 127.

    Y['note']    = {"name": pk['Yname'], "data": y}

    pickle.dump(y, open(pname, 'wb'))
    return x, Y


# given a score, and a performance, extract necessary data, use retrieve_data for read the representation
def extract_data(score, performance):
    data = {}
    data['X'] = score.score_features[performance.valid_xmlidx]
    data['Xname'] = score.train_features
    data['Xdim'] = data['X'].shape
    data['Y'] = {}
    data['Yonset'] = {}
    data['Ymeasure'] = {}
    data['Yslur'] = {}
    data['Yname'] = performance.Ynotename
    data['Ybigname'] = ['bpm_raw','bpm_norm', 'scale_bpm_mean']
    data['Yslurname'] = ['bpm_raw','bpm_ratio','bpm_mean']

    for fname in data['Yname']:
        if fname not in ['scale_bpm_onset_mean','scale_bpm_measure_mean']:
            data['Y'][fname] = performance.Y[fname][performance.valid_mpidx]
        else:
            data['Y'][fname] = performance.Y[fname]
    for fname in data['Ybigname']:
        data['Yonset'][fname] = performance.Yonset[fname]
        data['Ymeasure'][fname] = performance.Ymeasure[fname]
    for fname in data['Yslurname']:
        data['Yslur'][fname] = performance.Yslur[fname]

    return data


def retrieve_data(files):  #'all_data/pkl/1010/'
    """
    Given a list of pickle file, stack and extract information
    # files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]
    """
    pk = pickle.load(open(files[0], 'rb'))
    x = pk['X']
    Y = {}
    y = {}
    yonset = {}
    ymeasure = {}
    yslur = {}

    for fname in pk['Yname']:
        if fname not in ['scale_bpm_onset_mean','scale_bpm_measure_mean']:
            y[fname] = pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))
        else:
            y[fname] = [pk['Y'][fname]]
    for fname in pk['Ybigname']:
        if fname not in ['scale_bpm_mean']:
            yonset[fname] = pk['Yonset'][fname].reshape((len(pk['Yonset'][fname]), 1))
            ymeasure[fname] = pk['Ymeasure'][fname].reshape((len(pk['Ymeasure'][fname]), 1))
        else:
            yonset[fname] = [pk['Yonset'][fname]]
            ymeasure[fname] = [pk['Ymeasure'][fname]]
    for fname in pk['Yslurname']:
        yslur[fname] = [pk['Yslur'][fname]]

    for fn in files[1:]:
        pk = pickle.load(open(fn, 'rb'))
        x = np.vstack((x, pk['X']))
        for fname in pk['Yname']:
            if fname not in ['scale_bpm_onset_mean','scale_bpm_measure_mean']:
                y[fname] = np.vstack((y[fname], pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))))
            else:
                y[fname].append(pk['Y'][fname])
        for fname in pk['Ybigname']:
            if fname not in ['scale_bpm_mean','scale_bpm_mean']:
                yonset[fname] = np.vstack((yonset[fname], pk['Yonset'][fname].reshape((len(pk['Yonset'][fname]), 1))))
                ymeasure[fname] = np.vstack((ymeasure[fname], pk['Ymeasure'][fname].reshape((len(pk['Ymeasure'][fname]), 1))))
            else:
                yonset[fname].append(pk['Yonset'][fname])
                ymeasure[fname].append(pk['Ymeasure'][fname])
        for fname in pk['Yslurname']:
            yslur[fname] += pk['Yslur'][fname]

    Y['note']    = {"name": pk['Yname'], "data": y}
    Y['onset']   = {"name": pk['Ybigname'], "data": yonset}
    Y['measure'] = {"name": pk['Ybigname'], "data": ymeasure}
    Y['slur'] = {"name": pk['Yslurname'], "data": yslur}

    return x, Y


def construct_marking_category(xml_dirs = ['all_data/xml_beethoven/','all_data/xml_chopin/'],
                                            fn1 = 'all_data/cat_dynamic_marking.p',
                                            fn2 = 'all_data/cat_tempo_marking.p'):
    """
    It aims to get unique name of dynamic marking in the dataset
    and store as a pickle file
    """
    # open from the pickle file
    print('construct dynamic and tempo marking category file')
    list_dynamic_marking = []
    list_tempo_marking = []
    cat_dynamic_marking = dict()
    cat_tempo_marking = dict()

    for xml_dir in xml_dirs:
        for f in os.listdir(xml_dir):
            if f.endswith('.xml'):
                print('reading: %s' + xml_dir + f)
                part = parse_music_xml(xml_dir + f).score_parts[0]
                part.expand_grace_notes()
                for d in part.get_loudness_directions():
                    if d.text not in cat_dynamic_marking:
                        list_dynamic_marking.append(d.text)
                        #cat_dynamic_marking_reverse[len(cat_dynamic_marking)] = d.text
                        cat_dynamic_marking[d.text] = len(cat_dynamic_marking)   # map to index
                for d in part.get_tempo_directions():
                    if d.text not in cat_tempo_marking:
                        list_tempo_marking.append(d.text)
                        cat_tempo_marking[d.text] = len(cat_tempo_marking)

    pickle.dump([list_dynamic_marking, cat_dynamic_marking],open(fn1, 'wb'))
    pickle.dump([list_tempo_marking, cat_tempo_marking],open(fn2, 'wb'))
    print('write to: %s' + fn1)
    print('write to: %s' + fn2)


def slice_test_data(X, seq_len = 64):
    # slice X according to sequence length
    feature_num = X.shape[1]
    num_input = int(X.shape[0] / seq_len + 1)
    pad_amount = num_input * seq_len - X.shape[0]
    inputX_test = np.pad(X, ((0,pad_amount), (0, 0)), 'constant').reshape((num_input,seq_len,feature_num))
    return inputX_test



def slice_data(Y, seq_len = 64, slidingwindow = True):
    """
    slice data into blocks of sequences using sliding window
    """

    if slidingwindow:
        num_input = Y.shape[0] - seq_len + 1
        Yout = np.zeros((num_input, seq_len, Y.shape[1]))
        for i in range(num_input-1):
            Yout[i] = Y[i:(i+seq_len),:]
    else:
        num_input = int(Y.shape[0] / seq_len + 1)
        pad_amount = num_input * seq_len - Y.shape[0]
        Yout = np.pad(Y, ((0,pad_amount), (0, 0)), 'constant').reshape((num_input,seq_len,Y.shape[1]))
    return Yout


def prepare_data(raw_data, seq_len = 50, slidingwindow = True):
    """
    Given an array of data (data has X,Y, etc.)
    prepare data for the model
    segment the data into sequences
    """

    def seg_X_Y_slide(data, seq_len):
        """
        Segment data into sequences (with sliding window)
        """
        num_input = data['X'].shape[0] - seq_len + 1
        inputX = np.zeros((num_input, seq_len, data['X'].shape[1]))
        outputY = np.zeros((num_input, seq_len, data['Y'].shape[1]))

        for i in range(num_input-1):
            inputX[i] = data['X'][i:(i+seq_len),:]
            outputY[i] = data['Y'][i:(i+seq_len),:]
        return inputX, outputY

    def seg_X_Y(data, seq_len):
        """
        Segment data into sequences (without repeat)
        """
        num_input = data['X'].shape[0] / seq_len + 1

        inputX = np.zeros((num_input, seq_len, data['X'].shape[1]))
        outputY = np.zeros((num_input, seq_len, data['Y'].shape[1]))

        for i in range(num_input-1):
            inputX[i] = data['X'][i*seq_len:(i+1)*seq_len,:]
            outputY[i] = data['Y'][i*seq_len:(i+1)*seq_len,:]
        # the last one
        l = (num_input-1) * seq_len
        r = len(data['X']) - l
        inputX[-1,:r] = data['X'][l:]
        outputY[-1,:r] = data['Y'][l:]
        return inputX, outputY

    if slidingwindow:
        X_all, Y_all = seg_X_Y_slide(raw_data[0], seq_len)
        for i in range(1,len(raw_data)):
            x,y = seg_X_Y(raw_data[i], seq_len)
            X_all = np.vstack((X_all, x))
            Y_all = np.vstack((Y_all, y))
    else:
        X_all, Y_all = seg_X_Y(raw_data[0], seq_len)
        for i in range(1,len(raw_data)):
            x,y = seg_X_Y(raw_data[i], seq_len)
            X_all = np.vstack((X_all, x))
            Y_all = np.vstack((Y_all, y))

    return X_all, Y_all



def construct_training_data(xml_dir = 'all_data/xml_chopin/',
                            match_dir = 'all_data/match_chopin/',
                            data_name = 'data1227.npy',smoothlevel = 1.0):
    """
    Construct training data
    """
    #if not os.path.isfile('data/cat_dynamic_marking.p'):
    #    construct_dynamic_marking_category()

    training_data= []
    for f in os.listdir(xml_dir):
        if f.endswith('.xml'):
            name = f.split('.')[0]
            xml_fn = xml_dir + f
            match_fn = match_dir + name + '.match'
            print('reading: ' + name)
            data = feature_extract.get_X_Y(xml_fn, match_fn, smoothlevel)
            data['attribute'] = "smoothlevel: %f" % (smoothlevel)
            training_data.append(data)
    print('save training data to: ' + data_name)
    np.save(data_name, training_data)
    return training_data