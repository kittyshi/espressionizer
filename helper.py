import numpy as np



def get_unique_seq(onsets, offsets, unique_onset_idxs=None,
                   return_diff=False):
    """
    Get unique onsets of a sequence of notes
    """
    eps = np.finfo(np.float32).eps

    first_time = np.min(onsets)

    # ensure last score time is later than last onset
    last_time = max(np.max(onsets) + eps, np.max(offsets))

    total_dur = last_time - first_time

    if unique_onset_idxs is None:
        # unique_onset_idxs = unique_onset_idx(score[:, 0])
        unique_onset_idxs = get_unique_onset_idxs(onsets)

    u_onset = np.array([np.mean(onsets[uix]) for uix in unique_onset_idxs])
    # add last offset, so we have as many IOIs as notes
    u_onset = np.r_[u_onset, last_time]

    output_dict = dict(
        u_onset=u_onset,
        total_dur=total_dur,
        unique_onset_idxs=unique_onset_idxs)

    if return_diff:
        output_dict['diff_u_onset'] = np.diff(u_onset)

    return output_dict


def get_ibi(score_uonset, perf_uonset, smooth_beat = 1.0):
    """
    Get a smoothed version of tempo curve based on beat
    smooth_beat = 0.5: every sixteenth note
    smooth_beat = 1: every eighth note
    smooth_beat = 2: every quarter note
    smooth_beat = 4: every half note
    """
    N = len(score_uonset)
    # onbeat location: maps from beat position to unique onset index (start from beat 0 not -1)
    # meaning that beat 0 start with unique onset index onbeat_location[0], if it's zero, then there is no onset on that beat
    # how many beats are there in total
    iprev = -1
    prevbeat = -1
    onbeat_tempo = np.zeros((N))
    beatgrid_tempo = np.zeros((score_uonset[-1]-score_uonset[0]+1))

    bi = 0
    for i in range(N):
        onset_bt = score_uonset[i]
        grid = onset_bt - onset_bt%smooth_beat
        #print onset_bt
        # if it's on the grid
        if onset_bt == grid:
            if grid == 0:
                iprev = i
                if iprev > 0:
                    ibi_score = (score_uonset[i] - score_uonset[iprev]) # / (grid - prevgrid)
                    ibi_perf = (perf_uonset[i] - perf_uonset[iprev])    # / (grid - prevgrid)
                    onbeat_tempo[i] = ibi_perf/ibi_score
                    beatgrid_tempo[bi] = ibi_perf/ibi_score
            #else:
                #ibi_score = (score_uonset[i] - score_uonset[iprev]) # / (grid - prevgrid)
                #ibi_perf = (perf_uonset[i] - perf_uonset[iprev])    # / (grid - prevgrid)
                #onbeat_tempo[i] = ibi_perf/ibi_score
                #iprev = i

    # smoothing
    left = 0
    right = 0
    while (right < N):
        if onbeat_tempo[right] == 0:
            #print "detect onbeat_tempo[%d] = 0, continue to next" % right
            right += 1
        else:
            if onbeat_tempo[left] == 0:
                #print "detect next nonzero value at onbeat_tempo[%d] = %f, fill in onbeat_tempo[%d:%d] = %f" % (right, onbeat_tempo[right], left, right, onbeat_tempo[right])
                onbeat_tempo[left:right] = onbeat_tempo[right]
                right += 1
                left = right
            else:
                #print "value filled at onbeat_tempo[%d] = %f, continue" % (left, onbeat_tempo[left])
                right += 1
                left = right

    # edge case
    if onbeat_tempo[-1] == 0:
        onbeat_tempo[left-1:] = onbeat_tempo[left-1]
        #print "from %d to last is zero, fill in equals onbeat_tempo[%d] = %f" % (left-1, left-1, onbeat_tempo[left-1])

    return onbeat_tempo[:-1]


def get_smooth_tempo_curve(score_uonset, perf_uonset, smooth_beat = 2):
    """
    Get a smoothed version of tempo curve based on beat
    smooth_beat = 0.5: every sixteenth note
    smooth_beat = 1: every eighth note
    smooth_beat = 2: every quarter note
    smooth_beat = 4: every half note
    """
    N = len(score_uonset)
    # onbeat location: maps from beat position to unique onset index (start from beat 0 not -1)
    # meaning that beat 0 start with unique onset index onbeat_location[0], if it's zero, then there is no onset on that beat
    # how many beats are there in total
    iprev = -1
    prevbeat = -1
    onbeat_tempo = np.zeros((N))
    beatgrid_tempo = np.zeros((score_uonset[-1]-score_uonset[0]+1))

    bi = 0
    for i in range(N):
        onset_bt = score_uonset[i]
        grid = onset_bt - onset_bt%smooth_beat
        #print onset_bt
        # if it's on the grid
        if onset_bt == grid:
            if grid == 0:
                iprev = i
                if iprev > 0:
                    ibi_score = (score_uonset[i] - score_uonset[iprev]) # / (grid - prevgrid)
                    ibi_perf = (perf_uonset[i] - perf_uonset[iprev])    # / (grid - prevgrid)
                    onbeat_tempo[i] = ibi_perf/ibi_score
                    beatgrid_tempo[bi] = ibi_perf/ibi_score
            #else:
                #ibi_score = (score_uonset[i] - score_uonset[iprev]) # / (grid - prevgrid)
                #ibi_perf = (perf_uonset[i] - perf_uonset[iprev])    # / (grid - prevgrid)
                #onbeat_tempo[i] = ibi_perf/ibi_score
                #iprev = i

    # smoothing
    left = 0
    right = 0
    while (right < N):
        if onbeat_tempo[right] == 0:
            #print "detect onbeat_tempo[%d] = 0, continue to next" % right
            right += 1
        else:
            if onbeat_tempo[left] == 0:
                #print "detect next nonzero value at onbeat_tempo[%d] = %f, fill in onbeat_tempo[%d:%d] = %f" % (right, onbeat_tempo[right], left, right, onbeat_tempo[right])
                onbeat_tempo[left:right] = onbeat_tempo[right]
                right += 1
                left = right
            else:
                #print "value filled at onbeat_tempo[%d] = %f, continue" % (left, onbeat_tempo[left])
                right += 1
                left = right

    # edge case
    if onbeat_tempo[-1] == 0:
        onbeat_tempo[left-1:] = onbeat_tempo[left-1]
        #print "from %d to last is zero, fill in equals onbeat_tempo[%d] = %f" % (left-1, left-1, onbeat_tempo[left-1])

    return onbeat_tempo[:-1]



### to fix a problem with some of the match files
def _is_monotonic(s, mask, deltas):
    """Return True when `s[mask]` is monotonic. When `strict` is True,
    check for strict monotonicity.

    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    mask : ndarray(bool)
        a boolean mask to be applied to s

    strict : bool
        when True, return a strictly monotonic sequence (default: True)

    """
    if s[mask].shape[0] < 4:
        return True
    else:
        return np.all(np.diff(s[mask]) / np.diff(deltas[mask]) > 0.0)


def _select_problematic(s, mask, deltas=None):
    """Return the index i of an element of s, such that removing s[i]
    reduces the non-monotonicity of s[mask]. This method assumes the
    first and last element of `s` are the the minimum and maximum,
    respectively.
    # from Carlos
    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    mask : ndarray(bool)
        a boolean mask to be applied to s

    Returns
    =======

    int
       an index i such that removing s[i] from s reduces
       non-monotonicity

    """
    if deltas is not None:
        diffs = np.diff(s[mask]) / np.diff(deltas[mask])
    else:
        diffs = np.diff(s[mask])
    # ignore first and last elements
    problematic = np.argmin(diffs[1:] * diffs[:-1]) + 1

    return np.where(mask)[0][problematic]


def monotonize_times(perftimes, deltas, mask = None):
    """
    Return a mask such that s[mask] is monotonic.
    perftimes: performance times in the match file (some are corrupted)
    deltas: score onset times from xml
    """
    if mask is None:
        mask = np.ones(len(perftimes), dtype=np.bool)
    if _is_monotonic(perftimes, mask, deltas):
        return mask
    else:
        # print('no')
        p = _select_problematic(perftimes, mask, deltas)
        mask[p] = False

        return monotonize_times(perftimes, deltas, mask)


def get_unique_onset_idxs(onsets, eps=1e-6, return_unique_onsets=False):
    """
    Get unique onsets and their indices.

    Parameters
    ----------
    onsets : np.ndarray
        Score onsets in beats.
    eps : float
        Small epsilon (for dealing with quantization in symbolic scores).
        This is particularly useful for dealing with triplets and other
        similar rhytmical structures that do not have a finite decimal
        representation.
    return_unique_onsets : bool (optional)
        If `True`, returns the unique score onsets.

    Returns
    -------
    unique_onset_idxs : np.ndarray
        Indices of the unique onsets in the score.
    unique_onsets : np.ndarray
        Unique score onsets
    """
    # Do not assume that the onsets are sorted
    # (use a stable sorting algorithm for preserving the order
    # of elements with the same onset, which is useful e.g. if the
    # notes within a same onset are ordered by pitch)
    sort_idx = np.argsort(onsets, kind='mergesort')
    split_idx = np.where(np.diff(onsets[sort_idx]) > eps)[0] + 1
    unique_onset_idxs = np.split(sort_idx, split_idx)

    if return_unique_onsets:
        # Instead of np.unique(onsets)
        unique_onsets = np.array([onsets[uix].mean()
                                  for uix in unique_onset_idxs])

        return unique_onset_idxs, unique_onsets
    else:
        return unique_onset_idxs


def segment_score_perf_times(matched_performance, uox):
    """
    Segment a list of pairs (score onset, performance onset), based on the
    euclidean distance between (chronological) subsequent pairs.

    Parameters
    ----------

    score_perf_onsets : ndarray
        ndarray of shape N x 2, conveying N pairs of score and performance
        onsets, respectively

    Returns
    -------

    sort_idx : ndarray
        Index that sorts `score_performance_onsets` correctly for segmentation

    breakpoints: list
        Indices of segment start elements in
        `score_performance_onsets[sort_idx]`. The index of the first segment
        (0) is not included.
    """

    assert len(matched_performance) > 0
    # matched_performance is sorted according to increasing score onsets;
    # within score onsets, y is sorted according to increasing performance onsets:
    # secondary sort: performance time
    i1 = np.argsort(matched_performance['perf_onset'])
    # primary sort: score time
    i2 = np.argsort(matched_performance['score_onset'][i1], kind='mergesort')
    sort_idx = i1[i2]
    matched_performance = matched_performance[sort_idx]

    # quantize score onsets to determine unique score onsets
    #if uox is None:
    #    uox = get_unique_onset_idxs((matched_performance['score_onset'] * 10000).astype(np.int))

    u_onset_score = np.array([np.mean(matched_performance['score_onset'][x]) for x in uox])
    u_onset_perf = np.array([np.mean(matched_performance['perf_onset'][x]) for x in uox])

    # version of y that contains unique score/perf onsets
    y_u = np.column_stack((u_onset_score, u_onset_perf))

    # rescale score and performance times to [0,1], to compute
    # euclidean distance
    y_u -= np.min(y_u, axis=0)
    y_u /= np.max(y_u, axis=0)

    diff_u = np.diff(y_u, axis=0)
    dist_u = np.sum(diff_u ** 2, axis=1) ** .5

    med_d = np.median(dist_u)
    max_d = np.max(dist_u)
    max_med_ratio = max_d / med_d

    # segment the performance if max distance between adjacent points is more
    # than max_ratio times larger than the median:
    max_ratio = 10
    do_segment = max_med_ratio > max_ratio

    # minimal size of a segment. If the segmentation yields segments smaller
    # than min_seg_length, no segmentation will be applied
    min_seg_length = 3

    if do_segment:
        breakpoints_u = find_segment_boundaries(dist_u)
        if np.any(np.diff(np.r_[0, breakpoints_u, len(dist_u)]) < min_seg_length):
            breakpoints = []
        else:
            breakpoints = np.searchsorted(matched_performance['score_onset'],
                                            u_onset_score[breakpoints_u], side='right')
    else:
        breakpoints = []

    return sort_idx, breakpoints


def find_segment_boundaries(dist, max_from_best=10):
    """
    Determine segment boundaries by segmenting using a greedy search
    (segmenting at the largest gaps first). The loss of the segmentation
    is measured by the ratio (max dist within segments) / (min dist at
    segment boundaries).

    Parameters
    ----------

    dist : ndarray
        Array of euclidean distance values

    max_from_best : int
        Stop the search if the best loss value was observed more than
        `max_from_best` candidates ago (and return the best segmentation
        up to that point)

    Returns
    -------

    ndarray
        Array of segmentation boundaries (start index of each segment,
        excluding 0)

    """
    sidx = np.argsort(dist)[::-1]
    bp = []
    best_loss = eval_segments(dist, bp)
    best_i = 0
    since_best = 0
    for i in range(len(sidx)):
        loss = eval_segments(dist, sorted(sidx[:i + 1]))
        if loss < best_loss:
            best_loss = loss
            best_i = i
        else:
            since_best += 1
        if since_best > max_from_best:
            break
    return sorted(sidx[:best_i + 1])


def eval_segments(dist, bp):
    """
    Compute the ratio (max dist within segments) / (min dist at segment
    boundaries). If there is only one segment, return the maximal within segment
    distance.

    Parameters
    ----------

    dist : ndarray
        Array of euclidean distance values

    bp : ndarray
        Array of segmentation boundaries (start index of each segment,
        excluding 0).

    Returns
    -------

    float
        Loss value (lower means better segmentation)
    """

    N = len(dist)
    idx = np.zeros(N, np.bool)
    idx[bp] = True
    if len(bp) == 0:
        within_dist = np.max(dist[~idx])
        return within_dist
    else:
        within_dist = np.max(dist[~idx])
        between_dist = np.min(dist[idx])
        return within_dist / between_dist







# from lbm.utils.match modified (remove sustain)
# This is for constructing dynamics
# this is to combine category of dynamics and tempo
def combine_dynamic_tempo_marking(d1 = 'data/cat_dynamic_marking_beethoven.p',
                                  d2 = 'data/cat_dynamic_marking_chopin.p',
                                  t1 = 'data/cat_tempo_marking_beethoven.p',
                                  t2 = 'data/cat_tempo_marking_chopin.p',
                                  d_combine = 'data/cat_dynamic_marking_bc.p',
                                  t_combine = 'data/cat_tempo_marking_bc.p'):
    def combine(pickle1, pickle2):
        l1, c1 = pickle.load(open(pickle1, "rb"))
        l2, c2 = pickle.load(open(pickle2,"rb"))
        l3 = []
        c3 = dict()
        for l in l1:
            if l not in l3:
                l3.append(l)
                c3[l] = len(c3)
        for l in l2:
            if l not in l3:
                l3.append(l)
                c3[l] = len(c3)
        return [l3,c3]

    pickle.dump(combine(d1, d2),open(d_combine, 'wb'))
    pickle.dump(combine(t1, t2),open(t_combine, 'wb'))
    print('combine marking to: ' + d_combine + ' and ' + t_combine)

