import numpy as np
import pickle
import to_class

key2num = {'000000000000': 6,
 '000000000222': 10,
 '000000222222': 7,
 '000022223333': 0,
 '111100000033': 0,
 '111111000000': 8,
 '111111000022': 0,
 '111111000222': 4,
 '111111002344': 0,
 '111111111111': 14,
 '111111111122': 11,
 '111111111222': 3,
 '111111112344': 0,
 '111111122222': 0,
 '111111222000': 0,
 '111111222200': 0,
 '111111222222': 2,
 '111111222233': 0,
 '111111222333': 5,
 '111111223344': 12,
 '111112222233': 0,
 '111122220033': 0,
 '111122223333': 1,
 '111122334444': 13,
 '111222000000': 0,
 '111222222000': 0,
 '111222333444': 0,
 '112222233333': 0,
 '112222333311': 0,
 '112222333344': 0,
 '112223344455': 0,
 '112233444444': 9,
 2.0: 15,
 3.0: 16}

valid_markings= ['piano','rit',
 'pp','a tempo',
 'ff', 'forte',
 'sotto voce',
 'sf', 'Fermata',
 'dolce', 'ten', 'mf']

class BeatFeatures:
    def __init__(self,score, Mname, numerical = False):
        self.score = score
        self.Mname = Mname
        self.numerical = numerical
        self.markings = pickle.load(open('Mazurkas/MazurkaBL/markings.p','rb'))
        self.NB = self.score.num_beats
        self.extract_features()
        self.one_hot()
        self.stack_features()

    def extract_features(self):
        self.bX = {}
        self.Fnames = ['max_pitch','min_pitch','num_notes','rhythm','accent','staccato',
                        'beat_phase','phrase_start','phrase_end','pitch_diff']
        self.Fnames += valid_markings
        for fname in self.Fnames:
            self.bX[fname] = np.zeros((self.NB,1))
        self.bX['num_notes_onehot'] = np.zeros((self.NB, 7))
        self.bX['rhythm_onehot'] = np.zeros((self.NB, 17))
        self.bX['beat_phase_onehot'] = np.zeros((self.NB, 3))
        self.bX['pitch_diff_onehot'] = np.zeros((self.NB, 27))

        self.score.X['pitch'] = (self.score.X['pitch']*127.-21)/87   # 21-108 as piano keys
        for b in range(self.NB):
            if b in self.score.beat2xmlidxs:
                idxs = self.score.beat2xmlidxs[b]
                pitches = self.score.X['pitch'][idxs]
                self.bX['max_pitch'][b] = pitches.max()
                self.bX['min_pitch'][b] = pitches.min()
                if b > 0:
                    self.bX['pitch_diff'][b] = self.bX['max_pitch'][b] - self.bX['max_pitch'][b-1]
                    _,self.bX['pitch_diff_onehot'][b] = to_class.convert_pitch_diff(np.round(self.bX['pitch_diff'][b] * 87))
                self.bX['num_notes'][b] = min(len(pitches), 7)
                self.bX['num_notes_onehot'][b][int(self.bX['num_notes'][b])-1] = 1
                self.bX['rhythm'][b] = self.beat2rhythm(b)[1]
                self.bX['rhythm_onehot'][b][int(self.bX['rhythm'][b])] = 1
                self.bX['beat_phase'][b] = self.score.X['beatphase'][idxs][0]
                self.bX['beat_phase_onehot'][b][int(self.bX['beat_phase'][b])] = 1
                # add note markings
                accents   = sum(self.score.X['accent'][idxs])[0]
                staccatos = sum(self.score.X['staccato'][idxs])[0]
                if accents > 0:
                    self.bX['accent'][b] = 1
                if staccatos > 0:
                    self.bX['staccato'][b] = 1
                # add dynamic markings
                if self.Mname in self.markings:
                    for mark_name in self.markings[self.Mname].keys():
                        for mark_b in self.markings[self.Mname][mark_name]:
                            self.bX[mark_name][mark_b] = 1
            else:
                for fname in self.Fnames:
                    if fname == 'beat_phase':
                        self.bX[fname][b] = np.mod(self.bX[fname][b-1] + 1, 3.0)
                    elif fname == 'phrase_start' or fname == 'phrase_end':
                        pass
                    else:
                        self.bX[fname][b] = self.bX[fname][b-1]

        # adding phase in the phrase
        for my_range in self.score.onbeat_phraserange:
           self.bX['phrase_start'][my_range[0]] = 1
           self.bX['phrase_end'][my_range[-1]] = 1


    def one_hot(self):
        # numerical vs. one_hot encoding
        if self.numerical:
            self.bX['num_notes'] = (self.bX['num_notes'] - 1) / 6.
            self.bX['rhythm'] /=  14.
            self.bX['beat_phase'] /= 2.0
            self.train_features = ['max_pitch','min_pitch','num_notes','rhythm','accent','staccato',
                                    'beat_phase','phrase_start','phrase_end','pitch_diff']
        else:
            self.train_features = ['max_pitch','min_pitch','num_notes_onehot','rhythm_onehot','accent','staccato',
                                    'beat_phase_onehot','phrase_start','phrase_end','pitch_diff_onehot']

    def stack_features(self):
        self.beat_features = np.zeros((self.NB, 0))
        for fname in self.train_features:
            data = self.bX[fname]
            self.beat_features = np.hstack((self.beat_features, data))
        self.beat_features_basics = self.beat_features[:,:6]

    def beat2rhythm(self, beat):
        """
        Convert each beat to a 12-d number (string)
        """
        onsets = self.score.beat2onsets[beat]
        the_idxs = []
        for o in onsets:
            xmlidxs = self.score.unique_onset_idxs[o]
            the_idx = xmlidxs[np.argmin([self.score.X['duration'][xmlidx] for xmlidx in xmlidxs])]
            the_idxs.append(the_idx)
        # if only one onset, and it's larger than a quarter note
        if len(the_idxs) == 1 and self.score.X['duration'][the_idxs[0]] > 1:
            numkey = self.score.X['duration'][the_idxs[0]][0]
            if numkey in key2num:
                return numkey, key2num[numkey]
            else:
                return numkey, -1

        number = np.zeros((12))  # 把一拍分成十二份
        n = 1
        beat_start_time = self.score.note_onset_beat[the_idxs[0]]
        for idx in the_idxs:
            note_start = self.score.note_onset_beat[idx]-beat_start_time
            note_end = self.score.note_offset_beat[idx]-beat_start_time
            number[int(round(note_start*12)): int(round(note_end*12))] = n
            n += 1
        numkey = ''.join([str(int(n)) for n in number])
        if numkey in key2num:
            return numkey, key2num[numkey]

        return numkey, 0