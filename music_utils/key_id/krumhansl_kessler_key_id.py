#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Key Identification algorithm based on tonal hierarchies by
Krumhansl and Kessler
"""
import numpy as np
from scipy.stats.stats import pearsonr
from key_profiles import KEYS,build_key_profile_matrix
from utils import pitch_class_profile

__author__ = ["Carlos Eduardo Cancino Chac\'on"]
__email__ = "carlos.cancino@ofai.at"
__version__ = "11503.31"

Key_prof_mat = build_key_profile_matrix(profile='kk')

def kk_kid(song_profile, Key_prof_mat = Key_prof_mat):
    """
    Krumhansl and Kessler's key finding algorithm

    Adapted form

    Krumhansl, C. Cognitive Foundations of Musical Pitch.
    Oxford University Press, 1990, pp. 79.
    """
    
    # Initialize list of correlation coefficients
    corr = []

    for key in Key_prof_mat:
        # Compute Pearson's correlation for each key profile
        corr.append(
            pearsonr(song_profile,key)[0])

    # Return the key with the highest correlation

    return KEYS[np.argmax(corr)]

if __name__=='__main__':

    # Trivial example
    
    onsets = np.arange(70)
    offsets = 1 + np.arange(70)
    notes = 5+np.array([60,62,63,65,67,68,71]*10)

    song_profile = pitch_class_profile(notes, offsets-onsets)

    key = kk_kid(song_profile,Key_prof_mat)

    print 'Notes'
    print notes
    print 'key'
    print key 
