#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Bayesian Polyphonic Key Identification by
David Temperley
"""

__author__ = ["Carlos Eduardo Cancino Chac\'on"]
__email__ = "carlos.cancino@ofai.at"
__version__ = "11505.27"


import numpy as np
from functools import partial
import logging
import itertools

LOGGER = logging.getLogger(__name__)

#From mptoolbox
from key_profiles import build_key_profile_matrix,KEYS
from utils import pitch_class_set,equal_time_segment

# Default matrix of key profiles (Kostka-Payne)
Key_prof_mat_kp = build_key_profile_matrix(profile = 'kp')

    
def _compute_emission_probability(x, Key_prof_mat = Key_prof_mat_kp):
    """
    Emission probability
    """
    x = np.array(x)
    P_seg_given_key = []
    for K_p in Key_prof_mat:
        p_key = np.prod(
            (K_p**x)*(1-K_p)**(1-x))

        P_seg_given_key.append(p_key)
    
    return np.array(P_seg_given_key)

def _compute_emission_probabilities(Key_prof_mat = Key_prof_mat_kp):
    """
    Matrix of emission probabilities
    """
    X = list(itertools.product([0, 1], repeat=12))
    X_dict = dict(zip(X,range(len(X))))
    B = []
    for ii in range(len(X)):
       B.append(_compute_emission_probability(X[ii]))

    B = np.array(B)

    return B.T,X_dict


def _compute_transition_probabilities(inertia_param = 0.8):
    """
    Matrix of transition probabilities
    """
    modulation_prob = (1 - inertia_param)/23.
    A = (modulation_prob*(np.ones(24)-np.eye(24))
         + inertia_param*np.eye(24))

    return A
    
def _build_pi_init(key = None):
    """
    Vector of initial probabilities
    """
    if key is None:
        pi_init = 1./24.*np.ones(24,dtype = np.float)
    else:
        pi_init = np.zeros(24, dtype = np.float)
        pi_init[KEYS.index(key)] = 1.
        
    return pi_init

def _compute_pc_segment_list(segment_list):
    """
    Compute the pitch class set of each segment in a list
    """
    return [pitch_class_set(segment[0]) for segment in segment_list]

def _hmm_model(transition_prob_matrix, pi_init, Key_prof_mat):
    """
    Build HMM model
    """
    if Key_prof_mat is None:
        Key_prof_mat = Key_prof_mat_kp

    sigma = ghmm.IntegerRange(0,2**12)
    A = transition_prob_matrix
    pi = pi_init
    B,X_dict = _compute_emission_probabilities(Key_prof_mat)
    
    model = ghmm.HMMFromMatrices(sigma, ghmm.DiscreteDistribution(sigma), A, B, pi)
    
    return (model,X_dict,sigma)

    
def bayesian_kid(segment_list, key = None,
                 Key_prof_mat = None,
                 pi_init = None,
                 A = None,
                 inertia_param = 0.9):
    """
    Temperley's polyphonic key finding algorithm

    Adapted from

    Temperley, D. Music and Probability.
    MIT Press, 2007, pp. 79-98.
    """
    try:
        import ghmm
    except ImportError as e:
        LOGGER.error('the function bayesian_kid cannot be used, because it '
                     'relies on the ghmm module, which cannot be found')
        raise e

    # Compute vector of initial probabilities
    if pi_init is None:    
        pi_init = _build_pi_init(key)

    # Compute matrix of transition probabilities
    if A is None:
        A = _compute_transition_probabilities(inertia_param)

    # Build the list of pitch class sets for each segment
    pc_segment_list = _compute_pc_segment_list(segment_list)

    # Build the model
    model,X_dict,sigma = _hmm_model(A,pi_init,Key_prof_mat)

    # Build sequence of observations (into ghmms internal format)
    seq = obs_to_seq(pc_segment_list,X_dict)
    # Compute viterbi path
    v = model.viterbi(ghmm.EmissionSequence(sigma,seq))
 
    viterbi_path = seq_to_key(v[0])
    
    return viterbi_path

def temperley_kid(pc_set,
                  Key_prof_mat = Key_prof_mat_kp):
    """
    Probabilistic key finding algorithm
    """
    em_probability = _compute_emission_probability(pc_set,
                                                   Key_prof_mat)
    return KEYS[np.argmax(em_probability)]
    
def obs_to_seq(obs_list,X_dict):
    return [X_dict[tuple(ii)] for ii in obs_list]

def seq_to_key(seq_list):
    return [KEYS[ii] for ii in seq_list]

if __name__ == '__main__':

    # Trivial example
    onsets = np.arange(707)
    offsets =1 + np.arange(707)
    notes = 6+np.array([60,62,63,65,67,68,71]*101)
                            
    seg_list = equal_time_segment(notes,
                                  onsets,
                                  offsets,
                                  segment_length = 100)
    pc_set = pitch_class_set(notes)
    seq_list=_compute_pc_segment_list(seg_list)
    viterbi_path = bayesian_kid(seg_list)
    
    key = temperley_kid(pc_set)
    
    for segment in seg_list:
        print 'segment_duration:{0}'.format(np.sum(segment[2]-segment[1]))
    
    
    print 'Most probable path:'
    print viterbi_path
    print 'Key'
    print key
