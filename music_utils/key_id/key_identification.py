import numpy as np

from bayesian_key_id import bayesian_kid,temperley_kid
from krumhansl_kessler_key_id import kk_kid
from utils import pitch_class_set,pitch_class_profile
from key_profiles import SCALE_DEGREES,SCALE_DEGREE_DICT,KEYS

# def gen_key_list():
#     keys_M = ['C', 'G', 'D', 'A','E','B','F#','C#','G#','D#','A#','F']
#     keys_m = ['a', 'e', 'b', 'f#', 'c#', 'g#', 'd#', 'a#', 'f', 'c', 'g', 'd']
#     KEYS = [ky for kk in zip(keys_M,keys_m) for ky in kk]
#     KEYS_o = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B",
#         "c","c#","d","d#","e","f","f#","g","g#","a","a#","b"]
#     return KEYS,keys_M,keys_m,KEYS_o

# KEYS,keys_M,keys_m,KEYS_o = gen_key_list()

keys_M = ['C', 'G', 'D', 'A','E','B','F#','C#','G#','D#','A#','F']
keys_m = ['a', 'e', 'b', 'f#', 'c#', 'g#', 'd#', 'a#', 'f', 'c', 'g', 'd']

def key_identification(segments,key_info,method = 'temperley'):
    key_seq = []
    pc_segment_list = []
    for segment in segments:
        if method in ['temperley','bayesian']:
            pc_segment_list.append(pitch_class_set(segment[:,0]))
        elif method == 'krumhansl':
            pc_segment_list.append(pitch_class_profile(
                segment[:,0],abs(segment[:,2]-segment[:,1])))
    
    if method in ['temperley','krumhansl']:
        key_seq = []
        
        for segment in pc_segment_list:
            if method == 'temperley':
                key_seq.append(temperley_kid(segment))
            elif method == 'krumhansl':
                key_seq.append(kk_kid(segment))
                
    elif method == 'bayesian':
        
        key_seq = bayesian_kid(segment_list = pc_segment_list,
                               key = key_info[0][0],
                               inertia_param = 0.99)
    return key_seq


def key_to_scaledegree(key_segment,key):
    return SCALE_DEGREE_DICT[key][key_segment]

def fifths_to_key(fifths,mode,data):
    if mode is None:
        # print('Estimating mode...')
        profile = pitch_class_profile(data[:,0],abs(data[:,2]-data[:,1]))
        ky_est = kk_kid(profile)
        
        if ky_est[0].istitle():
            mode = 'major'
        else:
            mode = 'minor'
        # print('Computed mode is {0}'.format(mode))
            
    if mode == 'minor':
        kp = keys_m
    elif mode == 'major':
        kp = keys_M
    return kp[fifths]
