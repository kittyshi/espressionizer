#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Assorted utilities for processing arrays of pieces to be
used for the key identification algorithms
"""
import numpy as np

__author__ = ["Carlos Eduardo Cancino Chac\'on"]
__email__ = "carlos.cancino@ofai.at"
__version__ = "11503.31"


def _to_pitch_classes(note_array):
    """
    Transforms an array of MIDI notes into an array of pitch classes
    """  
    return (np.mod(note_array.astype(np.int),12)).astype(np.int)


def pitch_class_set(note_array):
    """
    Transform an array of notes (encoded as midi pitches) into a
    set of pitch classes
    """
    # Array of pitch classes
    pc_array = _to_pitch_classes(note_array)
    # Array that the unique pitch classes appearing in the note_array
    pc_array_unique = np.unique(pc_array)
    # Set of pitch classes as a 12-D vector
    return np.bincount(pc_array_unique,minlength=12)

def pitch_class_profile(note_array,duration_array = None):
    """
    Transforms an array of notes (encoded as midi pitches) into an
    histogram of the pitch classes present in the array
    """

    # Array of pitch classes
    pc_array = _to_pitch_classes(note_array)
    
    if duration_array is None:
        # Array of occurences of each one of the pitch classes
        return np.bincount(pc_array, minlength = 12)
    else:
        profile = []

        for ii in range(12):
            # accumulate the duration for each pitch class
            profile.append(
                np.sum(duration_array[np.where(pc_array == ii)]))
            
        # Array of duration of each one of the pitch classes
        return np.array(profile)


def equal_time_segment(note_array,onsets,offsets,
                       n_segments = None, segment_length = 100):
    """
    Generate a list of equal time segments of a piece
    """
      
    segment_list = []
    seg_boundaries = []
    # Define length of the segment (the n_segments takes priority)
    if n_segments is not None:
        segment_length = offsets[-1] // n_segments

    # Initialize count and stopping conditions
    ii = 0
    end_list = False
    
    while end_list is False:

        if (ii+1)*segment_length < onsets[-1]:

            curr_seg_idx = np.where(np.logical_and((onsets >= ii*segment_length),
                                                   (onsets <= (ii + 1)*segment_length)))
            seg_boundaries.append((ii*segment_length, (ii + 1)*segment_length))

            ii +=1

        else:
            end_list = True
            curr_seg_idx = np.where(np.logical_and((onsets >= ii*segment_length),
                                                   (onsets <= onsets[-1])))
            seg_boundaries.append((ii*segment_length, offsets[-1]))
            
        segment_list.append(
            (note_array[curr_seg_idx],onsets[curr_seg_idx],offsets[curr_seg_idx])
             )
    #print seg_boundaries
    return segment_list



if __name__ == '__main__':

    # Trivial test

    notes = np.random.randint(60,84, size=(100,))
    duration = np.random.randint(0,10, size = (100,))
    onsets = np.cumsum(duration)
    print 'Array of notes:'
    print notes

    print 'Array of durations'
    print duration

    print 'Array of pitch classes'
    print _to_pitch_classes(notes)

    print 'Set of pitch classes'
    print pitch_class_set(notes)
    
    print 'Histogram of occurence of pitch classes'
    print pitch_class_profile(notes)

    print 'Histogram of duration of pitch classes'
    print pitch_class_profile(notes,duration)
    
    seg_list =  equal_time_segment(notes,onsets,onsets+duration,segment_length=21)

    for segment in seg_list:
        print 'segment_duration:{0}'.format(np.sum(segment[2]-segment[1]))
