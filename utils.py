import numpy as np
import scipy.signal as signal
from scipy.interpolate import interp1d

import numpy as np

def set_sustain(match):
    """given a list of "pedal" lines in the match files, set the offsets
    of performed notes in the match files to the sounding offsets

    """
    notes = [l.note for l in match.lines if hasattr(l, 'note')]
    offs = np.array([n.Offset for n in notes])

    first_off = np.min(offs)
    last_off = np.max(offs)

    pedal = np.array([(x.Time, x.Value > 63) for x in match.sustain_lines])
    # sort, just in case
    pedal = pedal[np.argsort(pedal[:,0]), :]
    # reduce the pedal info to just the times where there is a change in pedal state
    pedal = np.vstack(((min(pedal[0,0] - 1, first_off - 1), 0), # if there is an onset before the first pedal info, assume pedal is off
                       pedal[0,:],
                       pedal[np.where(np.diff(pedal[:, 1]) != 0)[0] + 1, :],
                       (max(pedal[-1,0] + 1, last_off + 1), 0) # if there is an offset after the last pedal info, assume pedal is off
                   ))
    #print(pedal)
    last_pedal_change_before_off = np.searchsorted(pedal[:,0], offs) - 1

    pedal_state_at_off = pedal[last_pedal_change_before_off, 1]
    pedal_down_at_off = pedal_state_at_off == 1
    next_pedal_time = pedal[last_pedal_change_before_off+1, 0]

    # np.savetxt('/tmp/onoff.txt', np.column_stack((np.arange(onoffs.shape[0]), onoffs)))
    offs[pedal_down_at_off] = next_pedal_time[pedal_down_at_off]
    # np.savetxt('/tmp/onoff1.txt', np.column_stack((np.arange(onoffs.shape[0]), onoffs)))

    for offset, note in zip(offs, notes):
        note.Offset = offset




def rescale_tempo(ioi_ratio, mean_beat_period):
    return 2**ioi_ratio * mean_beat_period
    #return tempo_param


### This is used by interpolation in get_perf_feature
### to fix a problem with some of the match files
def compute_interpolation_weights(u_onset_score):
    diff_u_onset_score = np.diff(u_onset_score)
    ioi_median = np.median(diff_u_onset_score)

    interp_weights = np.minimum(ioi_median,
                                diff_u_onset_score) / ioi_median
    interp_weights = (np.r_[interp_weights[0], interp_weights] +
                      np.r_[interp_weights, interp_weights[-1]]) / 2.

    return interp_weights


def interpolate_feature(idx, feature, defVal=0.0):
    assert feature.shape[0] > 0
    assert len(idx) > 0
    min_val = np.min(idx)
    max_val = np.max(idx)
    if feature[0, 0] > min_val:
        feature = np.vstack((np.array([[min_val, defVal]]), feature))
    if feature[-1, 0] < max_val:
        feature = np.vstack((feature, np.array([[max_val, defVal]])))

    f = interp1d(feature[:, 0], feature[:, 1])
    return f(idx)

def soft_normalize(v, preserve_unity=False):
    """
    Rescale v to the interval (-1, 1) using tanh (if v is
    non-negative, the target interval will be [0, 1)). If
    `preserve_unity` is True, the following will hold: np.where(v ==
    1) == np.where(soft_normalize(v, True) == 1). Which means that the
    target interval is (-1/np.tanh(1), 1/np.tanh(1)).

    """
    if preserve_unity:
        return np.tanh(v) / np.tanh(1)
    else:
        return np.tanh(v)


def smooth(x,k):
    """
    Convolve `x` with a Hamming window of length `k`.

    :param x: signal to be convolved
    :param k: size of Hamming window in samples

    :returns: convolved signal with same length as x
    """

    w = signal.hamming(k)
    w /= np.sum(w)
    return np.convolve(x,w,'same')

def normalize(v):
    """
    Rescale v to the range [0, 1]. If v is a 2d array, each column is rescaled to [0, 1].

    """
    vmin = np.min(v, 0)
    vmax = np.max(v, 0)
    if np.isscalar(vmin):
        if vmax > vmin:
            v = (v - vmin) / (vmax - vmin)
    else:
        for i in range(len(vmin)):
            if vmax[i] > vmin[i]:
                v[:, i] = (v[:, i] - vmin[i]) / (vmax[i] - vmin[i])
    return v