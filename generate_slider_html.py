TEMPLATE = """document.getElementById('%s_v').innerHTML = document.getElementById('%s').value;
document.getElementById('%s').oninput = function() {
    document.getElementById('%s_v').innerHTML = this.value;
}
"""

slider_name_map = { # copy from output_type above
                    # keep in sync with 'chart_slider_dict' in _charts.js
    # 'vtrend': 0,
    # 'v4_o_pitch_max': 4,       # value is the INDEX of the feature, NOT the VALUE
    # 'v40_calando': 40,
    # 'v37_f': 37,
    'onset_pitch_max': 50,
    'Downbeat': 50,
    #'Forzando': 50,
    'Slur': 101,

    # 'ibi': 2,
    # 'b30_fermata': 30,
    # 'b49_fz': 49,
    # 'b70_marcato': 70,
    'IOI_prev3': 50,
    'IOI_next3': 50,
    'Beatphase': 50,
}


for k in slider_name_map:
    print TEMPLATE % (k, k, k, k)