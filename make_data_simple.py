import to_class
import pickle
import os
import numpy as np
import random
from config import BINS

def data_for_train(data_pkl, seq_len = 64, slidingwindow = True):
    """
    Slice data and prepare for train
    data_pkl: a pikle file
    """
    data = pickle.load(open(data_pkl, 'rb'))
    Y_train = {}
    #names = ['vel_onset','vel_diff_onset','bpm_onset_norm','bpm_onset_ratio','log_articulation','sus_pedal','soft_pedal']
    names = ['vel_onset','vel_diff_onset','bpm_beat_norm','bpm_individual_norm','log_articulation','sus_pedal','soft_pedal']
    X_train = slice_data(data['X_train'], seq_len, slidingwindow)
    for i in range(len(names)):
        #Y_train[i] = slice_data(data['Y_train'][names[i]], seq_len, slidingwindow)
        Y_train[i] = slice_data(data['Y_train_raw']['note']['data'][names[i]], seq_len, slidingwindow)


    X_val = slice_data(data['X_val'], seq_len, slidingwindow)
    Y_val = {}
    for i in range(len(names)):
        #Y_val[i] = slice_data(data['Y_val'][names[i]], seq_len, slidingwindow)
        Y_val[i] = slice_data(data['Y_val_raw']['note']['data'][names[i]], seq_len, slidingwindow)


    d = {"X_train": X_train, "X_val": X_val, "Y_train": Y_train, "Y_val": Y_val}
    return d


def slice_data(Y, seq_len = 64, slidingwindow = True):
    """
    slice data into blocks of sequences using sliding window
    """

    if slidingwindow:
        num_input = Y.shape[0] - seq_len + 1
        Yout = np.zeros((num_input, seq_len, Y.shape[1]))
        for i in range(num_input-1):
            Yout[i] = Y[i:(i+seq_len),:]
    else:
        num_input = Y.shape[0] / seq_len + 1
        pad_amount = num_input * seq_len - Y.shape[0]
        Yout = np.pad(Y, ((0,pad_amount), (0, 0)), 'constant').reshape((num_input,seq_len,Y.shape[1]))
    return Yout

def test(pkl_dir = 'all_data/pkl/1201/', store = False, outname = None):
    """
    Given a directory of pickle file, stack and extract information for analysis
    """

    all_files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]

    X_train,Y_train_raw = get_datas(all_files)
    return X_train, Y_train_raw

def retrieve_data(pkl_dir = 'all_data/pkl/1205/', store = True, outname = None):
    """
    Given a directory of pickle file, stack and extract information for analysis
    """

    all_files = [pkl_dir + n for n in os.listdir(pkl_dir) if n.endswith('.p')]
    train_file = []
    valid_file = []
    for fn in random.sample(all_files, int(len(all_files) * 0.9)):
        train_file.append(fn)

    for fn in all_files:
        if fn not in train_file:
            valid_file.append(fn)

    X_train,Y_train_raw = get_datas_simple(train_file)
    X_val, Y_val_raw = get_datas_simple(valid_file)

    Y_train_onehot = convert_Y_onehot(Y_train_raw)
    Y_val_onehot = convert_Y_onehot(Y_val_raw)
    data = {"X_train": X_train, "Y_train_raw": Y_train_raw,
            "Y_train": Y_train_onehot,
            "X_val": X_val, "Y_val_raw": Y_val_raw,"Y_val": Y_val_onehot}
    if store:
        pickle.dump(data, open(outname, 'wb'))
    return data

def get_datas_simple(files):
    pk = pickle.load(open(files[0], 'rb'))
    x = pk['X']
    Y = {}
    y = {}

    for fname in pk['Yname']:
        if fname not in ['scale_beat_period_mean']:
            y[fname] = pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))
        else:
            y[fname] = [pk['Y'][fname]]

    for fn in files[1:]:
        pk = pickle.load(open(fn, 'rb'))
        x = np.vstack((x, pk['X']))
        for fname in pk['Yname']:
            if fname not in ['scale_beat_period_mean']:
                y[fname] = np.vstack((y[fname], pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))))
            else:
                y[fname].append(pk['Y'][fname])


    M =  np.percentile(y['beat_period_onbeat'], 99)
    y['beat_period_onbeat'][y['beat_period_onbeat'] > M ]= M
    y['beat_period_onbeat'][y['beat_period_onbeat'] < 0 ] = 0

    M2 =  np.percentile(y['beat_period_ratio'], 99)
    y['beat_period_ratio'][y['beat_period_ratio'] > M2 ]= M2
    y['beat_period_ratio'][y['beat_period_ratio'] < 0 ] = 0

    Y['note']    = {"name": pk['Yname'], "data": y}

    return x, Y



def get_datas(files):
    pk = pickle.load(open(files[0], 'rb'))
    x = pk['X']
    Y = {}
    y = {}
    yonset = {}
    ymeasure = {}
    yslur = {}

    for fname in pk['Yname']:
        if fname not in ['scale_bpm_onset_mean','scale_bpm_measure_mean']:
            y[fname] = pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))
        else:
            y[fname] = [pk['Y'][fname]]

    for fname in pk['Ybigname']:
        if fname not in ['scale_bpm_mean']:
            yonset[fname] = pk['Yonset'][fname].reshape((len(pk['Yonset'][fname]), 1))
            ymeasure[fname] = pk['Ymeasure'][fname].reshape((len(pk['Ymeasure'][fname]), 1))
        else:
            yonset[fname] = [pk['Yonset'][fname]]
            ymeasure[fname] = [pk['Ymeasure'][fname]]
    for fname in pk['Yslurname']:
        yslur[fname] = [pk['Yslur'][fname]]

    for fn in files[1:]:
        pk = pickle.load(open(fn, 'rb'))
        x = np.vstack((x, pk['X']))
        for fname in pk['Yname']:
            if fname not in ['scale_bpm_onset_mean','scale_bpm_measure_mean']:
                y[fname] = np.vstack((y[fname], pk['Y'][fname].reshape((len(pk['Y'][fname]), 1))))
            else:
                y[fname].append(pk['Y'][fname])
        for fname in pk['Ybigname']:
            if fname not in ['scale_bpm_mean','scale_bpm_mean']:
                yonset[fname] = np.vstack((yonset[fname], pk['Yonset'][fname].reshape((len(pk['Yonset'][fname]), 1))))
                ymeasure[fname] = np.vstack((ymeasure[fname], pk['Ymeasure'][fname].reshape((len(pk['Ymeasure'][fname]), 1))))
            else:
                yonset[fname].append(pk['Yonset'][fname])
                ymeasure[fname].append(pk['Ymeasure'][fname])
        for fname in pk['Yslurname']:
            yslur[fname] += pk['Yslur'][fname]

    M =  np.percentile(y['bpm_beat_norm'], 99)
    y['bpm_beat_norm'][y['bpm_beat_norm'] > M ]= M
    y['bpm_beat_norm'][y['bpm_beat_norm'] < 0 ] = 0

    M2 =  np.percentile(y['bpm_individual_norm'], 99)
    y['bpm_individual_norm'][y['bpm_individual_norm'] > M2 ]= M2
    y['bpm_individual_norm'][y['bpm_individual_norm'] < 0 ] = 0

    Y['note']    = {"name": pk['Yname'], "data": y}
    Y['onset']   = {"name": pk['Ybigname'], "data": yonset}
    Y['measure'] = {"name": pk['Ybigname'], "data": ymeasure}
    Y['slur'] = {"name": pk['Yslurname'], "data": yslur}

    return x, Y

def convert_Y_onehot(Y):
    # Ytrainname = ['vel_onset','vel_diff_onset','bpm_beat_norm','vel_onsetincrease',
    #             'bpm_individual_norm','log_articulation']
    Ytrainname = ['vel_onsetmax','vel_maxdiff','beat_period_onbeat',
                  'beat_period_ratio','log_articulation']
    Y_onehot = {}
    digits = None
    for name in Ytrainname:
        category_data = Y['note']['data'][name]
        digits = to_class.convert_category(category_data, BINS[name])
        Y_onehot[name] = to_class.convert_onehot(digits, len(BINS[name]) + 1)

    Y_onehot['sus_pedal'] = Y['note']['data']['sus_pedal']
    Y_onehot['soft_pedal'] = Y['note']['data']['soft_pedal']

    return Y_onehot
