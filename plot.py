import os
import sys
import matplotlib.pyplot as plt
from collections import defaultdict

usage = """
Usage: Generate a collage of 7 graphs showing model training/test loss; r^2 and loss in the title.

    python ../plot.py 20190523_aug_beethoven_50seqlen_100epoch [--silent]

"""

model_map = {
    'vtrend': 0,
    'vdev': 1,
    'ibi': 2,
    'time': 3,
    'art': 4,
    'sus_pedal': 5,
    'soft_pedal': 6,
    'ib_org': 7,
    'ti_org': 8,
    'areal': 9,
}

def get_idx_title(filename):
    # generate title str
    idx = -1
    titlestr = filename.replace(prefix, '').replace('.txt','')
    for k, v in model_map.items():
        if k in titlestr:
            titlestr = titlestr[titlestr.find(k):]
            titlestr = '(%d) %s' % (v, titlestr)
            idx = v
            break
    return idx, titlestr

def plot_one_file(filename):
    with open(filename) as fin:
        hc = defaultdict(list)
        last_val_loss = -1
        last_val_coeff_determination = -1
        for a in fin:
            if not a.startswith('loss'):
                continue
            tmp = dict(zip(
                ['loss', 'coeff_determination', 'val_loss', 'val_coeff_determination'],
                [float(b.split(':')[1]) for b in a.split('- ')]))
            hc['loss'].append(tmp['loss'])
            hc['val_loss'].append(tmp['val_loss'])
            hc['coeff_determination'].append(tmp['coeff_determination'])
            hc['val_coeff_determination'].append(tmp['val_coeff_determination'])
            last_val_loss = tmp['val_loss']
            last_val_coeff_determination = tmp['val_coeff_determination']

        # summarize history for accuracy
        plt.plot(hc['loss'])
        plt.plot(hc['val_loss'])
        # plt.plot(hc['coeff_determination'])
        # plt.plot(hc['val_coeff_determination'])
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper right')
        # plt.show()

        return float(last_val_loss), float(last_val_coeff_determination)


if __name__ == '__main__':

    if len(sys.argv) <2:
        print usage
        sys.exit()

    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    txtfiles = []
    prefix = sys.argv[1]

    for f in files:
        if f.startswith(prefix) and f.endswith('txt'):
            txtfiles.append(f)

    txtfiles = sorted(txtfiles)
    if len(txtfiles) > 0:
        fig = plt.figure(figsize=(16, 10))
        # subplot config nr * nc
        nr = 2
        nc = 5 #if len(txtfiles) >= 6 else 3

        for i in range(len(txtfiles)):
            idx, titlestr = get_idx_title(txtfiles[i])
            plt.subplot(nr, nc, idx+1 if idx > -1 else i+1)
            loss, r2 = plot_one_file(txtfiles[i])
            plt.title('%s [r2=%.2f, ls=%.2f]' % (titlestr, r2, loss))

        plt.suptitle(prefix, size=14)
        # plt.tight_layout()
        # plt.show()
        plt.savefig(prefix+'.png')
        if len(sys.argv) == 2:
            os.system('open "' + prefix + '.png"')

        os.system('tail -1 '+ prefix + '*.txt')

        # print 'Making a zip:'
        # os.system('zip %s.zip %s*' % (prefix, prefix))

        print model_map
