echo "seq_len	units	dropout	lr	bn	r2	loss	fn"
cat ../_param | xargs -n 1 -I {} ../analyze_massive.sh {} 2>&1 | tee best_models.txt

echo "----- Best models -----"
cat best_models.txt  | grep -v \* | cut -f8
