import numpy as np
import utils
import pickle
import to_class
from fractions import gcd
from scipy.interpolate import interp1d
from ofaidata_utils.data_handling.scoreontology import (TimeSignature, Measure, Note, Slur,
                                                        ConstantLoudnessDirection,
                                                        DynamicLoudnessDirection,
                                                        ImpulsiveLoudnessDirection,
                                                        ConstantTempoDirection,
                                                        DynamicTempoDirection,
                                                        ResetTempoDirection)


class ScoreFeature:
    def __init__(self, part, style = 0):
        self.part = part
        self.cat_names = ['Basics','Metrical',#'IOI',
                            'Rest','Notemark','Dynamics', 'Tempo', 'Slur']
        self.feature_dim = {}
        self.total_dim = 0
        self.xml_notes = np.array(part.notes)
        self.bm = part.beat_map
        self.bins = { "dur": [0.5, 0.25, 1.0],
                      "pitch": [63, 54, 73, 48, 59, 68, 79],   # convert from [ 48.  54.  59.  63.  68.  73.  79. ]
                      "slur": [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875],   # convert from in-direction [0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875]
                      "beatphase": [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875] }
        self.train_features = []
        self.raw_features = []
        self.extract_features()
        self.train_features += ['measures']
        self.score_features = np.zeros((self.N,0))

        self.score_features_raw = np.zeros((self.N,0))

        for fname in self.train_features:
           self.feature_dim[fname] = self.X[fname].shape[1]
           self.total_dim += self.X[fname].shape[1]
           self.score_features = np.hstack((self.score_features, self.X[fname]))

        for fname in self.raw_features:
            data = self.X[fname]
            if fname == "pitch":
                data = data/ 127.
            if fname == "pitch_class":
                data = data / 11.
            if fname == "octave":
                data = data / 7.
            if fname == "voice":
                data = data / 10.
            if fname == "minmax_pitch":
                data = data / 3.
            if fname == "pitch_onsetmax" or fname == "pitch_windowmax":
                data = data / 8.
            if fname == "dur_beat_raw":
                data = data / 8.
            self.score_features_raw = np.hstack((self.score_features_raw, data))

        self.extract_measuremean()

    def extract_measuremean(self):
        self.Xmeasure = {}
        for fname in self.raw_features:
            self.Xmeasure[fname] = np.zeros((self.num_measures,self.X[fname].shape[1]))

        self.Xmeasure_features_raw = np.zeros((self.num_measures,0))
        for measure in range(self.num_measures):
            note_idxs = self.dict_measure2noteidx[measure]
            for fname in self.raw_features:
                self.Xmeasure[fname][measure] = self.X[fname][note_idxs].mean()

        self.Xmeasure_feature_raw = self.Xmeasure['pitch'].copy()
        for fname in self.raw_features:
            if fname != 'pitch':
                self.Xmeasure_feature_raw = np.hstack((self.Xmeasure_feature_raw,self.Xmeasure[fname]))

    def extract_features(self):
        self.X = {}
        self.Xname = {}
        self.Xnameraw = {}
        self.Xonset = {}
        self.N = len(self.xml_notes)
        self.eps = 10**-6
        self.note_info = np.array([(n.midi_pitch, n.octave, n.voice, n.start.t, n.end.t) for n in self.xml_notes],
                            dtype=[('pitch', 'i4'), ('octave', 'i4'), ('voice','i4'),('onset', 'f4'),('offset', 'f4')])
        self.note_dur = [self.bm(n.end.t) - self.bm(n.start.t) for n in self.xml_notes]
        self.note_onset_beat = self.bm(self.note_info['onset'])
        self.note_offset_beat = self.bm(self.note_info['offset'])
        self.list_dynamic_marking, self.cat_dynamic_marking = pickle.load(open('all_data/cat_dynamic_marking.p', "rb"))
        self.list_tempo_marking, self.cat_tempo_marking = pickle.load(open('all_data/cat_tempo_marking.p', "rb"))

        self.X['measures'] = np.zeros((self.N, 1))

        for name in self.cat_names:
            self.extract_feature(name)


    def extract_feature(self, feature_name):
        if feature_name == "Basics":
            self.extract_basics()
            self.Xname['Basics'] = ['pitch_onehot','pitch_class_onehot','pitch_huffman_onehot','octave_onehot','voice_onehot',
                                'minmax_pitch_onehot','pitch_onsetmax_diff_onehot', 'pitch_windowmax_diff_onehot',
                                'chroma12','dur_huffman_onehot','dur_beat_twohot']
            self.Xnameraw['Basics'] = ['pitch','pitch_class','octave','voice','minmax_pitch','pitch_onsetmax',
                                        'pitch_windowmax','chroma12','dur_beat_raw','IOI_prev_onset','IOI_onset']
            self.train_features += self.Xname['Basics']
            self.raw_features += self.Xnameraw['Basics']
            self.extract_interval_diff()
        #elif feature_name == "Harmonic":
        #    self.extract_onsetwise_chroma()
        #    self.Xname['Harmonic'] = ['chroma12']
        elif feature_name == "Metrical":
            self.extract_metrical()
            self.Xname['Metrical'] = ['downbeat','beatphase_huffman_onehot']
            self.Xnameraw['Metrical'] = ['downbeat','beatphase']
            self.raw_features += self.Xnameraw['Metrical']
        elif feature_name == "Notemark":        # Feature 5: note marking (staccato, fermata, accent, staff)
            self.extract_notemark()
            self.Xname['Notemark'] = ['staccato','fermata','accent','staff']
            self.Xnameraw['Notemark'] = ['staccato','fermata','accent','staff']
            self.raw_features += self.Xnameraw['Notemark']
        elif feature_name == "Rest":
            self.extract_rest()
            self.Xname['Rest'] = ['preceds_rest']
            self.Xnameraw['Rest'] = ['preceds_rest']
            self.raw_features += ['preceds_rest']
        elif feature_name == "Dynamics":
            self.extract_dynamic_markings()
            self.Xname['Dynamics'] = self.list_dynamic_marking
            self.Xnameraw['Dynamics'] = self.list_dynamic_marking
            self.Xnameraw['Rest'] = ['preceds_rest']
            self.train_features += ['Dynamics']
            self.raw_features += ['Dynamics']
            #for n in self.list_dynamic_marking:
            #    self.train_features += [n]
        elif feature_name == "Tempo":
            self.extract_tempo_markings()
            self.Xname['Tempo'] = self.list_tempo_marking
            self.Xnameraw['Tempo'] = self.list_tempo_marking
            self.train_features += ['Tempo']
            self.raw_features += ['Tempo']
            #for n in self.list_tempo_marking:
            #    self.train_features += [n]
        elif feature_name == "Slur":
            self.extract_slur_old()
            self.Xname['Slur'] = ['is_slur', 'slur_pos1', 'slur_phase1', 'slur_main',
                                    'slur_pos2', 'slur_phase2', 'slur_second',
                                    'slur_pos3', 'slur_phase3', 'slur_third']
            self.Xnameraw['Slur'] = ['is_slur', 'slur_pos1', 'slur_phase1', 'slur_main',
                                    'slur_pos2', 'slur_phase2', 'slur_second',
                                    'slur_pos3', 'slur_phase3', 'slur_third']


    def extract_basics(self, window_size = 4):
        # dictionaries
        self.dict_noteid2xmlidx = {}
        self.dict_noteid2measure = {}   # maps all note id into measure index
        self.dict_noteid2onset = {}     # map note id to onset, for note-performance use

        self.dict_curr2previd = {}      # current note index to previous note id
        self.dict_curr2previds = {}

        self.dict_xmlidx2noteid = {}
        self.dict_onset2measure = {}

        self.dict_measure2onset = {}
        self.dict_measure2noteid = {}
        self.dict_measure2noteidx = {}
        self.dict_measure2starttime = {}
        self.dict_slur2noteidx = {}
        self.dict_noteidx2onset = {}    # map note index to onset index, for fast computation

        # info
        self.unique_onset_idxs = [np.where(self.note_info['onset'] == u)[0] for u in np.unique(self.note_info['onset'])]
        self.Xonsetmean = np.array(([np.mean(self.note_info['onset'][x]) for x in self.unique_onset_idxs]))
        self.num_onsets = len(self.unique_onset_idxs)
        self.unique_onset_times = np.array(([np.mean(self.note_onset_beat[uix]) for uix in self.unique_onset_idxs]))
        last_time = max(np.max(self.note_onset_beat) + 0.01, np.max(self.note_offset_beat))
        self.diff_u_onset = np.diff(np.r_[self.unique_onset_times, last_time])

        # used features in network
        # raw information
        self.X['dur_beat_raw'] = np.array((self.note_dur)).reshape((self.N,1))
        self.X['pitch_raw'] = self.note_info['pitch']
        self.X['pitch'] = self.X['pitch_raw'] - 21
        self.X['pitch_class'] = self.X['pitch_raw'] % 12
        self.X['octave'] = self.note_info['octave']
        self.X['voice']  = self.note_info['voice']

        self.X['pitch'] = self.X['pitch'].reshape((self.N,1))
        self.X['pitch_class'] = self.X['pitch_class'].reshape((self.N,1))
        self.X['octave'] = self.X['octave'].reshape((self.N, 1))
        self.X['voice'] = self.X['voice'].reshape((self.N, 1))

        self.X['chroma12'] = np.zeros((self.N, 12))

        self.X['minmax_pitch'] = np.zeros((self.N,1))
        self.X['pitch_onsetmax'] = np.zeros((self.N,1))            # maximum pitch of the onset
        self.X['pitch_windowmax'] = np.zeros((self.N,1))           # maximum pitch of the window size -4 to 4 onsets
        self.X['IOI_prev_onset'] = np.zeros((self.N,1))
        self.X['IOI_onset'] = np.zeros((self.N,1))
        self.Xonset['IOI_prev'] = np.zeros((self.num_onsets))
        self.Xonset['IOI'] = np.zeros((self.num_onsets))
        self.Xonset['dur_onset'] = np.zeros((self.num_onsets))
        self.Xonset['startbeat'] = np.zeros((self.num_onsets))

        # store onset index that are on the beat (i.e. on quarter notes)
        self.onbeat_onseti = []

        # one-hots
        self.X['pitch_onehot'] = np.zeros((self.N, 88))             # 0-87, 88 bits
        self.X['pitch_class_onehot'] = np.zeros((self.N, 12))       # 0-11, 12 bits
        self.X['pitch_huffman_onehot'] = np.zeros((self.N, to_class.bin_to_bit(self.bins["pitch"])))    # 3 bits
        self.X['dur_huffman_onehot'] = np.zeros((self.N, to_class.bin_to_bit(self.bins["dur"])))        # 2 bits
        self.X['dur_beat_twohot'] = np.zeros((self.N, 6))           # 6 bits
        self.X['octave_onehot'] = np.zeros((self.N, 8))             # 0-8, 8 bits
        self.X['voice_onehot'] = np.zeros((self.N, 10))             # 0-9, 10 bits
        self.X['minmax_pitch_onehot'] = np.zeros((self.N, 4))       # 0-3, 4 bits
        self.X['pitch_onsetmax_diff_onehot'] = np.zeros((self.N, 8))
        self.X['pitch_windowmax_diff_onehot'] = np.zeros((self.N, 8))
        self.X['IOI_prev_onset_onehot'] = np.zeros((self.N, 4))     # 0to3, 4 bits
        self.X['IOI_onset_onehot'] = np.zeros((self.N, 4))          # 0to3, 4 bits

        for i in range(self.N):
            assert 0<= self.X['pitch'][i] <= 87, "['pitch'] is not 0-87"
            self.X['pitch_onehot'][i][self.X['pitch']] = 1
            self.X['pitch_class_onehot'][i][self.X['pitch_class']] = 1
            self.X['pitch_huffman_onehot'][i] = to_class.convert_huffman_onehot(self.X['pitch_raw'][i], self.bins["pitch"])
            self.X['dur_huffman_onehot'][i] = to_class.convert_huffman_onehot(self.X['dur_beat_raw'][i], self.bins["dur"])
            self.X['dur_beat_twohot'][i] = to_class.convert_dur_twohot(self.X['dur_beat_raw'][i])
            self.X['octave_onehot'][i][self.note_info['octave'][i]] = 1
            self.X['voice_onehot'][i][min(self.note_info['octave'][i], 9)] = 1

        # IOI
        self.X['IOI_simple_next'] = np.zeros((self.N, 1))
        self.X['IOI_simple_prev'] = np.zeros((self.N, 1))

        for i in range(len(self.part.notes)):
            note = self.part.notes[i]
            pred1 = note.start.get_prev_of_type(Note)
            if len(pred1) >= 1:
                self.X['IOI_simple_prev'][i] = self.bm(note.start.t) - self.bm(pred1[0].start.t)
            succ1 = note.start.get_next_of_type(Note)
            if len(succ1) >= 1:
                self.X['IOI_simple_next'][i] = self.bm(succ1[0].start.t) - self.bm(note.start.t)


        # unique onset
        for i,idxs in enumerate(self.unique_onset_idxs):
            curr_pitches = self.note_info['pitch'][idxs]
            self.Xonset['dur_onset'][i] = np.median([self.note_dur[idx] for idx in idxs])
            curr_start_beat = self.bm(self.note_info['onset'][idxs[0]])
            self.Xonset['startbeat'][i] = curr_start_beat
            # get IOI
            if i > 0:
                prev_onset_noteidxs = self.unique_onset_idxs[i-1]
                prev_start_beat = self.bm(self.note_info['onset'][prev_onset_noteidxs[0]])
                self.Xonset['IOI_prev'][i] = curr_start_beat - prev_start_beat
                self.X['IOI_prev_onset'][self.unique_onset_idxs[i]] =  curr_start_beat - prev_start_beat

            # get pitch difference
            note_ids = []
            for idx in idxs:
                # some mapping
                note_id = self.xml_notes[idx].id
                note_ids.append(note_id)
                self.dict_noteidx2onset[idx] = i
                self.dict_noteid2onset[note_id] = i
                curr_pitch = self.note_info['pitch'][idx]
                self.X['pitch_onsetmax'][idx] = max(curr_pitches)
                self.X['pitch_onsetmax_diff_onehot'][idx] = to_class.convert_pitch_diff_onehot(max(curr_pitches) - curr_pitch)
                # onehot
                _, self.X['IOI_prev_onset_onehot'][idx] =  to_class.convert_ioi(self.X['IOI_prev_onset'][idx])
                # extreme pitch, if only one note then zero out everything
                if len(idxs) > 1:
                    pitch_category = to_class.convert_minmax_pitch(curr_pitch, curr_pitches)
                    self.X['minmax_pitch'][idx] = pitch_category
                    self.X['minmax_pitch_onehot'][idx][pitch_category] = 1
                # chroma for the onset
                self.X['chroma12'][idx, [int(c)%12 for c in curr_pitches]] = 1

        #self.X['IOI_onset'][:-1] = self.X['IOI_prev_onset'][1:]
        #self.X['IOI_onset_onehot'][:-1] = self.X['IOI_prev_onset_onehot'][1:]

        self.Xonset['IOI'][:-1] = self.Xonset['IOI_prev'][1:]

        # max pitch over a window -4 to 4 onsets
        for idxs in self.unique_onset_idxs:
            for idx in idxs:
                if idx < self.N - 1:
                    leftbound = max(0, idx-window_size)   # to ensure >= 0
                    rightbound = min(idx+window_size+1, self.N)  # to ensure within the bound
                    pitch_windowmax = max(self.X['pitch_onsetmax'][leftbound:rightbound])
                    self.X['pitch_windowmax'][idx] = pitch_windowmax
                    self.X['pitch_windowmax_diff_onehot'][idx] = to_class.convert_pitch_diff_onehot(pitch_windowmax - self.note_info['pitch'][idx])


    def extract_interval_diff(self):
        """
        Extract pitch interval difference as well as IOI interval difference

        """
        for name in ['pitch_diff_prev','pitch_diff_next','pitch_diff_prev_cat','pitch_diff_next_cat',
                     'step_leap','step_leap_dir', 'dur_diff_prev', 'dur_diff_next',
                     'dur_diff_prev_cat', 'dur_diff_next_cat','num_sim_notes','num_sim_notes_cat',
                     'IOI_prev','IOI_next','IOI_prev_cat','IOI_next_cat']:
            self.X[name] = np.zeros((self.N, 1))

        # Feature to use in the network
        self.X['pitch_diff_prev_onehot'] = np.zeros((self.N, 27))   # -13to13, 27 bits
        self.X['pitch_diff_next_onehot'] = np.zeros((self.N, 27))   # -13to13, 27 bits
        self.X['dur_diff_prev_onehot'] = np.zeros((self.N, 4))      # 0to3, 4 bits
        self.X['dur_diff_next_onehot'] = np.zeros((self.N, 4))      # 0to3, 4 bits
        self.X['num_sim_notes_onehot'] = np.zeros((self.N, 3))      # 0to2, 3 bits
        self.X['IOI_prev_onehot'] = np.zeros((self.N, 4))           # 0to3, 4 bits
        self.X['IOI_next_onehot'] = np.zeros((self.N, 4))           # 0to3, 4 bits

        self.train_features += ['pitch_diff_prev_onehot', 'pitch_diff_next_onehot',
                                'dur_diff_prev_onehot','dur_diff_next_onehot', 'num_sim_notes_onehot',
                                'IOI_prev_onehot', 'IOI_next_onehot']

        # add to name
        self.Xname['Basics'] += ['pitch_diff_prev_cat','pitch_diff_next_cat', 'step_leap','step_leap_dir',
                                 'dur_diff_prev_cat', 'dur_diff_next_cat','num_sim_notes_cat',
                                 'IOI_prev_cat','IOI_next_cat', 'IOI_prev_onset_cat', 'IOI_next_onset_cat']


        # map current note index to previous note index (usually in the same voice, and only one)
        for i in range(self.N):
            self.dict_noteid2xmlidx[self.xml_notes[i].id] = i
            self.dict_xmlidx2noteid[i] = self.xml_notes[i].id
            prev_notes = self.xml_notes[i].previous_notes_in_voice
            next_notes = self.xml_notes[i].next_notes_in_voice
            self.dict_curr2previds[i] = [n.id for n in prev_notes]
            prev_pitches = [n.midi_pitch for n in prev_notes]
            curr_pitches = []
            v = self.xml_notes[i].voice
            if len(prev_notes) == 0:
                self.X['pitch_diff_prev'][i] = self.xml_notes[i].midi_pitch
                continue
            prev_i = None
            if len(prev_notes) == 1:
                prev_i = 0   # the 0th (and the only note) to compare
            else:
                sim_notes = self.xml_notes[i].simultaneous_notes_in_voice
                if len(sim_notes) == 0:
                    # no simultaneous notes, count the interval between current and the lowest/highest pitch on the previous
                    # depending on the register of the outer voice
                    if self.xml_notes[i].staff == 1:
                        # get the maximum pitch
                        prev_i = np.argmax(prev_pitches)
                    else:
                        prev_i = np.argmin(prev_pitches)
                else:
                    self.X['num_sim_notes'][i] = len(sim_notes)
                    curr_pitches = [n.midi_pitch for n in sim_notes]
                    # count the rank
                    # get the index
                    r = 0
                    if self.xml_notes[i].midi_pitch >= curr_pitches[0]:
                        r = 0
                    elif self.xml_notes[i].midi_pitch <= curr_pitches[-1]:
                        r = len(curr_pitches)
                    else:
                        for r in range(len(curr_pitches)-1):
                            if self.xml_notes[i].midi_pitch < curr_pitches[r] and self.xml_notes[i].midi_pitch > curr_pitches[r+1]:
                                break
                        r += 1

                    # if same number of notes, then one-on-one, figure out the ranking
                    if len(prev_pitches) == len(curr_pitches) + 1:
                        prev_i = r
                    # if previous notes are more than current
                    elif len(prev_pitches) > len(curr_pitches) + 1:
                        #  FECBA (prev)
                        #   \ ||
                        #    321 (curr)
                        if r == len(curr_pitches):
                            prev_i = len(prev_pitches) - 1
                        else:
                            prev_i = r

                    # if previous notes are less than current
                    else:
                        #    321 (prev)     1 : A
                        #  //|||            2 : B
                        #  FECBA (curr)     3 : C, E, F
                        prev_i = min(r, len(prev_pitches) - 1)

            self.X['pitch_diff_prev'][i] = self.xml_notes[i].midi_pitch - prev_notes[prev_i].midi_pitch
            self.dict_curr2previd[i] = self.xml_notes[prev_i].id

            prev_note_dur = self.bm(prev_notes[prev_i].end.t) - self.bm(prev_notes[prev_i].start.t)
            if prev_note_dur != 0:
                self.X['dur_diff_prev'][i] = self.note_dur[i] / prev_note_dur
                if i > 0:
                    if self.X['dur_diff_prev'][i] != 0:
                        self.X['dur_diff_next'][i-1] = 1./ self.X['dur_diff_prev'][i]
                    else:
                        self.X['dur_diff_next'][i-1] = 10
            else:
                self.X['dur_diff_prev'][i] = 10
                #if i > 0:
                #    self.X['dur_diff_next'][i-1] = 0   # as default 0

            # step or leap (0: same, 1: step, 2: leap)
            if abs(ord(self.xml_notes[i].step) - ord(prev_notes[prev_i].step)) == 1:
                self.X['step_leap'][i] = 1
            elif abs(ord(self.xml_notes[i].step) - ord(prev_notes[prev_i].step)) > 1:
                self.X['step_leap'][i] = 2

            # step leap direction (0: same, 1 up, 2 down)
            if ord(self.xml_notes[i].step) > ord(prev_notes[prev_i].step):
                self.X['step_leap_dir'][i] = 1
            elif ord(self.xml_notes[i].step) < ord(prev_notes[prev_i].step):
                self.X['step_leap_dir'][i] = -1

            # IOI
            self.X['IOI_prev'][i] = self.bm(self.xml_notes[i].start.t) - self.bm(prev_notes[prev_i].start.t)
            #self.X['IOI_next'][i] = self.bm(next_notes[0].start.t) - self.bm(self.xml_notes[i].start.t)


        # conduct to the other direction
        self.X['pitch_diff_next'][:-1] = -self.X['pitch_diff_prev'][1:]
        #self.X['IOI_next'][:-1] = self.X['IOI_prev'][1:]

        # convert into categorical variables
        for i in range(1, self.N):
            self.X['pitch_diff_prev_cat'][i], self.X['pitch_diff_prev_onehot'][i] = to_class.convert_pitch_diff(self.X['pitch_diff_prev'][i])
            self.X['pitch_diff_next'][i-1] = -self.X['pitch_diff_prev'][i]
            self.X['pitch_diff_next_cat'][i-1], self.X['pitch_diff_next_onehot'][i] = to_class.convert_pitch_diff(self.X['pitch_diff_next'][i-1])
            self.X['dur_diff_prev_cat'][i], self.X['dur_diff_prev_onehot'][i] = to_class.convert_dur_diff(self.X['dur_diff_prev'][i])
            self.X['dur_diff_next_cat'][i-1], self.X['dur_diff_next_onehot'][i] = to_class.convert_dur_diff(self.X['dur_diff_next'][i-1])

            self.X['num_sim_notes_cat'][i], self.X['num_sim_notes_onehot'][i] = to_class.convert_num_sim_notes(self.X['num_sim_notes'][i])
            self.X['IOI_prev_cat'][i], self.X['IOI_prev_onehot'][i] = to_class.convert_ioi(self.X['IOI_prev'][i])


        # conduct to the other direction
        #self.X['pitch_diff_next_cat'][:-1] = -self.X['pitch_diff_prev_cat'][1:]
        self.X['IOI_next_cat'][:-1] = self.X['IOI_prev_cat'][1:]
        self.X['IOI_next_onehot'][:-1] = self.X['IOI_prev_onehot'][1:]

    # def extract_onsetwise_chroma(self):
    #     # to-do: convert according to the key
    #     self.X['chroma12'] = np.zeros((self.N, 12))
    #     for u, p in zip(self.unique_onset_idxs, self.unique_pitches):
    #         for c in p:
    #             self.X['chroma12'][u,int(c)%12] = 1
    #     self.train_features += ['chroma12']

    def extract_metrical(self):

        # where change the time signature
        self.ts = np.array([(t.start.t, t.beats, t.beat_type) for t in self.part.timeline.get_all_of_type(TimeSignature)])
        self.tsi = np.searchsorted(self.ts[:, 0], self.note_info['onset'], side='right') - 1
        # Which beat starts a measure = bm(measure_start_time) -- downbeat
        self.measure_startbeat = self.bm(np.array([m.start.t for m in self.part.timeline.get_all_of_type(Measure)]))
        self.num_measures = len(self.measure_startbeat)
        self.msi = np.searchsorted(self.measure_startbeat, self.note_onset_beat, side='right') - 1
        # assert np.all(tsi >= 0)
        # assert np.all(msi >= 0)
        #self.X['IOI_prev_onbeat']
        # Downbeat and beat phase
        self.X['downbeat'] = np.zeros((self.N, 1))    # 0-1, 1 bit
        self.X['beatphase'] = np.zeros((self.N, 1))
        self.X['beatphase_huffman_onehot'] = np.zeros((self.N, to_class.bin_to_bit(self.bins["beatphase"])))  # 3 bits


        for i,n in enumerate(self.part.notes):
            self.dict_noteid2xmlidx[n.id] = i
            self.dict_xmlidx2noteid[i] = n.id

        # Group all notes that belong to the same measure
        for measure_idx in range(self.num_measures):
            st = self.measure_startbeat[measure_idx]
            et = self.measure_startbeat[-1] * 2
            if measure_idx < self.num_measures - 1:
                et = self.measure_startbeat[measure_idx + 1]
            measure_notes = []
            measure_noteidxs = []
            self.dict_measure2noteid[measure_idx] = []
            self.dict_measure2onset[measure_idx] = []
            for i in range(self.N):
                if st <= self.note_onset_beat[i] < et:
                    curr_onset = self.dict_noteidx2onset[i]
                    self.dict_onset2measure[curr_onset] = measure_idx
                    if curr_onset not in self.dict_measure2onset[measure_idx]:
                        self.dict_measure2onset[measure_idx].append(curr_onset)

                    measure_notes.append(self.part.notes[i].id)
                    measure_noteidxs.append(i)
                    self.dict_noteid2measure[self.part.notes[i].id] = measure_idx
                    self.X['measures'][i] = measure_idx
            self.dict_measure2noteid[measure_idx] = measure_notes
            self.dict_measure2noteidx[measure_idx] = measure_noteidxs


        for onset_i, index in enumerate(self.unique_onset_idxs):
            if self.note_onset_beat[index[0]] < self.eps:
                m_position = self.note_onset_beat[index[0]]
            else:
                m_position = self.note_onset_beat[index[0]] - self.measure_startbeat[self.msi[index[0]]]
            ts_num = self.ts[self.tsi[onset_i], 1]
            factor = gcd(ts_num, self.ts[self.tsi[onset_i], 2])
            beat_loc = np.mod(m_position, ts_num/factor)   # i.e 12/8, factor = 4, then every 3
            # update inter-quarter-or-basic-beat-onset
            if beat_loc == 0:
                self.onbeat_onseti.append(onset_i)
            for i in index:
                beatphase =  beat_loc/ float(ts_num)
                if beatphase == 0:
                    self.X['downbeat'][i] = 1
                self.X['beatphase'][i] = beatphase
                self.X['beatphase_huffman_onehot'][i] = to_class.convert_huffman_onehot(beatphase, self.bins["beatphase"])

        self.X['piecephase'] = (self.note_onset_beat - self.note_onset_beat[0])/(self.note_onset_beat[-1] - self.note_onset_beat[0])
        self.train_features += ['downbeat']
        self.train_features += ['beatphase_huffman_onehot']


    def extract_notemark(self):
        notemark_features = ['staccato','fermata','accent','staff']
        for name in notemark_features:
            self.X[name] = np.zeros((self.N, 1))
            self.train_features += [name]

        for i in range(self.N):
            n = self.xml_notes[i]
            self.X['staccato'][i] = n.staccato
            self.X['fermata'][i] = n.fermata
            self.X['accent'][i] = n.accent
            if n.staff > 1:
                self.X['staff'][i] = 1

    def extract_rest(self):
        self.X['preceds_rest'] = np.array([1 if len(n.end.get_starting_objects_of_type(Note)) == 0 else 0 for n in self.xml_notes])
        self.X['preceds_rest'] = self.X['preceds_rest'].reshape((self.N, 1))
        self.train_features += ['preceds_rest']

    ###### Dynamics ########

    def ensure_constant_at_start_end(self, default_direction=u'mf'):
        """
        Make sure that there is a ConstantLoudnessDirection at the start and end
        time. An "mf" Direction is insterted as a default when a
        ConstantLoudnessDirection is missing.

        Parameters
        ----------

        Returns
        -------
        """

        constant = [d for d in self.directions
                    if isinstance(d, ConstantLoudnessDirection)]
        start = self.part.timeline.points[0]
        end = self.part.timeline.points[-1]
        if not constant:
            cld = ConstantLoudnessDirection(default_direction)
            start.add_starting_object(cld)
            self.directions.insert(0, cld)
            end.add_ending_object(cld)
        else:
            if constant[0].start.t > start:
                cld = ConstantLoudnessDirection(default_direction)
                start.add_starting_object(cld)
                self.directions.insert(0, cld)
                constant[0].start.add_ending_object(cld)
            if constant[-1].end.t < end:
                cld = ConstantLoudnessDirection(default_direction)
                constant[-1].end.add_starting_object(cld)
                self.directions.append(cld)
                end.add_ending_object(cld)


    # refer to featurebases l491
    def fill_out_direction_times(self):
        # for all dynamic: if unset end_time, end_time=timeOfNextConstant or lastTime
        # for all impulsive: end_time = begin_time+epsilon
        # for all constant: end_time = max(begin_time+epsilon,timeOfNextConstant)
        # for last constant: end_time = lastTime
        self.directions = self.part.get_loudness_directions()
        for i, d in enumerate(self.directions):
            if isinstance(d, DynamicLoudnessDirection):
                # print(d.text, d.start.t, d.end.t)
                d.intermediate.append(d.end)
                d.end = None
        for i, d in enumerate(self.directions):

            if isinstance(d, ImpulsiveLoudnessDirection):
                self.directions[i].end = self.directions[i].start
            else:
                if d.end == None:
                    n = d.start.get_next_of_type(ConstantLoudnessDirection)
                    if n:
                        self.directions[i].end = max(d.start, n[0].start)
                    else:
                        self.directions[i].end = self.part.timeline.points[-1]


    def make_local_loudness_direction_feature(self, direction) :
        """
        from featurebases.py line 259, simplified version
        """

        if isinstance(direction, DynamicLoudnessDirection):
            d = np.array([(direction.start.t, 0),
                          (direction.end.t, 1),
                          (direction.end.t + self.eps, 0.0)])
        elif isinstance(direction, ConstantLoudnessDirection):
            d = np.array([(direction.start.t - self.eps, 0),
                          (direction.start.t, 1),
                          (direction.end.t - self.eps, 1),
                          (direction.end.t, 0)])
        else:
            d = np.array([(direction.start.t - self.eps, 0),
                          (direction.start.t, 1),
                          (direction.start.t + self.eps, 0)])
        return utils.interpolate_feature(self.note_info['onset'], d)


    def extract_dynamic_markings(self):
        """
        Refer to LoudnessAnnotationBasis
        """
        self.fill_out_direction_times()
        self.ensure_constant_at_start_end()

        self.X['Dynamics'] = np.zeros((self.N, len(self.cat_dynamic_marking)))
        for i, d in enumerate(self.directions):
            if d.text not in self.cat_dynamic_marking:
                print(d.text + ' is not in cat_dynamic_marking')
            else:
                #print d.text + ' is in category: %d ' % cat_dynamic_marking[d.text]
                self.X['Dynamics'][:, self.cat_dynamic_marking[d.text]] += self.make_local_loudness_direction_feature(d)



    ########### Tempo ###############

    def make_local_tempo_direction_feature(self,direction):

        if isinstance(direction, DynamicTempoDirection):
            if direction.end is None:
                end = self.note_info['onset'][-1] + 1   # last time + 1
                nextd = direction.start.get_next_of_type(ConstantTempoDirection)
                if len(nextd) > 0:
                    end = nextd[0].start.t
            else:
                end = direction.end.t

            d = np.array([(direction.start.t, 0),
                          (end - self.eps, 1),
                          (end, 0.0)])

        elif isinstance(direction, ConstantTempoDirection):
            end = self.note_info['onset'][-1] + 1
            nextd = direction.start.get_next_of_type(ConstantTempoDirection)
            if len(nextd) > 0 and nextd[0].text not in (u'a_tempo', u'in_tempo'):
                end = nextd[0].start.t

            d = np.array([(direction.start.t - self.eps, 0),
                          (direction.start.t, 1),
                          (end - self.eps, 1),
                          (end, 0)])
            return utils.interpolate_feature(self.note_info['onset'], d)
        else:
            d = np.array([(direction.start.t - self.eps, 0),
                          (direction.start.t, 1),
                          (direction.start.t + self.eps, 0)])
        return utils.interpolate_feature(self.note_info['onset'], d)

    def extract_tempo_markings(self):
        """
        Refer to LoudnessAnnotationBasis
        """

        self.t_directions = self.part.get_tempo_directions()
        first_tempo = next((d for d in self.t_directions if
                                isinstance(d, ConstantTempoDirection)), None)

        for d in self.t_directions:
            if d.text == 'tempo_1':
                if first_tempo and first_tempo.text != 'tempo_1':
                    d.text = first_tempo.text
        # what about a tempo

        self.X['Tempo'] = np.zeros((self.N, len(self.cat_tempo_marking)))   # cat_dynamic is the category of dynamic markings
        for i, d in enumerate(self.t_directions):
            if d.text not in self.cat_tempo_marking:
                print(d.text + ' is not in cat_tempo_marking')
            else:
                self.X['Tempo'][:, self.cat_tempo_marking[d.text]] += self.make_local_tempo_direction_feature(d)


    def extract_slur(self):
        slurs = self.part.timeline.get_all_of_type(Slur)

        # Placeholders
        self.slur_infos = []
        self.slur_samenotes = []
        for name in ["is_slur", "slur_grace", "slur_phase1", "slur_phase2", "slur_phase3",
                     "slur_pos1", "slur_pos2", "slur_pos3"]:
            self.X[name] = np.zeros((self.N, 1))

        for name in ["slur_main", "slur_second", "slur_third",
                     "slur_pos1_onehot","slur_pos2_onehot","slur_pos3_onehot"]:
            self.X[name] = np.zeros((self.N, to_class.bin_to_bit(self.bins["slur"])))
            self.train_features += [name]

        # Construct dictionary to map note index to slur group number (index in slur_infos)
        to_slurmain = dict()
        to_slur2 = dict()
        to_slur3 = dict()

        # construct slur information
        slur_no = 0
        for slur_i in range(len(slurs)):
            s = slurs[slur_i]
            # grab all notes belong to the slur
            note_cands = [i for i in range(self.N) if self.note_info[i]['voice'] == s.voice
                             and (self.note_info[i]['onset'] >= s.start.t and self.note_info[i]['onset'] <= s.end.t)]
            slur_info = {"note_idx": note_cands, "num_notes": len(note_cands),
                         "start_beat": self.bm(s.start.t), "end_beat":self.bm(s.end.t),
                         "num_beats": self.bm(s.end.t) - self.bm(s.start.t)}

            # if only one note, or same start/end time, then probably a grace note
            if slur_info["start_beat"] == slur_info["end_beat"] or len(note_cands) == 1:
                self.X['slur_grace'][note_cands[0]] = 1
            else:
                # if two notes of the same pitch, could be a tie
                if len(note_cands) == 2 and self.X['pitch_raw'][note_cands[0]] == self.X['pitch_raw'][note_cands[1]]:
                    self.slur_samenotes.append(slur_info)
                else:
                    # otherwise, it's a slur
                    self.slur_infos.append(slur_info)
                    self.dict_slur2noteidx[slur_no] = note_cands
                    slur_no += 1

                    # map note index to slur group number (sometimes one note are in different group), and fill in values
                    for idx in range(len(note_cands)):
                        i = note_cands[idx]    # the note id
                        self.X['is_slur'][i] = 1
                        slur_pos, pos_huffman_onehot, phase, slur_onehot = to_class.fill_slur_val(idx, slur_info, self.note_onset_beat[i], self.bins["slur"])
                        if i not in to_slurmain:
                            to_slurmain[i] = slur_no - 1
                            # fill in values
                            self.X['slur_pos1'][i] = slur_pos
                            self.X['slur_pos1_onehot'][i] = pos_huffman_onehot
                            self.X['slur_phase1'][i] = phase
                            self.X['slur_main'][i] = slur_onehot
                        else:
                            if i not in to_slur2:
                                # compare length, if slur2 is longer, swap 1 with 2
                                len1 = self.slur_infos[to_slurmain[i]]["num_notes"]
                                if len(note_cands) > len1:
                                    to_slurmain[i], to_slur2[i] = slur_no - 1, to_slurmain[i]
                                    self.X['slur_pos1'][i], self.X['slur_pos2'][i] = slur_pos, self.X['slur_pos1'][i]
                                    self.X['slur_pos1_onehot'][i], self.X['slur_pos2_onehot'][i] = pos_huffman_onehot, self.X['slur_pos1_onehot'][i]
                                    self.X['slur_phase1'][i], self.X['slur_phase2'][i] = phase, self.X['slur_phase1'][i]
                                    self.X['slur_main'][i], self.X['slur_second'][i] = slur_onehot, self.X['slur_main'][i]
                                else:
                                    to_slur2[i] = slur_no - 1
                                    self.X['slur_pos2'][i], self.X['slur_pos2_onehot'][i], self.X['slur_phase2'][i], self.X['slur_second'][i] = slur_pos, pos_huffman_onehot, phase, slur_onehot

                            else:
                                len2 = self.slur_infos[to_slur2[i]]["num_notes"]
                                if i not in to_slur3:
                                    # compare length and re-org
                                    if len(note_cands) <= len2:  # 1 2 3
                                        to_slur3[i] = slur_no - 1
                                        self.X['slur_pos3'][i], self.X['slur_pos3_onehot'][i], self.X['slur_phase3'][i], self.X['slur_third'][i] = slur_pos, pos_huffman_onehot, phase, slur_onehot
                                    elif len(note_cands) > len(note_cands):
                                        # 3 1 2
                                        to_slur3[i] = to_slur2[i]
                                        self.X['slur_pos3'][i], self.X['slur_phase3'] = self.X['slur_pos2'][i], self.X['slur_phase2'][i]
                                        self.X['slur_pos3_onehot'][i], self.X['slur_third'][i] = self.X['slur_pos2_onehot'], self.X['self_second']
                                        to_slurmain[i], to_slur2[i] = slur_no - 1, to_slurmain[i]
                                        self.X['slur_pos1'][i], self.X['slur_pos2'][i] = slur_pos, self.X['slur_pos1'][i]
                                        self.X['slur_pos1_onehot'][i], self.X['slur_pos2_onehot'][i] = pos_huffman_onehot, self.X['slur_pos1_onehot'][i]
                                        self.X['slur_phase1'][i], self.X['slur_phase2'][i] = self.X['slur_phase3'][i], phase
                                        self.X['slur_main'][i], self.X['slur_second'][i] = slur_onehot, self.X['slur_main'][i]
                                    else:
                                        # 1 3 2
                                        to_slur2[i], to_slur3[i] = slur_no - 1, to_slur2[i]
                                        self.X['slur_pos2'][i], self.X['slur_pos3'][i] = self.X['slur_pos3'][i], slur_pos
                                        self.X['slur_pos2_onehot'][i], self.X['slur_pos3_onehot'][i] = self.X['slur_pos3_onehot'][i], pos_huffman_onehot
                                        self.X['slur_phase2'][i], self.X['slur_phase3'][i] = self.X['slur_phase3'][i], phase
                                        self.X['slur_second'][i], self.X['slur_third'][i] = self.X['slur_third'][i], slur_onehot
                                #else:
                                #     print("%d has three slurs" % i)

        self.dict_noteidx2slur = to_slurmain


    ##### Slur old #######
    def extract_slur_old(self):
        """
        Only use slur increase
        """
        slurs = self.part.timeline.get_all_of_type(Slur)
        self.X['Slur'] = np.zeros((self.N, 3), dtype=np.float32)
        ss = np.array([(s.voice, s.start.t, s.end.t)
                                   for s in slurs
                                   if (s.start is not None and
                                       s.end is not None)])
        for v, start, end in ss:
            tmap = np.array([[min(np.min(self.note_info['onset']), start - self.eps),  0],
                             [start - self.eps,              0],
                             [start,                    self.eps],
                             [end,                      1],
                             [end + self.eps,                0],
                             [max(np.max(self.note_info['onset']), end + self.eps),     0]])
            incr = interp1d(tmap[:, 0], tmap[:, 1])
            self.X['Slur'][:, 0] += incr(self.note_info['onset'])

        """
        Use percentage
        """
        for s in slurs:
            note_cand = [i for i in range(self.N) if self.note_info[i]['voice'] == s.voice
                             and self.note_info[i]['onset'] >= s.start.t and self.note_info[i]['onset'] <= s.end.t]
            #if len(note_cand) == 2 and note_cand[0]['pitch'] == note_cand[1]['pitch']:
                # to-do: merge two notes
            #if len(note_cand) == 1:
                # to-do: grace note
            if len(note_cand) > 1:
                slur_len = s.end.t - self.note_info['onset'][note_cand[0]]
                if slur_len > 0:
                    # go to the third buffer area
                    if np.max(self.X['Slur'][note_cand,1]) > 0:
                        self.X['Slur'][note_cand[0],2] += self.eps
                        for i in range(1,len(note_cand)):
                            self.X['Slur'][note_cand[i],2] += (self.note_info['onset'][note_cand[i]]- self.note_info['onset'][note_cand[0]])/slur_len
                    else:
                        self.X['Slur'][note_cand[0],1] += self.eps
                        for i in range(1,len(note_cand)):
                            self.X['Slur'][note_cand[i], 1] += (self.note_info['onset'][note_cand[i]]- self.note_info['onset'][note_cand[0]])/slur_len