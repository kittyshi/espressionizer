import prepare_data
import numpy as np
import aiplay
import os
import sys
import datetime

usage = """
    time python takegradient.py xml_test_chopin/chopin_op09_No1.xml 0
"""

USE_CPU = True
if len(sys.argv) == 3 and sys.argv[2] in [str(x) for x in range(8)]:
    USE_CPU = False

print '*'*40
if USE_CPU:
    # force CPU
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = ""
    print 'Use CPU'
else:
    os.environ["CUDA_VISIBLE_DEVICES"] = sys.argv[2]
    print 'Use GPU', sys.argv[2]
print '*'*40

import tensorflow as tf
from keras import backend as k

# if 'tensorflow' == k.backend():
#     import tensorflow as tf
#     from keras.backend.tensorflow_backend import set_session
#     config = tf.ConfigProto()
#     config.gpu_options.allow_growth = True
#     config.gpu_options.visible_device_list = "2"
#     #session = tf.Session(config=config)
#     set_session(tf.Session(config=config))

def jacobian_tensorflow(x,model):
    sess = k.get_session()
    jacobian_matrix = []
    for m in range(model['seq_len']):
        # We iterate over the M elements of the output vector
        grad_func = tf.gradients(model['model'].output[:, m], model['model'].input)
        gradients = sess.run(grad_func, feed_dict={model['model'].input: x})
        jacobian_matrix.append(gradients[0][0,:])
    return np.array(jacobian_matrix)

def getGradients(X, model):
    input_data = prepare_data.slice_test_data(X, model['seq_len'])
    g = jacobian_tensorflow(input_data, model)
    return g, input_data



if __name__ == '__main__':
    print sys.argv

    xml_dir = '../testdata/'

    mtype = ['vtrend', 'vdev','ibi','time','art','sustainpedal','softpedal']

    fn = 'beethoven_op111_mv1.xml'
    if len(sys.argv) >= 2:
        fn = sys.argv[1]

    # parse models
    if 'beethoven' in fn:
        model_dir = '0530_bb_best/'
    else:
        model_dir = '0531_best_chopin/'
    modeldict = aiplay.get_model_dict(model_dir)

    xml_fn = xml_dir + fn                   # '../testdata/xml_test_beethoven/beethoven_op111_mv1.xml'

    # for f in os.listdir(xml_dir):
    #     if f.endswith('.xml'):
            # For each xml file (each piece)
    # xml_fn = xml_dir + f                         #'testdata/xml_test_chopin/chopin_op35_Mv1.xml'
    X, xmlscore = aiplay.read_xml(xml_fn)
    # Calculate gradient for each (5 total, no need for pedal right now)
    for i in range(5):
        g_output_fn = '%s_%s_g.npy' % (xml_fn, mtype[i])
        input_output_fn = '%s_%s_input.npy' % (xml_fn, mtype[i])

        if os.path.exists(g_output_fn) and os.path.exists(input_output_fn):
            print '> Skipping %s' % g_output_fn
            print '> Skipping %s' % input_output_fn
            continue

        start = datetime.datetime.now()

        input_data = prepare_data.slice_test_data(X, modeldict[mtype[i]]['seq_len'])
        g, input_data = getGradients(X, modeldict[mtype[i]])
        # store g and corresponding input_data (if no space, then don't store input_data)
        np.save(g_output_fn, g)
        np.save(input_output_fn, input_data)
        del g
        del input_data

        print '> Done in %d seconds (%s) ' % ((datetime.datetime.now() - start).seconds, mtype[i])

