import matched_perf
import numpy as np
import pretty_midi

class PerformanceFeature:
    def __init__(self, score, match_fn, midi_fn = None):
        self.mp = matched_perf.MatchedPerf(score, match_fn)
        self.matched_perf = self.mp.matched_perf
        self.onset2mp = self.mp.onset2mp
        self.mp2onset = self.mp.mp2onset
        self.measure2mp = self.mp.measure2mp
        self.M = self.mp.M
        self.score = score
        self.prepare_extraction()
        self.encode_onset()
        self.encode_onbeat()
        self.encode_articulation()
        self.encode_measure()
        self.encode_measure4()
        self.post_processing()
        self.encode_beattimes()

        if midi_fn is None:
            midi_fn = match_fn[:-10] + '.mid'
        self.pm = pretty_midi.PrettyMIDI(midi_fn)
        self.Y['sus_pedal'] = self.get_pedal(pedal_type = "sustain")
        self.Y['soft_pedal'] = self.get_pedal(pedal_type = "soft")


    def encode_beattimes(self):
        self.beat_times = np.zeros((self.score.num_beats))
        for beat in range(self.score.num_beats):
            if beat in self.score.beat2onset:
                onset = self.score.beat2onset[beat]
                if onset in self.onset2mp:
                    mps = self.onset2mp[onset]
                    onset_times = self.matched_perf['perf_onset'][mps]
                    self.beat_times[beat] = np.average(onset_times)
        for i in range(1,len(self.beat_times)-1):
            if self.beat_times[i] == 0:
                self.beat_times[i] = self.beat_times[i-1] + (self.beat_times[i+1] - self.beat_times[i-1])/2

    def get_pedal(self, pedal_type = "sustain", pedal_min = 64):
        pedal = []
        number = 64
        if pedal_type == "soft":
            number = 67
        for cc in self.pm.instruments[0].control_changes:
            if cc.number == number:
                pedal.append(cc)
        if len(pedal) == 0:
            return np.zeros((self.M))

        L = np.max([int(self.matched_perf['perf_offset'][-1]), int(pedal[-1].time)])
        p_curve = np.zeros((L))

        pedal_on = False
        pedal_ontime = 0
        pedal_offtime = 0

        for p in pedal:
            if p.value > 63:
                if not pedal_on:
                    pedal_on = True
                    pedal_ontime = p.time
            else:
                if pedal_on:
                    pedal_offtime = p.time
                    p_curve[int(pedal_ontime):int(pedal_offtime)] = 1
                    pedal_on = False

        note_pedal = np.zeros((self.M, 1))
        for i in range(self.M):
            onidx = int(self.matched_perf['perf_onset'][i])
            offidx = int(self.matched_perf['perf_offset'][i])
            if np.sum(p_curve[onidx:min(offidx,len(p_curve)-1)]) >= 1:  # from onset to offset, if there is one pedal on
                note_pedal[i] = 1

        return note_pedal



    def post_processing(self):
        med_bpm = self.Y['bpm_raw'][self.Y['bpm_raw'] < 10]
        med_bpm = med_bpm[med_bpm > 0]
        med = np.median(med_bpm)
        self.Y['bpm_raw'][self.Y['bpm_raw'] >= 10] = med
        self.Y['bpm_raw'][self.Y['bpm_raw'] < 0] = med

        med_bpm_m = self.Ymeasure['bpm_raw'][self.Ymeasure['bpm_raw'] < 10]
        med_bpm_m = med_bpm_m[med_bpm_m > 0]
        med_m = np.median(med_bpm_m)
        self.Ymeasure['bpm_raw'][self.Ymeasure['bpm_raw'] >= 10] = med
        self.Ymeasure['bpm_raw'][self.Ymeasure['bpm_raw'] < 0] = med

    def prepare_extraction(self):
        self.Y = {}
        self.Y_featurename = ['vel','vel_onsetmax', 'vel_maxdiff',
                              'vel_measuremean','vel_diff_measure',
                              'bpm_raw','time','beat_period_raw',
                              'articulation','log_articulation','x_perftime',
                              'measure_bpm_raw','measure_bpm_diff']
        for name in self.Y_featurename:
            self.Y[name] = np.zeros((self.M))

        self.Yonset = {}
        self.Yonset_featurename = ['IOI','bpm_raw','beat_period_raw',
                                   'x_perftime','vel_max']
        for name in self.Yonset_featurename:
            self.Yonset[name] = np.zeros((self.score.num_onsets))

        self.Yonbeat = {}
        self.Ymeasure = {}
        self.Ymeasure_featurename = ['vel_mean','bpm_raw','x_scoreonset','x_perftime',
                                        'beat_period_raw']
        for name in self.Ymeasure_featurename:
            self.Ymeasure[name] = np.zeros((self.score.num_measures))

        self.Ymeasure4 = {}
        self.Ymeasure4name = ['bpm_raw']
        for name in self.Ymeasure4name:
            self.Ymeasure4[name] = np.zeros((int(self.score.num_measures/4)))

    def encode_measure4(self):
        start_measure = 0
        if np.diff(self.score.measure_startbeat)[0] < np.diff(self.score.measure_startbeat)[1]:
            start_measure = 1
        self.Ymeasure4['x_perftime'] = self.Ymeasure['x_perftime'][start_measure::4]
        self.Ymeasure4['x_scoreonset'] = self.Ymeasure['x_scoreonset'][start_measure::4]
        self.Ymeasure4['bpm_raw'] = np.zeros((len(self.Ymeasure4['x_scoreonset'])))
        self.Ymeasure4['bpm_raw'][:-1] = np.diff(self.Ymeasure4['x_scoreonset']) / np.diff(self.Ymeasure4['x_perftime'])
        self.Ymeasure4['bpm_raw'][-1] = self.Ymeasure4['bpm_raw'][-2]
        # no beat period necessary
        self.Ymeasure4['vel_mean'] = np.zeros((len(self.Ymeasure4['x_scoreonset'])))
        for i in range(len(self.Ymeasure4['vel_mean'])):
            self.Ymeasure4['vel_mean'][i] = self.Ymeasure['vel_mean'][start_measure+i*4:start_measure+(i+1)*4].mean()


    def encode_measure(self):
        #for measure in range(self.score.num_measures):
        measure0 = False
        m0idx = min(self.score.measure_indexs)
        for measure in self.measure2mp:
            if measure in self.measure2mp:
                mp_idxs = self.measure2mp[measure]
                # find the first onset in the measure
                mp0 = mp_idxs[np.argmin(self.matched_perf['score_onset'][mp_idxs])]
                onset_i = self.mp2onset[mp0]
                if onset_i in self.onset2mp:
                    onset_valid = self.onset2mp[onset_i]
                    curr_perf_onset  = self.matched_perf['perf_onset'][onset_valid].mean()
                    curr_score_onset = self.matched_perf['score_onset'][onset_valid].mean()
                    measure_notes = self.matched_perf[mp_idxs]
                    #self.Ymeasure['vel_mean'][measure-m0idx] = np.mean(measure_notes['perf_velocity'])
                    self.Ymeasure['vel_mean'][measure] = np.mean(measure_notes['perf_velocity'])
                    self.Y['vel_measuremean'][mp_idxs] = self.Ymeasure['vel_mean'][measure]
                    self.Y['vel_diff_measure'][mp_idxs] = self.Ymeasure['vel_mean'][measure] - self.Y['vel'][mp_idxs]
                    self.Ymeasure['x_scoreonset'][measure-m0idx] = curr_score_onset
                    self.Ymeasure['x_perftime'][measure-m0idx] = curr_perf_onset
                    if not measure0:
                        measure0 = True
                    else:
                        p_ioi =  curr_perf_onset - prev_perf_onset
                        s_ioi =  curr_score_onset - prev_score_onset
                        if p_ioi == 0:
                            self.Ymeasure['bpm_raw'][measure-1] =  s_ioi / 0.01
                        else:
                            self.Ymeasure['bpm_raw'][measure-1] =  s_ioi / p_ioi
                        # store on note-level
                        if measure-1 in self.measure2mp:
                            curr_mp = self.measure2mp[measure-1]
                            if len(curr_mp) > 0:
                                self.Y['measure_bpm_raw'][curr_mp] = self.Ymeasure['bpm_raw'][measure-1]
                            if s_ioi == 0:
                                self.Ymeasure['beat_period_raw'][measure-m0idx-1] = p_ioi / 0.01
                            else:
                                self.Ymeasure['beat_period_raw'][measure-m0idx-1] = p_ioi / s_ioi
                    prev_perf_onset  = curr_perf_onset
                    prev_score_onset = curr_score_onset
        self.Y['measure_bpm_diff'] = self.Y['bpm_raw'] - self.Y['measure_bpm_raw']


    def encode_onset(self):
        self.Y['vel'] = self.matched_perf['perf_velocity']
        for onset_i in self.mp.onset2mp:
            mp_idxs = self.mp.onset2mp[onset_i]
            for mp_idx in mp_idxs:
                self.Y['x_perftime'][mp_idx] = self.matched_perf[mp_idx]['perf_onset']
            curr_notes = self.matched_perf[mp_idxs]
            curr_maxvel = self.Y['vel'][mp_idxs].max()
            self.Y['vel_onsetmax'][mp_idxs] = curr_maxvel
            self.Yonset['vel_max'][onset_i] = curr_maxvel
            self.Y['vel_maxdiff'][mp_idxs] = curr_maxvel - self.Y['vel'][mp_idxs]
            #self.Yonset['x_perftime'][0] = curr_notes['perf_onset'].mean()
            # find previous onset, encode bpm
            self.Yonset['x_perftime'][onset_i] = curr_notes['perf_onset'].mean()
            if onset_i-1 in self.mp.onset2mp:
                prev_mp_idxs = self.mp.onset2mp[onset_i-1]
                prev_notes = self.matched_perf[prev_mp_idxs]
                self.Yonset['IOI'][onset_i -1] = curr_notes['perf_onset'].mean() - prev_notes['perf_onset'].mean()
                Yonset_scoreIOI = curr_notes['score_onset'].mean() - prev_notes['score_onset'].mean()

                if self.Yonset['IOI'][onset_i-1] > 0:
                    self.Yonset['bpm_raw'][onset_i-1] = Yonset_scoreIOI / self.Yonset['IOI'][onset_i-1]
                if Yonset_scoreIOI > 0:
                    self.Yonset['beat_period_raw'][onset_i-1] = self.Yonset['IOI'][onset_i-1] / Yonset_scoreIOI

                for mp_idx in prev_mp_idxs:
                    if self.Yonset['IOI'][onset_i-1] == 0:
                        self.Y['bpm_raw'][mp_idx] = 10
                    else:
                        self.Y['bpm_raw'][mp_idx] = self.score.Xonset['IOI'][onset_i-1]/self.Yonset['IOI'][onset_i-1]
        # the final onset
        perf_onset = curr_notes['perf_onset'].mean()
        perf_offset = curr_notes['perf_offset'].mean()
        score_onset = curr_notes['score_onset'].mean()
        score_offset = curr_notes['score_offset'].mean()

        self.Yonset['beat_period_raw'][-1] = (perf_offset - perf_onset) / (score_offset - score_onset)
        self.Yonset['bpm_raw'][-1] = (score_offset - score_onset) / (perf_offset - perf_onset)


    def encode_onbeat(self):
        self.onbeat_mp = []
        self.onbeat_all = []
        self.onbeat_onseti = []
        self.onbeat_mp2beat = {}
        for onset in self.score.onbeat_onseti:
            if onset in self.onset2mp:
                self.onbeat_mp.append(self.onset2mp[onset])
                self.onbeat_all.append(self.score.onset2beat[onset])
                self.onbeat_onseti.append(onset)
                self.onbeat_mp2beat[len(self.onbeat_mp)-1] = self.score.onset2beat[onset]
            #else:
            #    self.onbeat_all.append(-1)    # mark -1 when onbeat onset is not there

        self.Yonbeat['beat_period_raw'] = np.zeros((len(self.onbeat_mp)))
        self.Yonbeat['bpm_raw'] = np.zeros((len(self.onbeat_mp)))
        L = int(self.score.onset2beat[[k for k in self.score.onset2beat.keys()][-1]]) + 1
        self.Yonbeat['bpm_beat'] = np.zeros((L))
        self.Yonbeat['vel_mean'] = np.zeros((len(self.onbeat_mp)))
        self.Yonbeat['x_perftime'] = np.zeros((len(self.onbeat_mp)))
        self.Y['onbeat_bpmraw'] = np.zeros((self.M))
        self.Yonbeat['bpm_raw_real'] = np.zeros((self.score.num_beats))
        self.Yonbeat['bpm_raw_real_relative'] = np.zeros((self.score.num_beats))
        # vel-mean
        #`return
        for i in range(len(self.onbeat_mp)-1):
            #self.Yonbeat['vel_mean'][i] = self.matched_perf['perf_velocity'][self.onbeat_mp[-1]].mean()
            perf_time = self.matched_perf[self.onbeat_mp[i+1]]['perf_onset'].mean()
            perf_prev_time = self.matched_perf[self.onbeat_mp[i]]['perf_onset'].mean()
            score_time = self.matched_perf[self.onbeat_mp[i+1]]['score_onset'].mean()
            score_prev_time = self.matched_perf[self.onbeat_mp[i]]['score_onset'].mean()
            self.Yonbeat['beat_period_raw'][i] = (perf_time - perf_prev_time) / max((score_time - score_prev_time),0.1)
            bpm_raw = (score_time - score_prev_time) / max((perf_time - perf_prev_time),0.1)
            the_onsets = range(self.onbeat_onseti[i],self.onbeat_onseti[i+1])
            for the_onset in the_onsets:
                if the_onset in self.onset2mp:
                    self.Y['onbeat_bpmraw'][self.onset2mp[the_onset]] =  bpm_raw
            self.Yonbeat['bpm_raw'][i] = bpm_raw
            self.Yonbeat['bpm_beat'][int(self.onbeat_all[i])] = bpm_raw
            self.Yonbeat['x_perftime'][i] = perf_prev_time  # change from perf_time
            if i in self.onbeat_mp2beat:
                b = self.onbeat_mp2beat[i]
                self.Yonbeat['bpm_raw_real'][int(b)] = self.Yonbeat['bpm_raw'][i]

        perf_onset = self.matched_perf[self.onbeat_mp[i+1]]['perf_onset'].mean()
        perf_offset = self.matched_perf[self.onbeat_mp[i+1]]['perf_offset'].mean()
        score_onset = self.matched_perf[self.onbeat_mp[i+1]]['score_onset'].mean()
        score_offset = self.matched_perf[self.onbeat_mp[i+1]]['score_offset'].mean()

        self.Yonbeat['beat_period_raw'][-1] = (perf_offset - perf_onset) / (score_offset - score_onset)
        self.Yonbeat['bpm_raw'][-1] = (score_offset - score_onset) / (perf_offset - perf_onset)
        self.Yonbeat['x_perftime'][-1] = self.Yonbeat['x_perftime'][-2] + 0.1
        self.Yonbeat['bpm_raw_real'][-1] = self.Yonbeat['bpm_raw'][-1]

        #self.Yonbeat['vel_mean'][-1] = self.matched_perf['perf_velocity'][self.onbeat_mp[-1]].mean()

        #
        for i in range(len(self.Yonbeat['bpm_beat'])):
            if self.Yonbeat['bpm_beat'][i] == 0:
                self.Yonbeat['bpm_beat'][i] = self.Yonbeat['bpm_beat'][i-1]

        for i in range(len(self.Yonbeat['bpm_raw_real'])):
            if self.Yonbeat['bpm_raw_real'][i] == 0:
                self.Yonbeat['bpm_raw_real'][i] = self.Yonbeat['bpm_raw_real'][i-1]

        # relative
        for i in range(1,len(self.Yonbeat['bpm_raw_real_relative'])):
            self.Yonbeat['bpm_raw_real_relative'][i] = (self.Yonbeat['bpm_raw_real'][i] - self.Yonbeat['bpm_raw_real'][i-1]) / np.average(self.Yonbeat['bpm_raw_real'])


        for mp_idx in self.onbeat_mp[-1]:
            self.Y['onbeat_bpmraw'][mp_idx] = self.Yonbeat['bpm_raw'][-1]


    def encode_articulation(self):
        for mp in range(self.M):
            perf_dur = self.matched_perf[mp]['perf_offset'] - self.matched_perf[mp]['perf_onset']
            score_dur = self.matched_perf[mp]['score_offset'] - self.matched_perf[mp]['score_onset']
            xml_idx = self.matched_perf[mp]['xml_idx']
            onset_i = self.score.dict_xmlidx2onset[xml_idx]
            if score_dur == 0:
                #if self.Y['bpm_raw'][mp] != 0:
                #    self.Y['articulation'][mp] = 10 / self.Y['bpm_raw'][mp]
                #else:
                self.Y['articulation'][mp] = 10
            else:
                self.Y['articulation'][mp] = perf_dur * self.Y['bpm_raw'][mp] / (score_dur)
        self.Y['articulation'][self.Y['articulation']<=0] = 0.01
        self.Y['log_articulation'] = np.log2(self.Y['articulation'])
