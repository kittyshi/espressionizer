"""
This python file runs the whole system.
The system contains a javascript module that builds the interface
The system also runs python as its underlying algorithm
To run this, you need to install flask, bokeh, and panda
Flask bridges javascript and python. (It uses jinja language maybe?)
Bokeh is a python library that visualizes data
Panda deals with data processing
In javascript, we display audio wavefile using wavesurfer-js
Let me know if I forget to include anything
"""

# import my python module
"""
# displayWav is a basic python module to load the wave file using wavesurfer-js.
I remember that I tried to make the code clean by separating it out.
But somehow the code won't work when I make it clean.
Ignore displayWav.py for now, but might be useful in the future
"""
# import displayWav

"""
This is the 'algorithm' module that does the real work, which currently does nothing.
"""
from expressive_algorithm import ExpresiveEngine

# for using flask
from flask import Flask, render_template, request, jsonify
from flask import redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename

# some basic python library
import os
import sys
import pandas as pd
import numpy as np

# for data visualization
from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.resources import INLINE
from bokeh.util.string import encode_utf8
from bokeh.models import ColumnDataSource, OpenURL, HoverTool, TapTool, CustomJS
from bokeh.io import show, output_file
from bokeh.plotting import figure
from bokeh import events
from bokeh.models import CustomJS, Div, Button
from bokeh.layouts import column, row

# Some global variable
app = Flask(__name__)
music_dir = 'static/music'    # this is the local directory that stores your music file
ALLOWED_EXTENSIONS = set(['mp3', 'wav'])   # this is allowed extension of the music files


usage = """

Usage: python expRUN [FULL_MODE]

- If FULL_MODE is not specified, it will run in DEBUG MODE (only vtrend is loaded)
"""
print usage

"""
This is the main function.
It renders a template that stores under template/index.html
It calls a function in receiver/bokehselect that let javascript and python talks
It
The html (footer.html) loads our javascript functions to help handle the interaction on the web page
For more detail about the interaction of selection, see static/js/byexample.js
"""

DEBUG_MODE = 0 if len(sys.argv)==2 else 1
ee_b = ExpresiveEngine(debug_mode=DEBUG_MODE, model_name='beethoven')
ee_c = ExpresiveEngine(debug_mode=DEBUG_MODE, model_name='chopin')
ee = ee_b

@app.route('/')  #exploratorium
def index():

    XML_FILES = [
        #
        # HACK: change between beethoven/chopin, change 'model_dir' in ee.py
        #
        "testdata/xml_test_beethoven/beethoven_op002_no1_mv1.xml",
        "testdata/xml_test_beethoven/beethoven_op002_no1_mv2.xml",
        #"testdata/xml_test_beethoven/beethoven_op002_no1_mv3.xml",
        #"testdata/xml_test_beethoven/beethoven_op054_mv1.xml",
        "testdata/xml_test_beethoven/beethoven_op090_mv2.xml",
        "testdata/xml_test_chopin/chopin_op35_Mv1.xml",
        "testdata/xml_test_chopin/chopin_op60.xml",
        "testdata/xml_test_chopin/chopin_op72_No1.xml",
        "testdata/xml_test_chopin/chopin_op28_No11.xml",
    ]
    SONG_NAMES = [a.split('/')[-1].replace('.xml','') for a in XML_FILES]

    # Plot some dummy coordinates
    source = ColumnDataSource(data=dict(
        x = [3.8, 3,     4, 1.9, 1.8, 1.2, 2.7],
        y = [4,   4.2, 4.5, 3.1, 3.5,   3, 3.2],
        color = ["navy", "blue", "olive", "firebrick", "red", "gold", "orange"],
        wavName = XML_FILES,
        composer = [x.split('/')[1].split('_')[-1] for x in XML_FILES],
        songName = SONG_NAMES))

    # music_file = "1.wav"
    # The hover tool so that I can hover the mouse onto a datapoint to see something
    hover = HoverTool(tooltips=[
      ("index", "$index"),
      ("composer","@composer"),
      ("songname","@songName")])
    # ("(x,y)", "($x, $y)")])

    callback = CustomJS(args=dict(source=source),code="""
        var data = source.data;
        console.log(cb_data)
        var idx = cb_data.source.selected['1d'].indices[0]
        var songname = data.wavName[idx]
        var song_prettyname = data.songName[idx]

        function sendToEditor(songname, song_prettyname){
            var pass_params = {
                name: songname,
                song_prettyname: song_prettyname,
                'vtrend': 1,
                'ibi': 1,
            };
            console.log("[js->python]",pass_params)
            $.ajax({
                    url: "receiver/bokehselect",
                    type: "POST",
                    data: JSON.stringify(pass_params),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function( result ){
                        console.log("[python -> js] ", result)
                        byexample_currsongname = songname
                        document.getElementById("songname").innerHTML = song_prettyname
                        plot_charts(result['yout_dict'], true)
                        wavesurfer_inspect.load(result['new_wave_name'])
                        console.log(result['new_wave_name'])
                        // update master slider
                        update_master_slider_value(result['master_slider_value'], result['master_slider_percent'])
                        // update audio
                        update_html5_audio_div('audio_original', result['new_wave_name'])
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    },
                });
        }

        sendToEditor(songname, song_prettyname)
        """)
        # % update_waveform_string)

    # somehow needs to call this taptool from bokeh library
    taptool = TapTool(callback = callback)
    xLab = "Chopin ---------------------------------------- Beethoven"

    # plot the data
    p = figure(plot_width=400, plot_height=400,x_axis_label = xLab, y_axis_label= "",
        tools=[hover,taptool],title="Click and Listening")
    p.circle('x','y',color='color',
        size=15,
        source = source)

    # grab the static resources
    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()

    # deal with this figure in bokeh
    script, div = components(p)

    # pass in the variables and render the template
    return render_template('index.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources,
        music_file = '')


"""
Upload Handling
Ignore this for now
"""
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/receiver/bokehselect', methods=['POST'])
def process_bokeh_select():
    """
    Checks wave file under 'static/music':
        - if it doesn't exist, it will generate MIDI and wave on the fly
    Returns:
        - the weights (yout_dict)
        - the wave filename
    """
    data = request.get_json()
    songname = data['name']
    print '> Switch bokeh to ', songname

    # prepares the wave for the xml file
    global ee
    if 'beethoven' in songname:
        ee = ee_b
    else:
        ee = ee_c
    # 1. compute weights
    ee.reload_xml(songname)
    yout_dict = ee.get_weights()

    new_wave_name = 'static/music/%s-change.wav' % ee.get_piecename()
    if not os.path.isfile(new_wave_name):
        # 2. compute midi
        ee.get_expressive_midi({})
        # 3. compute wave
        new_wave_name = ee.get_expressive_wave()
    else:
        print '> Loading pre-genearted wav:', new_wave_name
    # 4. update master slider
    master_slider_value, master_slider_percent = ee.get_master_slider_value()

    return jsonify({
        "status": "done",
        "new_wave_name": new_wave_name,
        "yout_dict": yout_dict,
        "master_slider_value": master_slider_value,
        "master_slider_percent": master_slider_percent,
        })

"""
The following functions' aim are to let javascript talk to python
"""
@app.route('/receiver/exampleselect', methods=['POST'])
def process_sel():
    data =  request.get_json()
    print 'getting song: %s from: %s to ' % \
        (data['songname'], str(data['start']), str(data['end']))
    return jsonify({
        "status": "done",
        "start": data['start'],
        "end": data['end']
        })

@app.route('/receiver/get_weights', methods=['POST'])
def process_get_weights():
    global ee
    # print "> (process_get_weights)", ee.get_piecename()
    data =  request.get_json()
    yout_dict = ee.get_weights()
    # polynomial fitting
    return jsonify({
        "status": "done",
        "yout_dict": yout_dict,
        })


# This function's aim is to let javascript talk to python
@app.route('/receiver/expressive_midi', methods=['POST'])
def process_expression():
    global ee
    # print "> (get_expressive_midi)", ee.get_piecename()
    data = request.get_json()
    new_midi_file = ee.get_expressive_midi(data)
    return jsonify({
        "status": "done",
        "new_midi_name": new_midi_file
        })


# This function's aim is to let javascript talk to python
@app.route('/receiver/expressive_wave', methods=['POST'])
def process_expressive_wave():
    global ee
    # print "> (get_expressive_wave)", ee.get_piecename()
    data = request.get_json()
    new_wave_file = ee.get_expressive_wave()
    return jsonify({
        "status": "done",
        "new_wave_name": new_wave_file
        })

"""
The main 'main' function that does whatever
"""
if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host = '127.0.0.1', debug=False)
