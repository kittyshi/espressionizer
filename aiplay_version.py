from keras.models import load_model
import aiplay
import prepare_data
from ofaidata_utils.data_handling.musicxml import parse_music_xml
from keras import backend as K
from performance_generator import Performance
import numpy as np
import os

def coeff_determination(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )



def predict_regression1207(input_data, curr_model, piece_length):
    Y = {}
    out_data = curr_model.predict(input_data)
    #piece_length = len(score.score_features)
    Y['vtrend'] = out_data[0].reshape((-1,out_data[0].shape[-1]))[:piece_length,:] * 127.
    Y['vdev'] = out_data[1].reshape((-1,out_data[1].shape[-1]))[:piece_length,:] * 127.
    Y['ibi'] = out_data[2].reshape((-1,out_data[2].shape[-1]))[:piece_length,:]
    Y['time'] = out_data[3].reshape((-1,out_data[3].shape[-1]))[:piece_length,:]
    Y['art'] = out_data[4].reshape((-1,out_data[4].shape[-1]))[:piece_length,:]
    Y['sustainpedal'] = out_data[5].reshape((-1,out_data[5].shape[-1]))[:piece_length,:] /127.
    Y['softpedal']  = out_data[6].reshape((-1,out_data[6].shape[-1]))[:piece_length,:]
    return Y

def sample(preds):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    exp_preds = np.exp(np.log(preds))
    preds = exp_preds / np.sum(exp_preds)        # softmax
    probas = np.random.multinomial(1, preds, 1)  # draw one example
    return np.argmax(probas)

def aiplay_1203(model_path = 'models/20191204/20191204_bigbilstm_chopin_128sqlen_200epch_9000bs_48unit_0.4dr_0.001lr_1bilstm_bn.h5',
                mydir = 'testdata/xml_test_chopin/'):
    curr_model = load_model(model_path,
                            custom_objects={'coeff_determination': coeff_determination})

    for fn in os.listdir(mydir):
        if fn.endswith('.xml'):
            xml_fn = mydir + fn
            score = aiplay.read_xml(xml_fn)
            piece_name = xml_fn.split('/')[-1].split('.')[0]
            input_data = prepare_data.slice_test_data(score.score_features, 64)

            # big model
            in_notes = np.zeros((1,64,397))
            in_notes[:,:,:330] = score.score_features[0:64]
            out_params = np.zeros((score.score_features.shape[0],67))
            out_bin = np.zeros((score.score_features.shape[0],7))

            for i in range(score.score_features.shape[0]-63):
                #print i
                # predict and sample from distribution
                next_note = curr_model.predict(in_notes)[0,-1]
                next_vel = sample(next_note[:15])   # velocity
                next_veldev = sample(next_note[15:25]) # vel-dev
                next_ibi = sample(next_note[25:42])
                next_time = sample(next_note[42:50])
                next_art = sample(next_note[50:65])
                next_sus = next_note[65]
                next_soft = next_note[66]

                # fill in out_bin for performance construction, and out_params to feed into the input
                out_bin[i,0] = next_vel
                out_bin[i,1] = next_veldev
                out_bin[i,2] = next_ibi
                out_bin[i,3] = next_time
                out_bin[i,4] = next_art
                out_bin[i,5] = next_sus
                out_bin[i,6] = next_soft
                out_params[i,next_vel] = 1
                out_params[i,next_veldev+15] = 1
                out_params[i,next_ibi+25] = 1
                out_params[i,next_time+42] = 1
                out_params[i,next_art+50] = 1
                out_params[i,65] = next_sus
                out_params[i,66] = next_soft

                in_notes[:,:63] = in_notes[:,1:]
                in_notes[:,:,:330] = score.score_features[i:64+i]
                in_notes[:,:,330:] = out_params[i]
            Y = {}

            Y['vtrend'] = out_params[:,:15]
            Y['vdev'] = out_params[:,15:25]
            Y['ibi'] = out_params[:,25:42]
            Y['time'] = out_params[:,42:50]
            Y['art'] = out_params[:,50:65]
            Y['sustainpedal'] = out_params[:,65]

            p = Performance(piece_name, score, Y, 80,
                            'output/20191204/' + piece_name + '.mid',
                           option = 0, add_pedal = False)

