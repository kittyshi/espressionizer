import numpy as np
import pretty_midi
import helper

# Generate a performance given values
class Performance:
    def __init__(self, piece_name, score, Y, out_name = None, bpm = 120, add_pedal = True):
        """
        Option 0: decode from direct velocity
        Option 1: decode from increase velocity
        """
        #piece_name = xml_fn.split('/')[-1].split('.')[0]
        self.out_name = out_name
        if out_name is None:
            self.out_name = self.piece_name + '_out.mid'
        self.score = score
        self.vels = (Y['vtrend']-Y['vdev'])*127 # vel_onset
        #self.vel_diffs = Y['vdev']             # vel_diff_onset
        self.bpms = Y['ibi']                    # bpm_onset_norm
        #self.mbpm = Y['mbpm']
        #self.ibpm = Y['ibpm']
        #self.bpms = Y['mbpm'] + Y['ibpm']
        #self.bpm_ratios = Y['time']            # bpm_onset_ratio
        self.articulations = 2** Y['art']       # log_articulation
        self.soft_pedals = Y['soft_pedal']
        self.sustain_pedals = Y['sus_pedal']

        self.bpm = bpm
        self.add_pedal = add_pedal
        self.decode()
        self.write_midi()


    def decode(self):
#         self.mbpm_average = np.zeros((self.score.N))
#         for m in self.score.dict_measure2xmlidx:
#             xmlidxs = self.score.dict_measure2xmlidx[m]
#             self.mbpm_average[xmlidxs] = np.mean(self.mbpm[xmlidxs])
#         self.bpms = self.mbpm_average + self.ibpm

        self.bpms[self.bpms <= 0] = min(self.bpms[self.bpms > 0])
        self.articulations[self.articulations > 5] = 5
        score_info = helper.get_unique_seq(onsets =self.score.note_onset_beat,
                                           offsets=self.score.note_offset_beat,
                                           unique_onset_idxs=None,
                                           return_diff=True)
        my_bpm = np.array([np.mean(self.bpms[uix]) for uix in self.score.unique_onset_idxs])
        ioi_perf = score_info['diff_u_onset'] / my_bpm
        u_onset_perf = np.cumsum(np.r_[0, ioi_perf])

        self.Onsets = np.zeros((self.score.N))
        self.Durations = np.zeros((self.score.N))
        for i, jj in enumerate(self.score.unique_onset_idxs):
            self.Onsets[jj] = u_onset_perf[i]
            d = self.score.X['duration'][i][0] * self.articulations[jj] / max(0.1,my_bpm[i])  # / self.BPM_AVE
            self.Durations[jj] = d.reshape((len(d)))

    def write_midi(self):
        pm = pretty_midi.PrettyMIDI()
        p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
        piano = pretty_midi.Instrument(program=p_program)

        # Add note one by one according to the score
        for i in range(self.score.N):
            pitch = self.score.note_info['pitch'][i]
            onset = self.score.dict_xmlidx2onset[i]
            onset_time = self.Onsets[i]
            offset_time = onset_time + self.Durations[i]
            v = max(int(np.round(self.vels[i])), 30)
            v = min(v, 110)
            my_note = pretty_midi.Note(velocity=v, pitch=pitch, start=onset_time, end=offset_time)
            piano.notes.append(my_note)

            if self.add_pedal:
                sustain = self.sustain_pedals[i]
                soft = self.soft_pedals[i]
                isSustainOn = False
                isSoftOn = False
                thres_sustain = 0.5
                thres_soft    = 0.5

                if sustain >= thres_sustain and not isSustainOn:
                    isSustainOn = True
                    piano.control_changes.append(pretty_midi.ControlChange(64,100,onset_time))
                elif sustain < thres_sustain and isSustainOn:
                    isSustainOn = False
                    piano.control_changes.append(pretty_midi.ControlChange(64,0,onset_time))
                if soft >= thres_soft and not isSoftOn:
                    isSoftOn = True
                    piano.control_changes.append(pretty_midi.ControlChange(67,100,onset_time))
                elif soft < thres_soft and isSoftOn:
                    isSoftOn = False
                    piano.control_changes.append(pretty_midi.ControlChange(67,0,onset_time))

        pm.instruments.append(piano)
        pm.write(self.out_name)
        print('save midi to: ', self.out_name)