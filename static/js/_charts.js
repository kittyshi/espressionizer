var chart_slider_dict = {
    // keep in sync with slider_name_map in ee.py
    'vtrend': {
        'onset_pitch_max': 50,
        'Downbeat': 50,
        //'Forzando': 50,
        'Slur': 101,
        // 'v40_calando': 50,
        // 'v37_f': 50,
    },
    'ibi': {                    // <-- chart_key
        // 'b30_fermata': 50,      // <-- slider_key
        // 'b49_fz': 50,
        // 'b70_marcato': 50,
        'IOI_prev3': 50,
        'IOI_next3': 50,
        'Beatphase': 50,
    }
}
var chart_title_dict = {
    'vtrend': 'Velocity',
    'ibi': 'Beat Period'
}
var plot_objects_dict = {}

function callPython_get_chart() {
    var data_sel = {
        'vtrend': 1,
        'ibi': 1,
    };
    console.log("[js -> python] ", data_sel)
    $.ajax({
        url: "receiver/get_weights",
        type: "POST",
        data: JSON.stringify(data_sel),
        dataType: 'json',
        contentType: 'application/json',
        success: function(result){
            console.log("python -> js", result)
            plot_charts(result['yout_dict'])
            // result['new_midi_name']
        }
    })
}

function build_chart_slider() {
//  Generate something like this:
//      <div class = "col-md-6" id ='vtrend_div'>
//         <div class = "row" id="vtrend_chart_div">
//             <div id='vtrend_chart'> </div>
//         </div>
//         <div class = "row" id="vtrend_slider_div">
//             <div style="text-align:center"><h4>Velocity</h4></div>
//             <div id='vtrend_sliders'> </div>
//         </div>
//     </div>
//     <div class = "col-md-6">
//         <div id='bpr_chart'> </div>
//     </div>
    var output = document.getElementById("charts");

    var chart_width = 12 / Object.keys(chart_slider_dict).length

    for (var chart_key in chart_slider_dict) {
        // build slider html
        var slider_html = ""
        var slider_width = 12 / Object.keys(chart_slider_dict[chart_key]).length
        for (var slider_chart_key in chart_slider_dict[chart_key]) {
            var init_value = 50
            var tmp = `
                        <div class = "col-md-${slider_width}">
                            <input type="range" min="20" max="80" value="${init_value}" class="slider" id="${slider_chart_key}">
                            <p>${slider_chart_key}: <span id="${slider_chart_key}_v"></span></p>
                        </div>
            `
            slider_html += tmp


        }
        // put chart and slider together
        var chart_title = chart_title_dict[chart_key]
        var template = `
            <div class = 'col-md-${chart_width}' id ='${chart_key}_div'>
                <div class = 'row' id='${chart_key}_chart_div'>
                    <div style="text-align:center">
                        <h4>${chart_title}</h4>
                    </div>
                    <div id='${chart_key}_chart' style='height:150px'> </div>
                </div>
                <div class = 'row' id='${chart_key}_slider_div'>
                    <div id='${chart_key}_sliders'>
                    ${slider_html}
                    </div>
                </div>
            </div>
        `
        output.innerHTML += template;
    }
}


build_chart_slider();
callPython_get_chart();

// for (var chart_key in chart_slider_dict) {
//     for (var slider_chart_key in chart_slider_dict[chart_key]) {
//         (function () {
//             document.getElementById(slider_chart_key).oninput = function() {
//                 console.log(slider_chart_key)
//                 document.getElementById(slider_chart_key+"_v").innerHTML = this.value;
//             }
//         }());
//     }
// }

function plot_charts(yout, reset_axis=false) {
    // if reset_axis is true, xmin/xmax will be reset to fit the whole data
    var options = {
        series: {
            curvedLines: {
                active: true
            }
        }
    };
    for (var chart_key in chart_slider_dict) {
        if (!reset_axis & chart_key in plot_objects_dict) {
            xmin = plot_objects_dict[chart_key].getAxes().xaxis.options.min
            xmax = plot_objects_dict[chart_key].getAxes().xaxis.options.max
        } else {
            xmin = 0
            xmax = yout[chart_key].length
        }

        var data = [{
            data: yout[chart_key],
            color: "#0086e5",
            points: {
                symbol: "circle",
                fillColor: "#ffffff",
                radius: 5
            },
            lines: {
                show: true
            },
            points: {
                show: false
            },
            curvedLines: {
                apply: true,
                monotonicFit: true
            }
        }];
        p = $.plot("#" + chart_key + "_chart", data, {
            series: {
                curvedLines: {
                    active: true
                }
            },
            xaxis: {
                tickColor: '#def2ff',
                tickDecimals: 0,
                show: false,
                min: xmin,
                max: xmax,
            },
            yaxis: {
                tickLength: 0,
                show: true,
                // min: 0,
                // max: 1,
            },
            grid: {
                backgroundColor: {
                    colors: ["#effaff", "#d7f3ff"]
                },
                borderWidth: 0
            }
        });
        plot_objects_dict[chart_key] = p
    }


    /*series: {
                lines: { show: true },
                points: { show: true },
                curvedLines: {active: true}
            },*/

}

function callPython_get_expressive_midi(){
    // build midi
    var data_sel = {};
    for (var chart_key in chart_slider_dict) {
        data_sel[chart_key] = {}
        for (var slider_chart_key in chart_slider_dict[chart_key]) {
            data_sel[chart_key][slider_chart_key] = document.getElementById(slider_chart_key).value
        }
        data_sel['start'] = plot_objects_dict[chart_key].getAxes().xaxis.options.min
        data_sel['end'] = plot_objects_dict[chart_key].getAxes().xaxis.options.max
    }
    data_sel['master_tempo'] = document.getElementById('master_tempo_slider').value;
    data_sel['master_bpr_mean'] = document.getElementById('master_bpr_mean_slider').value;
    data_sel['master_bpr_std'] = document.getElementById('master_bpr_std_slider').value;
    data_sel['master_art_mean'] = document.getElementById('master_art_mean_slider').value;
    data_sel['master_art_std'] = document.getElementById('master_art_std_slider').value;

    console.log("[js -> python]", data_sel)
    $.ajax({
        url: "receiver/expressive_midi",
        type: "POST",
        data: JSON.stringify(data_sel),
        dataType: 'json',
        contentType: 'application/json',
        success: function(result){
            console.log("python -> js", result)
            callPython_get_chart()
        }

    })
}

function update_html5_audio_div(audio_obj_name, new_wave_name, auto_play=false){
    audio = document.getElementById(audio_obj_name)
    document.getElementById(audio_obj_name + '_src').src = new_wave_name
    audio.load()
    if (auto_play) {
        audio.play()
    }
}

function update_master_slider_value(master_slider_value, master_slider_percent) {
    document.getElementById('master_tempo_slider').value = master_slider_percent['master_tempo'];
    document.getElementById('master_bpr_mean_slider').value = master_slider_percent['master_bpr_mean'];
    document.getElementById('master_bpr_std_slider').value = master_slider_percent['master_bpr_std'];
    document.getElementById('master_art_mean_slider').value = master_slider_percent['master_art_mean'];
    document.getElementById('master_art_std_slider').value = master_slider_percent['master_art_std'];

    document.getElementById('master_tempo_slider_v').innerHTML = master_slider_value['master_tempo'].toFixed(2);
    document.getElementById('master_bpr_mean_slider_v').innerHTML = master_slider_value['master_bpr_mean'].toFixed(2);
    document.getElementById('master_bpr_std_slider_v').innerHTML = master_slider_value['master_bpr_std'].toFixed(2);
    document.getElementById('master_art_mean_slider_v').innerHTML = master_slider_value['master_art_mean'].toFixed(2);
    document.getElementById('master_art_std_slider_v').innerHTML = master_slider_value['master_art_std'].toFixed(2);
}

function callPython_get_expressive_wave(){
    // build data_set
    var data_sel = {};
     for (var chart_key in chart_slider_dict) {
        data_sel[chart_key] = {}
        for (var slider_chart_key in chart_slider_dict[chart_key]) {
            data_sel[chart_key][slider_chart_key] = document.getElementById(slider_chart_key).value
        }
    }
    console.log("[js -> python]", data_sel)
    $.ajax({
        url: "receiver/expressive_wave",
        type: "POST",
        data: JSON.stringify(data_sel),
        dataType: 'json',
        contentType: 'application/json',
        success: function(result){
            console.log("python -> js", result)
            wavesurfer_inspect.load(result['new_wave_name'])
            update_html5_audio_div('audio_new', result['new_wave_name'], auto_play=true)
        }

    })
}




