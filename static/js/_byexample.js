// global variable of current selected sample
var g_songname
var g_fromStart
var g_fromEnd

var TimelinePlugin = window.WaveSurfer.timeline
var RegionsPlugin = window.WaveSurfer.regions

var last_selected_region = null
var byexample_currsongname = ""
var like_list = [];
var dislike_list = [];

var slider_dict = {}

// wavesurfer inspector
var wavesurfer_inspect = WaveSurfer.create({
                container: '#waveform_inspect',
                waveColor: 'red',
                progressColor: 'yellow',
                scrollParent: true,
                cursorColor: '#fff',
                barWidth: 0.7,
                plugins: [
                    TimelinePlugin.create({
                         container: '#waveform_inspect-timeline'
                    }),
                    RegionsPlugin.create()
                 ]
});

// wavesurfer under the button
// var wavesurfer_button = WaveSurfer.create({
//     container: '#waveform_button',
//     waveColor: 'violet',
//     progressColor: 'yellow',
//     scrollParent: true,
//     cursorColor: '#fff',
//     barWidth: 0.7,
//     plugins: [
//         RegionsPlugin.create()
//      ]
//     });
// // wavesurfer_button.load("static/music/empty.wav")
// wavesurfer_button.load("")

var playing_inspect = false;

// play pause
function playPause_inspect() {
    if (playing_inspect) {
        wavesurfer_inspect.pause();
        playing_inspect = false;
    } else {
        wavesurfer_inspect.play();
        playing_inspect = true;
    }
    setPlayPauseButton_inspect();
}

function setPlayPauseButton_inspect() {
  if (playing_inspect) {
      $('#playpause_inspect').html('&#9612;&#9612;  Pause');
  } else {
      $('#playpause_inspect').html('&#9658; Play');
  }
}

// wavesurfer_button.on('ready',function () {
//     // clear all the region when it's loaded
//     // wavesurfer_button.enableDragSelection({});
// });

wavesurfer_inspect.on('region-update-end', function (region, e) {
    console.log('selected region %s, (%.1f sec, %.1f sec)', region.id, region.start, region.end)
    var duration = wavesurfer_inspect.getDuration();
    for (var key in plot_objects_dict){
        var plot = plot_objects_dict[key];
        var number_total_data_points = plot.getData()[0].data.length;
        plot.getAxes().xaxis.options.min = region.start / duration * number_total_data_points;
        plot.getAxes().xaxis.options.max = region.end / duration * number_total_data_points;
        plot.setupGrid();
        plot.draw()
    }
});

wavesurfer_inspect.on('ready',function () {
    // clear all the region when it's loaded
    wavesurfer_inspect.enableDragSelection({});
    wavesurfer_inspect.regions.clear();
});

// update last_selected_region when clicking the current region
wavesurfer_inspect.on('region-click',function(region,e){
    // play on click, loop on shift click
    e.shiftKey ? region.playLoop() : region.play();
    // store current region
    console.log("selected region of %s at: %f and %f", byexample_currsongname,region.start, region.end)
    // call python

    last_selected_region = region
});

// wavesurfer_inspect.load("static/music/mozart_kv279_1_1_a.wav")
wavesurfer_inspect.load("static/music/beethoven_op002_no1_mv1.xml-change-original.wav")

function func_refine_select(){

}

function func_refresh_region(){

}

function func_example_select(){
    // When selected, look at example type and selection region
    var example_type = document.getElementById("example_type_select")

    var id_name = (example_type.value == "example_pos")? "preference_like_list":"preference_dislike_list"
    var btn_class = (example_type.value == "example_pos")? "btn btn-info": "btn btn-danger"

    // If no region is selected, then take the whole thing
    if (last_selected_region != null){
        // appendEditorBuffer(wavesurfer_inspect.backend.buffer, last_selected_region.start, last_selected_region.end, option = example_type.value)
        var obj = {
                    songname: byexample_currsongname,
                    start: last_selected_region.start,
                    end: last_selected_region.end,
                    type: example_type.value
                }
    }
    else{
        var obj = {
                    songname: byexample_currsongname,
                    start: 0,
                    end: wavesurfer_inspect.getDuration(),
                    type: example_type.value
                }
    }
    // call python
    // callPython_selectSeg(obj.songname,obj.start,obj.end)

    if (example_type.value == "example_pos"){
        like_list.push(obj)
        nb = like_list.length - 1
    }
    else if (example_type.value == "example_neg"){
        dislike_list.push(obj)
        nb = dislike_list.length - 1
    }
    else{
        console.log("Warning! selected example type unknown!!!")
    }

    // draw according to list/dislike list

    var button = document.createElement("input");
    button.type = "button"
    button.value = nb
    button.id = "buttonid_" + example_type.value + button.value
    button.className =  btn_class
    button.onclick = function(){

        // get which parent does it belong to
        if (document.getElementById(button.id).parentNode.id == "preference_like_list"){
            // when click, lock the song with original selection of region
            var curr_songname = like_list[button.value].songname
            var fromStart = like_list[button.value].start
            var fromEnd = like_list[button.value].end
            // call python
            g_songname = curr_songname
            g_fromStart = fromStart
            g_fromEnd = fromEnd
            //callPython_selectSeg(curr_songname,fromStart,fromEnd)
        }
        else{
            var curr_songname = dislike_list[button.value].songname
            var fromStart = dislike_list[button.value].start
            var fromEnd = dislike_list[button.value].end

        }

        // wavesurfer_button.load("static/music/"+curr_songname)
        // wavesurfer_button.regions.clear()

        // wavesurfer_button.addRegion({
        //     start: fromStart, // time in seconds
        //     end: fromEnd,     // time in seconds
        //     color: 'hsla(100, 100%, 30%, 0.1)'
        //  });

    }
    document.getElementById(id_name).appendChild(button)
    // Add a button below
    // <button type="button" class="btn btn-info">Info</button>
}



function callPython_selectSeg(songname,start,end){
    var data_sel = {
        songname:songname,
        start:start,
        end:end
    };
    console.log("[js -> python]", data_sel)
    $.ajax({
        url: "receiver/exampleselect",
        type: "POST",
        data: JSON.stringify(data_sel),
        dataType: 'json',
        contentType: 'application/json',
        success: function(result){
            console.log("python -> js", result)
        }

    })
}


// Helper functions
function copyBuffer(fromBuffer, fromStart, fromEnd, toBuffer, toStart) {
    var sampleRate = fromBuffer.sampleRate
    var frameCount = (fromEnd - fromStart) * sampleRate
    for (var i = 0; i < fromBuffer.numberOfChannels; i++) {
        var fromChanData = fromBuffer.getChannelData(i)
        var toChanData = toBuffer.getChannelData(i)
        for (var j = 0, f = Math.round(fromStart*sampleRate), t = Math.round(toStart*sampleRate); j < frameCount; j++, f++, t++) {
            toChanData[t] = fromChanData[f]
        }
    }
}
