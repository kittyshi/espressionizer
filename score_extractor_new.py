import numpy as np
from math import gcd
import partitura.score as score
from scipy.interpolate import interp1d
import pickle
import to_class



class ScoreFeatures:
    def __init__(self,part,numerical):
        self.part = part
        self.numerical = numerical
        self.bm = part.beat_map
        self.note_info = np.array([(n.midi_pitch, n.octave, n.voice, n.start.t, n.end.t) for n in self.part.notes_tied],
                            dtype=[('pitch', 'i4'), ('octave', 'i4'), ('voice','i4'),('onset', 'f4'),('offset', 'f4')])



        self.note_onset_beat = self.bm(self.note_info['onset'])
        self.note_offset_beat = self.bm(self.note_info['offset'])
        self.N = len(self.note_info)
        self.unique_onset_idxs = [np.where(self.note_info['onset'] == u)[0] for u in np.unique(self.note_info['onset'])]
        self.onset2realonset = {}
        onset = 0
        for u in np.unique(self.note_info['onset']):
            #self.unique_onset_idxs.append([np.where(self.note_info['onset'] == u)[0]])
            self.onset2realonset[onset] = u
            onset += 1

        self.num_onsets = len(self.unique_onset_idxs)
#        self.dict_onset2measure = {}
        self.dict_xmlidx2onset = {}
        self.dict_measure2onset0 = {}   # measure number to the first onset of that measure
        self.dict_measure2beats = {}
        self.bins = { "dur": [0.5, 0.25, 1.0],
                      "pitch": [63, 54, 73, 48, 59, 68, 79],   # convert from [ 48.  54.  59.  63.  68.  73.  79. ]
                      "slur": [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875],   # convert from in-direction [0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875]
                      "beatphase": [0.5, 0.25, 0.75, 0.125, 0.375, 0.625, 0.875] }
        self.list_dynamic_marking, self.cat_dynamic_marking = pickle.load(open('all_data/cat_dynamic_marking_new.p', "rb"))
        self.list_tempo_marking, self.cat_tempo_marking = pickle.load(open('all_data/cat_tempo_marking_new.p', "rb"))
        self.extract_features()
        self.construct_training()

    def construct_training(self):
        self.score_features57 = np.zeros((self.N, 0))
        for fname in self.Xfeatures:
            data = self.X[fname]
            if self.numerical and fname == 'pitch':
                data /= 127.
            if self.numerical and fname == 'pitch_class':
                data /= 11.
            if self.numerical and fname == 'octave':
                data /= 8.
            if self.numerical and fname == 'voice':
                data /= 10.
            #if self.numerical and fname == 'IOI':
            #    data /= 2.
            #if self.numerical and fname == 'duration':
            #    data /= 3.
            self.score_features57 = np.hstack((self.score_features57, data))

        self.score_features = self.score_features57
        self.Xfeatures += ['pitch2', 'pitch3']
        self.Xfeatures += ['accent','staccato']
        self.Xfeatures += ['phrase_strength']

        self.score_features62 = np.zeros((self.N, 0))
        for fname in self.Xfeatures:
            data = self.X[fname]
            self.score_features62 = np.hstack((self.score_features62, data))


    def extract_features(self):
        self.X = {}
        self.XClass = {}
        self.Xonset = {}
        self.Xonset['IOI'] = np.zeros((self.num_onsets))
        self.Xonset['Highnote'] = np.zeros((self.num_onsets))  # adding highest note for each onset
        self.Xfeatures = []
        self.construct_dict()
        #self.extract_detached_legato()
        self.extract_basics()
        self.extract_pedals()
        self.extract_grace()
        self.extract_articulation_dir()
        self.extract_fermata()
        self.extract_metrical()
        self.extract_slur()
        self.extract_articulations()
        self.extract_loudness()
        self.extract_tempo()
        self.extract_phrase()
        self.extract_measuremean()


    def construct_dict(self):
        self.dict_measure2xmlidx = {}
        self.dict_xmlidx2measure = {}
        for i, note in enumerate(self.part.notes_tied):
            measure = note.measure_index
            self.dict_xmlidx2measure[i] = measure
            if measure not in self.dict_measure2xmlidx:
                self.dict_measure2xmlidx[measure] = [i]
            else:
                self.dict_measure2xmlidx[measure].append(i)

        # for m in range(self.num_measures):
        # for onset in range(self.num_onsets):
        #     xml_idxs = self.unique_onset_idxs[onset]
        #     measure_no = self.dict_xmlidx2measure[xml_idxs[0]]
        #     self.dict_measure2onset0[measure_no] = onset

        self.measure_nos = [c for c in self.dict_measure2xmlidx.keys()]
        # this could be the actual num measures + 1 (if one index)
        self.num_measures = self.measure_nos[-1] + 1
        #self.num_measures = self.measure_nos[-1] - self.measure_nos[0] + 1
        self.dict_slur2noteidx = {}

    def extract_measuremean(self):
        self.Xmeasure = {}
        for fname in self.Xfeatures:
            self.Xmeasure[fname] = np.zeros((self.num_measures,self.X[fname].shape[1]))

        self.Xmeasure_features_raw = np.zeros((self.num_measures,0))
        for measure in self.measure_nos:
            if measure in self.dict_measure2xmlidx:
                note_idxs = self.dict_measure2xmlidx[measure]
                for fname in self.Xfeatures:
                    self.Xmeasure[fname][measure] = self.X[fname][note_idxs].mean()

        self.Xmeasure_feature_raw = self.Xmeasure['pitch'].copy()
        for fname in self.Xfeatures:
            if fname != 'pitch':
                self.Xmeasure_feature_raw = np.hstack((self.Xmeasure_feature_raw,self.Xmeasure[fname]))



    def extract_basics(self):
        # pitch information (pitch, pitch class, octave, voice, minmax)
        pitches = self.note_info['pitch'].astype(np.float)
        self.X['pitch'] = pitches.reshape(self.N,1)
        self.XClass['pitch'] = 128
        self.X['pitch2'] = self.X['pitch']**2 / (127*127)
        self.X['pitch3'] = self.X['pitch']**3 / (127*127*127)
        self.X['pitch_class'] = self.X['pitch'] % 12
        self.XClass['pitch_class'] = 12
        self.X['octave'] = self.note_info['octave'].reshape(self.N,1)
        self.X['octave'] = self.X['octave'].astype('float')
        self.XClass['octave'] = 8
        self.X['voice']  = self.note_info['voice'].reshape(self.N,1)
        self.X['voice'] = self.X['voice'].astype('float')
        self.XClass['voice'] = 10
        self.X['pitch_minmax'] = np.zeros((self.N,1))
        self.XClass['pitch_minmax'] = 3
        self.X['IOI'] = np.zeros((self.N, 1))
        self.XClass['IOI'] = 6    # see to_class
        self.Xonset['p_rest'] = np.zeros((self.num_onsets))

        # onset2 pitch
        self.onset2pitches = {}
        for onset, idxs in enumerate(self.unique_onset_idxs):
            realonset = self.onset2realonset[onset]
            realoffset = np.min([self.note_info[idx]['offset'] for idx in idxs])
            # loop through all notes, find simultaneous note
            for note in self.note_info:
                if note['onset'] == realonset or (note['onset'] < realonset and note['offset'] >= realoffset):
                    if onset in self.onset2pitches:
                        self.onset2pitches[onset].append(note['pitch'])
                    else:
                        self.onset2pitches[onset] = [note['pitch']]


        for onset, idxs in enumerate(self.unique_onset_idxs):
            # min-max pitches, 1 if if max pitch or single note, -1 if min pitch, 0 else
            #self.Xonset['Highnote'][onset] = np.max(self.X['pitch'][idxs])
            self.Xonset['Highnote'][onset] = np.max(self.onset2pitches[onset])
            pitches = self.onset2pitches[onset]
            if len(pitches) == 1:
                self.X['pitch_minmax'][idxs[0]] = 1
                self.dict_xmlidx2onset[idxs[0]] = onset
            else:
                for idx in idxs:
                    self.dict_xmlidx2onset[idx] = onset
                    my_pitch = self.X['pitch'][idx]
                    if my_pitch == max(pitches):
                        self.X['pitch_minmax'][idx] = 1
                    elif my_pitch == min(pitches):
                        self.X['pitch_minmax'][idx] = -1
                    else:
                        self.X['pitch_minmax'][idx] = 0

            #print(onset)
            # if len(idxs) == 1:
            #     self.X['pitch_minmax'] [idxs[0]] = 1
            #     self.dict_xmlidx2onset[idxs[0]] = onset
            # else:
            #     pitches = self.X['pitch'][idxs]
            #     for idx in idxs:
            #         self.dict_xmlidx2onset[idx] = onset
            #         if self.X['pitch'][idx] == pitches.min():
            #             self.X['pitch_minmax'][idx] = -1
            #         elif self.X['pitch'][idx] == pitches.max():
            #             self.X['pitch_minmax'][idx] = 1
            #         else:
            #             self.X['pitch_minmax'][idx] = 0
            # IOI
            curr_startbeat =  self.note_onset_beat[idxs[0]]
            if onset < len(self.unique_onset_idxs) - 1:
                next_idxs = self.unique_onset_idxs[onset+1]
                next_startbeat = self.bm(self.note_info['onset'][next_idxs][0])
                self.X['IOI'][idxs] = next_startbeat - curr_startbeat
                self.Xonset['IOI'][onset] = next_startbeat - curr_startbeat
                if not self.numerical:
                    self.X['IOI'][idxs], _ = to_class.convert_ioi(next_startbeat - curr_startbeat)
            # get previous offset to calculate rest in LBDM
            if onset - 1 >= 0:
                prev_idxs = self.unique_onset_idxs[onset-1]
                # get highest pitch as melody line
                prev_idx = prev_idxs[np.argmax(self.X['pitch'][prev_idxs])]
                prev_endbeat = self.note_offset_beat[prev_idx]
                self.Xonset['p_rest'][onset] = curr_startbeat - prev_endbeat

        # duration
        nd = np.array([(n.start.t, n.end_tied.t) for n in self.part.notes_tied])
        self.X['duration'] = self.bm(nd[:,1]) - self.bm(nd[:,0])
        self.X['duration'] = self.X['duration'].reshape((self.N,1))
        self.X['duration_raw'] = self.X['duration'].copy()
        if not self.numerical:
            for i in range(self.N):
                self.X['duration'][i] = to_class.convert_dur_new(self.X['duration_raw'][i])
        self.Xfeatures += ['pitch','pitch_class','octave','voice','pitch_minmax',
                           'IOI','duration']
        self.XClass['duration'] = 9


    def extract_pedals(self):
        pedals = list(self.part.iter_all(
                score.PedalDirection, include_subclasses=True))
        self.X['sustainpedal'] = np.zeros((self.N, 1))
        for i in range(self.N):
            for pedal in pedals:
                if(pedal.start and pedal.end):
                    if pedal.start.t <= self.note_info[i]['onset'] <= pedal.end.t:
                        self.X['sustainpedal'][i] = 1

        self.Xfeatures += ['sustainpedal']
        self.XClass['sustainpedal'] = 2


    def extract_grace(self):
        # grace note, length, position
        self.X['grace'] = np.zeros((self.N,1))
        self.X['grace_n'] = np.zeros((self.N,1))
        self.X['grace_pos'] = np.zeros((self.N,1))
        for i, n in enumerate(self.part.notes_tied):
            if isinstance(n,score.GraceNote):
                self.X['grace'][i] = 1
                self.X['grace_n'][i] = n.grace_seq_len
                self.X['grace_pos'][i] = n.grace_seq_len - sum(1 for _ in n.iter_grace_seq()) + 1

        self.Xfeatures += ['grace']#, 'grace_n','grace_pos']
        self.XClass['grace'] = 2

    # basis funcion activation from ofai basis mixer (github.com/OFAI/basismixer)
    def basis_function_activation(self,direction):
        epsilon = 1e-6
        direction_end = -1
        if isinstance(direction, (score.DynamicLoudnessDirection,
                                  score.DynamicTempoDirection)):
            # a dynamic direction will be encoded as a ramp from d.start.t to
            # d.end.t, and then a step from d.end.t to the start of the next
            # constant direction.

            # There are two potential issues:

            # Issue 1. d.end is None (e.g. just a ritardando without dashes). In this case
            if direction.end:
                direction_end = direction.end.t
            else:
                # assume the end of d is the end of the measure:
                measure = next(direction.start.iter_prev(score.Measure, eq=True), None)
                if measure:
                    direction_end = measure.start.t
                else:
                    # no measure, unlikely, but not impossible.
                    direction_end = direction.start.t

        if isinstance(direction, score.TempoDirection):
            next_dir = next(direction.start.iter_next(
                score.ConstantTempoDirection), None)
            if isinstance(direction, score.ArticulationDirection):
                next_dir = next(direction.start.iter_next(
                    score.ConstantArticulationDirection), None)
            else:
                next_dir = next(direction.start.iter_next(
                    score.ConstantLoudnessDirection), None)

            if next_dir:
                # TODO: what do we do when next_dir is too far away?
                sustained_end = next_dir.start.t
            else:
                # Issue 2. there is no next constant direction. In that case the
                # basis function will be a ramp with a quarter note ramp
                sustained_end = direction_end + direction.start.quarter

            x = [direction.start.t,
                 direction_end - epsilon,
                 sustained_end - epsilon]
            y = [0, 1, 1]

        elif isinstance(direction, (score.ConstantLoudnessDirection,
                                    score.ConstantArticulationDirection,
                                    score.ConstantTempoDirection)):
            x = [direction.start.t - epsilon,
                 direction.start.t,
                 direction.end.t - epsilon,
                 direction.end.t]
            y = [0, 1, 1, 0]

        else:  # impulsive
            x = [direction.start.t - epsilon,
                 direction.start.t,
                 direction.start.t + epsilon]
            y = [0, 1, 0]

        return interp1d(x, y, bounds_error=False, fill_value=0)

    # from basismixer
    def extract_loudness(self):
        # piano, forte, crescendo, etc.
        onsets = self.note_info['onset']
        directions = list(self.part.iter_all(
                score.LoudnessDirection, include_subclasses=True))

        def to_name(d):
            if isinstance(d, score.ConstantLoudnessDirection):
                return d.text
            elif isinstance(d, score.ImpulsiveLoudnessDirection):
                return d.text
            elif isinstance(d, score.IncreasingLoudnessDirection):
                return 'loudness_incr'
            elif isinstance(d, score.DecreasingLoudnessDirection):
                return 'loudness_decr'
        basis_by_name = {}
        for d in directions:
            j, bf = basis_by_name.setdefault(to_name(d),
                                             (len(basis_by_name), np.zeros(self.N)))
            bf += self.basis_function_activation(d)(onsets)
        self.X['loudness'] = np.zeros((self.N, len(basis_by_name)))
        self.loudnessnames = [None] * len(basis_by_name)

        self.X['Dynamics'] = np.zeros((self.N, len(self.cat_dynamic_marking)))

        for name, (j, bf) in basis_by_name.items():
            if name not in self.cat_dynamic_marking:
                print('%s not in cat_dynamic_marking' % name)
            else:
                self.X['Dynamics'][:, self.cat_dynamic_marking[name]] = bf
            self.X['loudness'][:, j] = bf
            self.loudnessnames[j] = name
        self.Xfeatures += ['Dynamics']

    # from basismixer
    def extract_tempo(self):
        N = self.N
        onsets = self.note_info['onset']

        directions = list(self.part.iter_all(
            score.TempoDirection, include_subclasses=True))

        def to_name(d):
            if isinstance(d, score.ResetTempoDirection):
                ref = d.reference_tempo
                if ref:
                    return ref.text
                else:
                    return d.text
            elif isinstance(d, score.ConstantTempoDirection):
                return d.text
            elif isinstance(d, score.IncreasingTempoDirection):
                return 'tempo_incr'
            elif isinstance(d, score.DecreasingTempoDirection):
                return 'tempo_decr'

        basis_by_name = {}
        for d in directions:
            j, bf = basis_by_name.setdefault(to_name(d),
                                             (len(basis_by_name), np.zeros(N)))
            bf += self.basis_function_activation(d)(onsets)

        self.X['Tempo'] = np.zeros((self.N, len(self.cat_tempo_marking)))
        self.X['tempo'] = np.empty((self.N, len(basis_by_name)))
        self.temponames = [None] * len(basis_by_name)
        for name, (j, bf) in basis_by_name.items():
            if name not in self.cat_tempo_marking:
                print('%s not in cat_tempo_marking' % name)
            else:
                self.X['Tempo'][:, self.cat_tempo_marking[name]] = bf
            self.X['tempo'][:, j] = bf
            self.temponames[j] = name
        self.Xfeatures += ['Tempo']

    # from basismixer
    def extract_articulation_dir(self):
        N = self.N
        onsets = self.note_info['onset']

        directions = list(self.part.iter_all(
            score.ArticulationDirection, include_subclasses=True))

        def to_name(d):
            return d.text

        basis_by_name = {}

        for d in directions:

            j, bf = basis_by_name.setdefault(to_name(d),
                                             (len(basis_by_name), np.zeros(N)))
            bf += self.basis_function_activation(d)(onsets)

        self.X['articulation_dir'] = np.empty((len(onsets), len(basis_by_name)))
        self.articulation_dirnames = [None] * len(basis_by_name)

        for name, (j, bf) in basis_by_name.items():

            self.X['articulation_dir'][:, j] = bf
            self.articulation_dirnames[j] = name

    # from basismixer
    def extract_slur(self):
        slurs = self.part.iter_all(score.Slur)
        #return
        # onsets = self.note_info['onset']
        # self.X['slur_incr'] = np.zeros((self.N,1))
        # self.X['slur_decr'] = np.zeros((self.N,1))

        # for slur in slurs:
        #     if not slur.end:
        #         continue
        #     x = [slur.start.t, slur.end.t]
        #     y_inc = [0,1]
        #     y_dec = [1,0]
        #     self.X['slur_incr'] += interp1d(x,y_inc, bounds_error=False,fill_value=0)(onsets)
        #     self.X['slur_decr'] += interp1d(x,y_dec, bounds_error=False,fill_value=0)(onsets)

        # self.Xfeatures += ['slur_incr','slur_decr']

        # Placeholders
        self.slur_infos = []
        self.slur_samenotes = []
        for name in ["is_slur", "slur_grace", "slur_phase1", "slur_pos1"]:
            self.X[name] = np.zeros((self.N, 1))
            self.Xfeatures += [name]
        self.XClass['is_slur'] = 2
        self.XClass['slur_grace'] = 2
        self.XClass['slur_pos1'] = 9
        self.XClass['slur_phase1'] = 9

        #for name in ["slur_phase1_onehot","slur_pos1_onehot"]:
        #    self.X[name] = np.zeros((self.N, to_class.bin_to_bit(self.bins["slur"])))

        # Construct dictionary to map note index to slur group number (index in slur_infos)
        to_slurmain = dict()

        # construct slur information
        slur_no = 0
        for s in slurs:
            # grab all notes belong to the slur
            note_cands = [i for i in range(self.N) if self.note_info[i]['voice'] == s._start_note.voice and
                              (self.note_info[i]['onset'] >= s.start.t and self.note_info[i]['onset'] <= s.end.t)]
            slur_info = {"note_idx": note_cands, "num_notes": len(note_cands),
                         "start_beat": self.bm(s.start.t), "end_beat":self.bm(s.end.t),
                         "num_beats": self.bm(s.end.t) - self.bm(s.start.t)}

            # if only one note, or same start/end time, then probably a grace note
            if slur_info["start_beat"] == slur_info["end_beat"] or len(note_cands) == 1:
                self.X['slur_grace'][note_cands[0]] = 1
            else:
                # if two notes of the same pitch, could be a tie
                if len(note_cands) == 2 and self.note_info['pitch'][note_cands[0]] == self.note_info['pitch'][note_cands[1]]:
                    self.slur_samenotes.append(slur_info)
                else:
                    # otherwise, it's a slur
                    self.slur_infos.append(slur_info)
                    self.dict_slur2noteidx[slur_no] = note_cands
                    slur_no += 1

                    # map note index to slur group number (sometimes one note are in different group), and fill in values
                    for idx in range(len(note_cands)):
                        i = note_cands[idx]    # the note id
                        self.X['is_slur'][i] = 1
                        slur_pos = idx * 1./(len(slur_info["note_idx"])-1)
                        phase    = (self.note_onset_beat[i] - slur_info["start_beat"]) / (slur_info["end_beat"] - slur_info["start_beat"])
                        if i not in to_slurmain:
                            to_slurmain[i] = slur_no - 1
                            # fill in values
                            self.X['slur_pos1'][i] = slur_pos
                            self.X['slur_phase1'][i] = phase
                            if not self.numerical:
                                self.X['slur_pos1'][i] = np.floor(slur_pos/min(self.bins["slur"]))
                                self.X['slur_phase1'][i] = np.floor(phase/min(self.bins["slur"]))

    # from basismixer
    def extract_articulations(self):
        names = ['accent', 'strong-accent', 'staccato', 'tenuto',
                 'detached-legato', 'staccatissimo', 'spiccato',
                 'scoop', 'plop', 'doit', 'falloff', 'breath-mark',
                 'caesura', 'stress', 'unstress', 'soft-accent']
        for name in names:
            self.X[name] = np.zeros((self.N,1))

        basis_by_name = {}
        notes = self.part.notes_tied
        N = self.N
        for i, n in enumerate(notes):
            if n.articulations:
                for art in n.articulations:
                    if art in names:
                        self.X[art][i] = 1
                        #j, bf = basis_by_name.setdefault(
                        #    art,
                        #    (len(basis_by_name), np.zeros(N)))
                        #bf[i] = 1

        # M = len(basis_by_name)
        # self.X['articulation'] = np.empty((N, M))
        # self.artnames = [None] * M

        # for name, (j, bf) in basis_by_name.items():
        #     self.X['articulation'][:, j] = bf
        #     self.artnames[j] = name


    def extract_phrase(self):

        bs = [b for b in self.beat2onset.keys()]
        onset_phrase = []
        self.onbeat_phrasestart = []
        self.onbeat_phraseend = []
        self.onbeat_phraserange = []
        for i,n in enumerate(self.X['detached-legato']):
            if n == 1:
                onset_i = self.dict_xmlidx2onset[i]
                # to avoid duplicate
                if int(self.onset2beat[onset_i]) not in self.onbeat_phrasestart:
                    self.onbeat_phrasestart.append(int(self.onset2beat[onset_i]))
        for b in self.onbeat_phrasestart[1:]:
            self.onbeat_phraseend.append(b-1)
        self.onbeat_phraseend.append(bs[-1])
        for i in range(len(self.onbeat_phrasestart)):
            self.onbeat_phraserange.append(range(self.onbeat_phrasestart[i], self.onbeat_phraseend[i]+1))

        # return
        # onset_phrase = []
        # for i,n in enumerate(self.X['detached-legato']):
        #     if n == 1:
        #         onset_i = self.dict_xmlidx2onset[i]
        #         onset_phrase.append(onset_i)
        # onsets = []
        # phrase_end = 0
        # for onset_i in range(len(onset_phrase)-1):
        #     phrase_start = onset_phrase[onset_i]
        #     phrase_end = onset_phrase[onset_i+1]
        #     onsets.append(range(phrase_start, phrase_end))
        # onsets.append(range(phrase_end, self.num_onsets))

        self.X['phrase_strength'] = np.zeros((self.N,1))
        # for onset in onsets:
        #     nums = np.linspace(0,onset[-1]-onset[0],num=len(onset))/(onset[-1]-onset[0])
        #     for i in range(onset[0], onset[-1]):
        #         xml_idxs = self.unique_onset_idxs[i]
        #         self.X['phrase_strength'][xml_idxs] = nums[i-onset[0]]



    def extract_fermata(self):
        onsets = self.note_info['onset']
        self.X['fermata'] = np.zeros((self.N,1))
        for ferm in self.part.iter_all(score.Fermata):
            self.X['fermata'][onsets == ferm.start.t,0] = 1
        self.Xfeatures += ['fermata']
        self.XClass['fermata'] = 2

    def extract_metrical(self):
        self.onbeat_onseti = []
        self.onset2beat = {}
        self.beat2onset = {}      # onsets belong to this particular beat
        self.beat2onsets = {}     # all onsets belong to this beat and before next beat
        self.beat2xmlidxs = {}    # xml indexs that belong to this beat only
        self.beat2xmlidxsall = {} # xml indexs that belong to this beat and before next beat
        beat_count = 0
        self.measure_startbeat = self.bm(np.array([m.start.t for m in self.part.iter_all(score.Measure)]))
        ts = np.array([(ts.start.t, ts.beats, ts.beat_type)
                                for ts in self.part.iter_all(score.TimeSignature)])
        tsi = np.searchsorted(ts[:, 0], self.note_info['onset'], side='right') - 1
        # Which beat starts a measure = bm(measure_start_time) -- downbeat
        self.measure_indexs = [n.measure_index for n in self.part.notes_tied]
        #self.num_measures = len(self.measure_startbeat)
        msi = np.searchsorted(self.measure_startbeat, self.note_onset_beat, side='right') - 1
        self.X['downbeat'] = np.zeros((self.N, 1))
        self.X['beatphase'] = np.zeros((self.N, 1))
        self.eps = 0.001

        first_onbeat = False
        for onset_i, index in enumerate(self.unique_onset_idxs):
            if self.note_onset_beat[index[0]] < self.eps:
                m_position = self.note_onset_beat[index[0]]
            else:
                m_position = self.note_onset_beat[index[0]] - self.measure_startbeat[msi[index[0]]]
            ts_num = ts[tsi[onset_i], 1]
            factor = gcd(ts_num, ts[tsi[onset_i], 2])
            #beat_loc = np.mod(m_position, ts_num/factor)   # i.e 12/8, factor = 4, then every 3
            beat_loc = np.mod(m_position, 1.0)
            # update inter-quarter-or-basic-beat-onset
            if beat_loc == 0:
                if first_onbeat == False:
                    self.first_onbeat_val = self.note_onset_beat[index]
                    first_onbeat = True
                    curr_beat = 0
                else:
                    #curr_beat = self.note_onset_beat[index].mean() - self.note_onset_beat[0]  # force the 0th beat to be beat 0, doesn't work very well on everything
                    curr_beat = self.note_onset_beat[index].mean() - self.first_onbeat_val[0]  # force the first beat-beat to be beat 0
                self.onset2beat[onset_i] = curr_beat
                self.beat2onset[int(curr_beat)] = onset_i
                self.onbeat_onseti.append(onset_i)
                #self.onset2beat[onset_i] = beat_count
                #self.beat2onset[beat_count] = onset_i
                #beat_count += self.note_offset_beat[index].mean() - self.note_onset_beat[index].mean()
                #beat_count = int(np.round(beat_count))
                #beat_count += 1
            #else:
            #    beat_count += self.note_offset_beat[index].mean() - self.note_onset_beat[index].mean()
            beatphase =  np.mod(m_position, ts_num)
            for i in index:
                if beatphase == 0:
                    self.X['downbeat'][i] = 1
                self.X['beatphase'][i] = beatphase

        #self.Xfeatures += ['downbeat','beatphase']
        self.Xfeatures += ['downbeat']
        self.XClass['downbeat'] = 2

        # construct beat to onsets
        bs = [b for b in self.beat2onset.keys()]
        next_b = 0
        for i in range(len(bs)-1):
            curr_b = bs[i]
            next_b = bs[i+1]
            onsets = []
            for onset_i in range(self.beat2onset[curr_b], self.beat2onset[next_b]):
                onsets.append(onset_i)
            self.beat2onsets[curr_b] = onsets
        self.beat2onsets[next_b] = list(np.arange(self.beat2onset[next_b],len(self.unique_onset_idxs)))

        #construct beat to indexs
        for b in bs:
            onset = self.beat2onset[b]
            the_idxs = list(self.unique_onset_idxs[onset])
            self.beat2xmlidxs[b] = np.array((the_idxs))

            onsets_all = self.beat2onsets[b]
            the_idxs_all = []
            for o in onsets_all:
                xmlidxs = self.unique_onset_idxs[o]
                the_idxs += list(xmlidxs)
            self.beat2xmlidxsall[b] = np.array((the_idxs))

        self.num_beats = bs[-1] + 1

        # construct measure dictionary
        for m in self.dict_measure2xmlidx:
            xml_idxs = self.dict_measure2xmlidx[m]
            onsets = [self.dict_xmlidx2onset[x] for x in xml_idxs] # if x in self.dict_xmlidx2onset]
            self.dict_measure2onset0[m] = min(onsets)
            self.dict_measure2beats[m] = np.unique(np.array(([self.onset2beat[o] for o in onsets if o in self.onset2beat])))
            #onset_idxs = []
