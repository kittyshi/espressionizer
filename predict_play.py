import torch
import torch.nn as nn
import torch.nn.functional as F
import partitura
from score_extractor_new import ScoreFeatures
import pretty_midi
import numpy as np

def f2onehot(features):
    cats = [128,12,8,10,3,6,9,2,2,2,2,2,2,9,9]
    f = features[0]
    one_hot = np.zeros((cats[0]))
    one_hot[int(f)] = 1

    for i in range(1, len(cats)):
        f = features[i]
        if i == 4:
            f = f + 1
        the_one_hot = np.zeros((cats[i]))
        the_one_hot[int(f)] = 1
        one_hot = np.hstack((one_hot, the_one_hot))

#     num_features = 57
#     num_dynamics = 19
#     num_tempo = 23
#     for i in range(15, 15 + num_dynamics):
#         f = feature_m[i]
#         the_one_hot = np.zeros((2))
#         the_one_hot[int(f)] = 1
#         one_hot = np.hstack((one_hot, the_one_hot))
    return one_hot

class RNNModule(nn.Module):
    def __init__(self, input_size, seq_size, lstm_size, output_size,
                 num_lstm = 2,dr = 0.3):
        super(RNNModule, self).__init__()
        self.seq_size = seq_size
        self.lstm_size = lstm_size
        self.num_lstm = num_lstm
        self.dr = dr
        self.lstm = nn.LSTM(input_size,
                            lstm_size,
                            num_layers = num_lstm,
                            dropout = dr,
                            batch_first=True)
        
        self.bn = nn.BatchNorm1d(lstm_size)
        self.dense = nn.Linear(lstm_size, output_size)
        #self.out   = nn.Softmax()

    def forward(self, x, prev_state):
        output, state = self.lstm(x, prev_state)
        output = self.dense(output)
        
        return output, state
    
    def init_hidden(self, batch_size):
        return (torch.randn(self.num_lstm, batch_size, self.lstm_size),
                torch.randn(self.num_lstm, batch_size, self.lstm_size))


def play(xml_fn, IS_SIMPLE = True, out_dir='all_data/output/0208/simple/',ext='_60f',v0=80, bpm0 = 6):
    seq_size     = 256
    lstm_size    = 256
    
    # load model
    #device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = torch.device("cpu")
    if IS_SIMPLE:
        input_vector_dim1 = 387
        input_vector_dim2 = 4
    else:
        input_vector_dim1 = 465  #503 
        input_vector_dim2 = 60
    model = RNNModule(input_vector_dim1, seq_size, lstm_size, 128).to(device).float()
    if IS_SIMPLE:
        model.load_state_dict(torch.load('models/0208/0208-vel-simple_20200209_0156_vel-epoch045-loss1.408.pth'))
    else:
        model.load_state_dict(torch.load('models/0208/0208-vel_20200209_0200_vel-epoch015-loss1.809.pth',map_location='cuda:0'))
    model.eval()
    
    model_bpm = RNNModule(input_vector_dim2, seq_size, lstm_size, 1).to(device).float()
    if IS_SIMPLE:
        model_bpm.load_state_dict(torch.load('models/0208/0208-bpm-simple_20200209_0203_bpm_regression-epoch060-loss0.321.pth'))
    else:
        model_bpm.load_state_dict(torch.load('models/0208/0208-bpm_20200209_0207_bpm_regression-epoch180-loss0.072.pth',map_location='cuda:0'))
    model_bpm.eval()
    
    model.to(device)
    model_bpm.to(device)
    
    piece_name = xml_fn.split('/')[-1].split('.')[0]
    part = partitura.load_musicxml(xml_fn)
    a1 = ScoreFeatures(part, False)
    a2 = ScoreFeatures(part, True)
    
    ############# Velocity ###############
    # predict measure velocity
    vel_ms = np.zeros((a1.num_measures))
    vel_ns = np.zeros((a1.N))
    state = model.init_hidden(batch_size = 1)
    state = tuple([h.to(device) for h in state])
    for m in range(a1.num_measures):
        # construct feature
        if IS_SIMPLE:
            one_hot = np.zeros((128))
            one_hot[int(round(a1.Xmeasure['pitch'][m][0]))] = 1
        else:
            one_hot = np.zeros((input_vector_dim1 - 128 - 129 - 2))
            if m in a2.dict_measure2xmlidx:
                xmlidxs = a1.dict_measure2xmlidx[m]
                feature_m = np.mean(a1.score_features[xmlidxs], axis = 0)
                one_hot = f2onehot(feature_m)
        
        prev_vels = np.zeros((129))
        if m == 0:
            prev_vels[0] = 1
        else:
            prev_vels[1+int(vel_ms[m-1])] = 1
        upper_vels = np.zeros((128))
        upper_vels[int(v0)] = 1
        is_notes = np.zeros((2))
        is_notes[0] = 1
        feature_m = np.hstack((one_hot, prev_vels))
        feature_m = np.hstack((feature_m, upper_vels))
        feature_m = np.hstack((feature_m, is_notes))

        feature_m = feature_m.reshape((1,1,input_vector_dim1))
        feature_m = torch.from_numpy((feature_m)).float().to(device)
        
        # prediction
        output, state = model(feature_m, state)
        probs = F.softmax(output, dim = -1)
        p = torch.distributions.Categorical(probs)
        vel_ms[m] = p.sample()

    # predict note velocity
    state = model.init_hidden(batch_size = 1)
    state = tuple([h.to(device) for h in state])
    for i in range(a1.N):
        m = a1.dict_xmlidx2measure[i]
        feature_n = np.zeros((128))
        if IS_SIMPLE:
            curr_features = a1.X['pitch'][i]
            feature_n[int(curr_features)] = 1
        else:
            curr_feature = a1.score_features[i]
            feature_n = f2onehot(curr_feature)
    
        prev_vels = np.zeros(129)
        if i == 0 :
            prev_vels[0] = 1
        else:
            prev_vels[1+int(vel_ns[i-1])]
        upper_vels = np.zeros((128))
        measure_vel = int(round(vel_ms[m]))
        upper_vels[measure_vel] = 1
        is_notes = np.zeros((2))
        is_notes[1] = 1
        # gather
        feature_n = np.hstack((feature_n, prev_vels))
        feature_n = np.hstack((feature_n, upper_vels))
        feature_n = np.hstack((feature_n, is_notes))
        feature_n = feature_n.reshape((1,1,input_vector_dim1))
        feature_n = torch.from_numpy((feature_n)).float().to(device)
        # predict
        output, state = model(feature_n, state)
        probs = F.softmax(output, dim = -1)
        p = torch.distributions.Categorical(probs)
        vel_ns[i] = p.sample()

    ############# BPM ###############
    # predict measure bpm
    bpm_ms = np.zeros((a2.num_measures))
    bpm_ns = np.zeros((a2.N))
    state = model_bpm.init_hidden(batch_size = 1)
    state = tuple([h.to(device) for h in state])
    for m in range(a2.num_measures):
        # construct feature
        feature_m = np.zeros((input_vector_dim2 - 3)) 
        if m in a2.dict_measure2xmlidx:
            xmlidxs = a2.dict_measure2xmlidx[m]
            if IS_SIMPLE:
                feature_m = np.mean(a2.X['pitch'][xmlidxs], axis = 0)
            else:
                feature_m = np.mean(a2.score_features[xmlidxs], axis = 0)
        prev_bpms = -1
        if m != 0:
            prev_bpms = bpm_ms[m-1]
        upper_bpms = bpm0
        is_notes = 0
        feature_m = np.hstack((feature_m, prev_bpms))
        feature_m = np.hstack((feature_m, upper_bpms))
        feature_m = np.hstack((feature_m, is_notes))
        # predict
        feature_m = feature_m.reshape((1,1,input_vector_dim2))
        feature_m = torch.from_numpy((feature_m)).float().to(device)
        output, state = model_bpm(feature_m, state)
        bpm_ms[m] = output

    # predict note bpm
    state = model.init_hidden(batch_size = 1)
    state = tuple([h.to(device) for h in state])

    for i in range(a2.N):
        m = a2.dict_xmlidx2measure[i]
        if IS_SIMPLE:
            feature_n = a2.X['pitch'][i]
        else:
            feature_n = a2.score_features[i]
        #feature_n = a2.X['pitch'][i]
        prev_bpms = 0
        if i != 0:
            prev_bpms = 1+vel_ns[i-1]
        upper_bpms = bpm_ms[m]
        is_notes   = 1
        # gather
        feature_n = np.hstack((feature_n, prev_bpms))
        feature_n = np.hstack((feature_n, upper_bpms))
        feature_n = np.hstack((feature_n, is_notes))
        feature_n = feature_n.reshape((1,1,input_vector_dim2))
        feature_n = torch.from_numpy((feature_n)).float().to(device)
        # predict
        output, state = model_bpm(feature_n, state)
        bpm_ns[i] = output
    
    
    # calculate note onset and duration
    # write as midi file
    note_vels = vel_ns
    note_bps_raw = np.ones((a2.N, 1))
    note_iois_onset = np.zeros((a2.num_onsets,1))

    for onset, note_idxs in enumerate(a2.unique_onset_idxs):
        note_idxs = a2.unique_onset_idxs[onset]
        note_iois_onset[onset] = a2.Xonset['IOI'][onset] /bpm_ns[note_idxs].mean()

    note_durs_raw = np.zeros((len(note_vels)))
    for i in range(a2.N):
        note_durs_raw[i] = a2.X['duration'][i] 

        note_onset_times = np.cumsum(note_iois_onset)
        note_durs = note_durs_raw

    pm = pretty_midi.PrettyMIDI()
    p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
    piano = pretty_midi.Instrument(program=p_program)
    for i in range(a2.N):
        pitch = a2.note_info['pitch'][i]
        onset = a2.dict_xmlidx2onset[i]
        onset_time = note_onset_times[onset]
        offset_time = onset_time + note_durs[i]
        v = int(note_vels[i])
        my_note = pretty_midi.Note(velocity=v, pitch=pitch, start=onset_time, end=offset_time)
        piano.notes.append(my_note)
    pm.instruments.append(piano)
    pm.write(out_dir+piece_name + ext + '.mid')
