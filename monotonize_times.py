import numpy as np
from scipy.interpolate import interp1d

### to fix a problem with some of the match files
def _is_monotonic(s, mask, strict=True, deltas=None):
    """Return True when `s[mask]` is monotonic. When `strict` is True,
    check for strict monotonicity.

    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    mask : ndarray(bool)
        a boolean mask to be applied to s

    strict : bool
        when True, return a strictly monotonic sequence (default: True)

    """
    if s[mask].shape[0] < 4:
        return True
    else:
        if strict:
            return np.all(np.diff(s[mask]) / np.diff(deltas[mask]) > 0.0)
        else:
            return np.all(np.diff(s[mask]) / np.diff(deltas[mask]) >= 0.0)


def monotonize_times(s, strict=True, deltas=None, return_interp_seq=False):
    """Interpolate linearly over as many points in `s` as necessary to
    obtain a monotonic sequence. The minimum and maximum of `s` are
    prepended and appended, respectively, to ensure monotonicity at
    the bounds of `s`.

    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    strict : bool
        when True, return a strictly monotonic sequence (default: True)

    Returns
    =======

    ndarray
       a monotonic sequence that has been linearly interpolated using a subset of s

    """

    if strict:
        eps = 1e-3  # np.finfo(np.float32).eps
    else:
        eps = 1e-3

    _s = np.r_[s.min() - eps, s, s.max() + eps]
    if deltas is not None:
        _deltas = np.r_[min(deltas) - eps, deltas, max(deltas) + eps]
    else:
        _deltas = None
    mask = np.ones(_s.shape[0], dtype=np.bool)
    monotonic_mask = _monotonize_times(_s, mask, strict, _deltas)[1:-1]  # is the mask always changed in place?
    mask[0] = mask[-1] = False
    #s_mono = interp1d(idx[mask], _s[mask])(idx[1:-1])
    if return_interp_seq:
        idx = np.arange(_s.shape[0])
        s_mono = interp1d(idx[mask], _s[mask])(idx[1:-1])
        return s_mono
    else:
        return _s[mask], _deltas[mask]


def monotonize_times_old(s, strict=True, deltas=None, return_interp_seq=False):
    """Interpolate linearly over as many points in `s` as necessary to
    obtain a monotonic sequence. The minimum and maximum of `s` are
    prepended and appended, respectively, to ensure monotonicity at
    the bounds of `s`.

    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    strict : bool
        when True, return a strictly monotonic sequence (default: True)

    Returns
    =======

    ndarray
       a monotonic sequence that has been linearly interpolated using a subset of s

    """

    if strict:
        eps = 1e-3  # np.finfo(np.float32).eps
    else:
        eps = 1e-3

    _s = np.r_[np.min(s) - eps, s, np.max(s) + eps]
    if deltas is not None:
        _deltas = np.r_[np.min(deltas) - eps, deltas, np.max(deltas) + eps]
    else:
        _deltas = None
    mask = np.ones(_s.shape[0], dtype=np.bool)
    monotonic_mask = _monotonize_times(_s, mask, strict, _deltas)[1:-1]  # is the mask always changed in place?
    mask[0] = mask[-1] = False
    #s_mono = interp1d(idx[mask], _s[mask])(idx[1:-1])
    if return_interp_seq:
        idx = np.arange(_s.shape[0])
        s_mono = interp1d(idx[mask], _s[mask])(idx[1:-1])
        return s_mono
    else:
        return _s[mask], _deltas[mask]


def _select_problematic(s, mask, deltas=None):
    """Return the index i of an element of s, such that removing s[i]
    reduces the non-monotonicity of s[mask]. This method assumes the
    first and last element of `s` are the the minimum and maximum,
    respectively.

    Parameters
    ==========

    s : ndarray
        a sequence of numbers

    mask : ndarray(bool)
        a boolean mask to be applied to s

    Returns
    =======

    int
       an index i such that removing s[i] from s reduces
       non-monotonicity

    """
    if deltas is not None:
        diffs = np.diff(s[mask]) / np.diff(deltas[mask])
    else:
        diffs = np.diff(s[mask])
    # ignore first and last elements
    problematic = np.argmin(diffs[1:] * diffs[:-1]) + 1
    return np.where(mask)[0][problematic]


def _monotonize_times(s, mask, strict, deltas=None):
    """
    Return a mask such that s[mask] is monotonic.

    """

    # print('is monotonic?')
    if _is_monotonic(s, mask, strict, deltas):
        # print('yes')
        return mask
    else:
        # print('no')
        p = _select_problematic(s, mask, deltas)
        # print('select problematic', p)
        mask[p] = False

        return _monotonize_times(s, mask, strict, deltas)
