import prepare_data
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM, Bidirectional, TimeDistributed, BatchNormalization
from keras.optimizers import RMSprop, Adam


def construct_model(input_size = 24, output_size = 1, unit = 0, dropout = 0.4):
    model = Sequential()
    # input  : (xx, t_batch_size, input_size)
    # output : (xx, t_batch_size, output_size)
    t_batch_size = seq_len
    units = 156

    model.add(Bidirectional(LSTM(units, return_sequences=True, dropout=dropout),
                            input_shape=(t_batch_size, input_size)))
    model.add(Bidirectional(LSTM(units, return_sequences=True, dropout=dropout)))
    model.add(Bidirectional(LSTM(units, return_sequences=True, dropout=dropout)))
    model.add(BatchNormalization())
    # fully connected layer
    if unit == 0:
        model.add(TimeDistributed(Dense(output_size, activation='sigmoid')))
    elif unit == 1:
        model.add(TimeDistributed(Dense(output_size)))
    elif unit == 2:
        model.add(TimeDistributed(Dense(output_size, activation = 'linear')))

    model.compile(loss='mse', optimizer=Adam(lr=0.001, clipnorm=10), metrics=['mse'])
    return model

def train_model(model = None, unit = 0, seq_len = 20, num_epoch = 100, batch_size = 1000, model_save = None):
    X_train, X_test, Y_train, Y_test = prepare_data.load_train_test(data_name = 'data/data0116-chopin.npy', seq_len = seq_len, unit = unit)
    model = construct_model(unit = unit, dropout = 0.4)
    model.fit(X_train, Y_train,
          batch_size = batch_size,
          validation_split=0.05,
          epochs = num_epoch)
    if model_save is not None:
        model.save(model_save)
        print 'save model as: ' + model_save