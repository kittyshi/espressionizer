"""
This is the python file that explores different musical expressions
"""

import os
import numpy as np
import aiplay
import prepare_data
import reconstruct_performance
import librosa
import pretty_midi
from scipy.io import wavfile
import scipy
import copy

import datetime

class ExpresiveEngine:
    _initialized = False
    MODEL_DIR_MAP = {
        'chopin':    'models/20190531_chopinbest/',                  # chopin
        'beethoven': 'models/20190530/20190530_bb_beethoven/'        # beethoven
    }
    model_name = ''         # beethoven or chopin
    model_dir = ''

    X = None                # midi
    xmlscore = None
    modeldict = None
    yout = None
    g = {}
    input_data = {}
    g_name = {}
    DEBUG_MODE = 1
    # xml_fn = 'testdata/xml_test_beethoven/beethoven_op002_no1_mv1.xml'
    # xml_fn = 'testdata/xml_test_chopin/chopin_op72_No1.xml'
    xml_fn = 'testdata/xml_test_chopin/chopin_op28_No11.xml'

    piecename = ''
    out_midi_path = ''
    out_wave_path = ''

    default_master_slider_value = {
        # 'master_art_mean':  -0.8,
        # 'master_art_std':   1.22,
        # 'master_bpr_mean':  0.44,
        # 'master_bpr_std':   0.15,
        # 'master_tempo':     0.4,
        # For Beethoven
        'master_art_mean':  -0.89,
        'master_art_std':   1.22,
        'master_bpr_mean':  0.44,
        'master_bpr_std':   0.15,
        'master_tempo':     0.4,
    }
    master_slider_value = copy.deepcopy(default_master_slider_value)
    master_slider_min_max = {# min  # max
        'master_art_mean':  (-1.5,  1.5),
        'master_art_std':   (0.5,     1.5),
        'master_bpr_mean':  (-1,    1),
        'master_bpr_std':   (0,     0.5),
        'master_tempo':     (0,     1),
    }
    output_type = ['vtrend','vdev','ibi','time','art']
    chopin_master_slider_value = {
        'chopin_op28_No11.xml': { #_bpave0.10_bm0.50s0.40_am-0.50s0.30_p
            'master_tempo':     0.25,
            'master_bpr_mean':  0.5,
            'master_bpr_std':   0.4,
            'master_art_mean':  0.5,
            'master_art_std':   0.3,
        },
        'chopin_op35_Mv1.xml': {   #_bpave0.90_bm0.90s0.20_am-0.50s0.30_p.mid
            'master_tempo':     0.4,
            'master_bpr_mean':  0.5,
            'master_bpr_std':   0.3,
            'master_art_mean':  -0.3,
            'master_art_std':   0.3,
        },
        'chopin_op60.xml': {   #_bpave0.30_bm0.50s0.20_am0.10s0.30_p.mid
            'master_tempo':     0.3,
            'master_bpr_mean':  0.5,
            'master_bpr_std':   0.2,
            'master_art_mean':  0.1,
            'master_art_std':   0.3,
        },
        'chopin_op72_No1.xml': {   #_bpave0.50_bm1.50s0.30_am0.50s0.30_p.mid
            'master_tempo':     0.5,
            'master_bpr_mean':  1.5,
            'master_bpr_std':   0.3,
            'master_art_mean':  0.5,
            'master_art_std':   0.3,
        },
    }

    def init_gradient_name(self):
        self.g_name = {0: 'pitch',1: 'pitch^2',2: 'pitch^3',3: 'alter',4: 'o_pitch_max',5: 'o_pitch_min',6: 'o_chroma12',18: 'Downbeat',19: 'Beatphase',20: 'Piecephase',21: 'Beat',22: 'ioi_prev1',23: 'ioi_prev2',24: 'ioi_prev3',25: 'ioi_next1',26: 'ioi_next2',27: 'ioi_next3',28: 'preceds_rest',29: 'staccato',30: 'fermata',31: 'accent',32: 'staff',33: 'p',34: 'fp',35: 'pp',36: 'f',37: 'sfp',38: 'ff',39: 'sf',40: 'calando',41: 'diminuendo',42: 'crescendo',43: 'grazioso',44: 'dolce',45: 'mf',46: 'rinforzando',47: 'ppp',48: 'mp',49: 'fz',50: 'fff',51: 'sfz',52: 'allegro',53: 'rallentando',54: 'espressivo',55: 'ritardando',56: 'tenuto',57: 'lento',58: 'a_tempo',59: 'grave',60: 'con_brio',61: 'tempo_1',62: 'adagio',63: 'andante',64: 'presto',65: 'largo',66: 'con_espressione',67: 'semplice',68: 'prestissimo',69: 'vivace',70: 'marcato',71: 'leggiero',72: 'teneramente',73: 'piacevole',74: 'cantabile',75: 'con_fuoco',76: 'accelerando',77: 'risoluto',78: 'larghetto',79: 'pesante',80: 'risvegliato',81: 'tranquilo',82: 'dolente',83: 'appassionato',84: 'ritenuto',85: 'sotto_voce',86: 'stretto',87: 'vivo',88: 'smorzando',89: 'con_anima',90: 'con_forza',91: 'sostenuto',92: 'agitato',93: 'energico',94: 'animato',95: 'stringendo',96: 'in_tempo',97: 'assai',98: 'moderato',99: 'mancando',100: 'maestoso',101: 'Slur_incr'}
        for i in range(12):
            self.g_name[i+6] = 'o_chroma%d' % i


    def __init__(self, debug_mode=0, xml_fn="", model_dir="", model_name='beethoven'):
        """
            Model_name should never be empty
        """
        self.DEBUG_MODE = debug_mode
        self.init_gradient_name()
        self.model_name = model_name

        if len(model_dir) == 0:
            self.model_dir = self.MODEL_DIR_MAP[model_name]

        # load models
        print '='* 80
        print '  Models used: ', model_name
        print '='* 80
        if not self._initialized:
            start = datetime.datetime.now()
            if len(model_dir) > 0:
                self.model_dir = model_dir

            self.modeldict = aiplay.get_model_dict(self.model_dir, self.DEBUG_MODE)
            self._initialized = True
            print '> Time loading %d seconds' % (datetime.datetime.now() - start).seconds
        if self.DEBUG_MODE == 1:
            print '-'* 80
            print '  DEBUG MODE, only model vtrend is loaded'
            print '-'* 80

        if len(xml_fn) > 0:
            self.xml_fn = xml_fn
        self.reload_xml(self.xml_fn)


    def reload_xml(self, xml_fn):
        self.xml_fn = xml_fn
        self.g = {}
        self.input_data = {}
        self.piecename = self.xml_fn.split('/')[-1]
        self.out_midi_path = 'static/output_midi/'+self.piecename+'-change.mid'
        self.out_wave_path = 'static/output_music/'+self.piecename+'-change.wav'
        # print '>', self.model_name
        # print '>', self.piecename

        # update master_slide
        if self.piecename in self.chopin_master_slider_value:
            print '> Load known master slider values'
            for k in self.chopin_master_slider_value[self.piecename].keys():
                self.master_slider_value[k] = self.chopin_master_slider_value[self.piecename][k]
        else:
            print '> Load DEFAULT master slider values'
            for k in self.default_master_slider_value.keys():
                self.master_slider_value[k] = self.default_master_slider_value[k]

        print '>', '-'*80
        start = datetime.datetime.now()
        self.X, self.xmlscore = aiplay.read_xml(self.xml_fn)
        data_dir = 'all_data/gradients/gradient_' + self.piecename + '/'
        self.yout = aiplay.predict(self.X, self.modeldict)
        print '> Aiplay.predict took %d seconds' % (datetime.datetime.now() - start).seconds
        for k in self.output_type:
            prefix = data_dir + self.piecename + '_' + k
            if os.path.isfile(prefix + '_g.npy'):
                self.g[k] = np.load(prefix + '_g.npy')
                self.input_data[k] = np.load(prefix + '_input.npy')
            else:
                print '> [ERROR] Gradients "%s" not found: %s' % (k, prefix + '_g.npy')


    def parse_master_slider(self, data_from_ui, scale=True):
        for k in sorted(self.master_slider_min_max.keys()):
            if k in data_from_ui:
                if scale:
                    vmin = self.master_slider_min_max[k][0]
                    vmax = self.master_slider_min_max[k][1]
                    self.master_slider_value[k] = float(data_from_ui[k])/100.0 * (vmax - vmin) + vmin
                else:
                    self.master_slider_value[k] = data_from_ui[k]
                print '>    ', k, '\t%.2f' % self.master_slider_value[k]


    def get_expressive_midi(self, data_from_ui):
        """
        A function to take input data, and option, output a midi file
        """
        self.parse_master_slider(data_from_ui);

        start = datetime.datetime.now()
        slider_name_map = { # copy from output_type above
                            # 1. keep in sync with 'chart_slider_dict' in _charts.js
                            # 2. use 'generate_slider_html.py' for 'body.html'
            'vtrend': 0,
            'onset_pitch_max': 4,       # value is the INDEX of the feature, NOT the VALUE
            # 'v40_calando': 40,
            # 'v37_f': 37,
            'Downbeat': 18,
            #'Forzando': 36,
            'Slur': 101,

            'ibi': 2,
            'IOI_prev3': 25,
            'IOI_next3': 28,
            'Beatphase': 19,
            # 'b30_fermata': 30,
            # 'b49_fz': 49,
            # 'b70_marcato': 70,
        }

        # output = 0       #
        # feature_no = [1,3,5,7]  # which feature (input feature no 1 pitch^2,)
        # data_from_ui = {
        #     'vtrend': {
        #         'v_feature_0': 40,
        #         'v_feature_1': 51,
        #         'v_feature_2': 62,
        #     },
        #     'ibi': {
        #         'b_feature_0': 23,
        #         'b_feature_1': 34,
        #         'b_feature_2': 45,
        #     },
        #     start: start_idx,
        #     end: end_idx,
        # }
        # g = g_vtrend
        #input_data = input_vtrend
        yout_new = [self.yout[i] for i in range(len(self.yout))]



        for k, feature_value_map in data_from_ui.items():
            if k not in slider_name_map:
                continue
            deltax = np.zeros_like(self.input_data[k])
            g = self.g[k]
            idx = slider_name_map[k]
            for fv, v in feature_value_map.items():
                feature_no = slider_name_map[fv]
                emphasize = int(v)
                if emphasize == 50:
                    # 50 means no-op
                    continue
                print '> Apply %d to idx %d (%s), feature #%d' % (emphasize, idx, k, feature_no)

                deltax[:, :, feature_no] = emphasize / 100 - 0.5

            y = self.yout[idx]
            dx = np.einsum('ijk,ljk->il', g, deltax)
            yout_new[idx] = y + dx.reshape(deltax.shape[0] * deltax.shape[1],1)[:len(self.X)]


        self.yout = yout_new
        self.Yhatout = yout_new[0]
        for i in range(1, len(yout_new)):
            self.Yhatout = np.hstack((self.Yhatout, yout_new[i]))

        #velocity = yhat1
        #velocity = recenter(yhat1, velmean, velstd)
        #scaler_vel = MinMaxScaler(feature_range=(30, 90))
        #velocity = scaler_vel.fit_transform(velocity).astype(int)

        self.Yhatout[:,2] = aiplay.recenter(self.Yhatout[:,2], self.master_slider_value['master_bpr_mean'], self.master_slider_value['master_bpr_std'])
        # self.Yhatout[:,3] = aiplay.recenter(self.Yhatout[:,3], self.spreadmean, self.spreadstd)
        self.Yhatout[:,4] = aiplay.recenter(self.Yhatout[:,4], self.master_slider_value['master_art_mean'], self.master_slider_value['master_art_std'])

        addPedal = False
        if len(self.yout) > 5:
            self.Yhatout[:,5] = np.array([0 if n < 0.5 else 1 for n in self.Yhatout[:,5]])
            self.Yhatout[:,6] = np.array([0 if n < 0.5 else 1 for n in self.Yhatout[:,6]])
            addPedal = True


        midi_data = reconstruct_performance.synthesize(
            self.xmlscore,
            self.Yhatout,
            self.master_slider_value['master_tempo'],
            out_name = self.out_midi_path,
            addPedal = addPedal)

        print '> Synthesize MIDI in %d seconds' % (datetime.datetime.now() - start).seconds

        return self.out_midi_path



    def get_expressive_wave(self):
        # synthesize wave
        start = datetime.datetime.now()

        midi_data = pretty_midi.PrettyMIDI(self.out_midi_path)
        y = midi_data.fluidsynth(fs = 44100, sf2_path='static/sf/Estonia Final.sf2')
        # librosa.output.write_wav(self.out_wave_path, y, sr = 44100)

        maxv = np.iinfo(np.int16).max
        scipy.io.wavfile.write(self.out_wave_path, 44100, (y*maxv).astype(np.int16))

        print '> write to', self.out_wave_path
        print '> Synthesize Wave done in %d seconds' % (datetime.datetime.now() - start).seconds

        # do something here
        return self.out_wave_path


    def get_out_wave_path(self):
        return self.out_wave_path

    def get_piecename(self):
        return self.piecename

    def _to_list(self, y):
        tmp = [a[0] for a in y.tolist()]
        return zip(range(len(tmp)), tmp)


    def get_weights(self):
        return {
            'vtrend': self._to_list(self.yout[0]),
            'ibi': self._to_list(self.yout[2]) if self.DEBUG_MODE == 0 else self._to_list(self.yout[0])
            }

    def get_master_slider_value(self):
        x = dict(self.master_slider_value.items())
        for k in sorted(self.master_slider_min_max.keys()):
            vmin = self.master_slider_min_max[k][0]
            vmax = self.master_slider_min_max[k][1]
            x[k] = (x[k]-vmin)/(vmax - vmin) * 100.0
            print '>    ', k, 'value\t%.2f\t%.2f' % (self.master_slider_value[k], x[k])
        return self.master_slider_value, x

    #####
    def _load_from_pickle(self):
        self.modeldict = {
            'art': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.4dr_0.001lr_areal.h5',
                'seq_len': 64
            },
            'ibi': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.5dr_0.001lr_onset_ibi.h5',
                'seq_len': 64
            },
            'softpedal': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.4dr_0.001lr_soft_pedal.h5',
                'seq_len': 64
            },
            'sustainpedal': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.4dr_0.050lr_sus_pedal.h5',
                'seq_len': 64
            },
            'time': {
                'name': '20190530_bb_chopin_64sqlen_500epch_10240bs_64unit_0.4dr_0.001lr_note_time.h5',
                'seq_len': 64
            },
            'vdev': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.4dr_0.010lr_note_vdev.h5',
                'seq_len': 64
            },
            'vtrend': {
                'name': '20190530_bb_chopin_64sqlen_500epch_9240bs_64unit_0.5dr_0.001lr_onset_vtrend.h5',
                'seq_len': 64
            }}
        # for k,v in self.modeldict.items():
        #     v['model'] =

    def _save_model_dict_to_pickle(self):
        for k,v in self.modeldict.items():
            w = v['model'].get_weights()
            weight_file = '%sweights_%s.p' % (self.model_dir, v['name'])
            with open(weight_file, 'wb') as fpkl:
                pickle.dump(w, fpkl, protocol= pickle.HIGHEST_PROTOCOL)
                print 'Saved weights for %s to %s' % (k, weight_file)
