import os
import sys
import matplotlib.pyplot as plt
from collections import defaultdict

usage = """
Usage:

1. List all the combinations
    python ../analyze_massive.py soft_pedal

2. List the best model in each param
    $ 0528_msp_best_pm{master}$ cat ../_param | xargs -n 1 -I {} ../analyze_massive.sh {}

"""

def parse_filename(fn):
    # return the config struct
    # MODEL_NAME = '%s_%s_%s_%dsqlen_%depch_%dbs_%dunit_%.1fdr_%.3flr%s%s'
    words = fn.split('_')
    config = {}
    parse_map = {
        'sqlen': 'seq_len',
        'epch': 'epoch',
        'bs': 'batch_size',
        'unit': 'units',
        'dr': 'dropout',
        'lr': 'lr',
        'lstm': 'lstm'
    }
    config['lstm'] = 2
    for word in words:
        for k,v in parse_map.items():
            if word.endswith(k):
                tmp = word[:word.find(k)]
                config[v] = tmp
                # if k in ['dr', 'lr']:
                #     config[v] = float(tmp)
                # else:
                #     config[v] = int(tmp)
    if '_bn' in fn:
        config['bn'] = 'BN'
    else:
        config['bn'] = 'no-BN'
    # print fn
    # print config
    config['fname'] = fn
    return config

def get_last_loss(fn):
    with open(fn) as fin:
        hc = defaultdict(list)
        last_val_coeff_determination = -1
        last_val_loss = -1
        for a in fin:
            if not a.startswith('loss'):
                continue
            tmp = dict(zip(
                ['loss', 'coeff_determination', 'val_loss', 'val_coeff_determination'],
                [float(b.split(':')[1]) for b in a.split('- ')]))
            hc['loss'].append(tmp['loss'])
            hc['val_loss'].append(tmp['val_loss'])
            hc['coeff_determination'].append(tmp['coeff_determination'])
            hc['val_coeff_determination'].append(tmp['val_coeff_determination'])
            last_val_coeff_determination = tmp['val_coeff_determination']
            last_val_loss = tmp['val_loss']

    # summarize history for accuracy
    plt.plot(hc['loss'])
    plt.plot(hc['val_loss'])
    # plt.plot(hc['coeff_determination'])
    # plt.plot(hc['val_coeff_determination'])
    plt.ylabel('loss')
    plt.xlabel('epoch')

    return last_val_loss, last_val_coeff_determination

if __name__ == '__main__':

    if len(sys.argv) <2:
        print usage
        sys.exit()
    prefix = sys.argv[1]

    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    print '\t'.join([
                'seq_len',
                'units',
                'dropout',
                'lr',
                'bn',
                'r2',
                'loss',
                'fn'])
    legend_txt = []
    for f in files:
        if f.endswith(prefix + '.txt'):
            config = parse_filename(f)
            legend_txt += ['train - %s' % f, 'test - %s' % f]
            last_loss, last_r2 = get_last_loss(f)
            print '\t'.join([
                config['seq_len'],
                config['units'],
                config['dropout'],
                config['lr'],
                config['bn'],
                '%.3f' % (last_r2),
                '%.3f' % (last_loss),
                config['fname']])
            # break
    if len(legend_txt) > 0:
        plt.legend(legend_txt, loc='upper right')
        # plt.show()
