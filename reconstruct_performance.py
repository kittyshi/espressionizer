"""
Given Y, reconstruct a performance
"""
import numpy as np
import pretty_midi
import to_class
from config import BINS
import pretty_midi


def create_midi(score, performance,
                note_vels, note_onsets, note_durs, out_name):
    p = pretty_midi.PrettyMIDI()
    p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
    piano = pretty_midi.Instrument(program=p_program)

    for i in range(score.N):
        if i in performance.valid_xmlidx:
            # add note
            pitch = score.note_info['pitch'][i]
            onset_time = note_onsets[i]
            offset_time = note_offsets[i]
            v = int(note_vels[i])
            my_note = pretty_midi.Note(velocity=v, pitch=pitch, start=onset_time, end=offset_time)
            piano.notes.append(my_note)
    p.instruments.append(piano)
    p.write(out_name)
    print 'write to: ' + out_name


def decode_train(performance):
    AVE = performance.Y['scale_bpm_onset_mean']
    Y_onehot = {}
    digits = None
    for name in ['vel_onset','vel_diff_onset','bpm_onset_norm',
                    'bpm_onset_ratio','log_articulation']:
        category_data = performance.Y[name]
        digits = to_class.convert_category(category_data, BINS[name])
        Y_onehot[name] = to_class.convert_onehot(digits, len(BINS[name]) + 1)
    Y_onehot['sus_pedal'] = performance.Y['sus_pedal']
    Y_onehot['soft_pedal'] = performance.Y['soft_pedal']
    vels = to_class.decode_onehots(Y_onehot['vel_onset'],'vtrend')
    vel_diffs = to_class.decode_onehots(Y_onehot['vel_diff_onset'],'vdev')
    bpms = to_class.decode_onehots(Y_onehot['bpm_onset_norm'],'ibi')
    bpm_ratios = to_class.decode_onehots(Y_onehot['bpm_onset_ratio'], 'time')
    articulations = to_class.decode_onehots(Y_onehot['log_articulation'], 'art')
    note_bpms = np.zeros((score.N))
    note_arts = np.zeros((score.N))
    note_iois = np.zeros((score.N))
    note_durs = np.zeros((score.N))
    note_vels = np.zeros((score.N))
    bpms[bpms < 0] = np.min(bpms[bpms > 0])
    for onset, note_idxs in enumerate(score.unique_onset_idxs):
        # if synthesize from matched data
        note_idxs = [n for n in note_idxs if n in performance.valid_xmlidx]
        #mp_idxs = [dict_mpidx2MP[performance.noteidx2mp[note_idx]] for note_idx in note_idxs]
        mp_idxs = [performance.noteidx2mp[note_idx] for note_idx in note_idxs]
        if len(mp_idxs) > 0:
            note_bpms[note_idxs] = bpms[mp_idxs].mean() * bpm_ratios[mp_idxs] * AVE
            note_vels[note_idxs] = vels[mp_idxs] + vel_diffs[mp_idxs]
            note_arts[note_idxs] = 2 ** articulations[mp_idxs]   # convert from log2(articulation)
            note_iois[note_idxs] = score.Xonset['IOI_prev_onset'][onset] / note_bpms[note_idxs]
            note_durs[note_idxs] = note_arts[note_idxs] * np.array((score.note_dur))[note_idxs] / note_bpms[note_idxs]
    note_durs[note_durs == 0] = min(note_durs[note_durs > 0])
    note_onsets = np.cumsum(np.r_[0, note_iois])[:-1]
    note_offsets = note_onsets + note_durs
    return note_onsets, note_durs, note_vels



# measure-based reconstruction (old)
def reconstruct_notelist_measurewise(score, Y, outname):
    measure_bpms = np.zeros((score.num_measures))
    measure_vels = np.zeros((score.num_measures))
    for measure in range(score.num_measures):
        noteidxs = score.dict_measure2noteidx[measure]
        perf_measure_bpm_class = int(np.round(np.mean([np.argmax(y2[idx]) for idx in noteidxs])))
        measure_bpms[measure] = to_class.inverse_tempo(perf_measure_bpm_class)
        measure_vel_class = int(np.round(np.mean([np.argmax(y0[idx]) for idx in noteidxs])))
        measure_vels[measure] = to_class.inverse_vel_onehot(measure_vel_class)

    last_time = max(np.max(score.note_onset_beat) + 0.01, np.max(score.note_offset_beat))
    diff_u_onset = np.diff(np.r_[score.unique_onset_times, last_time])
    ioi_perf = np.zeros((score.N))

    measure_bpm = np.zeros((score.N))
    measure_ratio = np.zeros((score.N))
    note_bpm = np.zeros((score.N))
    #ioi_onset = np.zeros((len(score.unique_onset_idxs)))
    articulations = np.zeros((score.N))
    note_vels = np.zeros((score.N))
    durations = np.zeros((score.N))
    for i, uix in enumerate(score.unique_onset_idxs):
        # BPM and Articulation Reference
        score_onset = np.mean(score.note_onset_beat[uix])
        score_offset = np.mean(score.note_offset_beat[uix])
        note_id0 = score.dict_xmlidx2noteid[uix[0]]
        measure = score.dict_noteid2measure[note_id0]
        measure_bpm[uix] = np.mean([to_class.inverse_tempo(np.argmax(y2[xml])) for xml in uix])

        # Articulation and duration
        for xml_idx in uix:
            note_vels[xml_idx] = measure_vels[measure] + to_class.inverse_veldiff_onehot2(np.argmax(y1[xml_idx]))
            articulations[xml_idx] = to_class.inverse_articulation(np.argmax(y4[xml_idx]))
            measure_ratio[xml_idx] = to_class.inverse_tempo(np.argmax(y3[xml_idx]))
            note_bpm[xml_idx] = measure_bpm[uix[0]] * measure_ratio[xml_idx]
            if note_bpm[idx] != 0:
                ioi_perf[xml_idx] = diff_u_onset[i] * 1. / note_bpm[xml_idx]
            else:
                ioi_perf[xml_idx] = diff_u_onset[i] * 1. / np.max(note_bpm[xml_idx])

    # update duration
    for i in range(score.N):
        #onset_i = score.dict_noteidx2onset[i]
        if ioi_perf[i] != 0:
            durations[i] = articulations[i] * score.note_dur[i] /  ioi_perf[i]
        else:
            durations[i] = articulations[i] * score.note_dur[i]
    onset_perf = np.cumsum(np.r_[0, ioi_perf])

    ioi_perf = np.zeros((score.N))
    for i in range(score.N):
        onset_i = dict_noteidx2onset[i]
        ioi_perf[i] = diff_u_onset[onset_i] * 1. / note_bpm[i]

    p = pretty_midi.PrettyMIDI()
    p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
    piano = pretty_midi.Instrument(program=p_program)

    for i in range(score.N):
        # add note
        pitch = score.note_info['pitch'][i]
        #onset_i = score.dict_noteidx2onset[i]
        onset_time = onset_perf[i]
        offset_time = onset_time + durations[i]
        v = int(note_vels[i])
        my_note = pretty_midi.Note(velocity=v, pitch=pitch, start=onset_time, end=offset_time)
        piano.notes.append(my_note)
    p.instruments.append(piano)
    p.write(outname)
    print "write to: " + outname
    return p