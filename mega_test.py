# seq_len_list  = [32, 64, 128]
# bn_list = [True, False]
# unit_len_list = [16,  32,  48]
# dropout_list = [0.4, 0.5]
# lr_list  = [0.01,   0.001]

seq_len_list  = [64, 128]
bn_list = [True, False]
unit_len_list = [32,  48]
dropout_list = [0.4]
lr_list  = [0.01,   0.001]

TMPL = './1_massive_run.sh "--MODEL_CODENAME=%s --NUM_EPOCH=%d --BATCH_SIZE=%s --DROPOUT=%s --LR=%s --UNITS=%d  --SEQ_LEN=%d --DATA_SET=%s %s"'

codename = 'msc'
epoch = 200
dataset = 'chopin'  # beethoven
for dropout in dropout_list:
    for bn in bn_list:
        for unit_len in unit_len_list:
            for seq_len in seq_len_list:
                for lr in lr_list:
                    if dataset == 'beethoven':
                        if unit_len == 16:
                            batch_size = 20480
                        elif unit_len == 32:
                            batch_size = 10240
                        elif unit_len == 48:
                            batch_size = 10240
                    elif dataset == 'chopin':
                        if unit_len == 16:
                            batch_size = 20480
                        elif unit_len == 32:
                            batch_size = 10240
                        elif unit_len == 48:
                            batch_size = 9000

                    print TMPL % (
                        codename,
                        epoch,
                        batch_size,
                        str(dropout),
                        str(lr),
                        unit_len,
                        seq_len,
                        dataset,
                        '--USE_BN' if bn else ''
                        )
