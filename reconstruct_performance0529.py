"""
Given Y, reconstruct a performance
"""
import numpy as np
import feature_extract
import pretty_midi
import helper_old as helper


def decode_articulation(score_durations, articulation_parameter,
                        beat_period):

    art_ratio = (2 ** articulation_parameter)
    dur = art_ratio * score_durations * beat_period

    return dur


def decode(score, parameters, Bp_ave, flip = 1.0):
    Vel_trend = parameters[:,0]
    Vel_dev = parameters[:,1] * (-flip)
    Ibi = parameters[:,2]
    Spread = parameters[:,3]
    Articulation = parameters[:,4]

    # Velocity
    Velocities = np.round((Vel_trend + Vel_dev) * 127.).astype(np.int)

    # Note onset and offset
    score_onsets = score['onset'].astype(np.float64, copy=False)
    score_durations = (score['offset'] - score['onset']).astype(np.float64, copy=False)
    score_info = helper.get_unique_seq(onsets=score_onsets,
                                    offsets=score_onsets + score_durations,
                                    unique_onset_idxs=None,
                                    return_diff=True)
    unique_s_onsets = score_info['u_onset']
    unique_onset_idxs = score_info['unique_onset_idxs']
    total_dur = score_info['total_dur']
    diff_u_onset_score = score_info['diff_u_onset']

    ioi_ratio = np.array([np.mean(Ibi[uix]) for uix in unique_onset_idxs])  # Onset Bpr (mean ibi on that score position)
    beat_period = 2**ioi_ratio * Bp_ave    # if parameters[:,0] = np.log2(parameters[:,0]/mbp) in performance_extract
    #beat_period = ioi_ratio * Bp_ave      # if not
    ioi_perf = diff_u_onset_score * beat_period
    u_onset_perf = np.cumsum(np.r_[0, ioi_perf])

    # Onsets and duration
    Onsets = np.zeros((len(score_onsets)))
    Durations = np.zeros((len(score_onsets)))

    for i, jj in enumerate(unique_onset_idxs):
        # decode onset
        Onsets[jj] = u_onset_perf[i] - Spread[jj]
        # decode duration
        Durations[jj] = decode_articulation(
            score_durations=score_durations[jj],
            articulation_parameter=Articulation[jj],
            beat_period=beat_period[i])

    Onsets -= np.min(Onsets)

    return Onsets, Durations, Velocities


def synthesize(score, Y, Bp_ave, out_name = 'output/test.mid', flip = 1.0):
    """
    Given a score, and predicted performance feature Y (velocity, logBPR, logArticulation),
    synthesize it back to a MIDI file
    """
    p = pretty_midi.PrettyMIDI()
    p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
    piano = pretty_midi.Instrument(program=p_program)

    onsets, durs, vels = decode(score, Y, Bp_ave, flip)

    isSustainOn = False
    isSoftOn = False
    for i in range(len(score['pitch'])):
        # add note
        pitch = score['pitch'][i]
        onset = onsets[i]
        offset = onset + durs[i]
        v = min(vels[i],120)
        v = max(vels[i],20)
        my_note = pretty_midi.Note(velocity=int(v), pitch=pitch, start=onset, end=offset)
        piano.notes.append(my_note)

        # add sustain and soft pedal
        sustain =  Y[i,5]
        soft = Y[i,6]

        if sustain == 1.0 and not isSustainOn:
            isSustainOn = True
            piano.control_changes.append(pretty_midi.ControlChange(64,100,onset))
        elif sustain == 0.0 and isSustainOn:
            isSustainOn = False
            piano.control_changes.append(pretty_midi.ControlChange(64,0,onset))
        if soft == 1.0 and not isSoftOn:
            isSoftOn = True
            piano.control_changes.append(pretty_midi.ControlChange(67,100,onset))
        elif soft == 0.0 and isSoftOn:
            isSoftOn = False
            piano.control_changes.append(pretty_midi.ControlChange(67,0,onset))


    # the last note
    p.instruments.append(piano)
    print 'write to: ' + out_name
    p.write(out_name)


