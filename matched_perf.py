import numpy as np

# Given a part, and a match (from Nakamura's), get matched_performance info
class MatchedPerf:
    def __init__(self, score, match_fn):
        self.score = score
        self.note_idx_dict, self.note_array, self.skipped_note = self.parse_match_txt(match_fn)
        self.match_perf()

    def get_note_symbol(self,note):
        def alter_sign(a):
            if a is None:
                return ''
            elif a == -1:
                return 'b'
            elif a == 1:
                return '#'
            elif a == 2:
                return '##'
                #return 'x'
            elif a == -2:
                return 'bb'
            else:
                print("outlier: %d " % a)

        return '%s%s%d' % (note.step, alter_sign(note.alter), note.octave)

    def find_missing_note(self, note_symbol, note_idx):
        # find if this note was skipped by player
        tmp = note_idx.split('-')
        measure_index = int(tmp[1])
        for note_id, measure_idx, note_symbl in self.skipped_note:
            if measure_idx == measure_index and note_symbol == note_symbl:
                return True, note_id
        return False, None

    def match_perf(self):
        self.matched_perf = np.zeros((self.score.N), dtype =  [('score_onset','f4'),('score_offset','f4'),('midi_pitch','i4'),
             ('perf_onset','f4'), ('perf_offset','f4'), ('perf_velocity', 'i4'),
             ('onset_i','i4'),('xml_idx','i4'),('mp_idx','i4')])

        self.M = 0 # num_matched
        self.xmlidx2mp = {}
        self.onset2mp = {}
        self.mp2onset = {}
        self.measure2mp = {}
        for notes_tied_idx, note in enumerate(self.score.part.notes_tied):
            note_idx = 'P1-%s-%s' % (note.measure_index, note.note_index)
            note_symbol = self.get_note_symbol(note)

            #print("%s [%s] %d in notes_tied" % (note_symbol, note_idx, notes_tied_idx))
            if note_idx in self.note_idx_dict:
                note_id_j, note_symbol_j, is_error_note, note_onset_j, note_offset_j, note_velocity = self.note_idx_dict[note_idx]
                if note_symbol_j == note_symbol:
                    # good match!
                    # notes_tied_idx, note -  partitura
                    #print("%s [%s] %d in notes_tied <=> %d in midi, from %.2f to %.2f" % (note_symbol_j, note_idx, notes_tied_idx, note_id_j, note_onset_j, note_offset_j))
                    self.matched_perf[self.M]['score_onset'] = self.score.note_onset_beat[notes_tied_idx]
                    self.matched_perf[self.M]['score_offset'] = self.score.note_offset_beat[notes_tied_idx]
                    self.matched_perf[self.M]['perf_velocity'] = note_velocity
                    self.matched_perf[self.M]['midi_pitch'] = note.midi_pitch
                    self.matched_perf[self.M]['perf_onset'] = note_onset_j
                    self.matched_perf[self.M]['perf_offset'] = note_offset_j
                    self.matched_perf[self.M]['xml_idx'] = notes_tied_idx
                    self.xmlidx2mp[notes_tied_idx] = self.M
                    # construct dictionary
                    onset_i = self.score.dict_xmlidx2onset[notes_tied_idx]
                    self.mp2onset[self.M] = onset_i
                    if onset_i not in self.onset2mp:
                        self.onset2mp[onset_i] = [self.M]
                    else:
                        self.onset2mp[onset_i].append(self.M)
                    # construct measure2mp
                    if note.measure_index not in self.measure2mp:
                        self.measure2mp[note.measure_index] = [self.M]
                    else:
                        self.measure2mp[note.measure_index].append(self.M)

                    self.M += 1
                else:
                    # these are the edge cases
                    if is_error_note:
                        # Midi has the error key
                        #print('[OK ] Musician made a mistake. J: %s\tC: %s\t[%s]' % (note_symbol_j, note_symbol, note_idx))
                        pass
                    else:
                        #print('[ERR] J: %s\tC: %s\t[%s]' % (note_symbol_j, note_symbol, note_idx))
                        pass
            else:
                # symbol marked as skip in MIDI
                found_match, note_id_j = self.find_missing_note(note_symbol, note_idx)
                #print('[%s] %s [%s] not in MIDI' % ('OK ' if found_match else 'ERR', note_symbol, note_idx))
            if note_id_j is not None:
                self.note_array[note_id_j] = -1
        self.matched_perf = self.matched_perf[:self.M]

        # check if there's left-over notes in MIDI not being matched
        for idx, n in enumerate(self.note_array):
            if n != -1:
                if n[1] == 3:
                    # print('[OK ] this is an extra note')
                    continue
                else:
                    print(idx, n)
        print('-'*40)
        print('%d matched between xml (%d notes) and MIDI (%d notes).' % (self.M, len(self.score.part.notes_tied), len(self.note_array)))



    def parse_match_txt(self,match_fn):
        note_array = []       # array of the notes in order
        note_idx_dict = {}    # keyed on "measure_index-note_index"
        skipped_note = []
        error = 0
        extra = 0
        measure_index = 0
        with open(match_fn, 'r') as fin:
            for line in fin:
                # Line example:
                #      0    2.007210    4.579322    C2  89  80  0   0   0   P1-1-17 0   0
                # Fields are:
                #       0: ID
                #       1: (onset time)
                #       2: (offset time)
                #       3: (spelled pitch)
                #       4: (onset velocity)
                #       5: (offset velocity)
                #       6: channel
                #       7: (match status)
                #       8: (score time)
                #       9: (note ID)
                #      10: (error index)
                #      11: (skip index)
                if line.startswith('//'):
                    continue
                tmp = line.split('\t')
                note_id = int(tmp[0])         # 0-indexed
                note_onset = float(tmp[1])
                note_offset = float(tmp[2])
                note_symbol = tmp[3]          # C3
                velocity    = int(tmp[4])
                note_idx = tmp[9]             # P1-55-1
                error_code = int(tmp[10])     # 0 or 1(wrong note) or 3(extra note)

                # wrong note and extra note
                if error_code == 1:
                    error += 1
                elif error_code == 3:
                    extra += 1

                # skipped note
                if note_idx == '*':
                    skipped_note.append((note_id, measure_index, note_symbol))
                else:
                    # normal note
                    tmp = note_idx.split('-')
                    measure_index = int(tmp[1])
                    # store Px-y-z into a directionary
                    note_idx_dict[note_idx] = (note_id, note_symbol, error_code, note_onset, note_offset, velocity)
                note_array.append([note_symbol, error_code])
        print('%d notes in the file, %d skipped note (*), %d with error, %d extra' % (len(note_array), len(skipped_note), error, extra))

        return note_idx_dict, note_array, skipped_note