# input: xml name
# output: a rendered midi file

import feature_extract
import prepare_data
import reconstruct_performance0529 as reconstruct_performance
import score_extractor
import numpy as np
from ofaidata_utils.data_handling.musicxml import parse_music_xml
from keras.models import load_model
import score_extract
from sklearn.preprocessing import normalize, MinMaxScaler
import os


def read_xml(xml_fn):
    part = parse_music_xml(xml_fn).score_parts[0]
    part.expand_grace_notes()
    # construct score
    xmlscore = np.array([(note.start.t, note.end.t, note.midi_pitch)
                      for note in part.notes],
                     dtype=[('onset', 'f4'), ('offset', 'f4'), ('pitch', 'i4')])
    beat_map = part.beat_map
    xmlscore['onset'] = beat_map(xmlscore['onset'])
    xmlscore['offset'] = beat_map(xmlscore['offset'])

    #X_newpiece, input_feature_names = score_extract.get_score_feature(xmlscore,part)
    X_newpiece = score_extractor.ScoreFeature(part)

    # feature_num = X_newpiece.shape[1]
    # num_input = X_newpiece.shape[0] / seq_len + 1
    # pad_amount = num_input * seq_len - X_newpiece.shape[0]
    # inputX_test = np.pad(X_newpiece, ((0,pad_amount), (0, 0)), 'constant').reshape((num_input,seq_len,feature_num))

    return X_newpiece.score_features_raw, xmlscore
    #return X_newpiece, xmlscore


def get_model_dict(model_dir):

    def coeff_determination(y_true, y_pred):
        from keras import backend as K
        SS_res =  K.sum(K.square( y_true-y_pred ))
        SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
        return ( 1 - SS_res/(SS_tot + K.epsilon()) )

    models = dict()

    for f in os.listdir(model_dir):
        if f.endswith('.h5'):
            model = dict()
            model['name'] = f
            model['seq_len'] = int(f.split('sqlen')[0].split('_')[-1])
            model['model'] = load_model(model_dir + f, custom_objects={'coeff_determination': coeff_determination})
            if f.endswith('vtrend.h5'):
                models['vtrend'] = model
            elif f.endswith('vdev.h5'):
                models['vdev'] = model
            elif f.endswith('ibi.h5'):
                models['ibi'] = model
            elif f.endswith('time.h5'):
                models['time'] = model
            elif f.endswith('art.h5'):
                models['art'] = model
            elif f.endswith('sus_pedal.h5'):
                models['sustainpedal'] = model
            elif f.endswith('soft_pedal.h5'):
                models['softpedal'] = model

    return models


def get_models_old(model_names):

    def coeff_determination(y_true, y_pred):
        from keras import backend as K
        SS_res =  K.sum(K.square( y_true-y_pred ))
        SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
        return ( 1 - SS_res/(SS_tot + K.epsilon()) )

    model_vtrend = load_model(model_names[0], custom_objects={'coeff_determination': coeff_determination})
    model_vdev = load_model(model_names[1], custom_objects={'coeff_determination': coeff_determination})
    model_bpr = load_model(model_names[2], custom_objects={'coeff_determination': coeff_determination})
    model_time = load_model(model_names[3], custom_objects={'coeff_determination': coeff_determination})
    model_art = load_model(model_names[4], custom_objects={'coeff_determination': coeff_determination})
    model_sustain = load_model(model_names[5], custom_objects={'coeff_determination': coeff_determination})
    model_soft = load_model(model_names[6], custom_objects={'coeff_determination': coeff_determination})

    return [model_vtrend, model_vdev, model_bpr, model_time, model_art, model_sustain, model_soft]


def recenter(y, mean, std):
    # yi = m2 + (xi-m1) * s2/s1
    return mean + (y-np.mean(y)) * (std / np.std(y))



def predict(X, modeldict,seq_len = 64):

    #[model_vtrend, model_vdev, model_bpr, model_time, model_art, model_sustain, model_soft] = models
    # divide according to sequence length

    piece_length = len(X)
    #models = [modeldict['vtrend'], modeldict['vdev'], modeldict['ibi'], modeldict['time'], modeldict['art'], modeldict['sustainpedal'], modeldict['softpedal']]
    models = [modeldict['vtrend'], modeldict['vdev'], modeldict['ibi'], modeldict['time'], modeldict['art'], modeldict['sustainpedal']]

    yout = []
    for m in models:
        input_data = prepare_data.slice_test_data(X, seq_len)
        yhat = m['model'].predict(input_data)
        yhat_flat = yhat.reshape(-1, yhat.shape[-1])[:piece_length,:]
        yout.append(yhat_flat)

    return yout


def batchplay0529(xml_dir = 'testdata/xml_test_chopin/',
                  output_dir = 'output/chopin_compare/tmp/'):
    """
    To reproduce 0529 model
    """
    for fn in os.listdir(xml_dir):
        if fn.endswith('.xml'):
            xml_fn = xml_dir + fn
            part = parse_music_xml(xml_fn).score_parts[0]
            part.expand_grace_notes()
            X = score_extractor.ScoreFeature(part)
            X_newpiece = X.score_features_raw
            xmlscore = np.array([(note.start.t, note.end.t, note.midi_pitch)
                                  for note in part.notes],
                                 dtype=[('onset', 'f4'), ('offset', 'f4'), ('pitch', 'i4')])
            beat_map = part.beat_map
            xmlscore['onset'] = beat_map(xmlscore['onset'])
            xmlscore['offset'] = beat_map(xmlscore['offset'])
            name = xml_fn.split('/')[-1].split('.')[0]

            outname = output_dir + name + '.mid'
            play0529(xml_fn, modeldict,outname,64)

def play0529(xml_fn, modeldict, out_name, seq_len = 64,Bp_ave = 0.4,
            bprmean = 0.5, bprstd = 0.1, artmean = -0.89, artstd = 1.22,
            spreadmean = 0.04, spreadstd = 0.01):

    X_newpiece, xmlscore = read_xml(xml_fn)
    #[yhat1, yhat2, yhat3,yhat4,yhat5, yhat6, yhat7] = predict(X_newpiece, modeldict,seq_len)
    [yhat1, yhat2, yhat3,yhat4,yhat5, yhat6] = predict(X_newpiece, modeldict,seq_len)

    yhat3 = recenter(yhat3, bprmean, bprstd)
    yhat4 *= Bp_ave
    yhat5 = recenter(yhat5, artmean, artstd)
    yhat6 = np.array([0 if n < 0.5 else 1 for n in yhat6])
    #yhat7 = np.array([0 if n < 0.5 else 1 for n in yhat7])
    yhat7 = np.zeros_like(yhat6)

    Yhat = np.column_stack((yhat1,yhat2,yhat3,yhat4,yhat5,yhat6,yhat7))
    reconstruct_performance.synthesize(xmlscore, Yhat, Bp_ave, out_name, flip = 1.0)



def predict_old(input_data, models, piece_length):

    [model_vtrend, model_vdev, model_bpr, model_time, model_art, model_sustain, model_soft] = models

    Y1 = model_vtrend.predict(input_data)
    yhat1 = Y1.reshape(-1, Y1.shape[-1])[:piece_length,:]  # flatten out along the first axis

    Y2 = model_vdev.predict(input_data)
    yhat2 = Y2.reshape(-1, Y2.shape[-1])[:piece_length,:]

    Y3 = model_bpr.predict(input_data)
    yhat3 = Y3.reshape(-1, Y3.shape[-1])[:piece_length,:]

    Y4 = model_time.predict(input_data)
    yhat4 = Y4.reshape(-1, Y4.shape[-1])[:piece_length,:]

    Y5 = model_art.predict(input_data)
    yhat5 = Y5.reshape(-1, Y5.shape[-1])[:piece_length,:]

    Y6 = model_sustain.predict(input_data)
    yhat6 = Y6.reshape(-1, Y6.shape[-1])[:piece_length,:]

    Y7 = model_soft.predict(input_data)
    yhat7 = Y7.reshape(-1, Y7.shape[-1])[:piece_length,:]

    return [yhat1, yhat2, yhat3, yhat4, yhat5, yhat6, yhat7]



def play0527(xml_fn, seq_len, models, out_name, Bp_ave = 0.4,
            bprmean = 0.5, bprstd = 0.1, artmean = -0.89, artstd = 1.22,
            spreadmean = 0.04, spreadstd = 0.01):

    X_newpiece, input_data, xmlscore = read_xml(xml_fn, seq_len = seq_len, isnormalize=  False)

    [yhat1, yhat2, yhat3,yhat4,yhat5, yhat6, yhat7] = predict(input_data, models, len(X_newpiece))

    yhat3 = recenter(yhat3, bprmean, bprstd)
    yhat4 *= Bp_ave
    yhat5 = recenter(yhat5, artmean, artstd)
    yhat6 = np.array([0 if n < 0.5 else 1 for n in yhat6])
    yhat7 = np.array([0 if n < 0.5 else 1 for n in yhat7])

    Yhat = np.column_stack((yhat1,yhat2,yhat3,yhat4,yhat5,yhat6,yhat7))
    reconstruct_performance.synthesize(xmlscore, Yhat, Bp_ave, out_name, flip = 1.0)


def play0522_v2(xml_fn, seq_len, models, out_name, Bp_ave = 0.4,
            bprmean = 0.5, bprstd = 0.1, artmean = -0.89, artstd = 1.22,
            spreadmean = 0.04, spreadstd = 0.01):

    X_newpiece, input_data, xmlscore = read_xml(xml_fn, seq_len = seq_len, isnormalize=  False)

    [yhat1, yhat2, yhat3,yhat4,yhat5] = predict(input_data, models, len(X_newpiece))

    Ypred = np.hstack((yhat1, yhat2))
    Ypred = np.hstack((Ypred, recenter(yhat3, bprmean, bprstd)))
    Ypred = np.hstack((Ypred, yhat4*Bp_ave))
    Ypred = np.hstack((Ypred, recenter(yhat5, artmean, artstd)))
    reconstruct_performance.synthesize(xmlscore, Ypred, Bp_ave, out_name, flip = 1.0)


def play0522(xml_fn, seq_len, models, out_name, Bp_ave = 0.4,
            bprmean = 0.5, bprstd = 0.1, spreadmean = 0.04, spreadstd = 0.01):

    X_newpiece, input_data, xmlscore = read_xml(xml_fn, seq_len = seq_len, isnormalize=  False)

    [yhat1, yhat2, yhat3,yhat4,yhat5] = predict(input_data, models, len(X_newpiece))

    Ypred = np.hstack((yhat1, yhat2))
    Ypred = np.hstack((Ypred, recenter(yhat3, bprmean, bprstd)))
    Ypred = np.hstack((Ypred, yhat4*Bp_ave))
    Ypred = np.hstack((Ypred, yhat5 + np.log2(Bp_ave)))
    reconstruct_performance.synthesize(xmlscore, Ypred, Bp_ave, out_name, flip = 1.0)


def play0521(xml_fn, seq_len, models, out_name, Bp_ave = 0.5):

    X_newpiece, input_data, xmlscore = read_xml(xml_fn, seq_len = seq_len)

    [yhat1, yhat2, yhat3,yhat4,yhat5] = predict(input_data, models, len(X_newpiece))

    Ypred = np.hstack((yhat1, yhat2))   # minus because when training, I flip the sign
    Ypred = np.hstack((Ypred, yhat3*2))  # during training, make it -1 to 1 to fit tanh
    Ypred = np.hstack((Ypred, yhat4))
    Ypred = np.hstack((Ypred, yhat5))

    reconstruct_performance.synthesize(xmlscore, Ypred, Bp_ave, out_name, flip = 1)


def play(xml_fn, seq_len, models, out_name, Bp_ave = 0.5,
        velmean = 0.47, velstd = 0.05, bprmean = 0.44, bprstd = 0.15,
        spreadmean = 0.04, spreadstd = 0.01, artmean = -0.89, artstd = 1.22,
        flip = 1, isnormalize = True, classicModel = False):

    X_newpiece, input_data, xmlscore = read_xml(xml_fn, seq_len = seq_len, isnormalize = isnormalize)

    [yhat1, yhat2, yhat3,yhat4,yhat5] = predict(input_data, models, len(X_newpiece))
    velocity = yhat1
    #velocity = recenter(yhat1, velmean, velstd)
    #scaler_vel = MinMaxScaler(feature_range=(30, 90))
    #velocity = scaler_vel.fit_transform(velocity).astype(int)
    logibi = recenter(yhat3, bprmean, bprstd)
    spread = recenter(yhat4, spreadmean, spreadstd)
    logart = recenter(yhat5, artmean, artstd)
    if classicModel:
        yhat2 -= 1
    Ypred = np.hstack((yhat1, yhat2))
    Ypred = np.hstack((Ypred, logibi))
    Ypred = np.hstack((Ypred, spread))
    Ypred = np.hstack((Ypred, logart))

    reconstruct_performance.synthesize(xmlscore, Ypred, Bp_ave, out_name, flip)


def batchrender():
    model_sets = ['0520_c_beethoven_model','0520_c_chopin_model']
    model_types = ['_onset_vtrend.h5','_note_vdev.h5','_onset_ibi.h5','_note_time.h5','_note_art.h5']
    models_beethoven = get_models_old(['models/0520/' + model_sets[0] + m for m in model_types])
    models_chopin = get_models_old(['models/0520/' + model_sets[1] + m for m in model_types])

    # for beethoven (maybe)
    velmean = 0.47
    velstd = 0.05

    bprmean = 0.44
    bprstd = 0.15

    spreadmean = 0.04
    spreadstd = 0.01

    artmean = -0.89
    artstd = 1.22

    bpave = 0.5

    # for chopin
    # velmean = 0.47
    # velstd = 0.05

    # bprmean = 0.5
    # bprstd = 0.2

    # spreadmean = 0
    # spreadstd = 0.03

    # artmean = -0.5
    # artstd = 1

    # bpave = 0.5
    # import os
    # if not os.path.exists(output_dir):
    #     os.makedirs(output_dir)

    # for xml_name in os.listdir(xml_dir):
    #     if xml_name.endswith('.xml'):
    #         piece_name = os.path.splitext(xml_name)[0]
    #         ext = ''
    #         outname = output_dir + piece_name + ext + '.mid'
    #         aiplay.play(xml_dir + xml_name, models, outname, bpave,
    #                 velmean, velstd, bprmean, bprstd, spreadmean, spreadstd,
    #                 artmean, artstd)
