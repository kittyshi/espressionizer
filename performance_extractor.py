import helper
import helper_old
import numpy as np
import to_class
from ofaidata_utils.data_handling.musicxml import parse_music_xml
from ofaidata_utils.data_handling.matchfile import MatchFile
from config import BINS

class PerformanceFeature:
    def __init__(self, score, match_fn):
        # Prase score and match file
        self.score = score
        self.bm = score.bm
        self.match = MatchFile(match_fn)
        self.M = len(self.match.note_pairs)   # placeholder
        mcu = self.match.info('midiClockUnits')
        mcr = self.match.info('midiClockRate')
        self.mcu_ratio = mcr / (mcu * 10. ** 6)

        # for regression
        self.Ynotename = ['vel_onsetmax', 'vel_maxdiff',
                          'beat_period_onbeat', 'beat_period_ratio',
                          'log_articulation', 'sus_pedal', 'soft_pedal']

        self.init_Y()
        self.match_score_perf()
        self.extract_performance_feature()
        self.regulate()

    def regulate(self):
        self.Y['vel_train'] = self.Y['vel']/ 127.
        bpm_mean = self.Y['bpm_raw'].mean()
        bpm_std = self.Y['bpm_raw'].std()
        art_mean = self.Y['log_articulation'].mean()
        art_std = self.Y['log_articulation'].std()
        self.Y['bpm_train'] = (self.Y['bpm_raw'] - bpm_mean)/bpm_std
        self.Y['art_train'] = (self.Y['log_articulation'] - art_mean)/art_std


    def init_Y(self):
        # note-wise features
        self.Y = {}
        self.Ynotename = ['vel','vel_onsetmax','vel_maxdiff','vel_onsetmean','vel_diffprev',
                          'bpm_raw','beat_period_raw','IOI','time','articulation_raw',
                          'articulation_notebpm','log_articulation',
                          'vel_measuremean','vel_diff_measure',
                          'bpm_measureraw','bpm_measure_ratio','bpm_measure_norm']
        for name in self.Ynotename:
            self.Y[name] = np.zeros((self.M))

        # onset-wise features
        self.Yonset = {}
        self.Yonsetname = ['vel_max','vel_mean','IOI','beat_period_raw','dur_max',
                           'x_perftime', 'x_scoreonset', 'articulation_mean',
                           'vel_diff_prev','bpm_raw','bpm_norm', 'bpm_ratio_measure']

        self.Yonbeat = {}    #beat-grid wise information (mostly tempo)
        self.Yonbeatname = ['vel_mean', 'vel_max','bpm_mean','articulation_mean']
        for name in self.Yonbeatname:
            self.Yonbeat[name] = np.zeros((len(self.score.onbeat_onseti)))

        self.Ymeasure = {}
        # measure/slur wise
        self.Ymeasurename = ['vel_mean','vel_diff','bpm_raw','bpm_norm',
                             'beat_period_raw', 'articulation_mean',
                             'vel','x_scoreonset','x_perftime','x_startonset']
        for name in self.Ymeasurename:
            self.Ymeasure[name] = np.zeros((self.score.num_measures))

        self.Ymeasure4 = {}
        self.Ymeasure4name = ['bpm_raw']
        for name in self.Ymeasure4name:
            self.Ymeasure4[name] = np.zeros((int(self.score.num_measures/4)))


    def match_score_perf(self):
        """Given `match_lines`, a sequence of Snote-Note pairs from a MatchFile, and a
        sequence of Note objects from a ScorePart, return an array with two columnw,
        where each row [i, j] indicates that `match_lines`[i] corresponds to
        `xml_notes`[j].

        """
        #self.noteidx2matchidx = {}
        self.noteid2mp = {}
        self.noteidx2mp = {}    # matchidx is the index in self.matched_perf
        self.onset2perftime = {}
        self.onset2mp = {}
        self.onbeat2mp = {}
        self.measure2mp = {}
        self.mp2measure = {}
        self.measure42mp = {}
        self.mp2noteidx = {}
        self.mp2onset = {}
        self.mp2match = {}
        self.unique_mpidx = {}   # corresponds to unique_onset_idxs in score


        # fill in values
        self.matched_perf = np.zeros((self.M), dtype =  [('score_onset','f4'),('score_offset','f4'),('midi_pitch','i4'),
                                         ('perf_onset','f4'), ('perf_offset','f4'), ('perf_velocity', 'i4'),
                                         ('onset_i','i4'),('xml_idx','i4'), ('match_idx', 'i4'), ('note_id', 'i4')])

        mp_idx = 0
        for i, (sn, n) in enumerate(self.match.note_pairs):
            note_id = sn.Anchor.split('-')[0]
            if note_id in self.score.dict_noteid2xmlidx and note_id not in self.noteid2mp: #and i > 0 and abs(n.Onset * self.mcu_ratio - prev_perfonset) > 20
                xml_idx = self.score.dict_noteid2xmlidx[note_id]
                self.noteid2mp[note_id] = mp_idx
                self.noteidx2mp[xml_idx] = mp_idx
                self.mp2noteidx[mp_idx] = xml_idx
                # fill in the matched_perfomance info, the index corresponds to each entry in self.match_xml_idx
                self.matched_perf[mp_idx]['score_onset'] = self.score.note_onset_beat[xml_idx]
                self.matched_perf[mp_idx]['score_offset'] = self.score.note_offset_beat[xml_idx]
                self.matched_perf[mp_idx]['midi_pitch'] = self.score.note_info['pitch'][xml_idx]
                self.matched_perf[mp_idx]['perf_onset'] = n.Onset * self.mcu_ratio
                self.matched_perf[mp_idx]['perf_offset'] = n.Offset * self.mcu_ratio
                self.matched_perf[mp_idx]['perf_velocity'] = n.Velocity
                self.matched_perf[mp_idx]['xml_idx'] = xml_idx
                self.matched_perf[mp_idx]['match_idx'] = i
                self.matched_perf[mp_idx]['note_id'] = note_id

                # onset-level mapping
                onset_i = self.score.dict_noteid2onset[str(note_id)]
                self.mp2onset[mp_idx] = onset_i
                # create unique onset mpidx
                if onset_i not in self.unique_mpidx:
                    self.unique_mpidx[onset_i] = [self.noteidx2mp[idx] for idx in self.score.unique_onset_idxs[onset_i] if idx in self.noteidx2mp]

                self.matched_perf[mp_idx]['onset_i'] = onset_i
                if onset_i not in self.onset2mp:
                    self.onset2mp[onset_i] = [mp_idx]
                    self.onset2perftime[onset_i] = [n.Onset * self.mcu_ratio]
                else:
                    self.onset2mp[onset_i].append(mp_idx)
                    self.onset2perftime[onset_i].append(n.Onset * self.mcu_ratio)

                # measure-level mapping
                measure = self.score.dict_noteid2measure[str(note_id)]
                if measure not in self.measure2mp:
                    self.measure2mp[measure] = [mp_idx]
                else:
                    self.measure2mp[measure].append(mp_idx)
                self.mp2measure[mp_idx] = measure
                mp_idx += 1

        self.M = mp_idx
        self.matched_perf = self.matched_perf[:self.M]

        for name in self.Yonsetname:
            self.Yonset[name] = np.zeros((self.score.num_onsets))


        idx, self.br = helper.segment_score_perf_times(self.matched_perf, self.unique_mpidx)
        self.score_segs = np.split(self.matched_perf['score_onset'][idx], self.br)
        self.perf_segs = np.split(self.matched_perf['perf_onset'][idx], self.br)
        self.idx_segs = np.split(idx, self.br)
        self.br_onset = [self.mp2onset[b] for b in self.br]

    def extract_performance_feature(self):

        self.encode_onset()
        self.encode_onbeat()
        self.encode_tempo()
        self.encode_measure()
        self.encode_measure4()
        self.encode_diffs()

        self.encode_articulation()

        self.Y['sus_pedal'], _ = self.get_pedal(pedal_type = "sustain")
        self.Y['soft_pedal'], _ = self.get_pedal(pedal_type = "soft")

    def encode_diffs(self):
        # first regulate
        percent = np.percentile(self.Yonset['bpm_raw'], 99)
        self.Yonset['bpm_raw'][self.Yonset['bpm_raw'] > percent] = percent

        # bpm (beat-per-second)
        self.Y['bpm_diffs'] = np.zeros((self.M))
        self.Y['bpm_onset'] = np.zeros((self.M))
        for measure in range(len(self.Ymeasure['bpm_raw'])):
            if measure in self.measure2mp:
                mps = self.measure2mp[measure]
                for mp in mps:
                    onset_i = self.mp2onset[mp]
                    self.Y['bpm_diffs'][mp] = self.Ymeasure['bpm_raw'][measure] - self.Yonset['bpm_raw'][onset_i]
                    self.Y['bpm_onset'][mp] = self.Yonset['bpm_raw'][onset_i]
        # velocity
        self.Y['vel_diffs'] = np.zeros((self.M))
        for i in range(self.M):
            self.Y['vel_diffs'][i] = self.Y['vel_measuremean'][i] - self.Y['vel'][i]

    def encode_onset(self):
        """
        Given a matched score-performance segment index, fill in the parameters
        """
        self.Y['x_perftime'] = np.zeros((len(self.Y['bpm_raw'])))
        for onset_i, mp_idxs in self.onset2mp.items():
            # grab all notes on that onset
            curr_notes = self.matched_perf[mp_idxs]
            for mp_idx in mp_idxs:
                self.Y['x_perftime'][mp_idx] = self.matched_perf[mp_idx]['perf_onset']
            # fill in velocity
            self.Y['vel'][mp_idxs] = self.matched_perf[mp_idxs]['perf_velocity']
            curr_maxvel = self.Y['vel'][mp_idxs].max()
            self.Yonset['vel_max'][onset_i] = curr_maxvel
            self.Yonset['vel_mean'][onset_i] = self.Y['vel'][mp_idxs].mean()
            self.Y['vel_onsetmean'][mp_idxs] = self.Yonset['vel_mean'][onset_i]

            self.Yonset['dur_max'][onset_i] = max(curr_notes['perf_offset'] - curr_notes['perf_onset'])
            self.Yonset['x_perftime'][onset_i] = curr_notes['perf_onset'].mean()
            self.Yonset['x_scoreonset'][onset_i] = curr_notes['score_onset'].mean()

            # Onset velocity, and velocity trend (difference from max)
            self.Y['vel_onsetmax'][mp_idxs]   = curr_maxvel
            self.Y['vel_maxdiff'][mp_idxs]  = curr_maxvel - self.Y['vel'][mp_idxs]

            # encode difference
            if onset_i - 1 in self.onset2mp and onset_i not in self.br_onset:
                prev_mp_idxs = self.onset2mp[onset_i - 1]
                prev_notes = self.matched_perf[prev_mp_idxs]
                prev_maxvel = self.Yonset['vel_max'][onset_i - 1]
                self.Yonset['vel_diff_prev'][onset_i] = curr_maxvel - prev_maxvel
                self.Y['vel_diffprev'][mp_idxs] =  curr_maxvel - prev_maxvel
                self.Yonset['IOI'][onset_i-1] = curr_notes['perf_onset'].mean() - prev_notes['perf_onset'].mean()
                Yonset_scoreIOI = curr_notes['score_onset'].mean() - prev_notes['score_onset'].mean()
                self.Y['IOI'][mp_idxs] = self.Yonset['IOI'][onset_i-1]

                self.Yonset['bpm_raw'][onset_i-1] = Yonset_scoreIOI / self.Yonset['IOI'][onset_i-1]
                self.Yonset['beat_period_raw'][onset_i-1] = self.Yonset['IOI'][onset_i-1] / Yonset_scoreIOI

                # Calculate bpm and beat period
                for mp_idx in mp_idxs:
                    self.Y['beat_period_raw'][mp_idx] = self.Y['IOI'][mp_idx]/ max(self.score.Xonset['IOI'][onset_i], 0.1)
                    self.Y['bpm_raw'][mp_idx] = self.score.Xonset['IOI'][onset_i] / max(self.Y['IOI'][mp_idx], 0.1)
                    #print self.Y['bpm_raw'][mp_idx], self.score.Xonset['IOI'][onset_i]
                self.Yonset['beat_period_raw'][onset_i] = self.Y['beat_period_raw'][mp_idxs].mean()
                #self.Yonset['bpm_raw'][onset_i] = self.Y['bpm_raw'][mp_idxs].mean()

        for i in range(1,len(self.Yonset['x_perftime'])):
            if self.Yonset['x_perftime'][i] == 0:
                self.Yonset['x_perftime'][i] = self.Yonset['x_perftime'][i-1]



    def encode_onbeat(self):
        """
        Encode onbeat time for all onbeat onsets
        """
        # mps carries all valid mp that align with a beat
        self.onbeat_mp = []
        self.onbeat_onseti = []
        for onset in self.score.onbeat_onseti:
            if onset in self.onset2mp:
                self.onbeat_mp.append(self.onset2mp[onset])
                self.onbeat_onseti.append(onset)

        # self.dict_onbeat2onsets = {}
        # for i in range(len(self.onbeat_onseti)-1):
        #     onset = self.onbeat_onseti[i]
        #     self.dict_onbeat2onsets[onset] = range(self.onbeat_onseti[i],self.onbeat_onseti[i+1])

        self.Yonbeat['beat_period_raw'] = np.zeros((len(self.onbeat_mp)))
        self.Yonbeat['bpm_raw'] = np.zeros((len(self.onbeat_mp)))
        self.Yonbeat['x_perftime'] = np.zeros((len(self.onbeat_mp)))
        self.Y['onbeat_bpmraw'] = np.zeros((self.M))

        for i in range(len(self.onbeat_mp)-1):
            perf_time = self.matched_perf[self.onbeat_mp[i+1]]['perf_onset'].mean()
            perf_prev_time = self.matched_perf[self.onbeat_mp[i]]['perf_onset'].mean()
            score_time = self.matched_perf[self.onbeat_mp[i+1]]['score_onset'].mean()
            score_prev_time = self.matched_perf[self.onbeat_mp[i]]['score_onset'].mean()
            self.Yonbeat['beat_period_raw'][i] = (perf_time - perf_prev_time) / max((score_time - score_prev_time),0.1)
            bpm_raw = (score_time - score_prev_time) / max((perf_time - perf_prev_time),0.1)
            the_onsets = range(self.onbeat_onseti[i],self.onbeat_onseti[i+1])
            for the_onset in the_onsets:
                if the_onset in self.onset2mp:
                    self.Y['onbeat_bpmraw'][self.onset2mp[the_onset]] =  bpm_raw
            self.Yonbeat['bpm_raw'][i] = bpm_raw
            self.Yonbeat['x_perftime'][i] = perf_time
        self.Yonbeat['beat_period_raw'][-1] = self.Yonbeat['beat_period_raw'][-2]
        self.Yonbeat['bpm_raw'][-1] = self.Yonbeat['bpm_raw'][-2]
        self.Yonbeat['x_perftime'][-1] = self.Yonbeat['x_perftime'][-2] + 0.1

        for mp_idx in self.onbeat_mp[-1]:
            self.Y['onbeat_bpmraw'][mp_idx] = self.Yonbeat['bpm_raw'][-1]
        #self.Yonbeat['beat_period_raw'][self.Yonbeat['beat_period_raw']> np.percentile(self.Yonbeat['beat_period_raw'], 99)] = np.percentile(self.Yonbeat['beat_period_raw'], 99)
        #self.Yonbeat['bpm_raw'][self.Yonbeat['bpm_raw']> np.percentile(self.Yonbeat['bpm_raw'], 99)] = np.percentile(self.Yonbeat['beat_period_raw'], 99)


    def encode_tempo(self):

        self.valid_mpidx = range(self.M)
        self.valid_xmlidx = []
        # update individual notes
        for i, mp in enumerate(self.valid_mpidx):
            self.valid_xmlidx.append(self.matched_perf[mp]['xml_idx'])

        # fill in zeros of bpm_raw
        for i in range(self.M):
            if self.Y['bpm_raw'][i] == 0:
                j = i
                while(self.Y['bpm_raw'][j] == 0) and j+1 < self.M:
                    j += 1
                self.Y['bpm_raw'][i] = self.Y['bpm_raw'][j]

        for i in range(self.score.num_onsets):
            if self.Yonset['bpm_raw'][i] == 0:
                j = i
                while(self.Yonset['bpm_raw'][j] == 0) and j+1 < self.score.num_onsets:
                    j += 1
                self.Yonset['bpm_raw'][i] = self.Yonset['bpm_raw'][j]

        self.Yonset['bpm_raw'][0] = self.Yonset['bpm_raw'][1]
        self.Yscale = {}
        self.Yscale['bpm_mean'] = np.average(self.Yonset['bpm_raw'])
        self.Yscale['bpm_std'] = np.std(self.Yonset['bpm_raw'])


        self.Yonset['bpm_raw'][self.Yonset['bpm_raw'] == 0] = self.Yscale['bpm_mean']

        # fill in unique onset
        self.u_score_onset = np.zeros((self.score.num_onsets))
        self.u_perf_onset = np.zeros((self.score.num_onsets))
        for onset_i, idxs in enumerate(self.score.unique_onset_idxs):
            if onset_i in self.onset2mp:
                mps = self.onset2mp[onset_i]
                self.u_score_onset[onset_i] = self.matched_perf['score_onset'][mps].mean()
                self.u_perf_onset[onset_i] = self.matched_perf['perf_onset'][mps].mean()

        self.perf_onset_smooth = np.cumsum(self.u_score_onset/self.Yonset['bpm_raw'])

        # for onset_i in self.onset2mp:
        #     mp_idxs = self.onset2mp[onset_i]
        #     #self.matched_perf['perf_onset']
        #     if onset_i - 1 in self.onset2mp:
        #         prev_mpidxs = self.onset2mp[onset_i - 1]
        #         ioi_grid = self.perf_onset_smooth[mp_idxs] - self.perf_onset_smooth[prev_mpidxs].mean()
        #         ioi_perf = self.matched_performance['perf_onset'][mp_idxs] - self.matched_performance['perf_onset'][prev_mpidxs].mean()
        #         self.Y['time'][mp_idxs] = ioi_perf - ioi_grid




    def encode_measure(self):
        """
        Chopin treats every 4 measures as a phrase, excluding pickup measure
        """
        measure0 = False
        for measure in range(self.score.num_measures):
            # take care of extreme edge case where no note is matched to the very first measure

            if measure in self.measure2mp:
                mpidxs = [mp for mp in self.measure2mp[measure] if mp in self.valid_mpidx]
                if len(mpidxs) > 0:
                    # update velocity
                    mp0 = mpidxs[np.argmin(self.matched_perf['score_onset'][mpidxs])]
                    # find all notes belong to this onset
                    onset_i = self.mp2onset[mp0]
                    if onset_i in self.onset2mp:
                        onset_valid = [m for m in self.onset2mp[onset_i] if m in self.valid_mpidx]
                        curr_perf_onset  = self.matched_perf['perf_onset'][onset_valid].mean()
                        curr_score_onset = self.matched_perf['score_onset'][onset_valid].mean()
                        measure_notes = self.matched_perf[mpidxs]
                        self.Ymeasure['vel_mean'][measure]  = np.mean(measure_notes['perf_velocity'])
                        self.Y['vel_measuremean'][mpidxs] = self.Ymeasure['vel_mean'][measure]
                        self.Y['vel_diff_measure'][mpidxs] = self.Ymeasure['vel_mean'][measure] - self.Y['vel'][mpidxs]
                        self.Ymeasure['x_scoreonset'][measure] = curr_score_onset
                        self.Ymeasure['x_perftime'][measure] = curr_perf_onset
                        self.Ymeasure['x_startonset'][measure] = min(self.score.dict_measure2onset[measure])

                        if not measure0:
                            measure0 = True
                        else:
                            p_ioi =  curr_perf_onset - prev_perf_onset
                            s_ioi =  curr_score_onset - prev_score_onset
                            self.Ymeasure['bpm_raw'][measure-1] =  s_ioi / max(0.05, p_ioi)
                            self.Ymeasure['beat_period_raw'][measure-1] = p_ioi / max( 0.05, s_ioi)
                        prev_perf_onset  = curr_perf_onset
                        prev_score_onset = curr_score_onset

        # interpolate by simply copy the previous tempo
        self.Ymeasure['bpm_raw'][0] = self.Ymeasure['bpm_raw'][1]
        self.Ymeasure['beat_period_raw'][0] = self.Ymeasure['beat_period_raw'][1]
        for measure in range(1,self.score.num_measures):
            if self.Ymeasure['bpm_raw'][measure] == 0:
                self.Ymeasure['bpm_raw'][measure] = self.Ymeasure['bpm_raw'][measure-1]
            if self.Ymeasure['beat_period_raw'][measure] == 0:
                self.Ymeasure['beat_period_raw'][measure] = self.Ymeasure['beat_period_raw'][measure-1]

        self.Ymeasure['scale_bpm_mean'] = np.median(self.Ymeasure['bpm_raw'][self.Ymeasure['bpm_raw'] > 0])
        self.Ymeasure['bpm_norm'] = self.Ymeasure['bpm_raw'] / self.Ymeasure['scale_bpm_mean']
        self.Y['scale_bpm_measure_mean'] = self.Ymeasure['scale_bpm_mean']



        # map to local tempo
        for i, mp in enumerate(self.matched_perf):
            note_id = mp['note_id']
            note_idx = mp["xml_idx"]
            measure = self.score.dict_noteid2measure[str(note_id)]
            self.Y['bpm_measureraw'][i] = self.Ymeasure['bpm_raw'][measure]
            onset_i = self.score.dict_noteidx2onset[note_idx]
            if self.Ymeasure['bpm_raw'][measure] > 0:
                self.Y['bpm_measure_ratio'][i] = self.Yonset['bpm_raw'][onset_i] / self.Ymeasure['bpm_raw'][measure]
                self.Y['bpm_measure_norm'][i] = self.Y['bpm_measureraw'][i] / self.Ymeasure['bpm_norm'][measure]
                self.Yonset['bpm_ratio_measure'][onset_i] = self.Yonset['bpm_raw'][onset_i] / self.Ymeasure['bpm_raw'][measure]
                #self.Yonbeat_time_onsetwise_measurenorm[onset_i] /= self.Ymeasure['bpm_raw'][measure]
            else:
                self.Y['bpm_measure_ratio'][i] = 1
                self.Y['bpm_measure_norm'][i] = self.Y['bpm_measureraw'][i]
                self.Yonset['bpm_ratio_measure'][onset_i] = 1


    def encode_measure4(self):
        start_measure = 0
        if np.diff(self.score.measure_startbeat)[0] < np.diff(self.score.measure_startbeat)[1]:
            start_measure = 1
        self.Ymeasure4['x_perftime'] = self.Ymeasure['x_perftime'][start_measure::4]
        self.Ymeasure4['x_scoreonset'] = self.Ymeasure['x_scoreonset'][start_measure::4]
        self.Ymeasure4['bpm_raw'] = np.zeros((len(self.Ymeasure4['x_scoreonset'])))
        self.Ymeasure4['bpm_raw'][:-1] = np.diff(self.Ymeasure4['x_scoreonset']) / np.diff(self.Ymeasure4['x_perftime'])
        self.Ymeasure4['bpm_raw'][-1] = self.Ymeasure4['bpm_raw'][-2]
        # no beat period necessary
        self.Ymeasure4['vel_mean'] = np.zeros((len(self.Ymeasure4['x_scoreonset'])))
        for i in range(len(self.Ymeasure4['vel_mean'])):
            self.Ymeasure4['vel_mean'][i] = self.Ymeasure['vel_mean'][start_measure+i*4:start_measure+(i+1)*4].mean()



    # slur-wise
    def encode_slur(self):
        slur0 = False
        mainslur_idx = []
        for k in self.score.dict_noteidx2slur.keys():
            v = self.score.dict_noteidx2slur[k]
            if v not in mainslur_idx:
                mainslur_idx.append(v)

        for idx in mainslur_idx:
            slur_info = self.score.slur_infos[idx]
            mpidxs = [self.noteidx2mp[note_idx] for note_idx in slur_info["note_idx"]
                        if note_idx in self.noteidx2mp and self.noteidx2mp[note_idx] in self.valid_mpidx]
            if len(mpidxs) > 0:
                onset_is = self.matched_perf[mpidxs]['onset_i']
                note_vels = self.matched_perf['perf_velocity'][mpidxs]
                self.Y['vel_mean_slur'][mpidxs] = note_vels.mean()
                self.Y['vel_diff_slur'][mpidxs] = self.Y['vel_mean_slur'][mpidxs] - note_vels
                self.Yslur['velmean'].append(note_vels.mean())
                self.Yslur['veldiff'].append(self.Y['vel_diff_slur'][mpidxs])
                curr_bpm = self.Y['bpm_raw'][mpidxs]
                mean_bpm = curr_bpm.mean()
                self.Yslur['bpm_raw'].append(curr_bpm)
                if mean_bpm > 0:
                    self.Yslur['bpm_norm'].append(curr_bpm / mean_bpm)
                self.Yslur['bpm_mean'].append(mean_bpm)
                self.Yslur['bpm_ratio'].append([i/mean_bpm for i in curr_bpm])

                # List all onsets happening in a slur

                # Calculate the tempo variation inside slur
                curr_bpms = self.Yonset['bpm_raw'][onset_is]
                self.Y['bpm_slur_ratio'][mpidxs] = self.Y['bpm_raw'][mpidxs] / mean_bpm


    def encode_articulation(self):

        for mp in self.valid_mpidx:
            perf_dur = self.matched_perf[mp]['perf_offset'] - self.matched_perf[mp]['perf_onset']
            score_dur = self.matched_perf[mp]['score_offset'] - self.matched_perf[mp]['score_onset']
            onset_i = self.matched_perf[mp]['onset_i']
            #note_idx = self.matched_perf[mp]["note_id"]
            self.Y['articulation_raw'][mp] = (perf_dur / max(0.1, score_dur))
            self.Y['articulation_notebpm'][mp] = self.Y['articulation_raw'][mp] / max(0.1, self.Y['beat_period_raw'][mp])
            #self.Y['articulation_note_norm'][mp] = self.Y['articulation_raw'][mp] * self.Y['bpm_norm'][mp]
            #self.Y['articulation_onset_norm'][mp] = self.Y['articulation_raw'][mp] * self.Yonset['bpm_norm'][onset_i]
            #self.Y['articulation_measure_norm'][mp] = self.Y['articulation_raw'][mp] * self.Y['bpm_measure_norm'][mp]

            if self.Y['articulation_notebpm'][mp] > 0:
                self.Y['log_articulation'][mp] = np.log2(self.Y['articulation_notebpm'][mp])

        for onset_i in self.onset2mp:
            self.Yonset['articulation_mean'][onset_i] = self.Y['articulation_raw'][self.onset2mp[onset_i]].mean()

        # encode measure-wise average articulation
        # for measure in range(self.score.num_measures):
        #     mps = np.array((self.measure2mp[measure]))
        #     self.Ymeasure['articulation_mean'][measure] = self.Y['articulation_raw'][mps].mean()


    def get_pedal(self, pedal_type = "sustain", pedal_min = 64):
        pedal = np.array([(x.Time, x.Value > pedal_min) for x in self.match.sustain_lines])
        if pedal_type == 'soft':
            pedal = np.array([(x.Time, x.Value > pedal_min) for x in self.match.soft_lines])

        if len(pedal) == 0:
            return np.zeros((self.M, 1)), np.zeros((self.M, 1))

        pedal = pedal[np.argsort(pedal[:,0]), :]

        L = np.max([int(self.matched_perf['perf_offset'][-1]/self.mcu_ratio), pedal[-1][0]])
        p_curve = np.zeros((L))

        pedal_on = False
        pedal_ontime = 0
        pedal_offtime = 0

        for p in pedal:
            if p[1] == 1:                  # Value > 63
                if not pedal_on:
                    pedal_on = True
                    pedal_ontime = p[0]    # Time
            else:
                if pedal_on:
                    pedal_offtime = p[0]
                    p_curve[pedal_ontime:pedal_offtime] = 1
                    pedal_on = False

        note_pedal = np.zeros((self.M, 1))

        for i in range(self.M):
            onidx = int(self.matched_perf['perf_onset'][i]/self.mcu_ratio)
            offidx = int(self.matched_perf['perf_offset'][i]/self.mcu_ratio)
            if np.sum(p_curve[onidx:min(offidx,len(p_curve)-1)]) >= 1:  # from onset to offset, if there is one pedal on
                note_pedal[i] = 1

        return note_pedal, p_curve


