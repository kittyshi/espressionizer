import numpy as np
import score_extract
import score_extractor
import performance_extract
import collections
import os
from ofaidata_utils.data_handling.musicxml import parse_music_xml


def get_X_Y(xml_fn, match_fn,smoothlevel = 1.0):
    name = xml_fn.split('/')[-1].split('.')[0]
    part = parse_music_xml(xml_fn).score_parts[0]
    part.expand_grace_notes()

    score, performance, xml_idx, note_pedalss = performance_extract.get_score_and_performance_notes(match_fn, part, expand_grace=True, expandPedal = False)
    #W, input_feature_names = score_extract.get_score_feature(score, part)
    W = score_extractor.ScoreFeature(part)
    X = W.score_features_raw[xml_idx,:]
    input_feature_names = W.raw_features
    Y, bpr_mean, perf_feature_names = performance_extract.encode(score, performance, smoothlevel = smoothlevel)
    Y = np.column_stack((Y, note_pedalss))
    perf_feature_names.append('Sustain_pedal')
    perf_feature_names.append('Soft_pedal')

    data = collections.defaultdict()
    data['X'] = X
    data['Y'] = Y
    data['BPR_ave'] = bpr_mean
    data['X_name'] = input_feature_names
    data['Y_name'] = perf_feature_names
    data['piece_name'] = name

    return data


