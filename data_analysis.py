import matplotlib.pyplot as plt
import pickle
import os
import numpy as np
import random



def analyze_pitch(score):
    d_pd = dict()
    d_pr = dict()
    for i in range(score.N):
        pd = score.X['dur_diff_prev_cat'][i]
        pr = score.X['dur_diff_prev_cat'][i]
        if pd not in d_pd:
            d_pd[pd] = 1
        else:
            d_pd[pd] += 1

    plt.hist(score.X['pitch_raw'])


def analyze_slurs(score, is_plot = False):
    d = dict()
    num_slur = 0
    slur_val = []
    for i in range(score.N):
        if score.X['is_slur'][i] == 1:
            num_slur += 1
            slur_val.append(score.X['slur_main_raw'][i])
            if score.X['slur_main_raw'][i] not in d:
                d[score.X['slur_main_raw'][i]] = 1
            else:
                d[score.X['slur_main_raw'][i]] += 1

    d_sn = dict()
    for sn in score.slur_notes_infos:
        if len(sn[0]) not in d_sn:
            d_sn[len(sn[0])] = 1
        else:
            d_sn[len(sn[0])] += 1

    notes = {"num_slur": num_slur, "num_none_slur": score.N - num_slur, "dict_len_slur": d_sn  ,"slur_val": slur_val, "dict_slur_main": d}

    if is_plot:
        plt.subplot(3,1,1)
        plt.hist([len(sn[0]) for sn in score.slur_notes_infos], bins = len(d.keys()))

        plt.subplot(3,1,2)
        plt.hist(score.X['is_slur'],bins = 2)
        plt.title('histogram of number of slurred notes(cOp4_mv1)')

        plt.subplot(3,1,3)
        plt.hist(slur_val, bins = 8)
        plt.title('Histogram of slur value distribution for 8 bits')

    return notes