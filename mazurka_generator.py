import numpy as np
import pretty_midi
import helper

# Generate a mazurka performance given velocity (0-1) and tempo (0-6.66)
class Performance:
    def __init__(self, piece_name, score, Y, out_name = None):
        self.out_name = out_name
        self.piece_name = piece_name
        if self.out_name is None:
            self.out_name = self.piece_name + '_out.mid'
        self.score = score
        self.vels = Y['vel']*60.+35             # vel_onset
        #self.vels = Y['vel']*127.
        self.bpms = Y['tempo']                  # bpm_onset_norm
        self.decode()
        self.write_midi()


    def decode(self):
        self.bpms[self.bpms <= 0] = min(self.bpms[self.bpms > 0])
        self.articulations = np.ones((self.score.N))*4
        score_info = helper.get_unique_seq(onsets =self.score.note_onset_beat,
                                           offsets=self.score.note_offset_beat,
                                           unique_onset_idxs=None,
                                           return_diff=True)
        my_bpm = np.array([np.mean(self.bpms[uix]) for uix in self.score.unique_onset_idxs])
        ioi_perf = score_info['diff_u_onset'] / my_bpm
        u_onset_perf = np.cumsum(np.r_[0, ioi_perf])

        self.Onsets = np.zeros((self.score.N))
        self.Durations = np.zeros((self.score.N))
        for i, jj in enumerate(self.score.unique_onset_idxs):
            self.Onsets[jj] = u_onset_perf[i]
            d = self.score.X['duration'][i][0] * self.articulations[jj] / max(0.1,my_bpm[i])  # / self.BPM_AVE
            self.Durations[jj] = d.reshape((len(d)))

    def write_midi(self):
        pm = pretty_midi.PrettyMIDI()
        p_program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
        piano = pretty_midi.Instrument(program=p_program)
        isSustainOn = False
        # Add note one by one according to the score
        for i in range(self.score.N):
            pitch = self.score.note_info['pitch'][i]
            onset = self.score.dict_xmlidx2onset[i]
            onset_time = self.Onsets[i]
            offset_time = onset_time + self.Durations[i]
            #v = max(int(np.round(self.vels[i])), 30)
            #v = min(v, 100)
            my_note = pretty_midi.Note(velocity=int(self.vels[i]), pitch=pitch, start=onset_time, end=offset_time)
            piano.notes.append(my_note)
            if self.score.X['sustainpedal'][i] == 1 and not isSustainOn:
                piano.control_changes.append(pretty_midi.ControlChange(64,100,onset_time))
            elif self.score.X['sustainpedal'][i] == 0:
                piano.control_changes.append(pretty_midi.ControlChange(64,0,onset_time))

        pm.instruments.append(piano)
        pm.write(self.out_name)
        print('save midi to: ', self.out_name)