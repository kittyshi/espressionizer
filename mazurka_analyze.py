import numpy as np
import os
import partitura
from score_extractor_new import ScoreFeatures
from performance_extractor_new import PerformanceFeature

def get_tempo_computer(npy, score, onbeat_phrase):
    pred_tempo = np.load(npy)
    tempo_computer = []
    for b in score.beat2onset:
        onset = score.beat2onset[b]
        onsets = score.unique_onset_idxs[onset]
        tempo_computer.append(pred_tempo[onsets].mean())
    tempo_computer = np.array((tempo_computer))
    #states_computer = phrase2state(onbeat_phrase,tempo_computer)
    return tempo_computer

def mazurka_extract(a,fn):
    """
    Extract beat information from txt file
    """
    # construct dictionary given a labeled txt file
    perf_times = {}
    flag = 0
    with open(fn, 'r') as f:
        #x = f.readline()
        #line = f.readline()
        #while line:
        for cnt, line in enumerate(f):
            #print("Line {}: {}".format(cnt, line.strip()))
            #line = f.readline()
            if line != '' and line[0] != '#':
                t,b = line.strip().split('\t')
                if len(b.split(':')) == 1:
                    [bs0, bs1] = b.split('.')
                    flag = 1
                else:
                    [bs0, bs1] = b.split(':')
                perf_times[b] = float(t)
                #print('perf_times[%s] = %f' %(b, float(t)))
   # return perf_times
    # fill in perf time
    b = {}
    b['perf_time'] = np.zeros((a.N))
    valid_onsets = []
    valid_xmls = []
    for onset_i in range(a.num_onsets):
        xml_idx = a.unique_onset_idxs[onset_i]
        m = a.dict_xmlidx2measure[xml_idx[0]]
        phase = a.X['beatphase'][xml_idx[0]][0] + 1
        #print(m, phase, xml_idx)
        if int(phase) == phase:
            if flag == 1:
                key = str(m) + '.' + str(int(phase))
            else:
                key = str(m) + ':' + str(int(phase))
            if key in perf_times:
                valid_onsets.append(onset_i)
                valid_xmls += list(xml_idx)
                b['perf_time'][xml_idx] = perf_times[key]

    # construct bpm_raw
    b['bpm_raw'] = np.zeros((a.N))
    b['bpm_raw_onset'] = np.zeros((a.num_onsets))
    prev_onset = valid_onsets[0]
    prev_xmlidx = a.unique_onset_idxs[prev_onset]
    for onset_i in valid_onsets[1:]:
        xml_idx = a.unique_onset_idxs[onset_i]
        score_ioi = a.note_onset_beat[xml_idx[0]] - a.note_onset_beat[prev_xmlidx[0]]
        perf_ioi = b['perf_time'][xml_idx[0]] - b['perf_time'][prev_xmlidx[0]]
        #print(score_ioi, perf_ioi)
        b['bpm_raw'][prev_xmlidx] = score_ioi / perf_ioi
        b['bpm_raw_onset'][prev_onset] = score_ioi / perf_ioi
        #print('a.Xonset[bpm_raw][%d] = %f' % (prev_onset, score_ioi / perf_ioi))
        prev_xmlidx = xml_idx
        prev_onset = onset_i

    pt = []
    for k in perf_times.keys():
        pt.append(perf_times[k])
    return b, valid_xmls, pt


def get_mazurka_info(mazurka_dir = 'Mazurkas/labels/Mazurka_Op30_No2/', xml_fn = None):
    """
    Combine score with beat information
    """
    pts = {}
    names = {}
    tempos = {}

    # extract score information
    if xml_fn is None:
        xml_fn =  'xml-base.xml'
    part = partitura.load_musicxml(mazurka_dir + xml_fn)
    a = ScoreFeatures(part,True)

    i = 0
    for file in os.listdir(mazurka_dir):
        if file.endswith('.txt'):
            _, _, pts[i] = mazurka_extract(a, mazurka_dir + file)
            names[i] = file
            npdf = np.diff(pts[i])
            npdf[npdf <= 0] = 0.05
            tempos[i] = 1./npdf * 60
            i += 1

    # extract phrase information
    onset_phrase = []
    onbeat_phrase = []
    for i,n in enumerate(a.X['detached-legato']):
        if n == 1:
            onset_i = a.dict_xmlidx2onset[i]
            onset_phrase.append(onset_i)
            while onset_i not in a.onset2beat:
                onset_i -= 1
            onbeat_phrase.append(int(a.onset2beat[onset_i]-a.onset2beat[0]))
    onsets = []
    phrase_end = 0
    for onset_i in range(len(onset_phrase)-1):
        phrase_start = onset_phrase[onset_i]
        phrase_end = onset_phrase[onset_i+1]
        onsets.append(range(phrase_start, phrase_end))
    onsets.append(range(phrase_end, a.num_onsets))

    return a, pts, names, tempos, onbeat_phrase, onsets


def count_note_lengthening(tempos, onbeat_phrase):
    """
    count key: nth note, count val: number of times it's the lowest tempo
    count_r key: n to last note, count val: number of times it's the lowest tempo
    """

    count_r = {}
    count = {}
    for piece_no in range(len(tempos)):
        for i in range(len(onbeat_phrase)-1):
            tt = tempos[piece_no][onbeat_phrase[i]:onbeat_phrase[i+1]]
            phrase_len = onbeat_phrase[i+1] - onbeat_phrase[i]
            number = np.argmin(tt)
            number_r = number - phrase_len
            if number not in count:
               count[number] = 1
            else:
               count[number] += 1
            if number_r not in count_r:
                count_r[number_r] = 1
            else:
                count_r[number_r] += 1

        # the last phrase
        tt = tempos[piece_no][onbeat_phrase[i+1]:-1]
        phrase_len = len(tempos[piece_no]) - onbeat_phrase[i]
        number = np.argmin(tt)
        number_r = number - phrase_len

        if number not in count:
            count[number] = 1
        else:
            count[number] += 1
        if number_r not in count_r:
            count_r[number_r] = 1
        else:
            count_r[number_r] += 1
    return count, count_r