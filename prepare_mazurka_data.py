import os
import pickle
import partitura
import numpy as np
import random
import pandas as pd
import glob
from mazurkabl_data_processing import prepare_dataset
from score_extractor_new import ScoreFeatures
from score_beat_extractor import BeatFeatures


del_id = ['pid9063-03','pid9070-12','pid9111-04','pid9173-09','pid9173-11','pid9070-18','pid9075-10','pid9166d-17',
         'pid9060-12','pid9111-12','pid9156-10','pid9111-17','pid9111-20','pid917525-03','pid9070b-16',
         'pid53576-04','pid9049b-06','pid9049b-24','pid917506-05','pid918807-12']
tempo_thres = 6.66 



def select_pianists(N = 8, store = False):
    """
    For each Mazurka, select 8 cloest pianists
    """
    import scipy.stats
    import operator

    corr = {}
    corr_num = {}
    select_pianist_idxs = {}
    for key in Mazurka_info:
        corr[key] = {}
        corr_num[key] = np.zeros((len(Mazurka_info[key]),len(Mazurka_info[key])))
        # the minimum tempo curve length
        L = min([len(Mazurka_info[key][no].tempo) for no in range(len(Mazurka_info[key]))])
        for i in range(len(Mazurka_info[key])):
            for j in range(len(Mazurka_info[key])):
                if i != j:
                    the_corr = scipy.stats.pearsonr(Mazurka_info[key][i].tempo[:L],Mazurka_info[key][j].tempo[:L])
                    corr[key][str(i)+'-'+str(j)], _ = the_corr
                    corr_num[key][i,j], _ = the_corr

    # pick up the 8 closest pianists
    for key in Mazurka_info:
        closest = sorted(corr[key].items(), key = operator.itemgetter(1), reverse = True)[0][0]
        cand1 = int(closest.split('-')[0])
        cand2 = int(closest.split('-')[1])
        cand1_cand = np.argsort(corr_num[key][cand1,:])[::-1]
        cand2_cand = np.argsort(corr_num[key][cand2,:])[::-1]
        p1 = 1
        p2 = 1
        cands = [cand1, cand2]
        while len(cands) < N:
            while cand1_cand[p1] in cands or Mazurka_info[key][cand1_cand[p1]].id in del_id:
                p1 += 1
            while cand2_cand[p2] in cands or Mazurka_info[key][cand2_cand[p2]].id in del_id:
                p2 += 1
            if corr_num[key][cand1,cand1_cand[p1]] > corr_num[key][cand2,cand2_cand[p2]]:
                cands.append(cand1_cand[p1])
                p1 += 1
            else:
                cands.append(cand2_cand[p2])
                p2 += 1          
        select_pianist_idxs[key] = cands
    
    if store:
        pickle.dump(select_pianist_idxs, open('Mazurkas/MazurkaBL/selec_pianist_idxs.p', 'wb'))
    return select_pianist_idxs


def extract_markings(marking_dir = 'Mazurkas/MazurkaBL/markings/'):
    """
    To extract dynamic markings in csv files
    then, I can count the most frequent few, and use those in the training
    """
    Mazurka_markings = {}
    all_markings = {}
    for csvfile in os.listdir(marking_dir):
        if csvfile.endswith('.csv'):
            df = pd.read_csv(marking_dir + csvfile)
            df_dict = df.to_dict('list')
            key = csvfile[:5]
            #print(csvfile, key)
            Mazurka_markings[key] = {}
            for k,v in df_dict.items():
                ks = []
                # sometimes it's PP + tenuto
                if '+' in k:
                    ks += k.split('+')
                else:
                    ks = [k]
                for i in range(len(ks)):
                    # sometimes it's piano.2, piano.2, (piano)
                    if '.' in ks[i]:
                        ks[i] = ks[i].split('.')[0]
                    ks[i] = ks[i].replace('(', '')
                    ks[i] = ks[i].replace(')', '')

                for k in ks:
                    # some misspelling
                    if k == 'Cresendo':
                        k == 'Crescendo'
                    if k in Mazurka_markings[key]:
                        Mazurka_markings[key][k].append(v)
                        #print('adding', v, 'to all_markings[key][',k,']')
                    else:
                        Mazurka_markings[key][k] = [v]
                        #print('adding', ks, 'to all_markings[key][',k,']')
                    if k in all_markings:
                        all_markings[k] += 1
                    else:
                        all_markings[k] = 1

    # count valid (most frequent) markings
    valid_mark = []
    for k,v in all_markings.items():
        if v > 20:
            #print(k,v)
            valid_mark.append(k)
    
    # Copy most frequent markings into this dictionary, flatten out the list, and index right
    Mazurka_markings_valid = {}
    for Mkey in Mazurka_markings.keys():
        Mazurka_markings_valid[Mkey] = {}
        for k,v in Mazurka_markings[Mkey].items():
            if k in valid_mark:
                Mazurka_markings_valid[Mkey][k] = []         
                for vv in v:
                    if type(vv) is list:
                        Mazurka_markings_valid[Mkey][k].append(int(vv[0]) - 1)   # in csv it index from 1
                    else:
                        Mazurka_markings_valid[Mkey][k].append(int(vv))
        
    
    return Mazurka_markings_valid, Mazurka_markings, all_markings, valid_mark

def create_mazurka_beat_pickle(pickle_dir = 'MaZurkas/MazurkaBL/perf_datas_0421_select/'):
    """
    Step 1 of traing example creation, this is similar as create_mazurka_pickle, but it's for each beat
    """
    files_beat = glob.glob('Mazurkas/MazurkaBL/beat_time/*.csv')
    files_dyn = glob.glob('Mazurkas/MazurkaBL/beat_dyn/*.csv')
    files_mark = glob.glob('Mazurkas/MazurkaBL/markings/*.csv')
    files_mark_dyn = glob.glob('Mazurkas/MazurkaBL/markings_dyn/*.csv')

    Mazurka_info = prepare_dataset(files_beat, files_dyn, files_mark, files_mark_dyn)
    mazurka_dir = 'Mazurkas/MazurkaBL/xml_scores/'
    for xml_fn in os.listdir(mazurka_dir):
        if xml_fn.endswith('.musicxml'):
            Mname = 'M'+xml_fn[7:11]
            out_dir = pickle_dir + Mname + '/' 
            if not os.path.exists(out_dir):
                os.mkdir(out_dir)
            part = partitura.load_musicxml(mazurka_dir + xml_fn)
            a = ScoreFeatures(part,True)
            beat_feature = BeatFeatures(a, Mname)
            for perf_no in range(len(Mazurka_info[Mname])):
                if Mazurka_info[Mname][perf_no].id not in del_id:
                    Mazurka_info[Mname][perf_no].tempo[Mazurka_info[Mname][perf_no].tempo > tempo_thres] = tempo_thres
                    Y = {}
                    Y['vel'] = np.zeros((beat_feature.NB))
                    Y['tempo'] = np.zeros((beat_feature.NB))
                    Y['tempo_relative'] = np.zeros((beat_feature.NB))
                    for b in a.beat2onset.keys():
                        Y['vel'][b] = Mazurka_info[Mname][perf_no].dyn[b]
                        Y['tempo'][b] = Mazurka_info[Mname][perf_no].tempo[b]
                        Y['tempo_relative'][b] = (Mazurka_info[Mname][perf_no].tempo[b] - np.mean(Mazurka_info[Mname][perf_no].tempo)) / np.mean(Mazurka_info[Mname][perf_no].tempo)
                    for b in range(beat_feature.NB):
                        if Y['vel'][b] == 0:
                            Y['vel'][b] = Y['vel'][b-1]
                            Y['tempo'][b] = Y['tempo'][b-1]
                            Y['tempo_relative'][b] = Y['tempo_relative'][b-1]
                    # store data
                    Y['tempo_relative'][Y['tempo_relative'] > 1] = 1
                    Y['tempo_relative'][Y['tempo_relative'] < -1] = -1
                    data = {}
                    data['X'] = beat_feature.beat_features
                    data['Yvel'] = Y['vel']
                    data['Yibi'] = Y['tempo'] 
                    data['Yibi_relative'] = Y['tempo_relative']
                    out_p = out_dir + Mazurka_info[Mname][perf_no].id + '.p'
                    pickle.dump(data,open(out_p,'wb'))
                    print(out_p)

def create_mazurka_pickle(pickle_dir = 'Mazurkas/MazurkaBL/perf_datas_0418/'):
    """
    Step 1 of training example creation
    Create directories for each piece, 
    For each piece, loop through each performance, extract performance data (vel and tempo)
    Store performance data (with score feature62) into pickle files
    """
    files_beat = glob.glob('Mazurkas/MazurkaBL/beat_time/*.csv')
    files_dyn = glob.glob('Mazurkas/MazurkaBL/beat_dyn/*.csv')
    files_mark = glob.glob('Mazurkas/MazurkaBL/markings/*.csv')
    files_mark_dyn = glob.glob('Mazurkas/MazurkaBL/markings_dyn/*.csv')

    Mazurka_info = prepare_dataset(files_beat, files_dyn, files_mark, files_mark_dyn)
    #del Mazurka_info['M06-4']
    #del Mazurka_info['M63-2']
    #del Mazurka_info['M30-1']
    #del Mazurka_info['M56-2']

    mazurka_dir = 'Mazurkas/MazurkaBL/xml_scores/'
    for xml_fn in os.listdir(mazurka_dir):
        if xml_fn.endswith('.musicxml'):
            Mname = 'M'+xml_fn[7:11]
            out_dir = pickle_dir + Mname + '/' 
            if not os.path.exists(out_dir):
                os.mkdir(out_dir)
            part = partitura.load_musicxml(mazurka_dir + xml_fn)
            a = ScoreFeatures(part,True)
            
            # for each performance, extract data
            for perf_no in range(len(Mazurka_info[Mname])):
                # make sure the current alignment is not in the outliers
                if Mazurka_info[Mname][perf_no].id not in del_id:
                    # make sure these two offbeat start (but still count as a beat in annotation) are get rid of
                    # This step is now done in mazurkabl_extract.py
                    #if Mname == 'M41-1' or Mname == 'M50-1':
                    #    Mazurka_info[Mname][perf_no].tempo = Mazurka_info[Mname][perf_no].tempo[1:]
                    # for tempo above 400 bpm (too fast for Mazurka), cast it into 400 (6.66 beat-per-second)
                    Mazurka_info[Mname][perf_no].tempo[Mazurka_info[Mname][perf_no].tempo > tempo_thres] = tempo_thres
                    Y = {}
                    Y['vel'] = np.zeros((a.N))
                    Y['tempo'] = np.zeros((a.N))
                    Y['tempo_relative'] = np.zeros((a.N))
                    for b in a.beat2onset.keys():
                        onset = a.beat2onset[b]
                        idxs = a.unique_onset_idxs[onset]
                        Y['vel'][idxs] = Mazurka_info[Mname][perf_no].dyn[b]
                        Y['tempo'][idxs] = Mazurka_info[Mname][perf_no].tempo[b]
                        Y['tempo_relative'][idxs] = (Mazurka_info[Mname][perf_no].tempo[b] - np.mean(Mazurka_info[Mname][perf_no].tempo)) / np.mean(Mazurka_info[Mname][perf_no].tempo)

                    # fill in onsets that not on the beat
                    for onset_i, index in enumerate(a.unique_onset_idxs):    
                        if Y['vel'][index[0]] == 0:
                            prev_idxs = a.unique_onset_idxs[onset_i-1]
                            Y['vel'][index] = Y['vel'][prev_idxs[0]]
                            Y['tempo'][index] = Y['tempo'][prev_idxs[0]]
                            Y['tempo_relative'][index] = Y['tempo_relative'][prev_idxs[0]]
                    # store data
                    data = {}
                    data['X'] = a.score_features57
                    data['Yvel'] = Y['vel']
                    data['Yibi'] = Y['tempo'] 
                    data['Yibi_relative'] = Y['tempo_relative']
                    out_p = out_dir + Mazurka_info[Mname][perf_no].id + '.p'
                    pickle.dump(data,open(out_p,'wb'))
                    print(out_p)

def create_samples(out_p = 'Mazurkas/MazurkaBL/perf_data/data0416.p', perf_datas = 'Mazurkas/MazurkaBL/perf_datas_0416/', mazurka_dir = 'Mazurkas/MazurkaBL/xml_scores/'):
    """
    Step 2 of creating training data
    """
   
    all_files = ['M'+n[7:11] for n in os.listdir(mazurka_dir) if n.endswith('.musicxml')]
    train_file = []
    valid_file = []
    for fn in random.sample(all_files, int(len(all_files) * 0.8)):
        train_file.append(fn)
    for fn in all_files:
        if fn not in train_file:
            valid_file.append(fn)

    def getdata_mazurka(dirs,data_dir = perf_datas):
        all_ps = []
        for i in range(len(dirs)):
            all_ps += [os.path.join(data_dir + dirs[i],p) for p in os.listdir(data_dir+dirs[i]) if p.endswith('.p')]

        pk = pickle.load(open(all_ps[0],'rb'))
        X = pk['X']
        Y_vel = pk['Yvel']
        Y_bpm = pk['Yibi']
        # 0422 change for bpm_diff_ratio
        #Y_bpm[Y_bpm > 1.0] = 1.0
        #Y_bpm[Y_bpm < -0.9] = -0.9
        #print(min(Y_bpm), max(Y_bpm))
        # 0421
        Y_bpm[Y_bpm > 6.66] = 6.66
        Y_bpm_relative = pk['Yibi_relative']
        for i in range(1, len(all_ps)):
            pkl = pickle.load(open(all_ps[i],'rb'))
            print('reading: ', all_ps[i])
            X = np.vstack((X, pkl['X']))
            Y_vel = np.hstack((Y_vel, pkl['Yvel']))
            #pkl['Yibi'][pkl['Yibi'] > 1.0] = 1.0
            #pkl['Yibi'][pkl['Yibi'] < -0.9] = -0.9
            Y_bpm = np.hstack((Y_bpm, pkl['Yibi']))
            Y_bpm_relative = np.hstack((Y_bpm_relative, pkl['Yibi_relative']))

        data = {}
        data['X'] = X
        data['Y'] = {}
        data['Y']['vel'] = Y_vel
        data['Y']['bpm'] = Y_bpm
        data['Y']['bpm_relative'] = Y_bpm_relative
        return data
    
    d = {}
    d['train'] = getdata_mazurka(train_file)
    d['val'] = getdata_mazurka(valid_file)
    pickle.dump(d, open(out_p,'wb'))

    
def create_bpm(a, ybpm):
    features_raw = a.score_features62
    features_m = np.zeros((a.num_measures, features_raw.shape[1]))
    
    ybpm[ybpm > 6.66] = 6.66
    ybpm[ybpm <= 0] = np.average(ybpm[ybpm > 0])
    
    ks = [k for k in a.dict_measure2xmlidx.keys()]
    ymbpm = np.zeros((a.num_measures))
    m0 = ks[0]
    for measure in a.dict_measure2xmlidx.keys():
        onsets = [a.dict_xmlidx2onset[xmlidx] for xmlidx in a.dict_measure2xmlidx[measure]]
        beats = np.unique([int(a.onset2beat[o]) for o in onsets if o in a.onset2beat])
        ymbpm[measure] = np.average(ybpm[beats])
    
    ymbpm[ymbpm > 6.66] = 6.66
    ymbpm[ymbpm <= 0] = np.average(ymbpm[ymbpm > 0])
    
    prev_bpms = np.zeros((a.N,1))
    upper_bpms = np.zeros((a.N,1))
    is_notes = np.zeros((a.N,1))
    bpms = np.zeros((a.N,1))
    
    upper_mean = np.average(ymbpm)
    bpms_m = np.zeros((a.num_measures, 1))
    prev_bpms_m = np.zeros((a.num_measures, 1))
    upper_bpms_m = np.ones((a.num_measures, 1)) * upper_mean
    is_notes_m = np.zeros((a.num_measures, 1))

    for i in range(a.N):
        # upper bpm
        m = a.dict_xmlidx2measure[i]
        upper_bpms[i] = ymbpm[m]
        # previous bpm in relative to upper measure bpm
        curr_onset = a.dict_xmlidx2onset[i]
        while curr_onset not in a.onset2beat and curr_onset >= 0:
            curr_onset -= 1    
        if curr_onset < 0:
            b = 0
        else:
            b = int(a.onset2beat[curr_onset])
                
        if b == 0:
            prev_bpms[i] = -1
        else:
            prev_bpms[i] = ybpm[b-1]  
        
        # whether note or not
        is_notes[i] = 1
        # Label: velocity and bpm
        # vels[i,int(b.Y['vel'][i])] = 1
        #bpms.append(ybpm[b])
        bpms[i] = ybpm[b]
    
    for m in a.dict_measure2xmlidx.keys():
        # Feature 0-127: current feature (average pitch)
        xml_idxs = a.dict_measure2xmlidx[m]
        feature_mean = np.mean(a.score_features62[xml_idxs],axis=0)
        features_m[m] = feature_mean
        # Feature 128-256: bpm of previous measure
        if m == 0 or m == ks[0]:
            prev_bpms_m[m] = -1
        else:
            prev_bpms_m[m] = ymbpm[m-1] 
        # Feature 257--(257+128): upper bpm
        # upper_vels_m[m] = upper_mean
        # Label: average bpm
        bpms_m[m] = ymbpm[m]   # this should fix the problem
    
    # concate
    X_feature = np.hstack((features_raw, prev_bpms))
    X_feature = np.hstack((X_feature, upper_bpms))
    X_feature = np.hstack((X_feature, is_notes))

    Xm_feature = np.hstack((features_m, prev_bpms_m))
    Xm_feature = np.hstack((Xm_feature, upper_bpms_m))
    Xm_feature = np.hstack((Xm_feature, is_notes_m))
    
    # because in a.dict_measure2xmlidx, the mazurka set always starts from 1, so discard the 0th entry
    Xm_feature = Xm_feature[1:,:]
    bpms_m = bpms_m[1:]
    
    data = {}
    data['X'] = X_feature
    data['Ybpm'] = bpms
    data['Xm'] = Xm_feature
    data['Ymbpm'] = bpms_m
    return data